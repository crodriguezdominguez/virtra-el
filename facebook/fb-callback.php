<?php
	session_start();
	
	foreach ($_COOKIE as $k=>$v) {
	    if(strpos($k, 'FBRLH_')!==FALSE) {
	        $_SESSION[$k]=$v;
	    }
	}
	
	require_once __DIR__ . '/sdk/Facebook/autoload.php';
	
	$fb = new Facebook\Facebook([
	  'app_id' => '728440753923116',
	  'app_secret' => '068b6606701066c3446f46bf4af72b70',
	  'default_graph_version' => 'v2.5',
	  ]);
	
	$helper = $fb->getRedirectLoginHelper();
	
	try {
	  $accessToken = $helper->getAccessToken();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  // When Graph returns an error
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  // When validation fails or other local issues
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	if (! isset($accessToken)) {
	  if ($helper->getError()) {
	    header('HTTP/1.0 401 Unauthorized');
	    echo "Error: " . $helper->getError() . "\n";
	    echo "Error Code: " . $helper->getErrorCode() . "\n";
	    echo "Error Reason: " . $helper->getErrorReason() . "\n";
	    echo "Error Description: " . $helper->getErrorDescription() . "\n";
	  } else {
	    header('HTTP/1.0 400 Bad Request');
	    echo 'Bad request';
	  }
	  exit;
	}
	
	// Logged in
	
	// The OAuth 2.0 client handler helps us manage access tokens
	$oAuth2Client = $fb->getOAuth2Client();
	
	// Get the access token metadata from /debug_token
	$tokenMetadata = $oAuth2Client->debugToken($accessToken);
	
	if (! $accessToken->isLongLived()) {
	  // Exchanges a short-lived access token for a long-lived one
	  try {
	    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
	  } catch (Facebook\Exceptions\FacebookSDKException $e) {
	    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
	    exit;
	  }
	}
	
	$_SESSION['fb_access_token'] = (string) $accessToken;
	
	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me?fields=website,feed.limit(5000000),languages,sports.limit(5000000),devices.limit(5000000),family.limit(5000000),likes.limit(5000000),events.limit(5000000),friends.limit(0)', (string) $accessToken);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	$user = $response->getGraphUser();
	
	/*
	x Los dispositivos móviles que utiliza
	x Deportes que practica
	x Idiomas que utiliza
	x Página web
	x Suma por separado de: Fotografías, videos, foto perfil, albumes, etc.
	x Eventos a los que ha sido invitado (estos eventos que te llegan y que tienes que poner si asistirás o no)
	x Suma de los miembros de la familia registrados en Facebook.
	x Suma de los amigos de Facebook.
	x Juegos/Apps de Facebook que utiliza y las puntuaciones en cada uno.
	x Suma de los “Me gusta” que ha puesto el usuario
	x Suma de los grupos a los que pertenece
	x Suma de los posts que ha realizado la persona.
	~ Los comentarios para cada post y las personas etiquetadas. Voy a tratar de sumar por separado los comentarios del propio usuario y del resto de usuario. No tengo claro que Facebook permita diferenciar entre unos comentarios y otros (creo que no devuelve el autor de cada comentario).
	*/
	
	//do anything with the user
	//var_dump($user);
	$result = [];
	$result['devices'] = [];
	foreach ($user['devices'] as $device){
		$result['devices'][] = $device['hardware'].'-'.$device['os'];
	}
	
	$result['sports'] = [];
	foreach ($user['sports'] as $sport){
		$result['sports'][] = $sport['name'];
	}
	
	$result['languages'] = [];
	foreach ($user['languages'] as $lang){
		$result['languages'][] = $lang['languages'];
	}
	
	$result['website'] = 'Unknown';
	if (isset($user['website'])){
		$result['website'] = $user['website'];
	}
	
	$count_posts = 0;
	foreach ($user['feed'] as $entry){
		$count_posts++;
	}
	
	$count_family = 0;
	foreach ($user['family'] as $f){
		$count_family++;
	}
	
	$count_likes = 0;
	foreach ($user['likes'] as $like){
		$count_likes++;
	}
	
	$count_events = 0;
	foreach ($user['events'] as $e){
		$count_events++;
	}
	
	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me/photos?limit=5000000', (string) $accessToken);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	$photos = $response->getGraphEdge();
	$count_photos = 0;
	foreach ($photos as $p){
		$count_photos++;
	}
	
	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me/videos?limit=5000000', (string) $accessToken);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	$videos = $response->getGraphEdge();
	$count_videos = 0;
	foreach ($videos as $v){
		$count_videos++;
	}
	
	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me/albums?limit=5000000', (string) $accessToken);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	$albums = $response->getGraphEdge();
	$count_albums = 0;
	foreach ($albums as $a){
		$count_albums++;
	}
	
	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me/groups?limit=5000000', (string) $accessToken);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	$groups = $response->getGraphEdge();
	$count_groups = 0;
	foreach ($groups as $g){
		$count_groups++;
	}
	
	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me/scores?limit=5000000', (string) $accessToken);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo _('Error de Facebook: ') . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo _('Error de la biblioteca de Facebook: ') . $e->getMessage();
	  exit;
	}
	
	$scores = $response->getGraphEdge();
	$total_scores = [];
	foreach ($scores as $s){
		$total_scores[] = ['score'=>$s['score'], 'app'=>$s['application']['name']];
	}
	
	$result['numPostsAndComments'] = $count_posts;
	$result['numFamily'] = $count_family;
	$result['numFriends'] = $user['friends']->getTotalCount();
	$result['numLikes'] = $count_likes;
	$result['numPhotos'] = $count_photos;
	$result['numVideos'] = $count_videos;
	$result['numAlbums'] = $count_albums;
	$result['numGroups'] = $count_groups;
	$result['numAcceptedEvents'] = $count_events;
	$result['gameScores'] = $total_scores;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
	<head>
		<title>VIRTRA-EL - <?php echo _('Plataforma Virtual de Evaluaci&oacute;n e Intervenci&oacute;n Cognitiva en Mayores'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- css styles -->
		<link rel="stylesheet" type="text/css" href="../css/sweet-alert.css">
	    <link rel="stylesheet" href="../css/bootstrap-combined.no-icons.min.css">
	    <link rel="stylesheet" href="../css/virtrael.css">
	    <link rel="stylesheet" href="../css/datepicker.css">
	    <link rel="stylesheet" href="../css/font-awesome.min.css">
		<!--[if IE 7]>
		  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
	    <!--<link href="css/jquery.toChecklist.min.css" rel="stylesheet">-->

	    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	
	    <!-- favicon and touch icons (for iPhone, iPad and iPod Touch)
	    <link rel="shortcut icon" href="img/ico/favicon.ico">
	    <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
	    <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72x72.png">
	    <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114x114.png">
	    <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144x144.png">
	    -->
	</head>
	<body>
		<h1><?php echo _('Se van a guardar los siguientes datos de su cuenta de Facebook'); ?>:</h1>
		<p>
		<ul>
			<li><strong><?php echo _('Los dispositivos móviles que utilizas'); ?>: </strong><?php echo implode(', ', $result['devices']) ?></li>
			<li><strong><?php echo _('Los deportes que practicas'); ?>: </strong><?php echo implode(', ', $result['sports']) ?></li>
			<li><strong><?php echo _('Los idiomas que hablas'); ?>: </strong><?php echo implode(', ', $result['languages']) ?></li>
			<li><strong><?php echo _('Tu página Web'); ?>: </strong><?php echo $result['website']; ?></li>
			<li><strong><?php echo _('Número de fotografías publicadas'); ?>: </strong><?php echo $result['numPhotos']; ?></li>
			<li><strong><?php echo _('Número de videos publicados'); ?>: </strong><?php echo $result['numVideos']; ?></li>
			<li><strong><?php echo _('Número de álbumes de fotografías/videos'); ?>: </strong><?php echo $result['numAlbums']; ?></li>
			<li><strong><?php echo _('Número de eventos a los que quieres asistir o has asistido'); ?>: </strong><?php echo $result['numAcceptedEvents']; ?></li>
			<li><strong><?php echo _('Número de familiares registrados en Facebook'); ?>: </strong><?php echo $result['numFamily']; ?></li>
			<li><strong><?php echo _('Número de amigos en Facebook'); ?>: </strong><?php echo $result['numFriends']; ?></li>
			<li><strong><?php echo _('Puntuaciones en tus juegos de Facebook'); ?>: </strong><?php 
				$count = 0;
				foreach ($result['gameScores'] as $score){
					if ($count > 0){
						echo ', ';
					}
					echo $score['app'].' ('.$score['score'].')';
					$count++;
				}
			?></li>
			<li><strong><?php echo _('Número de "Me gusta" pulsados'); ?>: </strong><?php echo $result['numLikes']; ?></li>
			<li><strong><?php echo _('Número de grupos a los que perteneces'); ?>: </strong><?php echo $result['numGroups']; ?></li>
			<li><strong><?php echo _('Número de publicaciones y comentarios que has realizado'); ?>: </strong><?php echo $result['numPostsAndComments']; ?></li>
			
		</ul>
		</p>
	</body>
</html>
