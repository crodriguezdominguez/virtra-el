<?php
	session_start();
	
	require_once __DIR__ . '/sdk/Facebook/autoload.php';
	
	$fb = new Facebook\Facebook([
		'app_id' => '728440753923116',
		'app_secret' => '068b6606701066c3446f46bf4af72b70',
		'default_graph_version' => 'v2.5',
	]);

	$helper = $fb->getRedirectLoginHelper();
	
	$permissions = ['user_friends', 'user_posts', 'user_relationships', 'user_website', 'user_likes', 'user_photos', 'user_videos', 'user_games_activity', 'user_events']; // Optional permissions
	
	$host = str_replace('http://', '', $_SERVER['HTTP_HOST']);
	$host = str_replace('https://', '', $host);
	
	$loginUrl = $helper->getLoginUrl('http://'.$host.'/facebook/fb-callback.php', $permissions);
	
	foreach ($_SESSION as $k=>$v) {                    
	    if(strpos($k, 'FBRLH_')!==FALSE) {
	        if(!setcookie($k, $v)) {
	            //what??
	        } else {
	            $_COOKIE[$k]=$v;
	        }
	    }
	}
?>

<p class="lead">
	<?php echo _('En este apartado recopilaremos información sobre tu cuenta de Facebook para estudiar con mayor profundidad tu actividad en la red. En ningún caso guardaremos lo que publiques, información sobre tus contactos, tus imágenes o cualquier otra información privada sobre tus familiares o estilo de vida.'); ?>
</p>
<center><a class="btn btn-large btn-primary" target="_blank" href="<?php echo htmlspecialchars($loginUrl) ?>"><?php echo _('Obtener mi información de Facebook') ?></a></center>
<br /><br />
<center><a target="_blank" href="https://www.facebook.com"><?php echo _('Ir a la web de Facebook') ?></a></center>
