<?php
	require_once('locale/localization.php');
	
	session_start();
?>

<div>
	<center><a class="btn btn-large btn-primary" href="backend/logout.php"><i class="icon-off"></i> <?php echo _('Salir'); ?></a></center>
</div>

<div id="endSessionMedals">
	<center><h1><?php echo _('Medallero de la Sesi&oacute;n'); ?></h1></center>
	<center><span id="medals"></span></center>
</div>

<script type="text/javascript">

var sessionID = parseInt(lastSession['sessionID'])-1;
$('#exercise-title').html("<?php echo _('&iexcl;Muy bien!'); ?>");

if (sessionID == 1)
{
	$('#exercise-description').html("<?php echo _('Has terminado la sesi&oacute;n'); ?> "+sessionID+". <?php echo _('En la siguiente sesión realizarás una serie de ejercicios que medirán tu nivel de memoria, atención, razonamiento y planificación. &iexcl;Espero verte pronto!'); ?>");
}
else if (sessionID == 11)
{
	$('#exercise-description').html("<?php echo _('Has terminado la sesi&oacute;n'); ?> "+sessionID+". <?php echo _('En la siguiente sesión realizarás una serie de ejercicios para evaluar tu nivel actual de memoria, atención y razonamiento. &iexcl;Espero verte pronto!'); ?>");
}
else if (sessionID == 12)
{
	$('#exercise-description').html("<?php echo _('Has terminado la sesi&oacute;n'); ?> "+sessionID+". <?php echo _('En la siguiente sesión realizarás una serie de ejercicios que medirán tu nivel de memoria, atención y planificación. &iexcl;Espero verte pronto!'); ?>");
}
else
{
	$('#exercise-description').html("<?php echo _('Has terminado la sesi&oacute;n'); ?> "+sessionID+". <?php echo _('En las siguientes sesiones realizarás una serie de ejercicios para estimular tu memoria, atención, razonamiento y planificación. &iexcl;Espero verte pronto!'); ?>");
}

$("#medals").load('backend/ui/medals.php?sessionID='+sessionID);

</script>
