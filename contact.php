<?php
	require_once('locale/localization.php');
?>

<div class="page-header">
  <h1><?php echo _('Contacto'); ?></h1>
  <p class="lead"><?php echo _('Si tiene alguna duda o desea m&aacute;s informaci&oacute;n sobre VIRTRA-EL, contacte con nosotros a trav&eacute;s del siguiente formulario.'); ?></p>
</div>
<form action="javascript:checkForm();" class="form-horizontal" id="contact-form">
	<fieldset>
	<div class="control-group" id="name-group">
		<label class="control-label" for="name"><?php echo _('Nombre'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="name" name="name">
		</div>
    </div>
    <div class="control-group" id="email-group">
		<label class="control-label" for="email"><?php echo _('E-Mail'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="email" name="email">
		</div>
    </div>
    <div class="control-group" id="subject-group">
		<label class="control-label" for="subject"><?php echo _('Asunto'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="subject" name="subject">
		</div>
    </div>
    <div class="control-group" id="message-group">
    	<label class="control-label" for="message"><?php echo _('Texto'); ?></label>
		<div class="controls">
			<textarea class="input-xlarge" id="message" name="message" rows="5"></textarea>
		</div>
    </div>
    <div class="control-group" id="captcha-group">
		<label class="control-label" id="captcha-label" for="captcha"></label>
		<div class="controls">
			<input type="text" class="input-mini" maxlength="2" id="captcha" name="captcha">
			<span class="help-inline"><?php echo _('Esto evita que se haga un mal uso del formulario de contacto.'); ?></span>
		</div>
    </div>
    <div class="control-group">
	    <div class="controls">
			<button type="submit" class="btn btn-large btn-primary"><?php echo _('Enviar'); ?></button>
			<button type="reset" class="btn btn-large" id="reset-button"><?php echo _('Limpiar'); ?></button>
		</div>
	</div>
	</fieldset>
</form>
<p id="result-form"></p>

<script type="text/javascript">
var c;
reloadCaptcha();

function reloadCaptcha()
{
	var a = Math.ceil(getRandom() * 10);
	var b = Math.ceil(getRandom() * 10);
	c = a + b;
	
	$('#captcha-label').html("<?php echo _('Resultado de'); ?>"+' '+a+' + '+b);
};

function checkForm()
{
	var subject = $('#subject').val();
	var name = $('#name').val();
	var email = $('#email').val();
	var message = $('#message').val();
	var captcha = $('#captcha').val();

	$(".error").removeClass('error');
	if (name.length <= 0)
	{
		$('#name-group').addClass('error');
	}
	else if (email.length <= 0)
	{
		$('#email-group').addClass('error');
	}
	else if (subject.length <= 0)
	{
		$('#subject-group').addClass('error');
	}
	else if (message.length <= 0)
	{
		$('#message-group').addClass('error');
	}
	else if (captcha.length <= 0 || parseInt(captcha) != c)
	{
		$('#captcha-group').addClass('error');
	}
	else
	{
		var qry = 'subject='+subject+'&'+
				  'name='+name+'&'+
				  'email='+email+'&'+
				  'message='+message;
				  
		console.debug(qry);
			
		$.ajax({
			type: 'POST',
			data: qry,
			url: 'backend/contact.php',
			success: function(res) {
				$('#result-form').removeClass('contact-form-ok').removeClass('contact-form-error');
				
				if (res=='false')
				{
					$('#result-form').addClass('contact-form-error').html("<?php echo _('Ocurri&oacute; un error al enviar el mensaje. Por favor, int&eacute;ntelo m&aacute;s tarde.'); ?>");
				}
				else
				{
					reloadCaptcha();
					$('#reset-button').click();
					
					$('#result-form').addClass('contact-form-ok').html("<?php echo _('El mensaje se envi&oacute; correctamente. Contactaremos con usted lo antes posible.'); ?>");
				}
			}
		});
	}
};

</script>
