<?php
	require_once('locale/localization.php');
	session_start();
?>

<div class="page-header" style="display:table;width:100%;">
  <div class="avatar-cls">
	<img id="img-avatar-id" src='<?php global $user; if(isset($_SESSION['userID'])){ require_once(__DIR__.'/php/User.php'); $user = new User($_SESSION['userID']); echo $user->get_avatar(); } ?>' style="height:150px;width:auto;" />
  </div>
  <div class="bubble">
    <div style="position:absolute;top:33%;margin-left:-40px; width: 0;height: 0; border-top: 10px solid rgba(242,242,242,0);border-bottom: 10px solid rgba(242,242,242,0); border-right:30px solid #f2f2f2;" class="pull-left"></div>
  	<p class="pull-right" style="margin-right:10px;"><a id="hideInstructionsButton" href="javascript:void(0);" onclick="javascript:collapseMenu(true);"><i class="icon-eye-close"></i>&nbsp;<?php echo _('Ocultar Instrucciones'); ?></a><span id="miniMenuDivider">&nbsp;&nbsp;|</span>&nbsp;&nbsp;<a href="javascript:void(0);" id="fullscreen"><i class="icon-resize-full"></i>&nbsp;<?php echo _('Ocultar Men&uacute;'); ?></a></p>
  	<h1 id="exercise-title"></h1>
  	<p id="exercise-description" class="lead"></p>
  </div>
</div>

<div class="mini-menu" style="padding-bottom:0px; margin:5px 0px 5px 0px; border-bottom: 1px solid #eeeeee; width:100%; display: table;">
  <p class="pull-left" id="exercise-mini-description"></p>
  <p class="pull-right" style="margin-right:10px;font-size: 16px;"><a href="javascript:void(0);" onclick="javascript:expandMenu(true);"><i class="icon-question-sign"></i>&nbsp;<?php echo _('Mostrar Instrucciones'); ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0);" id="fullscreen2"><i class="icon-resize-full"></i>&nbsp;<?php echo _('Ocultar Men&uacute;'); ?></a></p>
</div>

<div id="exercise-container">
</div>

<?php if(isset($_SESSION['userID']) == false) { ?>

<script type="text/javascript">
    <?php if (isset($_GET['app']) && $_GET['app'] == '1') { ?>
    $('#bodyContainer').load('unregistered.php?init=1&app=1');
    <?php } else { ?>
	$('#bodyContainer').load('unregistered.php');
	<?php } ?>
</script>

<?php } else { ?>

<script type="text/javascript" src="js/jquery.xml2json.js"></script>
<script type="text/javascript" src="js/class.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jstorage.min.js"></script>
<script type="text/javascript" src="exercises/exercise.js"></script>
<script type="text/javascript" src="exercises/exercise-factory.js"></script>
<script type="text/javascript" src="exercises/timer.js"></script>

<!-- EXERCISES (PHP scripts that generate a localized JS script) -->
<?php include_once('exercises/IntroductionExercise.php'); ?>
<?php include_once('exercises/QuestionnaireExercise.php'); ?>
<?php include_once('exercises/DummyExercise.php'); ?>
<?php include_once('exercises/FirstHelpExercise.php'); ?>
<?php include_once('exercises/MouseDialogExercise.php'); ?>
<?php include_once('exercises/SemanticSeriesExercise.php'); ?>
<?php include_once('exercises/LogicalSeriesExercise.php'); ?>
<?php include_once('exercises/SecondHelpExercise.php'); ?>
<?php include_once('exercises/InstrumentalQuestionnaireExercise.php'); ?>
<?php include_once('exercises/WordListExercise.php'); ?>
<?php include_once('exercises/DirectNumbersExercise.php'); ?>
<?php include_once('exercises/NumbersAndVowelsExercise.php'); ?>
<?php include_once('exercises/PyramidsExercise.php'); ?>
<?php include_once('exercises/PackageDeliveryExercise.php'); ?>
<?php include_once('exercises/TaskListExercise.php'); ?>
<?php include_once('exercises/BalloonsExercise.php'); ?>
<?php include_once('exercises/GiftShoppingExercise.php'); ?>
<?php include_once('exercises/ClassifyExercise.php'); ?>
<?php include_once('exercises/SemanticAnalogiesExercise.php'); ?>
<?php include_once('exercises/SpatialReasoningExercise.php'); ?>
<?php include_once('exercises/ClassifyObjectsExercise.php'); ?>
<?php include_once('exercises/LostItemsExercise.php'); ?>
<?php include_once('exercises/BagItemsExercise.php'); ?>
<?php include_once('exercises/VRExercise.php'); ?>
<?php include_once('exercises/PositionsExercise.php'); ?>

<!-- EXERCISE MANAGER -->
<script type="text/javascript">
var sessions = null;
var lastSession = null;
var exercise = null;
var exerciseEntry = null;
var medalType = null;
var lastExercise = false;
var forceNext = false;
var allowShowMiniMenu = true;
var showInstructionsFullScreen = null;

$(".mini-menu").hide();

$.ajax({
	type: "GET",
	url: "sessions.php",
	dataType: "xml",
	success: function(xml) {
		sessions = $.xml2json(xml);
		getLastSession(function(result){
			lastSession = result;
            // console.log(lastSession);
            nextExercise();
		});
	}
});

function toggleFullScreen(){
	/*if (RunPrefixMethod(document, "FullScreen") || RunPrefixMethod(document, "IsFullScreen")) {
		$('#fullscreen').html('<i class="icon-resize-full"></i>&nbsp;<?php echo _('Ver a Pantalla Completa'); ?>');
		RunPrefixMethod(document, "CancelFullScreen");
	}
	else {
		RunPrefixMethod(document.getElementById('bodyContainer'), "RequestFullScreen");
		$('#fullscreen').html('<i class="icon-resize-small"></i>&nbsp;<?php echo _('Salir de Pantalla Completa'); ?>');
	}*/
	
	if ($(".navbar").hasClass('hide'))
	{
		$('#fullscreen').html('<i class="icon-resize-full"></i>&nbsp;<?php echo _('Ocultar Men&uacute;'); ?>');
		$('#fullscreen2').html('<i class="icon-resize-full"></i>&nbsp;<?php echo _('Ocultar Men&uacute;'); ?>');
		$("#bodyContainer").css('padding-top', '60px');
		$("#bodyContainer").css('padding-bottom', '40px');
		$(".navbar").removeClass('hide').fadeIn('slow');
		$(".footer").removeClass('hide').fadeIn('slow');
	}
	else
	{
		$('#fullscreen').html('<i class="icon-resize-small"></i>&nbsp;<?php echo _('Ver Men&uacute;'); ?>');
		$('#fullscreen2').html('<i class="icon-resize-small"></i>&nbsp;<?php echo _('Ver Men&uacute;'); ?>');
		$(".navbar").fadeOut('slow', function(){
			$(".navbar").addClass('hide');
			$("#bodyContainer").css('padding-top', '5px');
		})
		$(".footer").fadeOut('slow', function(){
			$(".footer").addClass('hide');
			$("#bodyContainer").css('padding-bottom', '5px');
		});
	}
};

var e = document.getElementById("fullscreen");
e.onclick = toggleFullScreen;

e2 = document.getElementById("fullscreen2");
e2.onclick = toggleFullScreen;

function RunPrefixMethod(obj, method) {
	var pfx = ["webkit", "moz", "ms", "o", ""];
	var p = 0, m, t;
	while (p < pfx.length && !obj[m]) {
		m = method;
		if (pfx[p] == "") {
			m = m.substr(0,1).toLowerCase() + m.substr(1);
		}
		m = pfx[p] + m;
		t = typeof obj[m];
		if (t != "undefined") {
			pfx = [pfx[p]];
			return (t == "function" ? obj[m]() : obj[m]);
		}
		p++;
	}
}

<?php
	if (!$user->get_therapist() && !$user->get_superuser())
	{
?>
$(document).ready(function(){
	setTimeout(function(){
		e.click();
	}, 1000);
});
<?php
	}
?>

function getLastSession(successFunction) {
	$.ajax({
			type: 'GET',
			dataType: 'json',
			data: '',
			url: 'backend/last_session.php',
			success: successFunction,
			async: false
		});
}

function updateSession(sessionID, exerciseID, result, repetitions, resultFunction) {
	var qry = 'sessionID='+sessionID+'&exerciseID='+exerciseID+'&result='+result+'&repetitions='+repetitions;
		
	$.ajax({
		type: 'POST',
		data: qry,
		url: 'backend/update_session.php',
		success: function(res) {
			getLastSession(resultFunction);
		},
		async: false
	});
}

function startSession() {
	var sessionID = lastSession['sessionID'];
	updateSession(sessionID, 0, 1, 0, null); //complete introductory screen
	updateSession(sessionID, 1, 0, 0, null); //start first exercise
	getLastSession(function(result){
		lastSession = result;
		nextExercise();
	});
};

function canShowMiniMenu(show) {
	allowShowMiniMenu = show;
	if (show)
	{
		$("#hideInstructionsButton").show();
		$("#miniMenuDivider").show();
	}
	else{
		$("#hideInstructionsButton").hide();
		$("#miniMenuDivider").hide();
	}
};

function expandMenu(animated){
	if (showInstructionsFullScreen === null){
		showInstructionsFullScreen = !$(".page-header").is(":visible");
	}
	
	var prev = showInstructionsFullScreen;
	if (!showInstructionsFullScreen)
	{
		showInstructions(animated);
	}
	else //show full screen instructions
	{
		$(".page-header").addClass('fullscreen-header');
		showInstructions(false, false);
	}
	showInstructionsFullScreen = prev;
};

function collapseMenu(animated) {
	if (allowShowMiniMenu){
		if (showInstructionsFullScreen === null){
			showInstructionsFullScreen = !$(".page-header").is(":visible");
		}
		
		var prev = showInstructionsFullScreen;
		showMiniMenu(!showInstructionsFullScreen);
		showInstructionsFullScreen = prev;
	}
};

function showMiniMenu(animated){
	if (allowShowMiniMenu){
		showInstructionsFullScreen = null;
		$('.page-header').hide();
		$(".page-header").removeClass('fullscreen-header');
		if (animated){
			$('.mini-menu').fadeIn('slow');
		}
		else {
			$('.mini-menu').show();
		}
	}
};

function showInstructions(animated, rmcls){
	showInstructionsFullScreen = null;
	$('.mini-menu').hide();
	
	if (typeof rmcls === 'undefined')
	{
		$(".page-header").removeClass('fullscreen-header');
	}
	else if (rmcls)
	{
		$(".page-header").removeClass('fullscreen-header');
	}
	
	if (animated) {
		$('.page-header').fadeIn('slow');
	}
	else{
		$('.page-header').show();
	}
};

function endDemo() {
	canShowMiniMenu(true);
	exercise.setAfterDemo(true);
	$('#exercise-container').load('beginExercise.php');
};

function endExercise() {
	showInstructions(false);
	
	exercise.finishExercise();
	var medalObj = exercise.hasMedal();
	if (medalObj.medal)
	{
		medalType = medalObj.type;
	}
	else medalType = null;
	
	$('#exercise-container').trigger('exerciseFinishedEvent');
};

function forceEndDemo(){
	endDemo();
};

function setForceNext(f)
{
	forceNext = f;
};

function shouldForceNext()
{
	return forceNext;
};

var jumpExerciseHTML = "<p id='exercise-subdescription'><?php echo _('<span style=\'text-decoration:underline;\'>Nota:</span> Este ejercicio tiene una prueba de uso. Si ya sabes cómo se hace el ejercicio y no quieres repetir la prueba, pulsa el botón comenzar ejercicio:'); ?> <a href='javascript:forceEndDemo();' class='btn btn-warning btn-small' style='font-weight: 500;text-shadow: none;'><?php echo _('Comenzar ejercicio'); ?></a>.</p>";

function forceRepeatExercise() {
	var sessionID = exercise.sessionID();
	var exerciseID = exercise.exerciseID();
	var repetition = exercise._repetition;
	exercise.setRepeating(true);
	exercise._repetition = 0;
	
	$('#exercise-title').html(exercise.title());
	var text = exercise.description();
	$('#exercise-description').html(text);
	if (exercise.demoUIurl() != null && repetition==0){
		canShowMiniMenu(false);
		$('#exercise-container').html(exercise.demoUI());
		
		if (exercise.canAvoidHelp())
		{
			if (text.indexOf('.', text.length - 1) === -1)
			{
				text += '.';
			}
			$('#exercise-description').html(text+jumpExerciseHTML);
		}
	}
	else
	{
		canShowMiniMenu(true);
		$('#exercise-container').html(exercise.ui());
	}
};

function nextExercise() {
	setForceNext(false);

    var end;

    $.ajax({
        type: 'GET',
        data: '',
        url: 'backend/last_exercise.php',
        success: function(r) {
            end = r;
        },
        async: false
    });

    if (end == 'false') {
        var sessionConfiguration = null;
        medalType = null;

        if (!lastSession) {
            //introductory session
            //register first session, exercise 0.
            updateSession(1, 0, 0, 0, function (result) {
                getLastSession(function (res) {
                    lastSession = res;
                });
            });
        }

        var sessionID = parseInt(lastSession['sessionID']);
        var exerciseID = parseInt(lastSession['exerciseID']);

        if (exerciseID > 0) {
            showInstructionsFullScreen = null;

            if (sessions.session[sessionID - 1].exercises.exerciseEntry instanceof Array) {
                exerciseEntry = sessions.session[sessionID - 1].exercises.exerciseEntry[exerciseID - 1];
            }
            else {
                exerciseEntry = sessions.session[sessionID - 1].exercises.exerciseEntry;
            }

            var repetition = parseInt(lastSession['repetitions']);
            exercise = Exercise.getInstance(sessionID, exerciseID, repetition, exerciseEntry);

            $('#exercise-title').html(exercise.title());
            var text = exercise.description();
            $('#exercise-description').html(text);
            if (exercise.demoUIurl() != null && repetition == 0) {
                canShowMiniMenu(false);
                $('#exercise-container').html(exercise.demoUI());

                if (exercise.canAvoidHelp()) {
                    if (text.indexOf('.', text.length - 1) === -1) {
                        text += '.';
                    }
                    $('#exercise-description').html(text + jumpExerciseHTML);
                }
            }
            else {
                canShowMiniMenu(true);
                $('#exercise-container').html(exercise.ui());
            }
            $('#exercise-container').bind('exerciseFinishedEvent', function () {
                $('#exercise-container').unbind('exerciseFinishedEvent');
                showInstructionsFullScreen = null;

                var finish = false;
                var endSession = false;
                lastExercise = false;

                showInstructions(false);

                if (shouldForceNext()) repetition = exerciseEntry.repetitions + 1;

                if (repetition + 1 >= exerciseEntry.repetitions) //exercise completed
                {
                    updateSession(sessionID, exerciseID, 1, exerciseEntry.repetitions, function (result) {
                        lastSession = result;
                    });

                    if ((sessions.session[sessionID - 1].exercises.exerciseEntry instanceof Array) && (exerciseID < sessions.session[sessionID - 1].exercises.exerciseEntry.length)) //take next exercise
                    {
                        updateSession(sessionID, exerciseID + 1, 0, 0, function (result) {
                            lastSession = result;
                        });
                    }
                    else {
                        lastExercise = true;

                        if (sessionID < sessions.session.length) //take next session
                        {
                            endSession = true;
                            updateSession(sessionID + 1, 0, 0, 0, function (result) {
                                lastSession = result;
                            });
                        }
                        else {
                            finish = true;
                        }
                    }
                }
                else //exercise incomplete
                {
                    updateSession(sessionID, exerciseID, 0, repetition + 1, function (result) {
                        lastSession = result;
                    });
                }

                if (!finish && medalType == null) {
                    if (!endSession) nextExercise();
                    else //show endSession UI
                    {
                        $.get('endSession.php', function (data) {
                            $('#exercise-container').html(data);
                        });
                    }
                }
                else if (!finish && medalType != null) {
                    $.get('medal.php', function (data) {
                        $('#exercise-container').html(data);
                        medalType = null;
                    });
                }
                else //finish VIRTRA-EL
                {
                    $('#exercise-title').html("<?php echo _('&iexcl;Enhorabuena, ya has terminado!'); ?>");
                    $('#exercise-description').html("<?php echo _('Has completado todas las sesiones de VIRTRA-EL. Esperamos haber contribuido a la mejora de tus capacidades.'); ?>");
                    $('#exercise-container').load("finalSurprise.php");
                }
            });
        }
        else {
            if (!sessionID) sessionID = 1;
            if (!exerciseID) exerciseID = 0;
            $('#exercise-title').html('<?php echo _('Sesi&oacute;n'); ?> ' + sessionID);
            $('#exercise-description').html(sessions.session[sessionID - 1].description);
            $('#exercise-container').html("<a href='javascript:void(0);' onclick='javascript:startSession();' class='btn btn-primary btn-large'><?php echo _('Comenzar'); ?></a>");
        }
    }
    else {
        $('#exercise-title').html("<?php echo _('&iexcl;Enhorabuena, ya has terminado!'); ?>");
        $('#exercise-description').html("<?php echo _('Has completado todas las sesiones de VIRTRA-EL. Esperamos haber contribuido a la mejora de tus capacidades.'); ?>");
        $('#exercise-container').load("finalSurprise.php");
    }
};
</script>

<?php } ?>
