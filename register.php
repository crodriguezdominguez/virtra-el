<?php
	require_once('locale/localization.php');
?>

<div class="page-header">
  <h1><?php echo _('Registro'); ?></h1>
  <p class="lead"><?php echo _('Debes registrarte para poder realizar los ejercicios de VIRTRA-EL. Por favor, rellena el siguiente formulario. Si eres un <b>terapeuta</b> o un <b>cuidador</b>, por favor, <b><a href="javascript:void(0);" onclick="javascript:$(\'#contactMenuItem\').click();">contacta con el administrador</a></b> para que te dé de alta.'); ?></p>
</div>
<form class="form-horizontal" id="contact-form">
	<fieldset>
	<div class="control-group">
		<label class="control-label" for="name"><?php echo _('Nombre'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="name" name="name">
		</div>
    </div>
    <div class="control-group">
		<label class="control-label" for="surname"><?php echo _('Apellidos'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="surname" name="surname">
		</div>
    </div>
    <div class="control-group">
		<label class="control-label" for="email"><?php echo _('E-Mail'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="email" name="email">
		</div>
    </div>
    <div class="control-group">
		<label class="control-label" for="email2"><?php echo _('Confirmar E-Mail'); ?></label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="email2" name="email2">
			<span class="help-block"><?php echo _('Compruebe que su e-mail sea correcto. Se le enviar&aacute; un mensaje para que confirme este registro antes de activar su cuenta.'); ?></span>
		</div>
    </div>
    <div class="control-group">
		<label class="control-label" for="password"><?php echo _('Contrase&ntilde;a'); ?></label>
		<div class="controls">
			<input type="password" class="input-small" id="password" name="password">
		</div>
    </div>
    <div class="control-group">
		<label class="control-label" for="password2"><?php echo _('Confirmar contrase&ntilde;a'); ?></label>
		<div class="controls">
			<input type="password" class="input-small" id="password2" name="password2">
		</div>
    </div>
    <div class="control-group" id="captcha-group">
		<label class="control-label" id="captcha-label" for="captcha"></label>
		<div class="controls">
			<input type="text" class="input-mini" maxlength="2" id="captcha" name="captcha">
			<span class="help-inline"><?php echo _('Esto evita que se haga un mal uso del formulario de registro.'); ?></span>
		</div>
    </div>
    <div class="control-group">
	    <div class="controls">
			<button type="submit" class="btn btn-large btn-primary"><?php echo _('Registrarse'); ?></button>
			<button type="reset" class="btn btn-large"><?php echo _('Limpiar'); ?></button>
		</div>
	</div>
	</fieldset>
</form>

<script type="text/javascript">
var c;
reloadCaptcha();

function reloadCaptcha()
{
	var a = Math.ceil(getRandom() * 10);
	var b = Math.ceil(getRandom() * 10);
	c = a + b;
	
	$('#captcha-label').html("<?php echo _('Resultado de'); ?>"+' '+a+' + '+b);
};

/*function checkForm()
{
	var subject = $('#subject').val();
	var name = $('#name').val();
	var email = $('#email').val();
	var message = $('#message').val();
	var captcha = $('#captcha').val();

	$(".error").removeClass('error');
	if (name.length <= 0)
	{
		$('#name-group').addClass('error');
	}
	else if (email.length <= 0)
	{
		$('#email-group').addClass('error');
	}
	else if (subject.length <= 0)
	{
		$('#subject-group').addClass('error');
	}
	else if (message.length <= 0)
	{
		$('#message-group').addClass('error');
	}
	else if (captcha.length <= 0 || parseInt(captcha) != c)
	{
		$('#captcha-group').addClass('error');
	}
	else
	{
		var qry = 'subject='+subject+'&'+
				  'name='+name+'&'+
				  'email='+email+'&'+
				  'message='+message;
				  
		console.debug(qry);
			
		$.ajax({
			type: 'POST',
			data: qry,
			url: 'backend/contact.php',
			success: function(res) {
				$('#result-form').removeClass('contact-form-ok').removeClass('contact-form-error');
				
				if (res=='false')
				{
					$('#result-form').addClass('contact-form-error').html("<?php echo _('Ocurri&oacute; un error al enviar el mensaje. Por favor, int&eacute;ntelo m&aacute;s tarde.'); ?>");
				}
				else
				{
					reloadCaptcha();
					$('#reset-button').click();
					
					$('#result-form').addClass('contact-form-ok').html("<?php echo _('El mensaje se envi&oacute; correctamente. Contactaremos con usted lo antes posible.'); ?>");
				}
			}
		});
	}
};*/

</script>
