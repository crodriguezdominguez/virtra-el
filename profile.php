<?php
	require_once('locale/localization.php');
	
	//tener en cuenta la personalizacion de la dificultad
	session_start();
?>

<?php
	if (isset($_SESSION['userID'])) {
		include_once('php/User.php');
		$user = new User($_SESSION['userID']);
?>
<ul class="nav nav-tabs">
	<li id="settings-access" class="active"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('access');"><?php echo _('Datos de Acceso'); ?></a></li>
	<?php
		if ($user->get_superuser())
		{
	?>
		<li id="settings-therapists"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('therapists');"><?php echo _('Terapeutas'); ?></a></li>
		<li id="settings-carers"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('carers');"><?php echo _('Cuidadores'); ?></a></li>
	<?php	
		}
	?>
	<?php
		if ($user->get_therapist() || $user->get_superuser() || $user->get_carer())
		{
	?>
		<?php
			if (!$user->get_superuser())
			{
		?>
		<li id="settings-patients"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('patients');"><?php echo _('Pacientes'); ?></a></li>
		<li id="settings-results"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('results');"><?php echo _('Estadísticas Globales'); ?></a></li>
		<?php
			}
			if (!$user->get_carer())
			{
		?>
		<li id="settings-test-settings"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('test_settings');"><?php echo _('Ajustes de Prueba'); ?></a></li>
		<?php
			}
		?>
	<?php
		}
		else //patient
		{
	?>
		<li id="settings-avatar"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('avatar');"><?php echo _('Personalizar Asistente'); ?></a></li>
		<li id="settings-therapist"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('therapist');"><?php echo _('Mi Terapeuta'); ?></a></li>
		<li id="settings-carer"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('carer');"><?php echo _('Mi Cuidador'); ?></a></li>
		<li id="settings-results"><a href="javascript:void(0);" onclick="javascript:load_settings_tab('results');"><?php echo _('Resultados'); ?></a></li>
	<?php	
		}
	?>
</ul>
<div id='settings-container'>
</div>

<script type="text/javascript">
	function load_settings_tab(tab_name){
		$("li[id^='settings']").removeClass('active');
		if (tab_name == 'test_settings')
		{
			$("#settings-test-settings").addClass('active');
			$("#settings-container").load('backend/ui/test_settings.php');
		}
		else if (tab_name == 'access')
		{
			$("#settings-access").addClass('active');
			$("#settings-container").load('backend/ui/access.php');
		}
		else if (tab_name == 'therapists')
		{
			$("#settings-therapists").addClass('active');
			$("#settings-container").load('backend/ui/therapists.php');
		}
		else if (tab_name == 'therapist')
		{
			$("#settings-therapist").addClass('active');
			$("#settings-container").load('backend/ui/therapist.php');
		}
		else if (tab_name == 'carer')
		{
			$("#settings-carer").addClass('active');
			$("#settings-container").load('backend/ui/carer.php');
		}
		else if (tab_name == 'patients')
		{
			$("#settings-patients").addClass('active');
			<?php
				if ($user->get_therapist() || $user->get_carer())
				{
			?>
			$("#settings-container").load('backend/ui/patients.php');
			<?php
				}
				else if ($user->get_superuser())
				{
			?>
				$("#settings-container").load('backend/ui/edit_user.php?th=0&adding=1');
			<?php
				}
			?>
		}
		else if (tab_name == 'carers')
		{
			$("#settings-carers").addClass('active');
			$("#settings-container").load('backend/ui/carers.php');
		}
		else if (tab_name == 'results')
		{
			$("#settings-results").addClass('active');
			$("#settings-container").load('backend/ui/results.php');
		}
		else if (tab_name == 'avatar')
		{
			$("#settings-avatar").addClass('active');
			$("#settings-container").load('backend/ui/avatar.php');
		}
	}
	
	$("#settings-container").load('backend/ui/access.php');
</script>
<?php
	} else {
?>

<script type="text/javascript">
$('#bodyContainer').load('unregistered.php');
</script>

<?php
	}
?>