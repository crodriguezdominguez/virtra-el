<?php
	include_once(__DIR__.'/config.php');
	
	session_start();
	
	function search($text, $lang, $th, $carer, $link)
	{
		$table_name = $carer != '0' ? 'carer_patient' : 'therapist_patient';
		$result = $link->query("SELECT * FROM `user` WHERE therapist='0' AND carer='0' AND (name LIKE '%$text%' OR surname LIKE '%$text%' OR email LIKE '%$text%') AND id NOT IN (SELECT patient_id FROM $table_name)");
		$ret = array();
		while ($row = $result->fetch_assoc())
		{
			array_push($ret, $row);
		}
		
		mysqli_free_result($result);
		
		return json_encode($ret);
	}
	
	function number_of_pages($text, $max_page_elements, $th, $carer, $link)
	{
		$table_name = $carer != '0' ? 'carer_patient' : 'therapist_patient';
		
		$result = $link->query("SELECT count(*) AS count FROM `user` WHERE therapist='0' AND carer='0' AND (name LIKE '%$text%' OR surname LIKE '%$text%' OR email LIKE '%$text%') AND id NOT IN (SELECT patient_id FROM $table_name)");
	
		if ($row = $result->fetch_assoc())
		{
			return ceil(intval($row['count'])/$max_page_elements);
		}
		else return 0;
	}
	
	function paginated_search($text, $lang, $max_page_elements, $page, $th, $carer, $link)
	{
		if ($max_page_elements > 0)
		{
			$table_name = $carer != '0' ? 'carer_patient' : 'therapist_patient';
			
			$low_limit = $page*$max_page_elements;
			$result = $link->query("SELECT * FROM `user` WHERE therapist='0' AND carer='0' AND (name LIKE '%$text%' OR surname LIKE '%$text%' OR email LIKE '%$text%') AND id NOT IN (SELECT patient_id FROM $table_name) LIMIT $low_limit,$max_page_elements");
			$ret['elements'] = array();
			while ($row = $result->fetch_assoc())
			{
				array_push($ret['elements'], $row);
			}
			
			mysqli_free_result($result);
			
			$ret['npages'] = number_of_pages($text, $max_page_elements, $th, $carer, $link);
			return json_encode($ret);
		}
		else return search($text, $lang, $th, $carer, $link);
	}

	$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
	mysqli_set_charset($link, 'utf8');
	
	$text = $_GET['q'];
	$limit = $_GET['l'];
	$page = $_GET['p'];
	$th = isset($_GET['th'])?$_GET['th']:'0';
	$carer = isset($_GET['carer'])?$_GET['carer']:'0';
	
	echo paginated_search($text, 'en', $limit, $page, $th, $carer, $link);
	
	$link->close();
?>