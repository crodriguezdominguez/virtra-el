<?php
	require_once(__DIR__.'/../php/User.php');

	session_start();
			
	if (isset($_SESSION['userID']) && isset($_POST['user']))
	{
		if ($_POST['user']==$_SESSION['userID'])
		{
			$creator = new User($_SESSION['userID']);
			
			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$email = $_POST['email'];
			$therapist = ($_POST['th']=='1')?true:false;
			$carer = ($_POST['carer']=='1')?true:false;
			$activated = ($_POST['activated']=='1')?true:false;
			$pass = isset($_POST['pass'])?$_POST['pass']:'';
			
			if ( $creator->get_superuser() || (!$therapist && $creator->get_therapist()) )
			{
				$user = null;
				if (isset($_POST['saveID']))
				{
					$user = new User($_POST['saveID']);
					if (strlen($pass) > 0)
					{
						$user->set_md5_password($pass);
					}
				}
				else
				{
					$user = new User();
					$user->set_md5_password($pass);
					$user->set_superuser(false);
				}
				
				$user->set_name($name);
				$user->set_surname($surname);
				$user->set_username($email);
				$user->set_therapist($therapist);
				$user->set_carer($carer);
				$user->set_activated($activated);
				$user->save();
				
				if ($creator->get_therapist() && !$therapist && !isset($_POST['saveID']))
				{
					$user->set_therapist_user($creator);
					$user->save();
				}
				
				echo 'true';
			}
			else echo 'false';
		}
		else echo 'false';
	}
	else echo 'false';
?>