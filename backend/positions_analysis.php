<?php
	include_once('config.php');
	require_once(__DIR__.'/PHPExcel.php');
	
	function userCode($link, $userID) {
		$ret = null;
		$result = $link->query("SELECT name,surname,dateOfBirth FROM user,userProfile WHERE userProfile.userID=user.id AND user.id='$userID'");
		if ($row = mysqli_fetch_array($result))
		{
			$date = split('-', $row['dateOfBirth']);
			$sSurname = split(' ', $row['surname']);
			$cod = '';
			if (count($sSurname) >= 2)
			{
				$cod = substr($row['surname'], 0, 2).substr($sSurname[1], 0, 2);
			}
			else
			{
				$cod = substr($row['surname'], 0, 2);
			}
			
			if (count($cod) == 0 || $cod == null){
				return mb_strtoupper($row['name'], 'UTF-8');
			}
			
			if (count($date) >= 3)
			{
				$d = $date[2];
				$m = $date[1];
				$cod = $cod.$d.$m;
			}
			$ret = mb_strtoupper($cod, 'UTF-8');
		}
		
		mysqli_free_result($result);
		
		return $ret;
	}
	
	function comp($array1, $array2) {
		$comp = strcmp($array1[0], $array2[0]);
		
		if ($comp == 0) {
			if ($array1[1] < $array2[1]) {
				return -1;
			}
			else if ($array1[1] > $array2[1]) {
				return 1;
			}
			else {
				if ($array1[2] < $array2[2]) {
					return -1;
				}
				else if ($array1[2] > $array2[2]) {
					return 1;
				}
				else {
					if ($array1[7] < $array2[7]) {
						return -1;
					}
					else {
						return 1;
					}
				}
			}
		}
		else {
			return $comp;
		}
	}
	
	function analysis($link) {
		$result = $link->query("SELECT * FROM partialResult WHERE exerciseType=25 ORDER BY userID,repetition");
	
		$entries = [];
		while ($row = $result->fetch_array())
		{
			$repetition = intval($row['repetition'])+1;
			$userID = userCode($link, $row['userID']);
			
			if ($userID != null) {
				$json_str = $row['entry'];
				$json_str = str_replace('["', '[', $json_str);
				$json_str = str_replace('"]', ']', $json_str);
				$json_str = str_replace('}","', '},', $json_str);
				
				$json = json_decode($json_str, true);
				
				$count = 0;
				foreach ($json as $json_entry) {
					if (array_key_exists('fullmap', $json_entry)) {
						$entry = [$userID, $repetition, $json_entry['helpLevel'], $json_entry['corrects'], $json_entry['fails'], $json_entry['omissions'], $json_entry['time'], $count];
						
						array_push($entries, $entry);
						
						$count++;
					}
				}
			}
		}
		
		mysqli_free_result($result);
		
		uasort($entries, 'comp');
		
		return $entries;
	}
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()->setCreator('VIRTRA-EL')
						 ->setLastModifiedBy('VIRTRA-EL')
						 ->setTitle(_('Datos extraídos de VIRTRA-EL'))
						 ->setSubject(_('Datos extraídos de VIRTRA-EL'))
						 ->setDescription(_('Datos sobre los resultados del ejercicio de test de posiciones de VIRTRA-EL'))
						 ->setKeywords(_('VIRTRA-EL datos pacientes'))
						 ->setCategory(_('Datos VIRTRA-EL'));
	
	$sheet = $objPHPExcel->setActiveSheetIndex(0);
	$sheet->setTitle(_('Test Posiciones'));
	
	$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
	$link->set_charset('utf8');
	
	$entries = analysis($link);
	
	$row = 1;
	
	$labels = ['Usuario', 'Repeticion', 'Nivel Ayuda', 'Aciertos', 'Fallos', 'Omisiones', 'Tiempo (seg.)'];
	
	for ($c = 0; $c < count($labels); $c++) {
		$sheet->setCellValueByColumnAndRow($c, $row, $labels[$c]);
		$sheet->getStyleByColumnAndRow($c, $row)->getFont()->setBold(true);
	}
	
	$row++;
	
	foreach ($entries as $entry) {
		for ($c = 0; $c < count($entry)-1; $c++) {
			$sheet->setCellValueByColumnAndRow($c, $row, $entry[$c]);
		}
		
		$row++;
	}
	
	$link->close();
						 
	// Redirect output to a client’s web browser (Excel2007)
	$filename = 'test-posiciones-'.date('d_m_Y_H_i_s').'.xlsx';
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$filename.'"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>