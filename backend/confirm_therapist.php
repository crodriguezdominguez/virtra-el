<?php
	require_once(__DIR__.'/../locale/localization.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>VIRTRA-EL</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- css styles -->
	    <link href="../css/bootstrap.min.css" rel="stylesheet">
	    <link href="../css/virtrael.css" rel="stylesheet">
	    <link href="../css/datepicker.css" rel="stylesheet">

	    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	
	    <!-- favicon and touch icons (for iPhone, iPad and iPod Touch)
	    <link rel="shortcut icon" href="img/ico/favicon.ico">
	    <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
	    <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72x72.png">
	    <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114x114.png">
	    <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144x144.png">
	    -->
	</head>
	<body>
		<div id="wrap">	
			<div id="bodyContainer" class="container">
<?php
	require_once(__DIR__.'/../php/User.php');

	session_start();
	
	if (!isset($_GET['therapist_id']) || !isset($_GET['patient_id']))
	{
		echo _('<p class="lead">El v&iacute;nculo no es correcto.</p>');
	}
	else if (!isset($_SESSION['userID']))
	{
		echo _('<p class="lead">Debe estar dado de alta en VIRTRA-EL y haber accedido a la plataforma para poder confirmar al usuario como su paciente.</p>');
	}
	else
	{
		$user = new User($_SESSION['userID']);
		if ($user->get_therapist())
		{
			$th_id = $_GET['therapist_id'];
			if ($th_id != $user->get_username())
			{
				echo _("<p class=\"lead\">Debe haber accedido a VIRTRA-EL con el usuario $th_id para poder confirmar al usuario como su paciente.</p>");
			}
			else
			{
				$p_id = $_GET['patient_id'];
				$patient = new User($p_id);
				$patient->set_therapist_user($user);
				$user->save();
				
				echo _('<p class="lead">Se ha asignado correctamente al usuario como su paciente. Puede ver los datos del paciente en su p&aacute;gina de gesti&oacute;n de pacientes de VIRTRA-EL.</p>');
			}
		}
		else
		{
			echo _('<p class="lead">Debe estar registrado como terapeuta para poder confirmar al usuario como su paciente.</p>');
		}
	}
?>
<p><a href="http://www.virtrael.es/" target="_self"><?php echo _('Ir a VIRTRA-EL'); ?></a></p>
			</div>
		</div>
	</body>