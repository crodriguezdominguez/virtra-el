<?php
	include_once('config.php');

	session_start();
	
	if (isset($_SESSION['userID']))
	{
		$userID = $_SESSION['userID'];
		$sessionID = $_POST['sessionID'];
		$exerciseID = $_POST['exerciseID'];
		
		$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
		
		$result = $link->query("SELECT * FROM exerciseResult WHERE userID='$userID' and sessionID='$sessionID' and exerciseID='$exerciseID'");
		$num_row = mysqli_num_rows($result);
		if ($num_row >=1)
		{
			$row=$result->fetch_assoc();
			$countCorrects = intval($row['countCorrects']);
			$countFails = intval($row['countFails']);
			$total = $countCorrects+$countFails;
			$perc = $countCorrects/$total;
			
			$kindOfMedal = 0;
			if ($perc >= 0.8)
			{
				$kindOfMedal = 2;
			}
			else if ($perc < 0.8 && $perc >= 0.6)
			{
				$kindOfMedal = 1;
			}
			
			mysqli_free_result($result);
			$result = $link->query("SELECT * FROM medal WHERE userID='$userID' and sessionID='$sessionID' and exerciseID='$exerciseID'");
			$num_row = mysqli_num_rows($result);
			if ($num_row >= 1)
			{
				$row = $result->fetch_assoc();
				$rowID = $row['id'];
				$link->query("UPDATE medal SET kindOfMedal='$kindOfMedal', date=NOW() WHERE id='$rowID'");
			}
			else
			{
				$link->query("INSERT INTO medal (`id`, `kindOfMedal`, `date`, `sessionID`, `exerciseID`, `userID`) VALUES(NULL, '$kindOfMedal', NOW(), '$sessionID',  '$exerciseID', '$userID')");
			}
			
			echo $kindOfMedal;
		}
		
	    mysqli_free_result($result);
	    $link->close();
	}
?>
