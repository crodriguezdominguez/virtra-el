<?php
	include_once('config.php');

	session_start();
	
	if (isset($_SESSION['userID']))
	{
		$userID = $_SESSION['userID'];
		$sessionID = $_POST['sessionID'];
		$exerciseID = $_POST['exerciseID'];
		$countCorrects = $_POST['countCorrects'];
		$countFails = $_POST['countFails'];
		$countOmissions = isset($_POST['countOmissions'])?$_POST['countOmissions']:"0";
		$finalScore = isset($_POST['finalScore'])?$_POST['finalScore']:"0";
		$seconds = isset($_POST['seconds'])?$_POST['seconds']:"0";
		
		$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
		
		$result = $link->query("SELECT * FROM exerciseResult WHERE userID='$userID' and sessionID='$sessionID' and exerciseID='$exerciseID'");
		$num_row = mysqli_num_rows($result);
		if ($num_row >=1)
		{
			$row=$result->fetch_assoc();
			$rowID = $row['id'];
			$rowCountCorrects = intval($row['countCorrects'])+intval($countCorrects);
			$rowCountFails = intval($row['countFails'])+intval($countFails);
			$rowCountOmissions = intval($row['countOmissions'])+intval($countOmissions);
			$rowSeconds = intval($row['seconds'])+intval($seconds);
			
			$link->query("UPDATE exerciseResult SET countCorrects='$rowCountCorrects', countFails='$rowCountFails', seconds='$rowSeconds', countOmissions='$rowCountOmissions', finalScore='$finalScore' WHERE id='$rowID'");
		}
		else
		{
			$link->query("INSERT INTO exerciseResult (`id`, `sessionID`, `exerciseID`, `userID`, `countCorrects`, `countFails`, `countOmissions`, `finalScore`, `seconds`) VALUES(NULL, '$sessionID', '$exerciseID', '$userID', '$countCorrects', '$countFails', '$countOmissions', '$finalScore', '$seconds')");
		}
		
	    mysqli_free_result($result);
	    $link->close();
	}
?>
