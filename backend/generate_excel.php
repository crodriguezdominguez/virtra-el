<?php
	require_once(__DIR__.'/../php/User.php');
	require_once(__DIR__.'/PHPExcel.php');
	include_once(__DIR__.'/config.php');
	
	session_start();
	
	$xml = simplexml_load_file(__DIR__.'/../sessions.php');
	
	function mapExerciseTitle($exerciseType)
	{
		$e = '';
		switch(intval($exerciseType))
		{
			// Memory
			case 1:
				$e = _('Números y vocales');
				break;
			case 2:
				$e = _('Lista de tareas');
				break;
			case 3:
				$e = _('Clasificación de objetos');
				break;
			case 18:
				$e = _('Lista de tareas');
				break;
			case 19:
				$e = _('Lista de palabras');
				break;
			case 7:
				$e = _('Dictado de números');
				break;
			case 8:
				$e = _('Lista de palabras');
				break;
			case 25:
				$e = _('Test de posiciones');
				break;
				
			// Attention
			case 4:
				$e = _('Globos');
				break;
			case 5:
				$e = _('Bolsa de objetos');
				break;
			case 6:
				$e = _('Pirámides');
				break;
			case 9:
				$e = _('Objetos desordenados');
				break;
				
			// Reasoning exercises
			case 11:
				$e = _('Series semánticas');
				break;
			case 12:
				$e = _('Series lógicas');
				break;				
			case 13:
				$e = _('Razonamiento espacial');
				break;				
			case 14:
				$e = _('¿Cuál es diferente?');
				break;
			case 15:
				$e = _('Analogías semánticas');
				break;
		
			// Planning
			case 16:
				$e = _('Comprar regalos');
				break;
			case 17:
				$e = _('Reparto de paquetes');
				break;
			case 20:
				$e = _('Realidad virtual (pre)');
				break;
			case 21:
				$e = _('Realidad virtual (post)');
				break;
				
			// Tests
			case 30:
				$e = _('Cuestionario (I)');
				break;
				
			case 32:
				$e = _('Ratón');
				break;
				
			case 33:
				$e = _('Cuestionario (II)');
				break;
				
			// Helps
			case 34:
				$e = _('Ej. de Introducción');
				break;
				
			case 101:
				$e = _('Primera ayuda');
				break;
		
			case 102:
				$e = _('Segunda ayuda');
				break;
		}
		
		return $e;
	}
	
	function key_name_for($sessionID, $exerciseID)
	{
		global $xml;
		foreach($xml as $session)
		{
			$compSessionID = $session['id'];
			if (intval($sessionID) == intval($compSessionID))
			{
				//look for the exercise
				foreach ($session->exercises->exerciseEntry as $exerciseEntry)
				{
					if (intval($exerciseEntry['id']) == intval($exerciseID))
					{
						$exerciseType = $exerciseEntry['type'];
						return array('type'=>$exerciseType, 'name'=>mapExerciseTitle($exerciseType));
					}
				}
			}
		}
		
		return null;
	}
	
	$maxTaskListRep = 0;
	$maxTaskListDemRep = 0;
	$maxPositionsRep = 0;
	$maxPyramids = 0;
	$maxPositionsC = array();
	
	function extract_data_from_userID($userID, $link, $pre, $sessionID=null)
	{
		global $maxTaskListRep;
		global $maxTaskListDemRep;
		global $maxPositionsRep;
		global $maxPyramids;
		global $maxPositionsC;
		
		$maxTaskListRep = 0;
		$maxTaskListDemRep = 0;
		$maxPyramids = 0;
		$maxPositionsC = array();

        $ret = array();
        $ret['id'] = "$userID";
        $ret[_('evaluador')] = $_SESSION['userID'];
		
		$query = "SELECT * FROM exerciseResult WHERE userID='$userID'";
		if ($sessionID === null) {
            if ($pre) $query.=' AND (sessionID=\'1\' OR sessionID=\'2\')';
            else $query.=' AND (sessionID=\'12\' OR sessionID=\'13\')';
        }
        else {
            $query.=" AND sessionID='$sessionID'";
        }
		
		$result = $link->query($query);
		
		while($row = $result->fetch_assoc())
		{
			$sessionID = $row['sessionID'];
			$exerciseID = $row['exerciseID'];
			$corrects = intval($row['countCorrects']);
			$fails = intval($row['countFails']);
			$omissions = intval($row['countOmissions']);
			$score = intval($row['finalScore']);
			$seconds = intval($row['seconds']);
			
			$keyValues = key_name_for($sessionID, $exerciseID);
			$exerciseType = $keyValues['type'];
			$exerciseName = $keyValues['name'];
			
			switch(intval($exerciseType))
			{
				case 1: //numbers and vowels
					$ret[_('NyVAc')] = $corrects;
					$ret[_('NyVErr')] = $fails;
					$ret[_('NyVTiempo')] = $seconds;
					break;
				case 2: //task list
				{
					$result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition ASC");
					while ($row2 = $result2->fetch_assoc())
					{
						$vals = json_decode($row2['entry'], true);
						$rep = intval($row2['repetition']);
						$maxTaskListRep = max($rep+1, $maxTaskListRep);
						$key = _('ListTareas').($rep+1);
						
						$rec = json_encode($vals['recognizementList'], JSON_UNESCAPED_UNICODE);
						$rec = str_replace('{', '', $rec);
						$rec = str_replace('}', '', $rec);
						$rec = str_replace('"', '', $rec);
						
						$ret[$key._('Recon')] = $rec;
						$ret[$key._('Mostradas')] = $vals['shownWords'];
						$ret[$key._('Ac')] = $vals['corrects'];
						$ret[$key._('Err')] = $vals['fails'];
						$ret[$key._('Omi')] = $vals['omissions'];
						$ret[$key._('Tiempo')] = $vals['time'];
                        $ret[$key._('NMostradas')] = count(explode(',', $vals['shownWords']));
					}
					mysqli_free_result($result2);
					
					$ret[_('ListTareasAcTotal')] = $corrects;
					$ret[_('ListTareasErrTotal')] = $fails;
					$ret[_('ListTareasOmiTotal')] = $omissions;
					
					break;
				}
				case 3: //classify objects
					$ret[_('MemYClasifObjAc')] = $corrects;
					$ret[_('MemYClasifObjErr')] = $fails;
					$ret[_('MemYClasifObjOmi')] = $omissions;

                    $result2 = $link->query("SELECT * FROM exerciseLevel WHERE userID='$userID' and exerciseType='$exerciseType'");
                    if ($row2 = $result2->fetch_assoc()) {
                        $ret[_('MemYClasifObjNiv')] = $row2['userLevel'];
                    }

                    mysqli_free_result($result2);

					break;
				case 18: //task list (2)
				{
					$result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition ASC");
					while ($row2 = $result2->fetch_assoc())
					{
						$vals = json_decode($row2['entry'], true);
						$rep = intval($row2['repetition']);
						
						$maxTaskListDemRep = max($maxTaskListDemRep, $rep+1);
						
						$key = _('ListTareasDem').($rep+1);
						
						$rec = json_encode($vals['recognizementList'], JSON_UNESCAPED_UNICODE);
						$rec = str_replace('{', '', $rec);
						$rec = str_replace('}', '', $rec);
						$rec = str_replace('"', '', $rec);
						
						$ret[$key._('Recon')] = $rec;
						$ret[$key._('Mostradas')] = $vals['shownWords'];
						$ret[$key._('Ac')] = $vals['corrects'];
						$ret[$key._('Err')] = $vals['fails'];
						$ret[$key._('Omi')] = $vals['omissions'];
						$ret[$key._('Tiempo')] = $vals['time'];
                        $ret[$key._('NMostradas')] = count(explode(',', $vals['shownWords']));
					}
					mysqli_free_result($result2);
					
					$ret[_('ListTareasDemAcTotal')] = $corrects;
					$ret[_('ListTareasDemErrTotal')] = $fails;
					$ret[_('ListTareasDemOmiTotal')] = $omissions;
					
					break;
				}
				case 8: //word list
				{
					$result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition ASC");
					while ($row2 = $result2->fetch_assoc())
					{
						$vals = json_decode($row2['entry'], true);
						$rep = intval($row2['repetition']);
						
						$key = null;
						if ($rep < 3)
						{
							$key = _('ListPalA').($rep+1);
						}
						else
						{
							$key = _('ListPalB');
						}
						
						$ret[$key] = $vals['writtenWords'];
						$ret[$key._('Mostradas')] = $vals['shownWords'];
						$ret[$key._('Ac')] = $vals['corrects'];
						$ret[$key._('Err')] = $vals['fails'];
						$ret[$key._('Omi')] = $vals['omissions'];
						$ret[$key._('Tiempo')] = $vals['time'];
					}
					mysqli_free_result($result2);
					
					$ret[_('ListPalAcTotal')] = $corrects;
					$ret[_('ListPalErrTotal')] = $fails;
					$ret[_('ListPalOmiTotal')] = $omissions;
					
					break;
				}
				case 7: //numbers
					$ret[_('DigAc')] = $corrects;
					$ret[_('DigErr')] = $fails;
					break;
				case 19: //word list (2)
				{	
					$result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition ASC");
					while ($row2 = $result2->fetch_assoc())
					{
						$vals = json_decode($row2['entry'], true);
						$rep = intval($row2['repetition']);
						$key = '';
						
						if ($rep == 0)
						{
							$key = _('ListPalDemEscr');
							$ret[$key] = $vals['writtenWords'];
							
						}
						else
						{
							$key = _('ListPalDemRecon');
							
							$rec = json_encode($vals['recognizementList'], JSON_UNESCAPED_UNICODE);
							$rec = str_replace('{', '', $rec);
							$rec = str_replace('}', '', $rec);
							$rec = str_replace('"', '', $rec);
							$ret[$key] = $rec;
						}
						
						$ret[$key._('Mostradas')] = $vals['shownWords'];
						$ret[$key._('Ac')] = $vals['corrects'];
						$ret[$key._('Err')] = $vals['fails'];
						$ret[$key._('Omi')] = $vals['omissions'];
						$ret[$key._('Tiempo')] = $vals['time'];
					}
					mysqli_free_result($result2);
					
					$ret[_('ListPalDemAcTotal')] = $corrects;
					$ret[_('ListPalDemErrTotal')] = $fails;
					$ret[_('ListPalDemOmiTotal')] = $omissions;
					
					break;
				}
				case 25: //positions
				{
					$result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition ASC");

					$lastHelpLevel = 0;
					$helpsReceived = 0;
					$totalPG = 0;
					$somethingCorrect = false;

                    $ret[_('PosicTotalLaminas')] = $result2->num_rows;

					while ($row2 = $result2->fetch_assoc())
					{
                        $intHelpLevel = 0;
                        $maxHelpLevel = 0;

						$newEntry = str_replace('"{', '{', $row2['entry']);
						$newEntry = str_replace('}"', '}', $newEntry);
						$vals = json_decode($newEntry, true);
						$rep = intval($row2['repetition']);
						
						$maxPositionsRep = max($maxPositionsRep, $rep+1);
						
						$newRep = $rep+1;
						
						$c=0;
						//$ret['PosicLastRep'] = $newRep;
						
						$firstCorrect = -1;
						$firstCorrectOne = -1;
						foreach($vals as $val)
						{
							//$ret[_('PosicLaminaRes').$newRep.'-'.$c] = $val['fullmap'];

							$elements = explode('|', $val['fullmap']);
							$newCorrects = 0;
							$newFails = 0;
							$newOmissions = 0;
							foreach ($elements as $element) {
							    $internalElements = explode(',', $element);
							    $abi = intval(explode(':', $internalElements[0])[1]);
							    $sel = intval(explode(':', $internalElements[1])[1]);
							    if ($abi !== FALSE && $abi !== -1) {
                                    if ($abi === 1 && $sel === 1) {
                                        $newCorrects++;
                                    }
                                    else if ($abi === 1 && $sel === 0) {
                                        $newOmissions++;
                                    }
                                    else if ($abi === 0 && $sel === 1) {
                                        $newFails++;
                                    }
                                }
                            }

							$ret[_('PosicAcLamina').$newRep.'-'.($c+1)] = $newCorrects;
							//$ret[_('PosicErrLamina').$newRep.'-'.$c] = $val['fails'];
							//$ret[_('PosicOmiLamina').$newRep.'-'.$c] = $val['omissions'];
							$ret[_('PosicTiempoLamina').$newRep.'-'.($c+1)] = $val['time'];
                            $ret[_('PosicNivAyudaLamina').$newRep.'-'.($c+1)] = $intHelpLevel;

                            if ($newCorrects < 5) {
                                $intHelpLevel++;
                                if ($intHelpLevel !== 0) {
                                    $helpsReceived++;
                                }
                            }

                            if ($intHelpLevel > 8) {
                                $intHelpLevel = 8;
                            }

							$intFails = $newFails;
							$intOmissions = $newOmissions;

							if ($intHelpLevel > $lastHelpLevel) {
                                $lastHelpLevel = $intHelpLevel;
                            }

                            if ($intHelpLevel > $maxHelpLevel) {
							    $maxHelpLevel = $intHelpLevel;
                            }

                            if ($intFails == 0 && $intOmissions == 0) {
                                $somethingCorrect = true;
                            }
							
							if (intval($val['sequentialCorrects']) >= 2 && $firstCorrect == -1)
							{
								$firstCorrect = $intHelpLevel;
							}

							if ($intFails == 0 && $intOmissions == 0 && $firstCorrectOne == -1) {
							    $firstCorrectOne = $intHelpLevel;
                            }
							
							$c++;
						}
						
						array_push($maxPositionsC, $c+1);

                        $ret[_('PosicNivel0Ayuda').$newRep] = $intHelpLevel == 0 ? 1 : 0;

                        $ret[_('PosicPrimeraAyudaEnsayoCorrecto').$newRep] = $firstCorrectOne;
						
						$ret[_('PosicPrimeraAyudaEnsayoDosConsecutivas').$newRep] = $firstCorrect;
						
						//$lastHelpLevel = intval($vals[$last]['helpLevel']);
						//$last = count($vals)-1;
						
						if ($firstCorrect != -1)
						{
							if($lastHelpLevel == 0) {
                                $ret[_('PosicPG').$newRep] = 9;
                            }
                            else {
                                $ret[_('PosicPG').$newRep] = 9-$firstCorrect;
                            }
						}
						else
						{
							$ret[_('PosicPG').$newRep] = 0;
						}

                        $totalPG += intval($ret[_('PosicPG').$newRep]);

                        $ret[_('PosicNivAyuda').$newRep] = $maxHelpLevel;
                        $ret[_('PosicAyudasPresentadas').$newRep] = $helpsReceived;
                        $ret[_('PosicFinalizaSinAciertos').$newRep] = $somethingCorrect ? 0 : 1;
                        $ret[_('PosicTotalLaminas').$newRep] = $c;
					}

                    $ret[_('PosicNivAyudaFinal')] = $lastHelpLevel;
					$ret[_('PosicAyudasPresentadas')] = $helpsReceived;
					$ret[_('PosicFinalizaSinAciertos')] = $somethingCorrect ? 0 : 1;
                    $ret[_('PosicPGTotal')] = $totalPG;

					mysqli_free_result($result2);
				}
				
					break;
				case 4: //balloons
					$ret[_('GlobAc')] = $corrects;
					$ret[_('GlobErr')] = $fails;
					$ret[_('GlobOmi')] = $omissions;

                    $result2 = $link->query("SELECT * FROM exerciseLevel WHERE userID='$userID' and exerciseType='$exerciseType'");
                    if ($row2 = $result2->fetch_assoc()) {
                        $level = intval($row2['userLevel'])-1;
                        $subLevel = intval($row2['userSubLevel'])-1;
                        $ret[_('GlobNivelFinal')] = $subLevel+($level*3);
                    }
                    mysqli_free_result($result2);

					break;
				case 5: //object bag
					$ret[_('BolsaObjAc')] = $corrects;
					$ret[_('BolsaObjErr')] = $fails;
					$ret[_('BolsaObjOmi')] = $omissions;
					$ret[_('BolsaObjTiempo')] = $seconds;
					$ret[_('BolsaObjPunt')] = $score;

                    $result2 = $link->query("SELECT * FROM exerciseLevel WHERE userID='$userID' and exerciseType='$exerciseType'");
                    if ($row2 = $result2->fetch_assoc()) {
                        $ret[_('BolsaObjNivel')] = intval($row2['userLevel'])-1;
                    }
                    mysqli_free_result($result2);

					break;
				case 6: //pyramids
				{
					$result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition ASC");
					while ($row2 = $result2->fetch_assoc())
					{
						$vals = json_decode($row2['entry'], true);
						$rep = $row2['repetition'];
						
						$maxPyramids = max($maxPyramids, $rep);
						
						$ret[_('PiramAcPan').$rep] = $vals['corrects'];
						$ret[_('PiramErrPan').$rep] = $vals['failures'];
						$ret[_('PiramOmiPan').$rep] = $vals['omissions'];
						$ret[_('PiramUltElemMarcadoPan').$rep] = $vals['lastCheckedIndex'];
					}
					mysqli_free_result($result2);
					
					$ret[_('PiramTotAc')] = $corrects;
					$ret[_('PiramTotErr')] = $fails;
					$ret[_('PiramTotOmi')] = $omissions;
					break;
				}
				case 9: //unsorted objects
					$ret[_('ObjDesordTiempo')] = $seconds;
                    $result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition DESC");

                    $coinsFound = 0;
                    $totalCoins = 0;
                    $sortedObjects = 0;
                    $unsortedObjects = 0;
                    if ($row2 = $result2->fetch_assoc()) {
                        $vals = json_decode($row2['entry'], true);
                        $ret[_('ObjDesordNiv')] = intval($vals['sublevel'])+(3*intval($vals['level']));
                        $coinsFound += $vals['coinsFound'];
                        $totalCoins += $vals['totalCoins'];
                        $sortedObjects += $vals['sortedObjects'];
                        $unsortedObjects += $vals['totalUnsortedObjects'];
                    }

                    while ($row2 = $result2->fetch_assoc()) {
                        $vals = json_decode($row2['entry'], true);
                        $coinsFound += $vals['coinsFound'];
                        $totalCoins += $vals['totalCoins'];
                        $sortedObjects += $vals['sortedObjects'];
                        $unsortedObjects += $vals['totalUnsortedObjects'];
                    }

                    $ret[_('ObjDesordPorcMonedas')] = 100.0*floatval($coinsFound)/floatval($totalCoins);
                    $ret[_('ObjDesordPorcOrd')] = 100.0*floatval($sortedObjects)/floatval($unsortedObjects);

                    mysqli_free_result($result2);
					break;
				case 11: //semantic series
					$ret[_('SSemanAc')] = $corrects;
					$ret[_('SSemanErr')] = $fails;
                    $ret[_('SSemanTiempo')] = sessionTime($link, $userID, $sessionID, $exerciseID);
					break;
				case 12: //logical series
					$ret[_('SLogAc')] = $corrects;
					$ret[_('SLogErr')] = $fails;
                    $ret[_('SLogTiempo')] = sessionTime($link, $userID, $sessionID, $exerciseID);
					break;				
				case 13: //spatial reasoning
					$ret[_('RazEspacAc')] = $corrects;
					$ret[_('RazEspacErr')] = $fails;
                    $ret[_('RazEspacTiempo')] = sessionTime($link, $userID, $sessionID, $exerciseID);
					break;				
				case 14: //which one is different
					$ret[_('ClasifAc')] = $corrects;
					$ret[_('ClasifErr')] = $fails;
                    $ret[_('ClasifTiempo')] = sessionTime($link, $userID, $sessionID, $exerciseID);
					break;
				case 15: //semantic analogies
					$ret[_('AnaSemanAc')] = $corrects;
					$ret[_('AnaSemanErr')] = $fails;
                    $ret[_('AnaSemanTiempo')] = sessionTime($link, $userID, $sessionID, $exerciseID);
					break;
				case 16: //gift shopping
					$ret[_('CompraRegTiempo')] = $seconds;

                    $result2 = $link->query("SELECT repetition,entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType' ORDER BY repetition DESC");

                    $giftSimilarity = 0;
                    if ($row2 = $result2->fetch_assoc()) {
                        $vals = json_decode($row2['entry'], true);
                        $ret[_('CompraRegNiv')] = $vals['level'];
                        $giftSimilarity += floatval(substr($vals['similarity'], 0, -1));
                        $ret[_('CompraRegSimUltimoNivel')] = $giftSimilarity;
                    }

                    while ($row2 = $result2->fetch_assoc()) {
                        $vals = json_decode($row2['entry'], true);
                        $giftSimilarity += floatval(substr($vals['similarity'], 0, -1));
                    }

                    $ret[_('CompraRegSimMedia')] = $giftSimilarity / floatval($result2->num_rows);

					break;
				case 17: //package delivery
				{
					$result2 = $link->query("SELECT entry FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID' AND exerciseType='$exerciseType'");
					if ($row2 = $result2->fetch_assoc())
					{
						$vals = json_decode($row2['entry'], true);
						$ret[_('PaquetesTiempoIni')] = $vals['time'];
						$ret[_('PaquetesMovFurg')] = $vals['vanMovements'];
						$ret[_('PaquetesMovPaq')] = $vals['packageMovements'];
					}
					
					$ret[_('PaquetesTiempo')] = $seconds;
					$ret[_('PaquetesAc')] = $corrects;
					$ret[_('PaquetesErrRecog')] = $fails;
					$ret[_('PaquetesErrEntrg')] = $omissions;
					
					mysqli_free_result($result2);
					
					break;
				}
				case 20: //VR pre
					break;
				case 21: //VR post
					break;
				case 30: //first questionnaire
					break;
					
				case 32: //mouse
					$ret[_('PunteriaAc')] = $corrects;
					$ret[_('PunteriaErr')] = $fails;
					$ret[_('PunteriaTiempo')] = $seconds;
					break;
					
				case 33: //second questionnaire
					break;
				case 34: //introduction
					break;
					
				case 101: //first help
					break;
			
				case 102: //second help
					break;
			}
		}
		
		mysqli_free_result($result);
		
		$result = $link->query("SELECT name,surname,dateOfBirth FROM user,userProfile WHERE userProfile.userID=user.id AND user.id='$userID'");
		if ($row = mysqli_fetch_array($result))
		{
			$date = split('-', $row['dateOfBirth']);
			$sSurname = split(' ', $row['surname']);
			$cod = '';
			if (count($sSurname) >= 2)
			{
				$cod = substr($row['surname'], 0, 2).substr($sSurname[1], 0, 2);
			}
			else
			{
				$cod = substr($row['surname'], 0, 2);
			}
			
			if (count($cod) == 0 || $cod == null){
				return mb_strtoupper($row['name'], 'UTF-8');
			}
			
			if (count($date) >= 3)
			{
				$d = $date[2];
				$m = $date[1];
				$cod = $cod.$d.$m;
			}
			$ret[_('Cod')] = mb_strtoupper($cod, 'UTF-8');
		}
		
		mysqli_free_result($result);
		
		return $ret;
	}

	function sessionTime($link, $userID, $sessionID, $exerciseID) {
	    $result = $link->query("SELECT * FROM sessionlog where sessionID='$sessionID' AND userID='$userID' AND exerciseID='$exerciseID' AND result='1'");
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $start = strtotime($row['startTime']);
            $end = strtotime($row['endTime']);
            if ($end-$start > 0) return $end-$start;
            else return 0;
        }

	    mysqli_free_result($result);

        return 0;
    }
	
	function all_sheet_keys()
	{
		global $maxTaskListRep;
		global $maxTaskListDemRep;
		global $maxPositionsRep;
		global $maxPyramids;
		global $maxPositionsC;
		
		$res = array('id', _('evaluador'), _('Cod'),  _('PunteriaAc'), _('PunteriaErr'), _('PunteriaTiempo'), _('SLogAc'), _('SLogErr'), _('SLogTiempo'), _('SSemanAc'), _('SSemanErr'), _('SSemanTiempo'));
				
		for ($i=0; $i<4; $i++)
		{
			$c = 8; //$maxPositionsC[$i];
			$rep = $i+1;
			
			for ($k=1; $k<=$c; $k++)
			{
				array_push($res, _('PosicNivAyudaLamina').$rep.'-'.$k, _('PosicAcLamina').$rep.'-'.$k, /*_('PosicErrLamina').$rep.'-'.$k, _('PosicOmiLamina').$rep.'-'.$k,*/ _('PosicTiempoLamina').$rep.'-'.$k/*, _('PosicLaminaRes').$rep.'-'.$k*/);
			}

			array_push($res, _('PosicPG').$rep, _('PosicNivel0Ayuda').$rep, _('PosicPrimeraAyudaEnsayoCorrecto').$rep, _('PosicPrimeraAyudaEnsayoDosConsecutivas').$rep, _('PosicNivAyuda').$rep, /*_('PosicAyudasPresentadas').$rep,*/ _('PosicFinalizaSinAciertos').$rep, _('PosicTotalLaminas').$rep);
		}

		array_push($res, _('PosicPGTotal'), _('PosicTotalLaminas'), _('PosicNivAyudaFinal'), _('PosicAyudasPresentadas'), _('PosicFinalizaSinAciertos'));
		
		array_push($res, _('ListPalAcTotal'), _('ListPalErrTotal'), _('ListPalOmiTotal'));
		for ($i = 0; $i<4; $i++)
		{
			$key = null;
			if ($i < 3)
			{
				$key = _('ListPalA').($i+1);
			}
			else
			{
				$key = _('ListPalB');
			}
			
			array_push($res, $key, $key._('Mostradas'), $key._('Ac'), $key._('Err'), $key._('Omi'), $key._('Tiempo'));
		}
		
		array_push($res, _('ListPalDemAcTotal'), _('ListPalDemErrTotal'), _('ListPalDemOmiTotal'));
		for ($i=0; $i<2; $i++)
		{
			$key = null;
			if ($i == 0)
			{
				$key = _('ListPalDemEscr');
			}
			else
			{
				$key = _('ListPalDemRecon');
			}
			
			array_push($res, $key, $key._('Mostradas'), $key._('Ac'), $key._('Err'), $key._('Omi'), $key._('Tiempo'));
		}
		
		array_push($res, _('DigAc'), _('DigErr'), _('NyVAc'), _('NyVErr'), _('NyVTiempo'));
		
		array_push($res, _('PiramTotAc'), _('PiramTotErr'), _('PiramTotOmi'));
		
		for ($i=1; $i<=$maxPyramids; $i++)
		{
			array_push($res, _('PiramAcPan').$i, _('PiramErrPan').$i, _('PiramOmiPan').$i, _('PiramUltElemMarcadoPan').$i);
		}
		
		array_push($res, _('PaquetesTiempoIni'), _('PaquetesMovFurg'), _('PaquetesMovPaq'), _('PaquetesTiempo'), _('PaquetesAc'), _('PaquetesErrRecog'), _('PaquetesErrEntrg'));

		array_push($res, _('MemYClasifObjAc'), _('MemYClasifObjErr'), _('MemYClasifObjOmi'), _('MemYClasifObjNiv'), _('GlobAc'), _('GlobErr'), _('GlobOmi'), _('GlobNivelFinal'), _('BolsaObjAc'), _('BolsaObjErr'), _('BolsaObjOmi'), _('BolsaObjTiempo'), _('BolsaObjPunt'), _('BolsaObjNivel'), /*_('ObjDesordPunt'),*/ _('ObjDesordPorcMonedas'), _('ObjDesordPorcOrd'), _('ObjDesordTiempo'), _('ObjDesordNiv'), _('RazEspacAc'), _('RazEspacErr'), _('RazEspacTiempo'), _('ClasifAc'), _('ClasifErr'), _('ClasifTiempo'), _('AnaSemanAc'), _('AnaSemanErr'), _('AnaSemanTiempo'), _('CompraRegNiv'), _('CompraRegSimMedia'), _('CompraRegSimUltimoNivel'), _('CompraRegTiempo'));
		
		array_push($res, _('ListTareasAcTotal'), _('ListTareasErrTotal'), _('ListTareasOmiTotal'));
		for ($i = 0; $i<$maxTaskListRep; $i++)
		{
			$key = _('ListTareas').($i+1);

			array_push($res, $key._('Recon'), $key._('Mostradas'), $key._('Ac'), $key._('Err'), $key._('Omi'), $key._('Tiempo'), $key._('NMostradas'));
		}
		
		array_push($res, _('ListTareasDemAcTotal'), _('ListTareasDemErrTotal'), _('ListTareasDemOmiTotal'));
		for ($i = 0; $i<$maxTaskListDemRep; $i++)
		{
			$key = _('ListTareasDem').($i+1);
			
			array_push($res, $key._('Recon'), $key._('Mostradas'), $key._('Ac'), $key._('Err'), $key._('Omi'), $key._('Tiempo'));
		}
		
		return $res;
	}
	
	$keyPositions = array();
	function migrate_values_to_sheet(&$sheet, $values, &$count)
	{
		global $keyPositions;
		$keys = all_sheet_keys();
		
		$sheet_name = $sheet->getTitle();
		
		if ($count == 1)
		{
			$keyPositions[$sheet_name] = array();
			$c = 0;
			foreach ($keys as $key)
			{
				$keyPositions[$sheet_name][$key] = $c;
				$sheet->setCellValueByColumnAndRow($c, $count, $key);
				$sheet->getStyleByColumnAndRow($c, $count)->getFont()->setBold(true);
				
				$c++;
			}
			
			$count++;
		}
		
		foreach ($keys as $key)
		{
			if (array_key_exists($key, $values))
			{
				$value = $values[$key];
				$column = $keyPositions[$sheet_name][$key];
				$sheet->setCellValueByColumnAndRow($column, $count, $value);
			}
		}
	}
	
	function all_questionnaire_keys()
	{
		return array('id', _('UsoOrden'), _('LugNac'), _('FechNac'), _('Edad'), _('Tlfno'), _('Sexo'), _('EstCivil'), _('ConviveCon'), _('NivEstudios'), _('HorLectura'), _('HorTalleres'), _('HorEjerc'), _('HorOrdenador'), _('NecAyudaComer'), _('NecAyudaBañarse'), _('NecAyudaVestir'), _('NecAyudaArregl'), _('IncontOrinar'), _('IncontDefec'), _('NecAyudaBaño'), _('NecAyudaIrCama'), _('NecAyudaAndar'), _('NecAyudaEscal'), _('NecAyudaTlfno'), _('NecAyudaCompra'), _('NecAyudaCocina'), _('NecAyudaCasa'), _('NecAyudaLimpRopa'), _('NecAyudaTransp'), _('NecAyudaMedic'), _('NecAyudaAsuntEcono'));	
	}
	
	$questPositions = array();
	function fill_questionnaire_value($userID, &$sheet, $link, $count)
	{
		global $questPositions;
		
		$result = $link->query("SELECT * FROM userProfile WHERE userID='$userID'");
		$res = array();
		$res['id'] = $userID;
		if ($row = $result->fetch_assoc())
		{
			$res[_('UsoOrden')] = $row['computerUse'];
			$res[_('LugNac')] = $row['placeOfBirth'];
			
			$phpdate = strtotime($row['dateOfBirth']);
			$res[_('FechNac')] = date('d-m-Y', $phpdate); //PHPExcel_Shared_Date::PHPToExcel($phpdate);
			$res[_('Edad')] = date_diff(date_create(date('Y-m-d', $phpdate)), date_create(date('Y-m-d')))->y;
			$res[_('Tlfno')] = $row['phoneNumber'];
			$res[_('Sexo')] = $row['sex'];
			$res[_('EstCivil')] = $row['civilStatus'];
			$res[_('ConviveCon')] = $row['partner'];
			$res[_('NivEstudios')] = $row['levelOfStudies'];
			$res[_('HorLectura')] = $row['hoursReading'];
			$res[_('HorTalleres')] = $row['hoursWorkshop'];
			$res[_('HorEjerc')] = $row['hoursExercise'];
			$res[_('HorOrdenador')] = $row['hoursComputer'];
			$res[_('NecAyudaComer')] = $row['eatingHelp'];
			$res[_('NecAyudaBañarse')] = $row['washingHelp'];
			$res[_('NecAyudaVestir')] = $row['clothingHelp'];
			$res[_('NecAyudaArregl')] = $row['smartenUPHelp'];
			$res[_('IncontOrinar')] = $row['urinateHelp'];
			$res[_('IncontDefec')] = $row['defecateHelp'];
			$res[_('NecAyudaBaño')] = $row['toiletHelp'];
			$res[_('NecAyudaIrCama')] = $row['movingHelp'];
			$res[_('NecAyudaAndar')] = $row['walkingHelp'];
			$res[_('NecAyudaEscal')] = $row['stepsHelp'];
			$res[_('NecAyudaTlfno')] = $row['phoneHelp'];
			$res[_('NecAyudaCompra')] = $row['shoppingHelp'];
			$res[_('NecAyudaCocina')] = $row['cookingHelp'];
			$res[_('NecAyudaCasa')] = $row['homecareHelp'];
			$res[_('NecAyudaLimpRopa')] = $row['clothCleaningHelp'];
			$res[_('NecAyudaTransp')] = $row['transportHelp'];
			$res[_('NecAyudaMedic')] = $row['medicationHelp'];
			$res[_('NecAyudaAsuntEcono')] = $row['economyHelp'];
		}
		
		mysqli_free_result($result);
		
		$keys = all_questionnaire_keys();
		
		$row = $count;
		if ($row == 1)
		{
			$c = 0;
			foreach ($keys as $key)
			{
				$questPositions[$key] = $c;
				$sheet->setCellValueByColumnAndRow($c, $row, $key);
				$sheet->getStyleByColumnAndRow($c, $row)->getFont()->setBold(true);
				
				$c++;
			}
			
			$row++;
		}
		
		//$c = 0;
		foreach ($keys as $key)
		{
			if (array_key_exists($key, $res))
			{
				$value = $res[$key];
				$column = $questPositions[$key];
				$sheet->setCellValueByColumnAndRow($column, $row, $value);
				
				/*if ($key == _('FechaNac'))
				{
					$sheet->getStyleByColumnAndRow($c, $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
				}*/
			}
			
			//$c++;
		}
	}
	
	function export_all_results($userID, &$objPHPExcel, $link, $row=1)
	{
        $array_pre = extract_data_from_userID($userID, $link, true);
		$sheet = $objPHPExcel->getSheet(0);

		$r = $row;
        migrate_values_to_sheet($sheet, $array_pre, $r);
		
		$array_post = extract_data_from_userID($userID, $link, false);
		$sheet = $objPHPExcel->getSheet(1);
		
		$r = $row;
		migrate_values_to_sheet($sheet, $array_post, $r);

		$sheet = $objPHPExcel->getSheet(2);
		fill_questionnaire_value($userID, $sheet, $link, $row);

		for ($i=0; $i<=8; $i++) {
            $array = extract_data_from_userID($userID, $link, false, $i+3);
            $sheet = $objPHPExcel->getSheet($i+5);
            $r = $row;
            migrate_values_to_sheet($sheet, $array, $r);
        }
		
		return $r;
	}
	
	function create_help(&$sheet)
	{
		$sheet->setCellValue('A1', _('Valores:'));
		for ($i=0; $i<9; $i++)
		{
			$sheet->setCellValueByColumnAndRow($i+1, 1, $i);
		}
		
		$sheet->getStyle('A1:J1')->getFont()->setBold(true);
		
		$sheet->setCellValue('A2', _('Sabe Usar Orden.:'));
		$sheet->setCellValue('B2', _('No'));
		$sheet->setCellValue('C2', _('Sí'));
		
		$sheet->setCellValue('A3', _('Sexo:'));
		$sheet->setCellValue('B3', '-');
		$sheet->setCellValue('C3', _('Femenino'));
		$sheet->setCellValue('D3', _('Masculino'));
		
		$sheet->setCellValue('A4', _('Estado civil:'));
		$sheet->setCellValue('B4', '-');
		$sheet->setCellValue('C4', _('Soltero/a'));
		$sheet->setCellValue('D4', _('Casado/a'));
		$sheet->setCellValue('E4', _('Viudo/a'));
		$sheet->setCellValue('F4', _('Divorciado/a'));
		$sheet->setCellValue('G4', _('Otro/a'));
		
		$sheet->setCellValue('A5', _('Convive con:'));
		$sheet->setCellValue('B5', '-');
		$sheet->setCellValue('C5', _('Esposo/a o pareja'));
		$sheet->setCellValue('D5', _('Solo/a'));
		$sheet->setCellValue('E5', _('Vive en una residencia'));
		$sheet->setCellValue('F5', _('Hijo/a'));
		$sheet->setCellValue('G5', _('Otros familiares'));
		$sheet->setCellValue('H5', _('Otros'));
		
		$sheet->setCellValue('A6', _('Nivel Estudios:'));
		$sheet->setCellValue('B6', '-');
		$sheet->setCellValue('C6', _('Leer/escribir'));
		$sheet->setCellValue('D6', _('Estudios primarios'));
		$sheet->setCellValue('E6', _('Graduado escolar/EGB'));
		$sheet->setCellValue('F6', _('Bachillerato/COU'));
		$sheet->setCellValue('G6', _('Formación profesional'));
		$sheet->setCellValue('H6', _('Diplomatura'));
		$sheet->setCellValue('I6', _('Licenciatura'));
		$sheet->setCellValue('J6', _('Doctorado'));
		
		$sheet->setCellValue('A7', _('Ayuda bañarse:'));
		$sheet->setCellValue('B7', '-');
		$sheet->setCellValue('C7', _('No'));
		$sheet->setCellValue('D7', _('Sí'));
		
		$sheet->setCellValue('A8', _('Ayuda vestirse:'));
		$sheet->setCellValue('B8', '-');
		$sheet->setCellValue('C8', _('No necesita'));
		$sheet->setCellValue('D8', _('Necesita poca'));
		$sheet->setCellValue('E8', _('Necesita mucha'));
		
		$sheet->setCellValue('A9', _('Ayuda arreglarse:'));
		$sheet->setCellValue('B9', '-');
		$sheet->setCellValue('C9', _('No'));
		$sheet->setCellValue('D9', _('Sí'));
		
		$sheet->setCellValue('A10', _('Ayuda comer:'));
		$sheet->setCellValue('B10', '-');
		$sheet->setCellValue('C10', _('No necesita'));
		$sheet->setCellValue('D10', _('Necesita poca'));
		$sheet->setCellValue('E10', _('Necesita mucha'));
		
		$sheet->setCellValue('A11', _('Incont. Orinar:'));
		$sheet->setCellValue('B11', '-');
		$sheet->setCellValue('C11', _('No'));
		$sheet->setCellValue('D11', _('Ocasionalmente'));
		$sheet->setCellValue('E11', _('Incontinente'));
		
		$sheet->setCellValue('A12', _('Incont. Defecar:'));
		$sheet->setCellValue('B12', '-');
		$sheet->setCellValue('C12', _('No'));
		$sheet->setCellValue('D12', _('Ocasionalmente'));
		$sheet->setCellValue('E12', _('Incontinente'));
		
		$sheet->setCellValue('A13', _('Ayuda ir aseo:'));
		$sheet->setCellValue('B13', '-');
		$sheet->setCellValue('C13', _('No necesita'));
		$sheet->setCellValue('D13', _('Necesita poca'));
		$sheet->setCellValue('E13', _('Necesita mucha'));
		
		$sheet->setCellValue('A14', _('Ayuda ir aseo:'));
		$sheet->setCellValue('B14', '-');
		$sheet->setCellValue('C14', _('No necesita'));
		$sheet->setCellValue('D14', _('Necesita poca'));
		$sheet->setCellValue('E14', _('Necesita mucha'));
		
		$sheet->setCellValue('A15', _('Ayuda ir cama:'));
		$sheet->setCellValue('B15', '-');
		$sheet->setCellValue('C15', _('No necesita'));
		$sheet->setCellValue('D15', _('Necesita poca'));
		$sheet->setCellValue('E15', _('Necesita mucha'));
		$sheet->setCellValue('F15', _('Grúa o personas'));
		
		$sheet->setCellValue('A16', _('Ayuda andar:'));
		$sheet->setCellValue('B16', '-');
		$sheet->setCellValue('C16', _('50m sin ayuda'));
		$sheet->setCellValue('D16', _('50m con poca ayuda'));
		$sheet->setCellValue('E16', _('Independ. Silla Ruedas'));
		$sheet->setCellValue('F16', _('Incapaz'));
	    
		$sheet->setCellValue('A17', _('Ayuda escaleras:'));
		$sheet->setCellValue('B17', '-');
		$sheet->setCellValue('C17', _('No necesita'));
		$sheet->setCellValue('D17', _('Necesita poca'));
		$sheet->setCellValue('E17', _('Incapaz'));
	    
	    $sheet->setCellValue('A18', _('Ayuda tlfno:'));
		$sheet->setCellValue('B18', '-');
		$sheet->setCellValue('C18', _('No necesita'));
		$sheet->setCellValue('D18', _('Bien algunos nums.'));
		$sheet->setCellValue('E18', _('Solo contestar'));
		$sheet->setCellValue('F18', _('Incapaz'));
		
		$sheet->setCellValue('A19', _('Ayuda compras:'));
		$sheet->setCellValue('B19', '-');
		$sheet->setCellValue('C19', _('Independiente'));
		$sheet->setCellValue('D19', _('Solo peq. indep.'));
		$sheet->setCellValue('E19', _('Acompañado/a'));
		$sheet->setCellValue('F19', _('Incapaz'));
		
		$sheet->setCellValue('A20', _('Ayuda comidas:'));
		$sheet->setCellValue('B20', '-');
		$sheet->setCellValue('C20', _('No nece. ayuda'));
		$sheet->setCellValue('D20', _('Nece. ingredientes'));
		$sheet->setCellValue('E20', _('No sigue dieta'));
		$sheet->setCellValue('F20', _('Nece. ayuda completa'));
	    
	    $sheet->setCellValue('A21', _('Ayuda casa:'));
		$sheet->setCellValue('B21', '-');
		$sheet->setCellValue('C21', _('Indep. o con ayuda ocasional'));
		$sheet->setCellValue('D21', _('Solo tareas ligeras'));
		$sheet->setCellValue('E21', _('Tar. lig., pero no limpieza'));
		$sheet->setCellValue('F21', _('Nece. ayuda completa'));
		$sheet->setCellValue('G21', _('No hace labores'));
		
		$sheet->setCellValue('A22', _('Ayuda lavar ropa:'));
		$sheet->setCellValue('B22', '-');
		$sheet->setCellValue('C22', _('No necesita'));
		$sheet->setCellValue('D22', _('Solo prendas peq.'));
		$sheet->setCellValue('E22', _('Incapaz'));
		
		$sheet->setCellValue('A23', _('Ayuda transp.:'));
		$sheet->setCellValue('B23', '-');
		$sheet->setCellValue('C23', _('No nece. ayuda'));
		$sheet->setCellValue('D23', _('Transp. publico acompañado'));
		$sheet->setCellValue('E23', _('Taxi/auto. con ayuda'));
		$sheet->setCellValue('F23', _('No puede viajar'));
		
		$sheet->setCellValue('A24', _('Ayuda medicamentos:'));
		$sheet->setCellValue('B24', '-');
		$sheet->setCellValue('C24', _('No necesita'));
		$sheet->setCellValue('D24', _('Necesita ayuda'));
		$sheet->setCellValue('E24', _('No puede solo/a'));
		
		$sheet->setCellValue('A25', _('Ayuda asunt. econ.:'));
		$sheet->setCellValue('B25', '-');
		$sheet->setCellValue('C25', _('No necesita'));
		$sheet->setCellValue('D25', _('Necesita ayuda'));
		$sheet->setCellValue('E25', _('No puede solo/a'));
	}
	
	function create_usability_results($patientID, &$sheet, $link, $count){
		$allKeys = array();
		for ($i = 1; $i<=32; $i++){
			array_push($allKeys, "qt$i");
		}
		
		$result = $link->query("SELECT result,fdate FROM virtrael_usability WHERE userID='$patientID' LIMIT 1");
		
		if ($row = $result->fetch_assoc())
		{
			$contents = json_decode($row['result'], true);
			
			if ($count == 2) {
				$sheet->setCellValueByColumnAndRow(0, 1, _('UserID'));
				$sheet->getStyleByColumnAndRow(0, 1)->getFont()->setBold(true);
				$column = 1;
				foreach ($allKeys as $key) {
					$sheet->setCellValueByColumnAndRow($column, 1, $key);
					$sheet->getStyleByColumnAndRow($column, 1)->getFont()->setBold(true);
					$column++;
				}
				$sheet->setCellValueByColumnAndRow($column, 1, _('Fecha'));
				$sheet->getStyleByColumnAndRow($column, 1)->getFont()->setBold(true);
			}
			
			$column = 1;
			$sheet->setCellValueByColumnAndRow(0, $count, $patientID);
			foreach ($allKeys as $key) {
				$sheet->setCellValueByColumnAndRow($column, $count, $contents[$key]);
				$column++;
			}
			$sheet->setCellValueByColumnAndRow($column, $count, $row['fdate']);
		}
		mysqli_free_result($result);
	}
	
	if (isset($_SESSION['userID']))
	{
		$user = new User($_SESSION['userID']);
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$fullname = $user->get_name().' '.$user->get_surname();
		
		// Add some data
		/*$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'Hello')
		            ->setCellValue('B2', 'world!')
		            ->setCellValue('C1', 'Hello')
		            ->setCellValue('D2', 'world!');
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);*/
		$objPHPExcel->getProperties()->setCreator($fullname)
							 ->setLastModifiedBy($fullname)
							 ->setTitle(_('Datos extraídos de VIRTRA-EL'))
							 ->setSubject(_('Datos extraídos de VIRTRA-EL'))
							 ->setDescription(_('Datos sobre los resultados obtenidos por los pacientes que hacen uso de VIRTRA-EL'))
							 ->setKeywords(_('VIRTRA-EL datos pacientes'))
							 ->setCategory(_('Datos VIRTRA-EL'));
		
		$objPHPExcel->createSheet(1)->setTitle(_('Resultados Post'));
		$objPHPExcel->createSheet(2)->setTitle(_('Cuestionarios'));
		$objPHPExcel->createSheet(3)->setTitle(_('AyudaCuestionarios'));
		$objPHPExcel->createSheet(4)->setTitle(_('Usabilidad'));

		for ($i = 0; $i<=8; $i++) {
            $objPHPExcel->createSheet($i+5)->setTitle(_('Sesión').($i+3));
        }

		$objPHPExcel->setActiveSheetIndex(0)->setTitle(_('Resultados Pre'));
		
		$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
		$link->set_charset('utf8');
		
		$userID = $user->get_id();
		
		if ($user->get_therapist() || $user->get_carer())
		{
			//patients' results
			$result = null;
			if ($user->get_therapist())
			{
				$result = $link->query("SELECT patient_id FROM therapist_patient WHERE therapist_id='$userID'");
			}
			else
			{
				$result = $link->query("SELECT patient_id FROM carer_patient WHERE carer_id='$userID'");
			}
			
			$count = 1;
			while ($row = $result->fetch_assoc())
			{
				$patientID = $row['patient_id'];
				$count = export_all_results($patientID, $objPHPExcel, $link, $count);
                $sheet = $objPHPExcel->getSheet(0);
                $sheet->setCellValueByColumnAndRow(0, $count, $patientID);

				$otherSheet = $objPHPExcel->getSheet(4);
				create_usability_results($patientID, $otherSheet, $link, $count);
				
				$count++;
			}

			if ($result !== null) {
                mysqli_free_result($result);
            }
		}
		else
		{
			//only your own results
			export_all_results($userID, $objPHPExcel, $link);
		}
		
		$link->close();
		
		create_help($objPHPExcel->getSheet(3));
		
		$sheet = $objPHPExcel->setActiveSheetIndex(0);
							 
		// Redirect output to a client’s web browser (Excel2007)
		$filename = 'virtrael-'.date('d_m_Y_H_i_s').'.xlsx';
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
?>