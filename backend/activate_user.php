<?php
	require_once(__DIR__.'/../php/User.php');

	session_start();
			
	if (isset($_SESSION['userID']) && isset($_POST['user']))
	{
		if ($_POST['user']==$_SESSION['userID'])
		{
			$creator = new User($_SESSION['userID']);
			
			$activated = ($_POST['activated']=='1')?true:false;
			$id = $_POST['actID'];
			
			$user = new User($id);
			
			if ( $creator->get_superuser() || (!$user->get_therapist() && $creator->get_therapist()) )
			{
				$user->set_activated($activated);
				$user->save();
				echo 'true';
			}
			else echo 'false';
		}
		else echo 'false';
	}
	else echo 'false';
?>