<?php
	include_once(__DIR__.'/config.php');
	include_once(__DIR__.'/../php/User.php');
	
	session_start();
	
	function search($user_id, $text, $lang, $th, $link, $carer)
	{
		$table_name = 'therapist_patient';
		$entry_id = 'therapist_id';
		if ($carer)
		{
			$table_name = 'carer_patient';
			$entry_id = 'carer_id';
		}
		
		$result = $link->query("SELECT `user`.id AS id, `user`.name AS name, `user`.surname AS surname, `user`.email AS email, `user`.status AS status FROM `user`, $table_name WHERE (`user`.id=$table_name.patient_id) AND ($table_name.$entry_id='$user_id') AND (name LIKE '%$text%' OR surname LIKE '%$text%' OR email LIKE '%$text%')");
		$ret = array();
		while ($row = $result->fetch_assoc())
		{
			array_push($ret, $row);
		}
		
		mysqli_free_result($result);
		
		return json_encode($ret);
	}
	
	function number_of_pages($user_id, $text, $max_page_elements, $th, $link, $carer)
	{
		$table_name = 'therapist_patient';
		$entry_id = 'therapist_id';
		if ($carer)
		{
			$table_name = 'carer_patient';
			$entry_id = 'carer_id';
		}
		
		$result = $link->query("SELECT count(`user`.id) AS count FROM `user`, $table_name WHERE (`user`.id=$table_name.patient_id) AND ($table_name.$entry_id='$user_id') AND (name LIKE '%$text%' OR surname LIKE '%$text%' OR email LIKE '%$text%')");
	
		if ($row = $result->fetch_assoc())
		{
			return ceil(intval($row['count'])/$max_page_elements);
		}
		else return 0;
	}
	
	function paginated_search($user_id, $text, $lang, $max_page_elements, $page, $th, $link, $carer)
	{
		$table_name = 'therapist_patient';
		$entry_id = 'therapist_id';
		if ($carer)
		{
			$table_name = 'carer_patient';
			$entry_id = 'carer_id';
		}
		
		if ($max_page_elements > 0)
		{
			$low_limit = $page*$max_page_elements;
			$result = $link->query("SELECT `user`.id AS id, `user`.name AS name, `user`.surname AS surname, `user`.email AS email, `user`.status AS status FROM `user`, $table_name WHERE (`user`.id=$table_name.patient_id) AND ($table_name.$entry_id='$user_id') AND (name LIKE '%$text%' OR surname LIKE '%$text%' OR email LIKE '%$text%') LIMIT $low_limit,$max_page_elements");
			$ret['elements'] = array();
			while ($row = $result->fetch_assoc())
			{
				array_push($ret['elements'], $row);
			}
			
			mysqli_free_result($result);
			
			$ret['npages'] = number_of_pages($user_id, $text, $max_page_elements, $th, $link, $carer);
			return json_encode($ret);
		}
		else return search($user_id, $text, $lang, $th, $link, $carer);
	}
	
	if (isset($_SESSION['userID']))
	{
		$user = new User($_SESSION['userID']);
		if ($user->get_therapist() || $user->get_carer())
		{
			$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
			mysqli_set_charset($link, 'utf8');
			
			$text = $_GET['q'];
			$limit = $_GET['l'];
			$page = $_GET['p'];
			$th = isset($_GET['th'])?$_GET['th']:'0';
			
			echo paginated_search($user->get_id(), $text, 'en', $limit, $page, $th, $link, $user->get_carer());
			
			$link->close();
		}
	}
?>