<?php
	include_once('config.php');

	session_start();
	
	if (isset($_SESSION['userID']))
	{
		$userID = isset($_POST['userID'])?$_POST['userID']:$_SESSION['userID'];
		$sessionID = $_POST['sessionID'];
		$exerciseID = $_POST['exerciseID'];
		$resultExercise = $_POST['result'];
		$repetitions = $_POST['repetitions'];
		
		$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
		
		$result = $link->query("SELECT * FROM sessionlog WHERE userID='$userID' and sessionID='$sessionID' and exerciseID='$exerciseID'");
		$num_row = mysqli_num_rows($result);
		if ($num_row >=1)
		{
			$row=$result->fetch_assoc();
			$rowID = $row['id'];
			
			$link->query("UPDATE sessionlog SET endTime=NOW(), result='$resultExercise', repetitions='$repetitions' WHERE id='$rowID'");
		}
		else
		{
			$link->query("INSERT INTO sessionlog (`id`, `sessionID`, `exerciseID`, `userID`, `startTime`, `endTime`, `repetitions`, `result`) VALUES(NULL, '$sessionID', '$exerciseID', '$userID', NOW(), NOW(), '$repetitions', '$resultExercise')");
		}
		
	    mysqli_free_result($result);
	    $link->close();
	}
?>
