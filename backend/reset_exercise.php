<?php
	include_once('config.php');
	require_once(__DIR__.'/../php/User.php');

	session_start();
	
	if (isset($_SESSION['userID']) && isset($_POST['patientID']) && isset($_POST['exerciseID']) && isset($_POST['sessionID']))
	{
		$th = new User($_SESSION['userID']);
		if ($th->get_therapist())
		{
			$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
			$userID = $_POST['patientID'];
			$exerciseID = $_POST['exerciseID'];
			$sessionID = $_POST['sessionID'];
		
			$link->query("UPDATE exerciseResult SET countCorrects=0, countFails=0, countOmissions=0, finalScore=0, seconds=0 WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID'");
			$link->query("DELETE FROM partialResult WHERE userID='$userID' AND sessionID='$sessionID' AND exerciseID='$exerciseID'");
			
			$link->close();
			
			echo 'ok';
		}
		else echo 'fail';
	}
	else echo 'fail2';
?>