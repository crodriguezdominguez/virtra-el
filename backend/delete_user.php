<?php
	require_once(__DIR__.'/../php/User.php');

	session_start();
			
	if (isset($_SESSION['userID']) && isset($_POST['user']))
	{
		if ($_POST['user']==$_SESSION['userID'])
		{
			$creator = new User($_SESSION['userID']);
			$id = $_POST['deleteID'];
			$user = new User($id);
			
			if ( $creator->get_superuser() || (!$user->get_therapist() && $creator->get_therapist()) )
			{
				User::remove_user($user);
				echo 'true';
			}
			else echo 'false';
		}
		else echo 'false';
	}
	else echo 'false';
?>