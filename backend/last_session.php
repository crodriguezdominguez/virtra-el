<?php
	include_once('config.php');

	session_start();
	
	if (!isset($_SESSION['userID']))
	{
		echo 'false';
	}
	else
	{
		$userID = isset($_GET['userID'])?$_GET['userID']:$_SESSION['userID'];
		$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
		
		$result = $link->query("SELECT * FROM sessionlog WHERE userID='$userID' and result!='1' ORDER BY endTime DESC");
		$num_row = mysqli_num_rows($result);
		$row=$result->fetch_array();
		
		echo json_encode($row);
		
	    mysqli_free_result($result);
	    $link->close();
	}
?>
