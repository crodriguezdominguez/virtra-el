<?php
	require_once(__DIR__.'/../../locale/localization.php');
	require_once(__DIR__.'/../../php/User.php');
	
	session_start();
	
	if (!isset($_SESSION['userID'])) exit(0);
	
	$user = new User($_SESSION['userID']);
	$therapist = $user->get_therapist_user();
?>

<div class="page-header">
<?php
	if ($therapist != null)
	{
?>
  <p class="lead"><?php echo _('Su terapeuta actual es').' <strong>'.$therapist->get_name().' '.$therapist->get_surname().'</strong>. Si tiene alguna duda, por favor, contacte con &eacute;l/ella a trav&eacute;s del siguiente formulario.'; ?></p>
<?php
	}
	else //therapist not assigned
	{
?>
<p class="lead"><strong><?php echo _('A&uacute;n no tiene un terapeuta asignado.'); ?></strong></p>
<p><?php echo _('Si sabe el e-mail de su terapeuta, y éste ya se encuentra registrado en VIRTRA-EL, puede introducirlo a continuación para solicitarle ser su paciente.'); ?></p>
<form action="javascript:checkForm2();" class="form-horizontal" id="contact-form">
	<fieldset>
	<div class="control-group" id="mail-group-known">
		<label class="control-label" for="mail-known"><?php echo _('Escriba el e-mail del terapeuta'); ?></label>
		<div class="controls">
			<input type="hidden" name="user-known" id="user-known" value="<?php echo $user->get_username(); ?>" />
			<input type="hidden" name="user-name-known" id="user-name-known" value="<?php echo $user->get_name(); ?>" />
			<input type="hidden" name="user-surname-known" id="user-surname-known" value="<?php echo $user->get_surname(); ?>" />
			<input type="text" class="input-xlarge" id="mail-known" name="mail-known" value="" placeholder="<?php echo _('E-Mail'); ?>">
		</div>
    </div>
    <div class="control-group" id="captcha2-group">
		<label class="control-label" id="captcha2-label" for="captcha2"></label>
		<div class="controls">
			<input type="text" class="input-mini" maxlength="2" id="captcha2" name="captcha2">
			<span class="help-inline"><?php echo _('Esto evita que se haga un mal uso del formulario de solicitud.'); ?></span>
		</div>
    </div>
    <div class="control-group">
	    <div class="controls">
			<button type="submit" class="btn btn-primary"><?php echo _('Solicitar'); ?></button>
		</div>
	</div>
</form>
<p id="result-form-known"></p>
<hr />
<p><?php echo _('De manera alternativa, puede contactar con nosotros a trav&eacute;s del siguiente formulario para que le asignemos uno.'); ?></p>
<?php
	}
?>
</div>
<form action="javascript:checkForm();" class="form-horizontal" id="contact-form">
	<fieldset>
		<input type="hidden" id="name" name="name" value="<?php echo $user->get_surname().', '.$user->get_name(); ?>">
		<input type="hidden" id="email" name="email" value="<?php echo $user->get_username(); ?>">
		<input type="hidden" id="subject" name="subject" value="<?php
			if ($therapist != null)
			{
				echo _('Consulta a mi terapeuta');
			}
			else
			{
				echo _('Solicitud de asignaci&oacute;n de terapeuta');
			}
			?>">
	<!--<div class="control-group" id="name-group">
		<label class="control-label" for="name"><?php echo _('Nombre'); ?></label>
		<div class="controls">
		</div>
    </div>
    <div class="control-group" id="email-group">
		<label class="control-label" for="email"><?php echo _('E-Mail'); ?></label>
		<div class="controls">
		</div>
    </div>
    <div class="control-group" id="subject-group">
		<label class="control-label" for="subject"><?php echo _('Asunto'); ?></label>
		<div class="controls">
		</div>
    </div>-->
    <div class="control-group" id="message-group">
    	<label class="control-label" for="message"><?php echo _('Texto'); ?></label>
		<div class="controls">
			<textarea class="input-xlarge" id="message" name="message" rows="5"></textarea>
		</div>
    </div>
    <div class="control-group" id="captcha-group">
		<label class="control-label" id="captcha-label" for="captcha"></label>
		<div class="controls">
			<input type="text" class="input-mini" maxlength="2" id="captcha" name="captcha">
			<span class="help-inline"><?php echo _('Esto evita que se haga un mal uso del formulario de contacto.'); ?></span>
		</div>
    </div>
    <div class="control-group">
	    <div class="controls">
			<button type="submit" class="btn btn-large btn-primary"><?php echo _('Enviar'); ?></button>
			<button type="reset" class="btn btn-large" id="reset-button"><?php echo _('Limpiar'); ?></button>
		</div>
	</div>
	</fieldset>
</form>
<p id="result-form"></p>

<script type="text/javascript">
var c;
var d;
reloadCaptcha();
reloadCaptcha2();

function reloadCaptcha()
{
	var a = Math.ceil(getRandom() * 10);
	var b = Math.ceil(getRandom() * 10);
	c = a + b;
	
	$('#captcha-label').html("<?php echo _('Resultado de'); ?>"+' '+a+' + '+b);
};

function reloadCaptcha2()
{
	var a = Math.ceil(getRandom() * 10);
	var b = Math.ceil(getRandom() * 10);
	d = a + b;
	
	$('#captcha2-label').html("<?php echo _('Resultado de'); ?>"+' '+a+' + '+b);
};

function checkForm()
{
	var subject = $('#subject').val();
	var name = $('#name').val();
	var email = $('#email').val();
	var message = $('#message').val();
	var captcha = $('#captcha').val();

	$(".error").removeClass('error');
	if (name.length <= 0)
	{
		$('#name-group').addClass('error');
	}
	else if (email.length <= 0)
	{
		$('#email-group').addClass('error');
	}
	else if (subject.length <= 0)
	{
		$('#subject-group').addClass('error');
	}
	else if (message.length <= 0)
	{
		$('#message-group').addClass('error');
	}
	else if (captcha.length <= 0 || parseInt(captcha) != c)
	{
		$('#captcha-group').addClass('error');
	}
	else
	{
		var qry = 'subject='+encodeURIComponent(subject)+'&'+
				  'name='+encodeURIComponent(name)+'&'+
				  'email='+encodeURIComponent(email)+'&'+
				  'message='+encodeURIComponent(message)<?php if ($therapist != null) echo "+'&dest='+encodeURIComponent('".$therapist->get_username()."')"; ?>;
			
		$.ajax({
			type: 'POST',
			data: qry,
			url: 'backend/contact.php',
			success: function(res) {
				$('#result-form').removeClass('contact-form-ok').removeClass('contact-form-error');
				
				if (res=='false')
				{
					$('#result-form').addClass('contact-form-error').html("<?php echo _('Ocurri&oacute; un error al enviar el mensaje. Por favor, int&eacute;ntelo m&aacute;s tarde.'); ?>");
				}
				else
				{
					reloadCaptcha();
					$('#reset-button').click();
					
					$('#result-form').addClass('contact-form-ok').html("<?php echo _('El mensaje se envi&oacute; correctamente.'); ?>");
				}
			}
		});
	}
};

function checkForm2()
{
	var subject = "<?php echo _('Solicitud de asignación como terapeuta'); ?>";
	var name = $("#user-name-known").val()+" "+$("#user-surname-known").val();
	var email = $("#user-known").val();
	var dest = $("#mail-known").val();
	var message = "<?php echo _('El usuario'); ?> "+name+" <?php echo _('le solicita que sea su terapeuta en la plataforma VIRTRA-EL. Puede contactar con dicho usuario a través del correo'); ?> "+email+". <?php echo _('También puede confirmar su solicitud a través del siguiente vínculo:'); ?> http://www.virtrael.es/backend/confirm_therapist.php?therapist_id="+dest+"&patient_id=<?php echo $_SESSION['userID'] ?>";
	
	var captcha = $('#captcha2').val();

	$(".error").removeClass('error');
	if (dest.length <= 0)
	{
		$('#mail-group-known').addClass('error');
	}
	else if (captcha.length <= 0 || parseInt(captcha) != d)
	{
		$('#captcha2-group').addClass('error');
	}
	else
	{
		var qry = 'subject='+encodeURIComponent(subject)+'&'+
				  'name='+encodeURIComponent(name)+'&'+
				  'email='+encodeURIComponent(email)+'&'+
				  'message='+encodeURIComponent(message)+'&'+
				  'dest='+encodeURIComponent(dest);
			
		$.ajax({
			type: 'POST',
			data: qry,
			url: 'backend/contact.php',
			success: function(res) {
				$('#result-form-known').removeClass('contact-form-ok').removeClass('contact-form-error');
				
				if (res=='false')
				{
					$('#result-form-known').addClass('contact-form-error').html("<?php echo _('Ocurri&oacute; un error al enviar el mensaje. Por favor, int&eacute;ntelo m&aacute;s tarde.'); ?>");
				}
				else
				{
					$('#result-form-known').addClass('contact-form-ok').html("<?php echo _('El mensaje se envi&oacute; correctamente.'); ?>");
					$("#mail-known").val("");
					$('#captcha2').val("");
				}
			}
		});
	}
};

</script>

