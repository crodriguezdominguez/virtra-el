<?php
	require_once(__DIR__.'/../../locale/localization.php');

	session_start();
	if (isset($_SESSION['userID']))
	{
		include_once(__DIR__.'/../../php/User.php');
		$user = new User($_SESSION['userID']);
		
		$group_by_exercise = (isset($_GET['exercises']) && $_GET['exercises'] == '1')?true:false;
		$filter = isset($_GET['filter'])?$_GET['filter']:null;
		$comp = isset($_GET['comp'])?$_GET['comp']:null;
		$value = isset($_GET['value'])?$_GET['value']:null;
		$stat_func = isset($_GET['stat'])?$_GET['stat']:User::STAT_AVG;
?>
<div>
	<?php if (($user->get_therapist() || $user->get_carer()) && !isset($_GET['patientID'])) {
		
	?>
		<!--<p class="lead"><?php echo _('Seleccione el modo de agrupación de los datos:'); ?></p>
		<select id="grouping-selector">
			<option value="0"><?php echo _('Por sesiones'); ?></option>
			<option value="1"><?php echo _('Por ejercicios'); ?></option>
		</select>
	<hr />-->
	<?php
			if (!$group_by_exercise)
			{
	?>
		<p><?php echo _('Seleccione una sesi&oacute;n para ver los resultados. Pulse sobre los iconos de la leyenda para mostrar/ocultar las diferentes series num&eacute;ricas. Pulse en el siguiente v&iacute;nculo para descargar los resultados en formato Excel: '); ?><a href="backend/generate_excel.php"><img src="img/icon-excel" width="18" height="18" />&nbsp;<strong><span style="color:green"><?php echo _('Descargar resultados en formato Excel'); ?></a></span></strong></p>
		<hr />
		<?php echo _('Sesión'); ?>:&nbsp;<select id="session_id_selector">
		</select>
		<?php
			}
			else
			{
		?>
		<p><?php echo _('Seleccione un ejercicio para ver los resultados. Pulse sobre los iconos de la leyenda para mostrar/ocultar las diferentes series num&eacute;ricas'); ?></p>
		<hr />
		<?php echo _('Ejercicio'); ?>:&nbsp;<select id="exercise_id_selector">
		</select>
		<?php
			}
		?>
			<select id="filter-stat">
				<option value="AVG"><?php echo _('Media'); ?></option>
				<option value="STDDEV"><?php echo _('Desviación Típica'); ?></option>
			</select>
			<hr />
			<p><?php echo _('Filtrar por:'); ?>
				<select id="filter">
					<!-- partner civilStatus hoursReading hoursWorkshop hoursExercise hoursComputer eatingHelp washingHelp clothingHelp smartenUPHelp urinateHelp defecateHelp toiletHelp movingHelp walkingHelp stepsHelp phoneHelp shoppingHelp cookingHelp homecareHelp clothCleaningHelp transportHelp medicationHelp economyHelp -->
					<option value="placeOfBirth" <?php echo ($filter=='placeOfBirth'?'CHECKED':''); ?>><?php echo _('Lugar de nacimiento'); ?></option>
					<option value="sex" <?php echo ($filter=='sex'?'CHECKED':''); ?>><?php echo _('Sexo'); ?></option>
					<option value="age" <?php echo ($filter=='age'?'CHECKED':''); ?>><?php echo _('Edad'); ?></option>
					<option value="levelOfStudies" <?php echo ($filter=='levelOfStudies'?'CHECKED':''); ?>><?php echo _('Nivel de estudios'); ?></option>
				</select>
				<select id="filtercomp" disabled>
					<option value="=" <?php echo ($comp=='='?'CHECKED':''); ?>><?php echo _('Igual a') ?></option>
					<option value="!=" <?php echo ($comp=='!='?'CHECKED':''); ?>><?php echo _('Distinto a') ?></option>
					<option value="<" <?php echo ($comp=='<'?'CHECKED':''); ?>><?php echo _('Menor a') ?></option>
					<option value=">" <?php echo ($comp=='>'?'CHECKED':''); ?>><?php echo _('Mayor a') ?></option>
					<option value="<=" <?php echo ($comp=='<='?'CHECKED':''); ?>><?php echo _('Menor o igual a') ?></option>
					<option value=">=" <?php echo ($comp=='>='?'CHECKED':''); ?>><?php echo _('Mayor o igual a') ?></option>
				</select>
				<select id="filter-value" class="hide">
					
				</select>
				<input type="text" value="<?php echo (($filter=='placeOfBirth' || $filter == 'age')?$value:''); ?>" name="filter-value-text" id="filter-value-text" />
				<br />
				<a class="btn btn-primary" href="javascript:void(0);" onclick="javascript:filter_data()"><?php echo _('Aplicar filtro'); ?></a>
				<a class="btn btn-default" href="javascript:void(0);" onclick="javascript:reset_data()"><?php echo _('Quitar filtros'); ?></a>
			</p>
			<hr />
	<?php } else{ ?>
	<p><?php echo _('Seleccione una sesi&oacute;n para ver los resultados. Pulse sobre los iconos de la leyenda para mostrar/ocultar las diferentes series num&eacute;ricas'); ?></p>
	<hr />
	<?php echo _('Sesión'); ?>:&nbsp;<select id="session_id_selector">
	</select>
	<?php } ?>
</div>
<div id="chart_div" style="height:500px;margin:0;padding:0;"></div>
<script type="text/javascript" src="js/jquery.xml2json.js"></script>
<script type="text/javascript" src="exercises/exercise-title-mapper.php"></script>
<script type="text/javascript">
<?php
		if ($user->get_therapist() && !isset($_GET['patientID'])) //show results from all patients (circular chart for each completed session, bar charts for avg. times, errors, avoids, etc.)
		{
?>
			var avgData = <?php echo json_encode($user->allResults(User::STAT_AVG, $group_by_exercise, $filter, $value, $comp)); ?>;
			var desvData = <?php echo json_encode($user->allResults(User::STAT_DESV, $group_by_exercise, $filter, $value, $comp)); ?>;
			var allData = avgData;
			
			for (var prop in allData)
			{
				$('#session_id_selector').append($('<option>', { 
			        value: prop,
			        text : prop 
			    }));
			}

			function reset_data()
			{
				$("#settings-container").load('backend/ui/results.php');
			}
			
			function filter_data()
			{
				var comp = $("#filtercomp").val();
				var filter = $("#filter").val();
				var val = 0;
				
				if (filter == "placeOfBirth")
				{
					val = $("#filter-value-text").val();
				}
				else if (filter == "sex")
				{
					val = $("#filter-value").val();
				}
				else if (filter == "age")
				{
					val = $("#filter-value-text").val();
				}
				else if (filter == "levelOfStudies")
				{
					val = $("#filter-value").val();
				}
				
				var qry = 'backend/ui/results.php?filter='+encodeURIComponent(filter)+'&comp='+encodeURIComponent(comp)+'&value='+ encodeURIComponent(val)+'&stat='+encodeURIComponent($("#filter-stat").val());
				<?php
					if(isset($_GET['exercises']))
					{
						echo 'qry += "&exercises='.$_GET['exercises'].'"';
					}
				?>
				
				$("#settings-container").load(qry);
			}
			
			function filter_change()
			{
				var val = $("#filter").val();
				if (val == "placeOfBirth")
				{
					$("#filtercomp").val('=');
					$("#filtercomp").prop('disabled', true);
					
					$("#filter-value-text").show();
					$("#filter-value").hide();
				}
				else if (val == "sex")
				{
					$("#filter-value-text").hide();
					
					$("#filtercomp").val('=');
					$("#filtercomp").prop('disabled', true);
					
					$('#filter-value').html('');
					$('#filter-value').append($('<option>', { 
				        value: '1',
				        text : '<?php echo _('Masculino'); ?>' 
				    }));
				    $('#filter-value').append($('<option>', { 
				        value: '2',
				        text : '<?php echo _('Femenino'); ?>' 
				    }));
				    
				    $("#filter-value").show();
				}
				else if (val == "age")
				{
					$("#filtercomp").val('=');
					$("#filtercomp").prop('disabled', false);
					
					$("#filter-value-text").show();
					$("#filter-value").hide();
				}
				else if (val == "levelOfStudies")
				{
					$("#filtercomp").val('=');
					$("#filtercomp").prop('disabled', false);
					
					$("#filter-value-text").hide();
					
					var finalStr = '<option value="1"><?php echo _('Leer/escribir'); ?></option>';
					finalStr += '<option value="2"><?php echo _('Estudios primarios'); ?></option>';
					finalStr += '<option value="3"><?php echo _('Graduado escolar/EGB'); ?></option>';
					finalStr += '<option value="4"><?php echo _('Bachillerato/COU'); ?></option>';
					finalStr += '<option value="5"><?php echo _('Formaci&oacute;n profesional'); ?></option>';
					finalStr += '<option value="6"><?php echo _('Diplomatura'); ?></option>';
					finalStr += '<option value="7"><?php echo _('Licenciatura'); ?></option>';
					finalStr += '<option value="8"><?php echo _('Doctorado'); ?></option>';
					
					$('#filter-value').html(finalStr);
				    $("#filter-value").show();
				}
			}
			
			<?php
				if ($filter != null)
				{
			?>
				$("#filter").val('<?php echo $filter; ?>');
				filter_change();
			<?php	
				}
			?>
			
			<?php
				if ($filter == 'sex' || $filter == 'levelOfStudies')
				{
			?>
				$("#filter-value").val('<?php echo $value; ?>');
			<?php	
				}
			?>
			
			<?php
				if ($filter == 'placeOfBirth' || $filter == 'age')
				{
			?>
				$("#filter-value-text").val('<?php echo $value; ?>');
			<?php	
				}
			?>
			
			<?php
				if ($comp != null)
				{
			?>
				$("#filtercomp").val('<?php echo $comp; ?>');
			<?php	
				}
			?>
			
			<?php
				if ($stat_func != null)
				{
			?>
				$("#filter-stat").val('<?php echo $stat_func; ?>');
				func_change();
			<?php	
				}
			?>
			
			$("#filter").on('change', function (e) {
				filter_change();
			});
			
			function func_change()
			{
				if ($("#filter-stat").val() == 'AVG')
				{
					allData = avgData;
				}
				else if ($("#filter-stat").val() == 'STDDEV')
				{
					allData = desvData;
				}
				
				for (var prop in allData)
				{
					$('#session_id_selector').html('');
					$('#session_id_selector').append($('<option>', { 
				        value: prop,
				        text : prop 
				    }));
				}
				
				drawChart();
			}
			
			$("#filter-stat").on('change', function (e) {
				func_change();
			});

			/*
			$("#grouping-selector").on('change', function (e) {
				var qry = 'backend/ui/results.php?exercises='+$("#grouping-selector").val();
				<?php
					if(isset($_GET['filter']))
					{
						echo 'qry += "&filter='.$_GET['filter'].'"';
					}
					else if(isset($_GET['comp']))
					{
						echo 'qry += "&comp='.$_GET['comp'].'"';
					}
					else if(isset($_GET['value']))
					{
						echo 'qry += "&value='.$_GET['value'].'"';
					}
					else if(isset($_GET['stat']))
					{
						echo 'qry += "&stat='.$_GET['stat'].'"';
					}
				?>
				
				$("#settings-container").load(qry);
			});*/
			
			var columnsEnabled = [true, true, true, true, true];
			function drawChart() {	
				var currentSession = $('#session_id_selector').val();
				var arr = allData[currentSession];
				var dataStr = '[<?php
					echo '["'._('Ejercicio').'","'._('Aciertos').'","'._('Fallos').'","'._('Omisiones').'","'._('Puntuación').'","'._('Tiempo (seg.)').'"],';
				?>';
				
				var i=0;
				
				if (arr !== null && arr !== undefined)
				{
					var count = Object.keys(arr).length;
				
					//exercise names
					$.ajax({
						type: "GET",
						url: "sessions.php",
						dataType: "xml",
						success: function(xml) {
							sessions = $.xml2json(xml);
							names = [];
							for (var sess in sessions.session)
							{
								if (parseInt(sess)+1 === parseInt(currentSession))
								{
									if (sessions.session[sess].exercises.exerciseEntry instanceof Array)
									{
										for (var ex in sessions.session[sess].exercises.exerciseEntry)
										{
											names.push(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry[ex].type));
										}
									}
									else
									{
										names.push(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry.type));
									}
									
									break;
								}
							}
							
							for (var prop in arr)
							{
								var exName = names[parseInt(prop)-1];
								dataStr += '["'+exName+'",'+(columnsEnabled[0]?arr[prop]['corrects']:0)+','+(columnsEnabled[1]?arr[prop]['fails']:0)+','+(columnsEnabled[2]?arr[prop]['omissions']:0)+','+(columnsEnabled[3]?arr[prop]['score']:0)+','+(columnsEnabled[4]?arr[prop]['seconds']:0)+']';
								if (i<count-1) dataStr += ',';
								i++;
							}
							
							dataStr += ']';
							
							var data = google.visualization.arrayToDataTable(JSON.parse(dataStr));
							var colors_opt = [(columnsEnabled[0]?'#3366cc':'#cccccc'), (columnsEnabled[1]?'#dc3912':'#cccccc'), (columnsEnabled[2]?'#ff9900':'#cccccc'), (columnsEnabled[3]?'#109618':'#cccccc'), (columnsEnabled[4]?'#990099':'#cccccc')];
							var options = {
							  title: '<?php echo _('Resultados'); ?>',
							  vAxis: {title: '<?php echo _('Ejercicios'); ?>',  titleTextStyle: {color: 'red'}},
							  hAxis: {minValue: 0},
							  legend: { position: 'top', maxLines: 3},
							  bar: { groupWidth: '95%' },
							  colors: colors_opt
							};
							
							var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
							
							function showHideSeries()
							{
								var sel = chart.getSelection();
						        // if selection length is 0, we deselected an element
						        if (sel.length > 0) {
						            // if row is undefined, we clicked on the legend
						            if (sel[0].row == null) {
						                var col = sel[0].column;
						                columnsEnabled[col-1] = !columnsEnabled[col-1];
						                drawChart();
						            }
						        }
							}
							
							chart.draw(data, options);
							google.visualization.events.addListener(chart, 'select', showHideSeries);
						}
					});
				}
			}
<?php
		}
		else
		{
			$patient = null;
			if ($user->get_therapist() && isset($_GET['patientID']))
			{
				$patient = new User($_GET['patientID']); //todo: avoid taking data from patients from other therapists
			}
			else
			{
				$patient = $user;
			}
			
			if ($patient !== null)
			{
?>
			var allData = <?php echo json_encode($patient->results()); ?>;
			for (var prop in allData)
			{
				$('#session_id_selector').append($('<option>', { 
			        value: prop,
			        text : prop 
			    }));
			}
			
			var columnsEnabled = [true, true, true, true, true];
			function drawChart() {
				var currentSession = $('#session_id_selector').val();
				var arr = allData[currentSession];
				var dataStr = '[<?php
					echo '["'._('Ejercicio').'","'._('Aciertos').'","'._('Fallos').'","'._('Omisiones').'","'._('Puntuación').'","'._('Tiempo (seg.)').'"],';
				?>';
				
				var i=0;
				
				if (arr !== undefined && arr !== null)
				{
					var count = Object.keys(arr).length;
				
					//exercise names
					$.ajax({
						type: "GET",
						url: "sessions.php",
						dataType: "xml",
						success: function(xml) {
							sessions = $.xml2json(xml);
							names = [];
							for (var sess in sessions.session)
							{
								if (parseInt(sess)+1 === parseInt(currentSession))
								{
									if (sessions.session[sess].exercises.exerciseEntry instanceof Array)
									{
										for (var ex in sessions.session[sess].exercises.exerciseEntry)
										{
											names.push(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry[ex].type));
										}
									}
									else
									{
										names.push(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry.type));
									}
									break;
								}
							}
							
							for (var prop in arr)
							{
								var exName = names[parseInt(prop)-1];
								dataStr += '["'+exName+'",'+(columnsEnabled[0]?arr[prop]['corrects']:0)+','+(columnsEnabled[1]?arr[prop]['fails']:0)+','+(columnsEnabled[2]?arr[prop]['omissions']:0)+','+(columnsEnabled[3]?arr[prop]['score']:0)+','+(columnsEnabled[4]?arr[prop]['seconds']:0)+']';
								if (i<count-1) dataStr += ',';
								i++;
							}
							
							dataStr += ']';
							
							var data = google.visualization.arrayToDataTable(JSON.parse(dataStr));
							var colors_opt = [(columnsEnabled[0]?'#3366cc':'#cccccc'), (columnsEnabled[1]?'#dc3912':'#cccccc'), (columnsEnabled[2]?'#ff9900':'#cccccc'), (columnsEnabled[3]?'#109618':'#cccccc'), (columnsEnabled[4]?'#990099':'#cccccc')];
							var options = {
							  title: '<?php echo _('Resultados'); ?>',
							  vAxis: {title: '<?php echo _('Ejercicios'); ?>',  titleTextStyle: {color: 'red'}},
							  hAxis: {minValue: 0},
							  legend: { position: 'top', maxLines: 3},
							  bar: { groupWidth: '95%' },
							  colors: colors_opt
							};
							
							var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
							
							function showHideSeries()
							{
								var sel = chart.getSelection();
						        // if selection length is 0, we deselected an element
						        if (sel.length > 0) {
						            // if row is undefined, we clicked on the legend
						            if (sel[0].row == null) {
						                var col = sel[0].column;
						                columnsEnabled[col-1] = !columnsEnabled[col-1];
						                drawChart();
						            }
						        }
							}
							
							chart.draw(data, options);
							google.visualization.events.addListener(chart, 'select', showHideSeries);
						}
					});
				}
			}
<?php
			}
		}
?>
	if (allData !== null && allData !== undefined)
	{
		if (Object.keys(allData).length > 0)
		{
			google.setOnLoadCallback(drawChart);
			drawChart();
			
			$('#session_id_selector').on('change', function (e) {
				drawChart();
			});
		}
	}
</script>
<?php	
	}
?>