<?php
	include_once(__DIR__.'/../config.php');
	require_once(__DIR__.'/../../locale/localization.php');

	session_start();
	
	global $gold; $gold=0;
	global $silver; $silver=0;
	global $bronze; $bronze=0;
	
	if (isset($_SESSION['userID']))
	{
		$suffix = '';
		if (isset($_GET['sessionID']))
		{
			$sessionID = $_GET['sessionID'];
			$suffix = " AND sessionID='$sessionID'";
		}
		
		$userID = $_SESSION['userID'];
		
		$link = mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
		
		$result = mysqli_query($link, "SELECT count(id) AS medals FROM medal WHERE userID='$userID' AND kindOfMedal='0'".$suffix);
		if ($row = $result->fetch_assoc())
		{
			$bronze = $row['medals'];
		}
		mysqli_free_result($result);
		
		$result = mysqli_query($link, "SELECT count(id) AS medals FROM medal WHERE userID='$userID' AND kindOfMedal='1'".$suffix);
		if ($row = $result->fetch_assoc())
		{
			$silver = $row['medals'];
		}
		mysqli_free_result($result);
		
		$result = mysqli_query($link, "SELECT count(id) AS medals FROM medal WHERE userID='$userID' AND kindOfMedal='2'".$suffix);
		if ($row = $result->fetch_assoc())
		{
			$gold = $row['medals'];
		}
		mysqli_free_result($result);
		
		$link->close();
	}
	else die();
?>

<table class="table">
	<tr>
		<td><center><img src="img/bronze_medal.png" alt="<?php echo _('Medallas de bronce'); ?>" width="80" /></center></td>
		<td><center><img src="img/silver_medal.png" alt="<?php echo _('Medallas de plata'); ?>" width="80" /></center></td>
		<td><center><img src="img/gold_medal.png" alt="<?php echo _('Medallas de oro'); ?>" width="80" /></center></td>
	</tr>
	<tr>
		<td><center><span class="lead" style="color:rgba(186, 113, 117, 1); font-weight:bold; font-size:48px;"><?php echo $bronze; ?></span></center></td>
		<td><center><span class="lead" style="color:#777; font-weight:bold; font-size:48px;"><?php echo $silver; ?></span></center></td>
		<td><center><span class="lead" style="color:orange; font-weight:bold; font-size:48px;"><?php echo $gold; ?></span></center></td>
	</tr>
</table>