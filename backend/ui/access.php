<?php
	require_once(__DIR__.'/../../locale/localization.php');

	session_start();
	if (isset($_SESSION['userID']))
	{
		include_once(__DIR__.'/../../php/User.php');
		$user = new User($_SESSION['userID']);
?>
<p class="lead"><?php
	echo _('Estás registrado como: <strong>');
	if ($user->get_superuser())
	{
		echo _('Administrador');
	}
	else if ($user->get_therapist())
	{
		echo _('Terapeuta');
	}
	else if ($user->get_carer())
	{
		echo _('Cuidador');
	}
	else
	{
		echo _('Paciente');
	}
	echo '</strong>.';
?></p>
<form class="form-horizontal" role="form" onsubmit="javascript:void(0);" action="javascript:saveUser(<?php echo $_SESSION['userID']; ?>);">
<input type="hidden" id="form-userID" value="<?php echo $_SESSION['userID']; ?>">
<div class="control-group">
	<div class="controls">
  	<label for="form-activated" class="checkbox"><?php echo _('Activado'); ?>
  		<input type="checkbox" disabled name="form-activated" class="form-control" id="form-activated" <?php if ($user->get_activated()) echo "CHECKED"; ?>>
  	</label>
	</div>
</div>
<div class="control-group">
<label for="form-email" class="control-label"><?php echo _('E-Mail'); ?> (*)</label>
<div class="controls">
	<input type="text" class="form-control" id="form-email" placeholder="<?php echo _('E-Mail'); ?>" value="<?php echo $user->get_username(); ?>">
</div>
</div>
<div class="control-group">
<label for="form-name" class="control-label"><?php echo _('Nombre'); ?> (*)</label>
<div class="controls">
	<input type="text" class="form-control" id="form-name" placeholder="<?php echo _('Nombre'); ?>" value="<?php echo $user->get_name(); ?>">
</div>
</div>
<div class="control-group">
<label for="form-surname" class="control-label"><?php echo _('Apellidos'); ?> (*)</label>
<div class="controls">
	<input type="text" class="form-control" id="form-surname" placeholder="<?php echo _('Apellidos'); ?>" value="<?php echo $user->get_surname(); ?>">
</div>
</div>
<div class="control-group">
<label for="form-pass" class="control-label"><?php echo _('Contraseña'); ?> (*)</label>
<div class="controls">
	<input type="password" class="form-control" id="form-pass" placeholder="<?php echo _('Contraseña'); ?>" value="">
	<span class="help-block"><?php echo _('Deje este campo en blanco si no desea cambiar la contraseña del usuario'); ?></span>
</div>
</div>
<div class="control-group">
<label for="form-pass2" class="control-label"><?php echo _('Repetir Contraseña'); ?> (*)</label>
<div class="controls">
	<input type="password" class="form-control" id="form-pass2" placeholder="<?php echo _('Repetir contraseña'); ?>" value="">
</div>
</div>
<div class="control-group">
	<div class="controls">
		<button type="submit" class="btn btn-primary"><?php echo _('Modificar Datos'); ?></button>
	</div>
</div>
</form>
<p id="form-result-id"></p>

<script type="text/javascript">
	
function saveUser(f_id)
{
	var name = $("#form-name").val();
	var surname = $("#form-surname").val();
	var email = $("#form-email").val();
	var therapist = "<?php echo $user->get_therapist()?'1':'0'; ?>";
	var carer = "<?php echo $user->get_carer()?'1':'0'; ?>";
	var activated = <?php echo ($user->get_activated()?'1':'0'); ?>;
	var password = $("#form-pass").val();
	var password2 = $("#form-pass2").val();
	
	if (name == '' || surname == '' || email == '')
	{
		$('#form-result-id').html('<span style="color:red;"><?php echo _('Debes rellenar todos los campos obligatorios.'); ?></span>');
	}
	else if (password != password2)
	{
		$('#form-result-id').html('<span style="color:red;"><?php echo _('Las contrase&ntilde;as deben coincidir.'); ?></span>');
	}
	else
	{
		var qry = "user=<?php echo $_SESSION['userID']; ?>&name=" + encodeURIComponent(name) + "&surname=" + encodeURIComponent(surname) + "&email=" + encodeURIComponent(email) + "&th=" + encodeURIComponent(therapist) + "&carer=" + encodeURIComponent(carer) + "&activated=" + encodeURIComponent(activated);
	
		if (f_id!=null)
		{
			qry = qry + "&saveID=" + f_id;
			if (password.length > 0)
			{
				qry = qry + "&pass=" + md5(password);
			}
		}
		else
		{
			qry = qry + "&pass=" + md5(password);
		}
		
		$.ajax({
			type: "POST",
			url: "backend/save_user.php",
			data: qry,
			success: function(html) {
				if(html!='false') //user id
				{
					$('#form-result-id').html('<span style="color:green;"><?php echo _('La información se guardó con éxito.'); ?></span>');
				}
				else
				{
					$('#form-result-id').html('<span style="color:red;"><?php echo _('Error desconocido en el servidor. Por favor, inténtalo de nuevo más tarde.'); ?></span>');
				}
			}
		});
	}
}
</script>
<?php
	}
?>