<?php
	require_once(__DIR__.'/../../locale/localization.php');
	require_once(__DIR__.'/../../php/User.php');
	
	session_start();
	if (isset($_SESSION['userID']))
	{
		$user = new User($_SESSION['userID']);
		if ($user->get_superuser())
		{
			$therapists = User::therapists();
?>
<div class="entry-panel row">
	<p><?php echo _('Hay').' '.count($therapists).' '.(count($therapists)==1?_('terapeuta registrado'):_('terapeutas registrados')); ?></p>
	<div style="margin-left:0px; margin-bottom:15px;">
		<button type="button" class="btn btn-primary btn-sm" onclick="javascript:addUser();"><i class='icon-plus icon-white'></i> <?php echo _('Agregar Terapeuta'); ?></button>
		<div class="span6 pull-right" style="margin-right:-10px;">
			<div class="input-append">
		      	<input type="search" id="search-input" class="form-control" placeholder="<?php echo _('Buscar (nombre, apellido o e-mail)'); ?>">
		        <button class="btn btn-primary" type="button" onclick="javascript:search();"><i class="icon-search icon-white"></i> <?php echo _('Buscar'); ?></button>&nbsp;<button onclick="javascript:$('#search-input').val('');search();" class="btn btn-default" type="button"><i class="icon-remove"></i> <?php echo _('Limpiar'); ?></button>
		    </div>
		</div>
	</div>
	<div class="table-responsive rounded-table">
		<p class="rounded-table-title"><?php echo _('Terapeutas'); ?></p>
		<table class="table table-hover" id="users_table">
			<thead>
				<tr id="table-header">
					<th>#</th>
					<th><?php echo _('E-Mail'); ?></th>
					<th><?php echo _('Nombre'); ?></th>
					<th><?php echo _('Apellidos'); ?></th>
					<th><?php echo _('Activado'); ?></th>
					<th><?php echo _('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="pagination pagination-centered">
			<ul id="paging">
			</ul>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#search-input").keyup(function(event){
	    if(event.keyCode == 13){
	        search();
	    }
	});
	
	function activateUser(f_id, act)
	{
		var status = act?'1':'0';
		var qry = "user=<?php echo $_SESSION['userID']; ?>&actID="+f_id+"&activated="+status;
		
		$.ajax({
			type: "POST",
			url: "backend/activate_user.php",
			data: qry,
			success: function(html) {
				if(html=='true')
				{
					
					$('#column_activated_'+f_id).html("<a href=\"javascript:activateUser('"+f_id+"', "+((status=='1')?"false":"true")+");\" class=\"btn btn-mini "+((status=='1')?"btn-success":"btn-danger")+"\">&nbsp;<i class=\""+((status=='1')?"icon-ok":"icon-remove")+" icon-white\"></i>&nbsp;</a>");
				}
				else
				{
					sweetAlert("<?php echo _('Ups...'); ?>", "<?php echo _('Error desconocido en el servidor. Por favor, inténtalo de nuevo más tarde.'); ?>", "error");
				}
			}
		});
	}

	function removeUser(f_id)
	{
		swal({
				title: "<?php echo _('¿Estás seguro?'); ?>",
				text: "<?php echo _('¿Estás seguro de que quieres eliminar el usuario #'); ?>"+ f_id +"? <?php echo _('Esta operación no puede deshacerse'); ?>.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "<?php echo _('Si, eliminarlo'); ?>",
				cancelButtonText: "<?php echo _('Cancelar'); ?>",
				closeOnConfirm: true
			},
			function(isConfirm){
				if (isConfirm)
				{
					var qry = "user=<?php echo $_SESSION['userID']; ?>&deleteID="+f_id;
			
					$.ajax({
						type: "POST",
						url: "backend/delete_user.php",
						data: qry,
						success: function(html) {
							if(html=='true')
							{
								var tr = $("#row_user_"+f_id);
								tr.fadeOut(400, function(){
									 tr.remove();
									 search();
								});
							}
							else
							{
								sweetAlert("<?php echo _('Ups...'); ?>", "<?php echo _('Error desconocido en el servidor. Por favor, inténtalo de nuevo más tarde.'); ?>", "error");
							}
						}
					});
				}
		});
	}
	
	function editUser(f_id)
	{
		$("#settings-container").load('backend/ui/edit_user.php?th=1&editID='+f_id);
	}
	
	function addUser()
	{
		$("#settings-container").load('backend/ui/edit_user.php?th=1');
	}
	
	function search()
	{
		search_page(0);
	}
	
	function search_page(page)
	{
		var text = $("#search-input").val();
		$.ajax({
				type: "GET",
				url: "backend/search_user.php?q="+text+"&l=10&th=1&p="+page,
				data: null,
				success: function(html) {
					var result = eval('('+html+')');
					reload_table(result);
					
					$("[id^='paging_item_']").removeClass('active');
					$("#paging_item_"+page).addClass('active');
					
					if (page+1 == result.npages)
					{
						$("#right_paging").addClass('disabled');
					}
					else
					{
						$("#right_paging").removeClass('disabled');
					}
					if (page == 0)
					{
						$("#left_paging").addClass('disabled');
					}
					else
					{
						$("#left_paging").removeClass('disabled');
					}
				}
		});
	}
	
	function reload_table(search_result)
	{
		$("#users_table").html("<thead>\
				<tr id=\"table-header\">\
					<th>#</th>\
					<th><?php echo _('E-Mail'); ?></th>\
					<th><?php echo _('Nombre'); ?></th>\
					<th><?php echo _('Apellidos'); ?></th>\
					<th><?php echo _('Activado'); ?></th>\
					<th><?php echo _('Acciones'); ?></th>\
				</tr>\
			</thead><tbody></tbody>");
	
		for (var i=0; i<search_result.elements.length; i++)
		{
			var item = search_result.elements[i];
			var tr = "<tr id='row_user_"+item.id+"'>";
			
			tr += "<td id='column_id_"+item.id+"'>"+item.id+"</td>";
			tr += "<td id='column_email_"+item.id+"'>"+item.email+"</td>";
			tr += "<td id='column_name_"+item.id+"'>"+item.name+"</td>";
			tr += "<td id='column_surname_"+item.id+"'>"+item.surname+"</td>";
			tr += "<td id='column_activated_"+item.id+"'><a href=\"javascript:activateUser('"+item.id+"', "+((item.status=='1')?"false":"true")+");\" class=\"btn btn-mini "+((item.status=='1')?"btn-success":"btn-danger")+"\">&nbsp;<i class=\""+((item.status=='1')?"icon-ok":"icon-remove")+" icon-white\"></i>&nbsp;</a></td>";
			tr += "<td><a class='btn btn-mini btn-default' onclick=\"javascript:editUser('"+item.id+"');\"><i class='icon-edit'></i> Editar</a>&nbsp;<a class='btn btn-mini btn-danger' onclick=\"javascript:removeUser('"+item.id+"');\"><i class='icon-remove icon-white'></i> Eliminar</a></td>";
			
			tr += "</tr>";
		
			$('#users_table').find('tbody:last').append(tr);
		}
		
		if (search_result.npages <= 1) $("#paging").html("");
		else
		{
			var pages = "<li id='left_paging' class=\"disabled\"><a href='javascript:prev_page();'>&laquo;</a></li>";
			for (var i=0; i<search_result.npages; i++)
			{
				pages += "<li"+(i==0?" class='active'":"")+" id='paging_item_"+i+"'><a href='javascript:search_page("+i+");'>"+i+"</a></li>";
			}
			pages += "<li id='right_paging'><a href='javascript:next_page();'>&raquo;</a></li>";
			
			$("#paging").html(pages);
		}
	}
	
	function next_page()
	{
		var page = parseInt($("#paging .active").attr('id').split("_")[2]);
		search_page(page+1);
	}
	
	function prev_page()
	{
		var page = parseInt($("#paging .active").attr('id').split("_")[2]);
		search_page(page-1);
	}
	
	$(function() {
		search();
  	});
</script>
	
<?php
		}
	}
?>