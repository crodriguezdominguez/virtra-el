<?php
	require_once(__DIR__.'/../../locale/localization.php');

	session_start();
	if (isset($_SESSION['userID']))
	{
		include_once(__DIR__.'/../../php/User.php');
		$user = new User($_SESSION['userID']);
?>

<table cellspacing="5" cellpadding="5" style="margin-bottom: 20px;">
	<tr>
	    <td><a id="avatar0" href="javascript:void(0);" class="thumbnail" style="width:100px;height:130px;" onclick="javascript:checkIndex(0);"></a></td>
	    <td><a id="avatar1" href="javascript:void(0);" class="thumbnail" style="width:100px;height:130px;" onclick="javascript:checkIndex(1);"></a></td>
	    <td><a id="avatar2" href="javascript:void(0);" class="thumbnail" style="width:100px;height:130px;" onclick="javascript:checkIndex(2);"></a></td>
	</tr>
	<tr>
	    <td><a id="avatar3" href="javascript:void(0);" class="thumbnail" style="width:100px;height:130px;" onclick="javascript:checkIndex(3);"></a></td>
	    <td><a id="avatar4" href="javascript:void(0);" class="thumbnail" style="width:100px;height:130px;" onclick="javascript:checkIndex(4);"></a></td>
	    <td><a id="avatar5" href="javascript:void(0);" class="thumbnail" style="width:100px;height:130px;" onclick="javascript:checkIndex(5);"></a></td>
	</tr>
</table>
<a class="btn btn-primary" href="javascript:void(0);" onclick="javascript:set_avatar();"><?php echo _('Asignar como asistente'); ?></a>
<p id="set-result"></p>

<script type="text/javascript">
	var imgs = ['img/pepa.png', 'img/juan.png', 'img/pepe_pesco-02.png', 'img/pepita.png', 'img/juanito.png', 'img/pepe_pesco-03.png'];
	var names = ['Pepa', 'Juan', 'Pepe Clásico', 'Pepita', 'Juanito', '\"Modern\" Pepe',];
	var avatar = '<?php echo $user->get_avatar(); ?>';
	
	var index = imgs.indexOf(avatar);
	$("#avatar"+index).addClass('semantic-exercise-toggle');
	
	for (var i=0; i<imgs.length; i++)
	{
		$("#avatar"+i).html("<img src='"+imgs[i]+"' style='height:100px;width:auto;' /><center><p>"+names[i]+"</p></center>");
	}
	
	function get_avatar(){
		return imgs[index];
	};
	
	function set_avatar(){
		var userID = '<?php echo $user->get_id(); ?>';
		avatar = imgs[index];
		
		var qry = 'user='+userID+'&avatar='+encodeURIComponent(avatar);
		
		$.ajax({
			type: "POST",
			url: "backend/save_avatar.php",
			data: qry,
			success: function(html) {
				if(html!='false') //user id
				{
					$("#set-result").html("<span style='color:green;'><?php echo _('Ha asignado su nuevo asistente correctamente. Podrá verlo durante la ejecución de los ejercicios.'); ?></span>");
				}
				else
				{
					$("#set-result").html("<span style='color:red;'><?php echo _('Error desconocido en el servidor. Por favor, inténtalo de nuevo más tarde.'); ?></span>");
				}
			}
		});
	}
	function checkIndex(k) {
		$('.semantic-exercise-toggle').removeClass('semantic-exercise-toggle');
		$("#avatar"+k).addClass('semantic-exercise-toggle');
		index = k;
	}
</script>

<?php
	}
?>