<?php
	require_once(__DIR__.'/../../php/User.php');

	session_start();
	
	global $user; $user = null;
	
	$therapist = ($_GET['th']=='1')?true:false;
	$carer = ($_GET['carer']=='1')?true:false;
	
	$currentUser = new User($_SESSION['userID']);
	
	if (isset($_GET['editID']))
	{
		$p_id = $_GET['editID'];
		$user = new User($p_id);
	}
	else
	{
		$user = new User();
		$user->set_therapist($therapist);
		$user->set_activated(true);
		$user->set_carer($carer);
	}

?>

<script type="text/javascript">
var carer = false;
</script>

<div class="entry-panel">
	<ul class="breadcrumb">
		<li><a href="javascript:void(0);" onclick="javascript:load_<?php echo ($therapist||$carer)?'therapists':'patients';?>();"><?php echo $therapist?_('Terapeutas'):($carer?_('Cuidadores'):_('Pacientes')); ?></a> <span class="divider">/</span></li>
		<li class="active"><?php if (isset($_GET['editID'])) echo $user->get_surname().', '.$user->get_name(); else echo $therapist?_('Agregar terapeuta'):($carer?_('Agregar Cuidador'):_('Agregar Paciente')); ?></li>
	</ul>
	<?php
		if (!$therapist && !$carer && $_GET['adding'] == '1')
		{
	?>
			<center>
			<div class="btn-group" data-toggle="buttons-radio">
			  <button onclick="javascript:existing_patient(false, false);" class="btn active"><?php echo _('Nuevo paciente'); ?></button>	  
			  <button onclick="javascript:existing_patient(true, false);" class="btn"><?php echo $currentUser->get_superuser()?_('Asignar Terapeutas'):_('Paciente existente'); ?></button>
			  <?php
				  if ($currentUser->get_superuser())
				  {
			   ?>
			   		<button onclick="javascript:existing_patient(true, true);" class="btn"><?php echo _('Asignar Cuidadores'); ?></button>
			  <?php
				  }
			  ?>
			</div>
			<hr />
			</center>
	<?php
		}
	?>
	<div class="row" id="new-user-form">
		<form class="form-horizontal" role="form" onsubmit="javascript:void(0);" action="javascript:saveUser(<?php echo isset($_GET['editID'])?$_GET['editID']:'null'; ?>);">
		    <input type="hidden" id="form-userID" value="<?php echo $_GET['editID']; ?>">
		  <div class="control-group">
		  	<div class="controls">
			  	<label for="form-activated" class="checkbox"><?php echo _('Activado'); ?>
			  		<input type="checkbox" name="form-activated" class="form-control" id="form-activated" <?php if ($user->get_activated()) echo "CHECKED"; ?>>
			  	</label>
		  	</div>
		  </div>
		  <div class="control-group">
		    <label for="form-email" class="control-label"><?php echo _('E-Mail'); ?> (*)</label>
		    <div class="controls">
		    	<input type="text" class="form-control" id="form-email" placeholder="<?php echo _('E-Mail'); ?>" value="<?php echo $user->get_username(); ?>">
		    </div>
		  </div>
		  <div class="control-group">
		    <label for="form-name" class="control-label"><?php echo _('Nombre'); ?> (*)</label>
		    <div class="controls">
		    	<input type="text" class="form-control" id="form-name" placeholder="<?php echo _('Nombre'); ?>" value="<?php echo $user->get_name(); ?>">
		    </div>
		  </div>
		  <div class="control-group">
		    <label for="form-surname" class="control-label"><?php echo _('Apellidos'); ?> (*)</label>
		    <div class="controls">
		    	<input type="text" class="form-control" id="form-surname" placeholder="<?php echo _('Apellidos'); ?>" value="<?php echo $user->get_surname(); ?>">
		    </div>
		  </div>
		  <div class="control-group">
		    <label for="form-pass" class="control-label"><?php echo _('Contraseña'); ?> (*)</label>
		    <div class="controls">
		    	<input type="password" class="form-control" id="form-pass" placeholder="<?php echo _('Contraseña'); ?>" value="">
		    	<?php if (isset($_GET['editID'])) { ?>
		    		<span class="help-block"><?php echo _('Deje este campo en blanco si no desea cambiar la contraseña del usuario'); ?></span>
		    	<?php } ?>
		    </div>
		  </div>
		  <div class="control-group">
		    <label for="form-pass2" class="control-label"><?php echo _('Repetir Contraseña'); ?> (*)</label>
		    <div class="controls">
		    	<input type="password" class="form-control" id="form-pass2" placeholder="<?php echo _('Repetir contraseña'); ?>" value="">
		    </div>
		  </div>
		  <div class="control-group">
		  	<div class="controls">
		  		<a class="btn" href="javascript:void(0);" onclick="javascript:load_<?php echo ($therapist||$carer)?'therapists':'patients';?>();"><?php echo _('Cancelar'); ?></a>
		  		<button type="submit" class="btn btn-primary"><?php echo _('Guardar'); ?></button>
		  	</div>
		  </div>
		</form>
	</div>
	<?php
		if (!$therapist && !$carer && $_GET['adding'] == '1')
		{
	?>
	<div class="row hidden" id="existing-user-form">
		<?php
			require_once(__DIR__.'/../../locale/localization.php');
			require_once(__DIR__.'/../../php/User.php');
			
			session_start();
			
			if (isset($_SESSION['userID']))
			{
				$user = new User($_SESSION['userID']);
				//if ($user->get_superuser())
				//{
				//	$patients = $user->get_patients();
		?>
		<div class="entry-panel row">
			<div class="span10">
				<div class="input-append">
			      	<input type="search" id="search-input" class="form-control" placeholder="<?php echo _('Buscar (nombre, apellido o e-mail)'); ?>">
			        <button class="btn btn-primary" type="button" onclick="javascript:search();"><i class="icon-search icon-white"></i> <?php echo _('Buscar'); ?></button>&nbsp;<button onclick="javascript:$('#search-input').val('');search();" class="btn btn-default" type="button"><i class="icon-remove"></i> <?php echo _('Limpiar'); ?></button>
			    </div>
			    <p id="search-note-id">(<?php echo _('Nota: sólo aparecen los pacientes que no tiene aún asignado un terapeuta'); ?>)</p>
			</div>
		</div>
		<div class="entry-panel">
			<div class="table-responsive rounded-table">
				<p class="rounded-table-title"><?php echo _('Pacientes'); ?></p>
				<table class="table table-hover" id="users_table">
					<thead>
						<tr id="table-header">
							<th>#</th>
							<th><?php echo _('E-Mail'); ?></th>
							<th><?php echo _('Nombre'); ?></th>
							<th><?php echo _('Apellidos'); ?></th>
							<th><?php echo _('Activado'); ?></th>
							<th><?php echo _('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div class="pagination pagination-centered">
					<ul id="paging">
					</ul>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
			var carer = false;
			
			$("#search-input").keyup(function(event){
			    if(event.keyCode == 13){
			        search();
			    }
			});
			
			function savePatient(th_id, p_id)
			{
				var qry = "user="+encodeURIComponent(th_id)+"&p_id=" + encodeURIComponent(p_id);
					
				$.ajax({
					type: "POST",
					url: "backend/add_patient.php",
					data: qry,
					success: function(html) {
						if(html!='false') //user id
						{
							load_settings_tab('patients');
						}
						else
						{
							sweetAlert({title:"<?php echo _('Ups...'); ?>", text:"<?php echo _('Error desconocido en el servidor. Por favor, inténtalo de nuevo más tarde.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
						}
					}
				});
			}
			
			function addPatient(p_id)
			{
				<?php if ($user->get_therapist()) { ?>
				savePatient("<?php echo $user->get_id(); ?>", p_id);
				//$("#settings-container").load('backend/ui/add_patient_'+(carer?'carer':'therapist')+'.php?patientID='+p_id);
				load_settings_tab('patients');
				<?php } else { ?>
				$("#settings-container").load('backend/ui/add_patient_'+(carer?'carer':'therapist')+'.php?patientID='+p_id);
				<?php } ?>
			}
			
			function search()
			{
				search_page(0);
			}
			
			function search_page(page)
			{
				var text = $("#search-input").val();
				$.ajax({
						type: "GET",
						url: "backend/search_free_patients.php?q="+text+"&l=10&th=0&p="+page+(carer?'&carer=1':''),
						data: null,
						success: function(html) {
							var result = eval('('+html+')');
							reload_table(result);
							
							$("[id^='paging_item_']").removeClass('active');
							$("#paging_item_"+page).addClass('active');
							
							if (page+1 == result.npages)
							{
								$("#right_paging").addClass('disabled');
							}
							else
							{
								$("#right_paging").removeClass('disabled');
							}
							if (page == 0)
							{
								$("#left_paging").addClass('disabled');
							}
							else
							{
								$("#left_paging").removeClass('disabled');
							}
						}
				});
			}
			
			function reload_table(search_result)
			{
				$("#users_table").html("<thead>\
						<tr id=\"table-header\">\
							<th>#</th>\
							<th><?php echo _('E-Mail'); ?></th>\
							<th><?php echo _('Nombre'); ?></th>\
							<th><?php echo _('Apellidos'); ?></th>\
							<th><?php echo _('Activado'); ?></th>\
							<th><?php echo _('Acciones'); ?></th>\
						</tr>\
					</thead><tbody></tbody>");
			
				for (var i=0; i<search_result.elements.length; i++)
				{
					var item = search_result.elements[i];
					var tr = "<tr id='row_user_"+item.id+"'>";
					
					tr += "<td id='column_id_"+item.id+"'>"+item.id+"</td>";
					tr += "<td id='column_email_"+item.id+"'>"+item.email+"</td>";
					tr += "<td id='column_name_"+item.id+"'>"+item.name+"</td>";
					tr += "<td id='column_surname_"+item.id+"'>"+item.surname+"</td>";
					tr += "<td id='column_activated_"+item.id+"'><a href=\"javascript:void(0);\" class=\"btn btn-mini "+((item.status=='1')?"btn-success":"btn-danger")+" disabled\">&nbsp;<i class=\""+((item.status=='1')?"icon-ok":"icon-remove")+" icon-white\"></i>&nbsp;</a></td>";
					tr += "<td><a class='btn btn-mini btn-primary' onclick=\"javascript:addPatient('"+item.id+"');\"><i class='icon-plus icon-white'></i> <?php echo _('Asignar'); ?> "+(carer?'<?php echo _('Cuidador'); ?>':'<?php echo _('Terapeuta'); ?>')+"</a></td>";
					tr += "</tr>";
				
					$('#users_table').find('tbody:last').append(tr);
				}
				
				if (search_result.npages <= 1) $("#paging").html("");
				else
				{
					var pages = "<li id='left_paging' class=\"disabled\"><a href='javascript:prev_page();'>&laquo;</a></li>";
					for (var i=0; i<search_result.npages; i++)
					{
						pages += "<li"+(i==0?" class='active'":"")+" id='paging_item_"+i+"'><a href='javascript:search_page("+i+");'>"+i+"</a></li>";
					}
					pages += "<li id='right_paging'><a href='javascript:next_page();'>&raquo;</a></li>";
					
					$("#paging").html(pages);
				}
			}
			
			function next_page()
			{
				var page = parseInt($("#paging .active").attr('id').split("_")[2]);
				search_page(page+1);
			}
			
			function prev_page()
			{
				var page = parseInt($("#paging .active").attr('id').split("_")[2]);
				search_page(page-1);
			}
			
			$(function() {
				search();
		  	});
		</script>
			
		<?php
				//}
				//else
				//{
		?>
			<!--<p class="lead"><?php echo _('Para asegurar la privacidad de los pacientes, no puede acceder a la lista de pacientes sin terapeuta asignado. Si desea agregar un paciente que ya está registrado en VIRTRA-EL, por favor, <a href="javascript:void(0);" onclick="javascript:$(\'#contactMenuItem\').click();">contacta</a> con el administrador.'); ?></p>-->
		<?php
				//}
			}
		?>
	</div>
	<?php
		}
	?>
</div>

<script type="text/javascript">
<?php
	if ($therapist)
	{
?>
function load_therapists()
{
	$("#settings-container").load('backend/ui/therapists.php');
}
<?php
	}
	else if ($carer)
	{
?>
function load_therapists()
{
	$("#settings-container").load('backend/ui/carers.php');
}
<?php
	}
	else
	{
?>
function existing_patient(existing, carers)
{
	if (carer != carers)
	{
		$("#search-note-id").html(carers?"(<?php echo _('Nota: sólo aparecen los pacientes que no tiene aún asignado un cuidador'); ?>)":"(<?php echo _('Nota: sólo aparecen los pacientes que no tiene aún asignado un terapeuta'); ?>)");
		search();
	}
	
	carer = carers;
	if (existing)
	{
		$("#existing-user-form").removeClass('hidden');
		$("#new-user-form").addClass('hidden');
	}
	else
	{
		$("#existing-user-form").addClass('hidden');
		$("#new-user-form").removeClass('hidden');
	}
}

function load_patients()
{
	$("#settings-container").load('backend/ui/patients.php');
}
<?php
	}
?>
function saveUser(f_id)
{
	var name = $("#form-name").val();
	var surname = $("#form-surname").val();
	var email = $("#form-email").val();
	var therapist = "<?php echo $therapist?'1':'0'; ?>";
	var carer = "<?php echo $carer?'1':'0'; ?>";
	var activated = $("#form-activated").prop('checked')?'1':'0';
	var password = $("#form-pass").val();
	var password2 = $("#form-pass2").val();
	
	if (name == '' || surname == '' || email == '' <?php if (!isset($_GET['editID'])){ echo '|| password==""'; } ?>)
	{
		sweetAlert("<?php echo _('Error'); ?>", "<?php echo _('Todos los campos obligatorios deben estar rellenos.'); ?>", "error");
	}
	else if (password != password2)
	{
		sweetAlert("<?php echo _('Error'); ?>", "<?php echo _('Las contrase&ntilde;as deben coincidir.'); ?>", "error");
	}
	else
	{
		var qry = "user=<?php echo $_SESSION['userID']; ?>&name=" + encodeURIComponent(name) + "&surname=" + encodeURIComponent(surname) + "&email=" + encodeURIComponent(email) + "&th=" + encodeURIComponent(therapist) + "&carer=" + encodeURIComponent(carer) + "&activated=" + encodeURIComponent(activated);
	
		if (f_id!=null)
		{
			qry = qry + "&saveID=" + f_id;
			if (password.length > 0)
			{
				qry = qry + "&pass=" + md5(password);
			}
		}
		else
		{
			qry = qry + "&pass=" + md5(password);
		}
		
		$.ajax({
			type: "POST",
			url: "backend/save_user.php",
			data: qry,
			success: function(html) {
				if(html!='false') //user id
				{
					<?php
						if ($therapist || $carer)
						{
					?>
					load_therapists();
					<?php
						}
						else
						{
					?>
					load_patients();
					<?php
						}
					?>
				}
				else
				{
					sweetAlert({title:"<?php echo _('Ups...'); ?>", text:"<?php echo _('Error desconocido en el servidor. Por favor, inténtalo de nuevo más tarde.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
				}
			}
		});
	}
}
</script>