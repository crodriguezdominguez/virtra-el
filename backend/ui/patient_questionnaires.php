<a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:load_external_url('usability/usability-form.php', '<?php echo _('Cuestionario de evaluación de la aplicación VIRTRA-EL'); ?>');"><i class="icon-pencil"></i>&nbsp;<?php echo _('Evaluación de VIRTRA-EL'); ?></a><br /><br />
<a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:load_external_url('personal-questionnaire/user-questionnaire-1.php', '<?php echo _('Algunas preguntas más sobre tí'); ?>');"><i class="icon-user"></i>&nbsp;<?php echo _('Personal'); ?></a><br /><br />
<a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:load_external_url('nutrition/cuestionario-frecuencia-alimenticia.php', '<?php echo _('Perfil Nutricional'); ?>');"><i class="icon-food"></i>&nbsp;<?php echo _('Perfil Nutricional'); ?></a><br /><br />


<script type="text/javascript">
	function load_external_url(url, title){
		$('#exercise-title').html(title);
		$('#exercise-description').html('<a class="btn btn-danger btn-large" href="javascript:void(0);" onclick="javascript:window.location.reload();"><i class="icon-rotate-left"></i>&nbsp;<?php echo _('Volver a los ejercicios'); ?></a>');
		$('#exercise-container').html('<iframe src="'+url+'" style="width:100%;height:600px;overflow:auto;border:0px;"></iframe>');
		
		$("#settingsMenuModal").modal('toggle');
		toggleFullScreen();
	}
</script>