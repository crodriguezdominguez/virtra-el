<?php
require_once(__DIR__ . '/../../locale/localization.php');
?>

<a href="javascript:void(0);" class="btn btn-danger" onclick="javascript:resetExercises()"
   style="margin-bottom:50px;"><i class="icon-undo"></i> <?php echo _('Reiniciar Pruebas'); ?></a><br/>
<h1><?php echo _('Ir a ejercicio'); ?></h1>
<?php echo _('Sesi&oacute;n:'); ?> <select id="sessionID">
</select>
<br/>
<?php echo _('Ejercicio:'); ?> <select id="exerciseID">
</select><br/>
<a href="javascript:void(0)" onclick="javascript:gotoExercise()" style="margin-top:15px;"
   class="btn btn-primary"><?php echo _('Ir a ejercicio'); ?> <i class="icon-fast-forward"></i></a>
<p class="lead"
   style="margin-top:40px;"><?php echo _('Los ejercicios colaborativos actualmente no se encuentran implementados en el servidor. Por tanto, no se puede acceder a ellos. Los ejercicios de realidad virtual se encuentran en las sesiones 14 y 15.'); ?></p>

<script type="text/javascript" src="js/jquery.xml2json.js"></script>
<script type="text/javascript" src="exercises/exercise-title-mapper.php"></script>
<script type="text/javascript">

    var sessions = null;

    $.ajax({
        type: "GET",
        url: "sessions.php",
        dataType: "xml",
        success: function (xml) {
            sessions = $.xml2json(xml);
            $.each(sessions.session, function (index, elem) {
                var k = index + 1;
                $("#sessionID").append('<option value="' + k + '">' + k + '</option>');
            });

            $("#sessionID").change(function () {
                $("#exerciseID").html("");
                var session = sessions.session[parseInt($("#sessionID").val()) - 1];

                if (session.exercises.exerciseEntry instanceof Array) {
                    $.each(session.exercises.exerciseEntry, function (index, elem) {
                        $("#exerciseID").append('<option value="' + elem.id + '">' + mapExerciseTitle(elem.type) + '</option>');
                    });
                }
                else {
                    var elem = session.exercises.exerciseEntry;
                    $("#exerciseID").append('<option value="' + elem.id + '">' + mapExerciseTitle(elem.type) + '</option>');
                }
            });

            $("#sessionID").change();
        }
    });

    function resetExercises() {
        $('#restore-message').hide();
        $('#goto-message').hide();
        $.ajax({
            type: 'POST',
            data: '',
            url: 'backend/reset_sessionlog.php',
            success: function (result) {
                window.location.hash = 'exercises';
                window.location.reload();
            }
        });
    }
    ;

    function gotoExercise() {
        $('#restore-message').hide();
        $('#goto-message').hide();
        $.ajax({
            type: 'POST',
            data: '',
            url: 'backend/reset_sessionlog.php',
            success: function (result) {
                $.ajax({
                    type: 'POST',
                    data: 'sessionID=' + $('#sessionID').val() + '&exerciseID=' + $('#exerciseID').val(),
                    url: 'backend/goto_session.php',
                    success: function (result) {
                        window.location.hash = 'exercises';
                        window.location.reload();
                    }
                });
            }
        });
    }
    ;
</script>