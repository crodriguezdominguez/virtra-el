<?php
	require_once(__DIR__.'/../../php/User.php');

	session_start();
	
	global $user; $user = null;
	
	if (!isset($_GET['userID']) || !isset($_SESSION['userID'])) exit(0);
	
	$p_id = $_GET['userID'];
	$user = new User($p_id);
	$questionnaire = $user->profile();
?>
<div class="entry-panel">
	<ul class="breadcrumb">
		<li><a href="javascript:void(0);" onclick="javascript:load_patients();"><?php echo _('Pacientes'); ?></a> <span class="divider">/</span></li>
		<li class="active"><?php echo $user->get_surname().', '.$user->get_name(); ?></li>
	</ul>
		<center>
		<div class="btn-group" data-toggle="buttons-radio">
		  <button onclick="javascript:show_statistics(false);" class="btn active"><?php echo _('Resultado de cuestionarios'); ?></button>
		  <button onclick="javascript:show_statistics(true);" class="btn"><?php echo _('Estadísticas y progreso'); ?></button>
		  <?php
			  $currentUser = new User($_SESSION['userID']);
		  ?>
		  <button onclick="javascript:show_carer();" class="btn"><?php echo $currentUser->get_therapist()?_('Cuidador'):_('Terapeuta'); ?></button>
		</div>
		<hr />
		</center>
	<div id="questionnaire-form">
	<?php
		if ($questionnaire !== null)
		{
	?>
		<form method="post" action="javascript:void(0);" class="form-horizontal" id="contact-form">
		<fieldset>
		<h3><?php echo _('Datos generales'); ?></h3>
		<div class="control-group">
			<label class="control-label" for="computerUse"><?php echo _('Ha usado ordenador antes'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="computerUse" name="sex" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['computerUse']=='1') echo 'SELECTED'; ?>><?php echo _('S&iacute;'); ?></option>
					<option value="2" <?php if ($questionnaire['computerUse']=='0') echo 'SELECTED'; ?>><?php echo _('No'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="sex"><?php echo _('Sexo'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="sex" name="sex" disabled="1" readonly>
					<option value="0">-</option>
					<option value="2" <?php if ($questionnaire['sex']=='2') echo 'SELECTED'; ?>><?php echo _('Masculino'); ?></option>
					<option value="1" <?php if ($questionnaire['sex']=='1') echo 'SELECTED'; ?>><?php echo _('Femenino'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group" id="placeOfBirthGroup">
			<label class="control-label" for="placeOfBirth"><?php echo _('Lugar de nacimiento'); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge" id="placeOfBirth" name="placeOfBirth" value="<?php echo $questionnaire['placeOfBirth']; ?>" readonly>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="dateOfBirth"><?php echo _('Fecha de nacimiento (aaaa-mm-dd)'); ?></label>
			<div class="controls">
				<input size="16" type="text" id="dateOfBirth" value="<?php echo $questionnaire['dateOfBirth']; ?>" name="dateOfBirth" readonly>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="civilStatus"><?php echo _('Estado civil'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="civilStatus" name="civilStatus" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['civilStatus']=='1') echo 'SELECTED'; ?>><?php echo _('Soltero/a'); ?></option>
					<option value="2" <?php if ($questionnaire['civilStatus']=='2') echo 'SELECTED'; ?>><?php echo _('Casado/a'); ?></option>
					<option value="3" <?php if ($questionnaire['civilStatus']=='3') echo 'SELECTED'; ?>><?php echo _('Viudo/a'); ?></option>
					<option value="4" <?php if ($questionnaire['civilStatus']=='4') echo 'SELECTED'; ?>><?php echo _('Divorciado/a'); ?></option>
					<option value="5" <?php if ($questionnaire['civilStatus']=='5') echo 'SELECTED'; ?>><?php echo _('Otro/a'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="partner"><?php echo _('Convivo con'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="partner" name="partner" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['partner']=='1') echo 'SELECTED'; ?>><?php echo _('Esposo/a o pareja'); ?></option>
					<option value="2" <?php if ($questionnaire['partner']=='2') echo 'SELECTED'; ?>><?php echo _('Solo/a'); ?></option>
					<option value="3" <?php if ($questionnaire['partner']=='3') echo 'SELECTED'; ?>><?php echo _('Vivo en una residencia'); ?></option>
					<option value="4" <?php if ($questionnaire['partner']=='4') echo 'SELECTED'; ?>><?php echo _('Hijo/a'); ?></option>
					<option value="5" <?php if ($questionnaire['partner']=='5') echo 'SELECTED'; ?>><?php echo _('Otros familiares'); ?></option>
					<option value="6" <?php if ($questionnaire['partner']=='6') echo 'SELECTED'; ?>><?php echo _('Otros'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="levelOfStudies"><?php echo _('Nivel de estudios'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="levelOfStudies" name="levelOfStudies" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['levelOfStudies']=='1') echo 'SELECTED'; ?>><?php echo _('Leer/escribir'); ?></option>
					<option value="2" <?php if ($questionnaire['levelOfStudies']=='2') echo 'SELECTED'; ?>><?php echo _('Estudios primarios'); ?></option>
					<option value="3" <?php if ($questionnaire['levelOfStudies']=='3') echo 'SELECTED'; ?>><?php echo _('Graduado escolar/EGB'); ?></option>
					<option value="4" <?php if ($questionnaire['levelOfStudies']=='4') echo 'SELECTED'; ?>><?php echo _('Bachillerato/COU'); ?></option>
					<option value="5" <?php if ($questionnaire['levelOfStudies']=='5') echo 'SELECTED'; ?>><?php echo _('Formaci&oacute;n profesional'); ?></option>
					<option value="6" <?php if ($questionnaire['levelOfStudies']=='6') echo 'SELECTED'; ?>><?php echo _('Diplomatura'); ?></option>
					<option value="7" <?php if ($questionnaire['levelOfStudies']=='7') echo 'SELECTED'; ?>><?php echo _('Licenciatura'); ?></option>
					<option value="8" <?php if ($questionnaire['levelOfStudies']=='8') echo 'SELECTED'; ?>><?php echo _('Doctorado'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="hoursReading"><?php echo _('Horas semanales dedicadas a...'); ?></label>
			<div class="controls docs-input-sizes">
				<?php echo _('Lectura'); ?> <input type="number" class="input-small" id="hoursReading" name="hoursReading" value="<?php echo $questionnaire['hoursReading'] ?>" min="0" readonly />
				<?php echo _('Talleres (memoria, pintura, teatro, etc.)'); ?> <input type="number" class="input-small" id="hoursWorkshop" name="hoursWorkshop" value="<?php echo $questionnaire['hoursWorkshop'] ?>" min="0" readonly />
				<?php echo _('Ejercicio f&iacute;sico (caminar, gimnasia, etc.)'); ?> <input type="number" class="input-small" id="hoursExercise" name="hoursExercise" value="<?php echo $questionnaire['hoursExercise'] ?>" min="0" readonly />
				<?php echo _('Utilizar el ordenador'); ?> <input type="number" class="input-small" id="hoursComputer" name="hoursComputer" value="<?php echo $questionnaire['hoursComputer'] ?>" min="0" readonly />
			</div>
	    </div>
	    <div class="control-group" id="phoneNumberGroup">
			<label class="control-label" for="phoneNumber"><?php echo _('Tel&eacute;fono'); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge" id="phoneNumber" name="phoneNumber" value="<?php echo $questionnaire['phoneNumber']; ?>" readonly />
			</div>
	    </div>
		<h3><?php echo _('A la hora de ...'); ?></h3>
		<fieldset>
		<div class="control-group">
			<label class="control-label" for="washingHelp"><?php echo _('... ba&ntilde;arme, ¿necesito ayuda?'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="washingHelp" name="washingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['washingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No'); ?></option>
					<option value="2" <?php if ($questionnaire['washingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('S&iacute;'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="clothingHelp"><?php echo _('... vestirme'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="clothingHelp" name="clothingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['clothingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No necesito ayuda'); ?></option>
					<option value="2" <?php if ($questionnaire['clothingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Necesito un poco de ayuda'); ?></option>
					<option value="3" <?php if ($questionnaire['clothingHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Necesito ser vestido por otra persona'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="smartenUPHelp"><?php echo _('... acicalarme, ¿necesito ayuda?'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="smartenUPHelp" name="smartenUPHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['smartenUPHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No'); ?></option>
					<option value="2" <?php if ($questionnaire['smartenUPHelp']=='2') echo 'SELECTED'; ?>><?php echo _('S&iacute;'); ?></option>
				</select>
			</div>
	    </div>
		<div class="control-group">
			<label class="control-label" for="eatingHelp"><?php echo _('... comer'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="eatingHelp" name="eatingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['eatingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No necesito ayuda'); ?></option>
					<option value="2" <?php if ($questionnaire['eatingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Necesito un poco de ayuda'); ?></option>
					<option value="3" <?php if ($questionnaire['eatingHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Necesito ser alimentado por otra persona'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="urinateHelp"><?php echo _('... orinar'); ?></label>
			<div class="controls">
				<select class="input-xlarge" id="urinateHelp" name="urinateHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['urinateHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No sufro episodios de incontinencia'); ?></option>
					<option value="2"  <?php if ($questionnaire['urinateHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Sufro ocasionalmente de alg&uacute;n episodio de incontinencia'); ?></option>
					<option value="3"  <?php if ($questionnaire['urinateHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy incontinente'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="defecateHelp"><?php echo _('... defecar'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="defecateHelp" name="defecateHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['defecateHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No sufro episodios de incontinencia. Si tengo sonda, puedo cambiar la bolsa solo'); ?></option>
					<option value="2" <?php if ($questionnaire['defecateHelp']=='2') echo 'SELECTED'; ?>><?php echo _('No sufro episodios de incontinencia'); ?></option>
					<option value="3" <?php if ($questionnaire['defecateHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy incontinente'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="toiletHelp"><?php echo _('... ir al aseo'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="toiletHelp" name="toiletHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['toiletHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No necesito ayuda'); ?></option>
					<option value="2" <?php if ($questionnaire['toiletHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Necesito un poco de ayuda'); ?></option>
					<option value="3" <?php if ($questionnaire['toiletHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Necesito mucha ayuda'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="movingHelp"><?php echo _('... ir a la cama/sill&oacute;n'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="movingHelp" name="movingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['movingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No necesito ayuda'); ?></option>
					<option value="2" <?php if ($questionnaire['movingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Necesito un poco de ayuda'); ?></option>
					<option value="3" <?php if ($questionnaire['movingHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Necesito mucha ayuda'); ?></option>
					<option value="4" <?php if ($questionnaire['movingHelp']=='4') echo 'SELECTED'; ?>><?php echo _('Necesito ayuda de una gr&uacute;a o de al menos dos personas fuertes'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="walkingHelp"><?php echo _('... andar'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="walkingHelp" name="walkingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['walkingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Puedo andar 50 metros o m&aacute;s sin ayuda'); ?></option>
					<option value="2" <?php if ($questionnaire['walkingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Para andar 50 metros necesito algo de ayuda (por ejemplo, de un andador)'); ?></option>
					<option value="3" <?php if ($questionnaire['walkingHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy independiente en silla de ruedas'); ?></option>
					<option value="4" <?php if ($questionnaire['walkingHelp']=='4') echo 'SELECTED'; ?>><?php echo _('Soy incapaz de andar'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="stepsHelp"><?php echo _('... subir y bajar escaleras'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="stepsHelp" name="stepsHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['stepsHelp']=='1') echo 'SELECTED'; ?>><?php echo _('No necesito ayuda'); ?></option>
					<option value="2" <?php if ($questionnaire['stepsHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Necesito ayuda'); ?></option>
					<option value="3" <?php if ($questionnaire['stepsHelp']=='3') echo 'SELECTED'; ?>><?php echo _('No soy capaz'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="phoneHelp"><?php echo _('... usar el tel&eacute;fono'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="phoneHelp" name="phoneHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['phoneHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de usar el tel&eacute;fono plenamente'); ?></option>
					<option value="2" <?php if ($questionnaire['phoneHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Soy capaz de marcar bien algunos n&uacute;meros familiares'); ?></option>
					<option value="3" <?php if ($questionnaire['phoneHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy capaz de contestar al tel&eacute;fono, pero no marcar'); ?></option>
					<option value="4" <?php if ($questionnaire['phoneHelp']=='4') echo 'SELECTED'; ?>><?php echo _('No soy capaz de usar el tel&eacute;fono'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="shoppingHelp"><?php echo _('... hacer compras'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="shoppingHelp" name="shoppingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['shoppingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Realizo todas las compras necesarias de forma independiente'); ?></option>
					<option value="2" <?php if ($questionnaire['shoppingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Solo realizo peque&ntilde;as compras de forma independiente'); ?></option>
					<option value="3" <?php if ($questionnaire['shoppingHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Necesito ir acompa&ntilde;ado/a para hacer cualquier compra'); ?></option>
					<option value="4" <?php if ($questionnaire['shoppingHelp']=='4') echo 'SELECTED'; ?>><?php echo _('Soy totalmente incapaz de comprar'); ?></option>
				</select>
			</div>
	    </div>
	     <div class="control-group">
			<label class="control-label" for="cookingHelp"><?php echo _('... hacer la comida'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="cookingHelp" name="cookingHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['cookingHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de organizar, preparar y servir las comidas'); ?></option>
					<option value="2" <?php if ($questionnaire['cookingHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Soy capaz de preparar las comidas si me dan los ingredientes'); ?></option>
					<option value="3" <?php if ($questionnaire['cookingHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy capaz de preparar, calentar y servir las comidas, pero no sigo una dieta adecuada'); ?></option>
					<option value="4" <?php if ($questionnaire['cookingHelp']=='4') echo 'SELECTED'; ?>><?php echo _('Necesito que me preparen y sirvan las comidas'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="homecareHelp"><?php echo _('... el cuidado de la casa'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="homecareHelp" name="homecareHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['homecareHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de mantener la casa solo/a o con ayuda ocasional'); ?></option>
					<option value="2" <?php if ($questionnaire['homecareHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Solo soy capaz de hacer tareas ligeras, como limpiar los platos o hacer la cama'); ?></option>
					<option value="3" <?php if ($questionnaire['homecareHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy capaz de hacer tareas ligeras, pero no puedo mantener un adecuado nivel de limpieza'); ?></option>
					<option value="4" <?php if ($questionnaire['homecareHelp']=='4') echo 'SELECTED'; ?>><?php echo _('Necesito ayuda en todas las labores de la casa'); ?></option>
					<option value="5" <?php if ($questionnaire['homecareHelp']=='5') echo 'SELECTED'; ?>><?php echo _('No participo en ninguna labor de la casa'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="clothCleaningHelp"><?php echo _('... lavar la ropa'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="clothCleaningHelp" name="clothCleaningHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['clothCleaningHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de lavar toda mi ropa'); ?></option>
					<option value="2" <?php if ($questionnaire['clothCleaningHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Solo soy capaz de lavar prendas peque&ntilde;as'); ?></option>
					<option value="3" <?php if ($questionnaire['clothCleaningHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Toda la ropa me la debe lavar otra persona'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="transportHelp"><?php echo _('... utilizar medios de transporte'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="transportHelp" name="transportHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['transportHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de viajar solo en transporte p&uacute;blico o soy capaz de conducir mi propio coche'); ?></option>
					<option value="2" <?php if ($questionnaire['transportHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Solo viajo en transporte p&uacute;blico si voy acompa&ntilde;ado'); ?></option>
					<option value="3" <?php if ($questionnaire['transportHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Solo uso el taxi o el autom&oacute;vil con ayuda de otros'); ?></option>
					<option value="4" <?php if ($questionnaire['transportHelp']=='4') echo 'SELECTED'; ?>><?php echo _('No soy capaz de viajar'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="medicationHelp"><?php echo _('... tomar mis medicamentos'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="medicationHelp" name="medicationHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['medicationHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de tomar mi medicaci&oacute;n a la hora y con la dosis correcta'); ?></option>
					<option value="2" <?php if ($questionnaire['medicationHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Solo soy capaz de tomar mi medicaci&oacute;n cuando es preparada previamente por otra persona'); ?></option>
					<option value="3" <?php if ($questionnaire['medicationHelp']=='3') echo 'SELECTED'; ?>><?php echo _('No soy capaz de tomar mi medicaci&oacute;n yo solo'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
			<label class="control-label" for="economyHelp"><?php echo _('... manejar los asuntos econ&oacute;micos'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="economyHelp" name="economyHelp" disabled="1" readonly>
					<option value="0">-</option>
					<option value="1" <?php if ($questionnaire['economyHelp']=='1') echo 'SELECTED'; ?>><?php echo _('Soy capaz de encargarme de mis asuntos econ&oacute;micos'); ?></option>
					<option value="2" <?php if ($questionnaire['economyHelp']=='2') echo 'SELECTED'; ?>><?php echo _('Solo realizo las compras de cada d&iacute;a, pero necesito ayuda en las grandes compras y en los bancos'); ?></option>
					<option value="3" <?php if ($questionnaire['economyHelp']=='3') echo 'SELECTED'; ?>><?php echo _('Soy incapaz de manejar dinero'); ?></option>
				</select>
			</div>
	    </div>
		</fieldset>
		</form>
		<?php
			}
			else
			{
		?>
			<p><?php echo _('El paciente no ha rellenado aún los cuestionarios'); ?></p>
		<?php
			}
		?>
	</div>
	<div class="hidden" id="results-form">
		<?php
			$sessionlog = $user->last_session_log();
			
			$sessionID = 0;
			$exerciseID = 0;
			
			if ($sessionlog != null)
			{
				$sessionID = intval($sessionlog['sessionID']);
				$exerciseID = intval($sessionlog['exerciseID']);
			}
		?>
		<?php
			if ( ($sessionID > 0 || $exerciseID > 0) && ($sessionlog != null) )
			{
		?>
			<div>
				<p><?php echo '<strong>'._('Sesi&oacute;n actual:').'</strong> <span id="session-id">'.$sessionID.'</span>'; ?>, <?php echo '<strong>'._('Ejercicio:').'</strong> <span id="exercise-id"></span>'; ?><a class="btn btn-primary" style="float:right;" id="next-exercise-a" href="javascript:void(0);"><?php echo _('Saltar ejercicio'); ?></a><a class="btn btn-danger" style="float:right; margin-right: 25px;" id="reset-exercise-a" href="javascript:void(0);"><?php echo _('Reiniciar ejercicio'); ?></a></p>
				<hr />
			</div>
			<div id="stats-form">
			</div>
			<script type="text/javascript" src="js/jquery.xml2json.js"></script>
			<script type="text/javascript" src="exercises/exercise-title-mapper.php"></script>
			<script type="text/javascript">
				var lastSession = null;
				var sessions = null;
				$.ajax({
					type: "GET",
					url: "sessions.php",
					dataType: "xml",
					success: function(xml) {
						sessions = $.xml2json(xml);
						<?php
							if ($exerciseID == '0')
							{
						?>
							$("#exercise-id").html("<?php echo _('Introducci&oacute;n a la sesi&oacute;n'); ?>");
						<?php
							}
							else
							{
						?>
						for (var sess in sessions.session)
						{
							if (parseInt(sess)+1 === <?php echo $sessionID; ?>)
							{
								if (sessions.session[sess].exercises.exerciseEntry instanceof Array)
								{
									for (var ex in sessions.session[sess].exercises.exerciseEntry)
									{
										if (sessions.session[sess].exercises.exerciseEntry[ex].id == '<?php echo $exerciseID; ?>')
										{
											$("#exercise-id").html(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry[ex].type));
											break;
										}
									}
								}
								else
								{
									if (sessions.session[sess].exercises.exerciseEntry.id == '<?php echo $exerciseID; ?>')
									{
										$("#exercise-id").html(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry.type));
									}
								}
								
								break;
							}
						}
						<?php
							}
						?>
					}
				});
				
				function getLastSession(successFunction){
					$.ajax({
						type: 'GET',
						dataType: 'json',
						data: '',
						url: 'backend/last_session.php?userID=<?php echo $p_id; ?>',
						success: successFunction,
						async: false
					});
				}
				
				getLastSession(function(result){
					lastSession = result;
					$("#next-exercise-a").click(function(){
						nextExercise();
					});
					
					$("#reset-exercise-a").click(function(){
						resetExercise();
					});
				});
					
				function updateSession(sessionID, exerciseID, result, repetitions, resultFunction) {
					var qry = 'userID=<?php echo $p_id; ?>&sessionID='+sessionID+'&exerciseID='+exerciseID+'&result='+result+'&repetitions='+repetitions;
						
					$.ajax({
						type: 'POST',
						data: qry,
						url: 'backend/update_session.php',
						success: function(res) {
							if (result != 1)
							{
								getLastSession(function(result){
									lastSession = result;
									$("#session-id").html(lastSession['sessionID']);
									if (lastSession['exerciseID'] == '0')
									{
										$("#exercise-id").html("<?php echo _('Introducci&oacute;n a la sesi&oacute;n'); ?>");
									}
									else
									{
										for (var sess in sessions.session)
										{
											if (parseInt(sess)+1 === parseInt(lastSession['sessionID']))
											{
												if (sessions.session[sess].exercises.exerciseEntry instanceof Array)
												{
													for (var ex in sessions.session[sess].exercises.exerciseEntry)
													{
														if (sessions.session[sess].exercises.exerciseEntry[ex].id == lastSession['exerciseID'])
														{
															$("#exercise-id").html(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry[ex].type));
															break;
														}
													}
												}
												else
												{
													if (sessions.session[sess].exercises.exerciseEntry.id == lastSession['exerciseID'])
													{
														$("#exercise-id").html(mapExerciseTitle(sessions.session[sess].exercises.exerciseEntry.type));
													}
												}
												
												break;
											}
										}
									}
								});
							}
						},
						async: false
					});
				};
				
				function resetExercise(){
					if(lastSession)
					{
						swal({
								title: "<?php echo _('¿Estás seguro?'); ?>",
								text: "<?php echo _('¿Estás seguro de que quieres reiniciar los resultados del último ejercicio? Esta operación no puede deshacerse'); ?>.",
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: "<?php echo _('Si, reiniciarlos'); ?>",
								cancelButtonText: "<?php echo _('Cancelar'); ?>",
								closeOnConfirm: true
							},
							function(isConfirm){
								if (isConfirm)
								{
									var sessionID = parseInt(lastSession['sessionID']);
									var exerciseID = parseInt(lastSession['exerciseID']);
									
									var qry = 'patientID=<?php echo $p_id; ?>&sessionID='+sessionID+'&exerciseID='+exerciseID;
									
									$.ajax({
										type: 'POST',
										data: qry,
										url: 'backend/reset_exercise.php',
										success: function(res) {
											loaded = false;
											show_statistics(true);
											swal('<?php echo _('Se han reiniciado correctamente los resultados del último ejercicio'); ?>');
										},
										error : function(e){
											console.log(e);
										},
										async: true
									});
								}
						});
					}
					else
					{
						swal("<?php echo _('El paciente aún no ha comenzado con los ejercicios'); ?>");
					}
				};
				
				function nextExercise() {
					var sessionConfiguration = null;
					medalType = null;
					
					if(lastSession)
					{
						swal({
								title: "<?php echo _('¿Estás seguro?'); ?>",
								text: "<?php echo _('¿Estás seguro de que quieres saltarle al paciente el ejercicio actual? Esta operación no puede deshacerse'); ?>.",
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: "<?php echo _('Si, saltar ejercicio'); ?>",
								cancelButtonText: "<?php echo _('Cancelar'); ?>",
								closeOnConfirm: true
							},
							function(isConfirm){
								if (isConfirm)
								{
									var sessionID = parseInt(lastSession['sessionID']);
									var exerciseID = parseInt(lastSession['exerciseID']);
									
									if (exerciseID > 0)
									{
										if (sessions.session[sessionID-1].exercises.exerciseEntry instanceof Array)
										{
											exerciseEntry = sessions.session[sessionID-1].exercises.exerciseEntry[exerciseID-1];
										}
										else
										{
											exerciseEntry = sessions.session[sessionID-1].exercises.exerciseEntry;
										}
										
										var repetition = parseInt(lastSession['repetitions']);
										
										updateSession(sessionID, exerciseID, 1, exerciseEntry.repetitions, function(result){ lastSession=result; });
												
										if ((sessions.session[sessionID-1].exercises.exerciseEntry instanceof Array) && (exerciseID < sessions.session[sessionID-1].exercises.exerciseEntry.length) ) //take next exercise
										{
											updateSession(sessionID, exerciseID+1, 0, 0, function(result){ lastSession=result; });
										}
										else
										{
											if (sessionID < sessions.session.length) //take next session
											{
												updateSession(sessionID+1, 0, 0, 0, function(result){ lastSession=result; });
											}
											else
											{
												//finish VIRTRA-EL
												$("#next-exercise-a").attr('disabled', '1');
												$("#reset-exercise-a").attr('disabled', '1');
											}
										}
									}
									else
									{
										updateSession(sessionID, 0, 1, 0, null); //complete introductory screen
										updateSession(sessionID, 1, 0, 0, null); //start first exercise
										getLastSession(function(result){
											lastSession = result;
											nextExercise();
										});
									}
								}
						});
					}
				};
			</script>
		<?php
			}
			else echo '<div><p>'._('El paciente no ha comenzado a&uacute;n los ejercicios').'</p><hr /></div>';
		?>
	</div>
	<div id="carer-form" class="hidden">
		<?php
			$carer = $currentUser->get_therapist()?$user->get_carer_user():$user->get_therapist_user();
			if ($carer != null)
			{
				$therapist = new User($_SESSION['userID']);
		?>
		<p class="lead"><?php echo ($currentUser->get_therapist()?_('Puede contactar con el cuidador de este paciente a través del siguiente formulario:'):_('Puede contactar con el terapeuta de este paciente a través del siguiente formulario:')); ?></p>
		<form action="javascript:checkForm();" class="form-horizontal" id="contact-form">
			<fieldset>
			<div class="control-group" id="name-group">
				<label class="control-label" for="name"><?php echo _('Nombre'); ?></label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="name" name="name" readonly value="<?php echo $therapist->get_surname().', '.$therapist->get_name(); ?>">
				</div>
		    </div>
		    <div class="control-group" id="email-group">
				<label class="control-label" for="email"><?php echo _('E-Mail'); ?></label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="email" name="email" readonly value="<?php echo $therapist->get_username(); ?>">
				</div>
		    </div>
		    <div class="control-group" id="subject-group">
				<label class="control-label" for="subject"><?php echo _('Asunto'); ?></label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="subject" name="subject" readonly value="<?php echo ($currentUser->get_therapist()?_('Consulta al cuidador del usuario'):_('Consulta al terapeuta del usuario')).' '.$user->get_name().' '.$user->get_surname().' ('.$user->get_username().')'; ?>">
				</div>
		    </div>
		    <div class="control-group" id="message-group">
		    	<label class="control-label" for="message"><?php echo _('Texto'); ?></label>
				<div class="controls">
					<textarea class="input-xlarge" id="message" name="message" rows="5"></textarea>
				</div>
		    </div>
		    <div class="control-group" id="captcha-group">
				<label class="control-label" id="captcha-label" for="captcha"></label>
				<div class="controls">
					<input type="text" class="input-mini" maxlength="2" id="captcha" name="captcha">
					<span class="help-inline"><?php echo _('Esto evita que se haga un mal uso del formulario de contacto.'); ?></span>
				</div>
		    </div>
		    <div class="control-group">
			    <div class="controls">
					<button type="submit" class="btn btn-primary"><?php echo _('Enviar'); ?></button>
					<button type="reset" class="btn" id="reset-button"><?php echo _('Limpiar'); ?></button>
				</div>
			</div>
			</fieldset>
		</form>
		<p id="result-form"></p>
		<script type="text/javascript">
			var c;
			reloadCaptcha();
			
			function reloadCaptcha()
			{
				var a = Math.ceil(getRandom() * 10);
				var b = Math.ceil(getRandom() * 10);
				c = a + b;
				
				$('#captcha-label').html("<?php echo _('Resultado de'); ?>"+' '+a+' + '+b);
			};
			
			function checkForm()
			{
				var subject = $('#subject').val();
				var name = $('#name').val();
				var email = $('#email').val();
				var message = $('#message').val();
				var captcha = $('#captcha').val();
			
				$(".error").removeClass('error');
				if (name.length <= 0)
				{
					$('#name-group').addClass('error');
				}
				else if (email.length <= 0)
				{
					$('#email-group').addClass('error');
				}
				else if (subject.length <= 0)
				{
					$('#subject-group').addClass('error');
				}
				else if (message.length <= 0)
				{
					$('#message-group').addClass('error');
				}
				else if (captcha.length <= 0 || parseInt(captcha) != c)
				{
					$('#captcha-group').addClass('error');
				}
				else
				{
					var qry = 'subject='+encodeURIComponent(subject)+'&'+
							  'name='+encodeURIComponent(name)+'&'+
							  'email='+encodeURIComponent(email)+'&'+
							  'message='+encodeURIComponent(message)+'&'+
							  'dest='+encodeURIComponent('<?php echo $carer->get_username(); ?>');
						
					$.ajax({
						type: 'POST',
						data: qry,
						url: 'backend/contact.php',
						success: function(res) {
							$('#result-form').removeClass('contact-form-ok').removeClass('contact-form-error');
							
							if (res=='false')
							{
								$('#result-form').addClass('contact-form-error').html("<?php echo _('Ocurri&oacute; un error al enviar el mensaje. Por favor, int&eacute;ntelo m&aacute;s tarde.'); ?>");
							}
							else
							{
								reloadCaptcha();
								$('#reset-button').click();
								
								$('#result-form').addClass('contact-form-ok').html("<?php echo _('El mensaje se envi&oacute; correctamente.'); ?>");
							}
						}
					});
				}
			};
						
			</script>
		<?php
			}
			else
			{
		?>
			<p><?php echo _('El paciente no tiene un cuidador asignado'); ?></p>
		<?php
			}
		?>
	</div>
</div>

<script type="text/javascript">
var loaded = false;

function show_statistics(sta)
{
	$("#carer-form").addClass('hidden');
	
	if (sta)
	{
		if (!loaded)
		{
			$("#stats-form").load('backend/ui/results.php?patientID=<?php echo $p_id; ?>');
			loaded = true;
		}
		
		$("#results-form").removeClass('hidden');
		$("#questionnaire-form").addClass('hidden');
	}
	else
	{
		$("#results-form").addClass('hidden');
		$("#questionnaire-form").removeClass('hidden');
	}
}

function show_carer()
{
	$("#results-form").addClass('hidden');
	$("#questionnaire-form").addClass('hidden');
	$("#carer-form").removeClass('hidden');
}

function load_patients()
{
	$("#settings-container").load('backend/ui/patients.php');
}

</script>