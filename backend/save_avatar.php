<?php
	require_once(__DIR__.'/../php/User.php');

	session_start();
			
	if (isset($_SESSION['userID']) && isset($_POST['avatar']) && isset($_POST['user']))
	{
		if ($_POST['user']==$_SESSION['userID'])
		{
			$user = new User($_POST['user']);
			$user->set_avatar($_POST['avatar']);
			echo 'true';
		}
		else echo 'false';
	}
	else echo 'false';
?>