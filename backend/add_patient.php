<?php
	require_once(__DIR__.'/../php/User.php');

	session_start();
			
	if (isset($_SESSION['userID']) && isset($_POST['user']))
	{
		$creator = new User($_SESSION['userID']);
		$p_id = $_POST['p_id'];
		$carer = (isset($_POST['carer']) && $_POST['carer']=='1')?true:false;
		
		if ( $creator->get_therapist() && $_SESSION['userID'] == $_POST['user'] )
		{
			$user = new User($p_id);
			$user->set_therapist_user($creator);
			$user->save();
			
			echo 'true';
		}
		else if ( $creator->get_carer() && $_SESSION['userID'] == $_POST['user'] )
		{
			$user = new User($p_id);
			$user->set_carer_user($creator);
			$user->save();
			
			echo 'true';
		}
		else if ($creator->get_superuser())
		{
			$th = new User($_POST['user']);
			
			$user = new User($p_id);
			if ($carer)
			{
				$user->set_carer_user($th);
			}
			else $user->set_therapist_user($th);
			$user->save();
			
			echo 'true';
		}
		else echo 'false';
	}
	else echo 'false';
?>