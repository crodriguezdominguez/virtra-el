<?php
	require_once(__DIR__.'/dbobject.php');
	class User extends DBObject {
		private $name;
		private $surname;
		private $username;
		private $password;
		private $superuser;
		private $therapist;
		private $carer;
		private $activated;
		
		function __construct($id=-1){
			parent::__construct($id);
		}
		
		function __destruct(){
			parent::__destruct();
		}
		
		protected function default_builder(){
			$this->username = '';
			$this->password = '';
			$this->name = '';
			$this->surname = '';
			$this->superuser = false;
			$this->therapist = false;
			$this->activated = false;
			$this->carer = false;
		}
		
		protected function fetch_builder(){
			$id = $this->id;
			//echo "SELECT name,surname,email,hashedPassword,admin,therapist,carer,status FROM user WHERE id='$id'";
			$result = $this->query("SELECT name,surname,email,hashedPassword,admin,therapist,carer,status FROM user WHERE id='$id'");
			
			if (count($result) > 0)
			{
				$row = $result[0];
				$this->name = $row['name'];
				$this->surname = $row['surname'];
				$this->username = $row['email'];
				$this->password = $row['hashedPassword'];
				$this->superuser = $row['admin']=='1'?true:false;
				$this->activated = $row['status']=='1'?true:false;
				$this->therapist = $row['therapist']=='1'?true:false;
				$this->carer = $row['carer']=='1'?true:false;
			}
		}
		
		public function get_name(){
			return $this->name;
		}
		
		public function set_name($name){
			$this->updated = true;
			$this->name = $name;
		}
		
		public function get_surname(){
			return $this->surname;
		}
		
		public function set_surname($name){
			$this->updated = true;
			$this->surname = $name;
		}
		
		public function get_username(){
			return $this->username;
		}
		
		public function set_username($name){
			$this->updated = true;
			$this->username = $name;
		}
		
		public function set_md5_password($md5password) {
			$this->updated = true;
			$this->password = $md5password;
		}
		
		public function get_md5_password() {
			return $this->password;
		}
		
		public function set_superuser($superuser){
			$this->updated = true;
			$this->superuser = $superuser;
		}
		
		public function get_superuser(){
			return $this->superuser;
		}
		
		public function set_therapist($therapist){
			$this->updated = true;
			$this->therapist = $therapist;
		}
		
		public function get_therapist(){
			return $this->therapist;
		}
		
		public function set_carer($carer){
			$this->updated = true;
			$this->carer = $carer;
		}
		
		public function get_carer(){
			return $this->carer;
		}
		
		public function get_activated(){
			return $this->activated;
		}
		
		public function set_carer_user($user){
			$therapist_id = $user->id;
			$patient_id = $this->id;
			$this->query("INSERT INTO carer_patient VALUES('$therapist_id', '$patient_id')");
		}
		
		public function get_carer_user() {
			$id = $this->id;
			$result = $this->query("SELECT carer_id FROM carer_patient WHERE patient_id='$id'");
			if (count($result) > 0)
			{
				$row = $result[0];
				return new User($row['carer_id']);
			}
			else return null;
		}
		
		public function set_therapist_user($user){
			$therapist_id = $user->id;
			$patient_id = $this->id;
			$this->query("INSERT INTO therapist_patient VALUES('$therapist_id', '$patient_id')");
		}
		
		public function get_therapist_user() {
			$id = $this->id;
			$result = $this->query("SELECT therapist_id FROM therapist_patient WHERE patient_id='$id'");
			if (count($result) > 0)
			{
				$row = $result[0];
				return new User($row['therapist_id']);
			}
			else return null;
		}
		
		public function add_patient($patient){
			if ($this->get_therapist())
			{
				$patient->set_therapist_user($this);
			}
			else if ($this->get_carer())
			{
				$patient->set_carer_user($this);
			}
		}
		
		public function set_avatar($avatar_url){
			$id = $this->id;
			$result = $this->query("SELECT user_id FROM user_avatar WHERE user_id='$id'");
			if (count($result) > 0)
			{
				$this->query("UPDATE user_avatar SET avatar='$avatar_url' WHERE user_id='$id'");
			}
			else
			{
				$this->query("INSERT INTO user_avatar VALUES('$id', '$avatar_url')");
			}
		}
		
		public function get_avatar()
		{
			$id = $this->id;
			$result = $this->query("SELECT avatar FROM user_avatar WHERE user_id='$id'");
			if (count($result) > 0)
			{
				$row = $result[0];
				return $row['avatar'];
			}
			else return 'img/pepa.png';
		}
		
		public static function remove_user($user)
		{
			$id = $user->get_id();
			
			$user->query("DELETE FROM therapist_patient WHERE therapist_id='$id'");
			$user->query("DELETE FROM carer_patient WHERE carer_id='$id'");
			$user->query("DELETE FROM user WHERE id='$id'");
		}
		
		public function remove_patient($patient){
			if ($this->get_therapist())
			{
				$therapist_id = $this->id;
				$patient_id = $patient->id;
				$this->query("DELETE FROM therapist_patient WHERE patient_id='$patient_id' AND therapist_id='$therapist_id'");
			}
			else if ($this->get_carer())
			{
				$therapist_id = $this->id;
				$patient_id = $patient->id;
				$this->query("DELETE FROM carer_patient WHERE patient_id='$patient_id' AND carer_id='$therapist_id'");
			}
		}
		
		public function get_patients() {
			if ($this->get_therapist())
			{
				$id = $this->id;
				$result = $this->query("SELECT patient_id FROM therapist_patient WHERE therapist_id='$id'");
				if (count($result) > 0)
				{
					$res = array();
					foreach ($result as $row)
					{
						array_push($res, new User($row['patient_id']));
					}
					return $res;
				}
				else return array();
			}
			else if ($this->get_carer())
			{
				$id = $this->id;
				$result = $this->query("SELECT patient_id FROM carer_patient WHERE carer_id='$id'");
				if (count($result) > 0)
				{
					$res = array();
					foreach ($result as $row)
					{
						array_push($res, new User($row['patient_id']));
					}
					return $res;
				}
				else return array();
			}
			else return array();
		}
		
		public function set_activated($activated){
			$this->updated = true;
			$this->activated = $activated;
		}
		
		public function activate($activation_key){
			if ($this->activated) return TRUE;
			else
			{
				$username = $this->username;
				$password = $this->password;
				$result = $this->query("SELECT activationKey FROM user WHERE email='$username' and hashedPassword='$password'");
				if (count($result) > 0)
				{
					$row = $result[0];
					$key = $row['activationKey'];
					if ($key === $activation_key)
					{
						$id = $this->id;
						$this->query("UPDATE user SET status='1' WHERE id='$id'");
					
						return TRUE;
					}
					else return FALSE;
				}
				else return FALSE;
			}
		}
		
		public static function therapists() {
			$user = new User();
			$result = $user->query("SELECT id FROM user WHERE therapist='1'");
			if (count($result) > 0)
			{
				$res = array();
				foreach($result as $row)
				{
					array_push($res, new User($row['id']));
				}
				
				return $res;
			}
			else return array();
		}
		
		public static function carers() {
			$user = new User();
			$result = $user->query("SELECT id FROM user WHERE carer='1'");
			if (count($result) > 0)
			{
				$res = array();
				foreach($result as $row)
				{
					array_push($res, new User($row['id']));
				}
				
				return $res;
			}
			else return array();
		}
		
		public static function login($username, $password) {
			$user = new User(); //dummy user, only for queries
			$result = $user->query("SELECT id FROM user WHERE email='$username' and hashedPassword='$password'");
			if (count($result) > 0)
			{
				$row = $result[0];
				$user = new User($row['id']);
				
				return $user;
			}
			else return FALSE;
		}
		
		public function profile() {
			$id = $this->id;
			$result = $this->query("SELECT * FROM userProfile WHERE userID='$id'");
			if (count($result) > 0)
			{
				return $result[0];
			}
			else return null;
		}
		
		public function results() {
			$id = $this->id;
			$result = $this->query("SELECT sessionID,exerciseID,countCorrects,countFails,countOmissions,finalScore,seconds FROM exerciseResult WHERE userID='$id' ORDER BY sessionID ASC, exerciseID ASC");
			$res = array();
			foreach($result as $row)
			{
				$session = $row['sessionID'];
				$exercise = $row['exerciseID'];
				if (!array_key_exists($session, $res))
				{
					$res[$session] = array();
				}
				if (!array_key_exists($exercise, $res[$session]))
				{
					$res[$session][$exercise] = array('corrects'=>$row['countCorrects'], 'fails'=>$row['countFails'], 'omissions'=>$row['countOmissions'], 'score'=>$row['finalScore'], 'seconds'=>$row['seconds']);
				}
			}
			return $res;
		}
		
		const STAT_AVG = 'AVG';
		const STAT_DESV = 'STDDEV';
		
		public function allResults($stat_func=User::STAT_AVG, $group_by_exercises=false, $filter=null, $value=null, $comp='=') {
			if (!$this->get_carer() && !$this->get_therapist())
			{
				return array();
			}
			
			if ($filter == 'age')
			{
				$filter = 'dateOfBirth';
				$seconds_for_age = ceil(intval($value) * 365.256366 * 24 * 3600);
				$value = time()-$seconds_for_age; //date of birth in epochs
				$value = date('Y-m-d', $value);
				
				if ($comp == '>')
				{
					$comp = '<=';
				}
				else if ($comp == '<')
				{
					$comp = '>=';
				}
				else if ($comp == '>=')
				{
					$comp = '<';
				}
				else if ($comp == '<=')
				{
					$comp = '>';
				}
			}
			
			$table_name = 'therapist_patient';
			$entry_name = 'therapist_id';
			
			if ($this->get_carer())
			{
				$table_name = 'carer_patient';
				$entry_name = 'carer_id';
			}
			
			$id = $this->id;
			$result = null;
			if ($group_by_exercises)
			{
				$result = $this->query("SELECT exerciseID, $stat_func(countCorrects) AS corrects, $stat_func(countFails) AS fails, $stat_func(countOmissions) AS omissions, $stat_func(finalScore) AS score, $stat_func(seconds) AS seconds FROM exerciseResult WHERE ".(($filter != null && $value != null)?"userID IN (SELECT userID FROM userProfile WHERE $filter$comp'$value') AND ":'')."userID IN (SELECT patient_id AS userID FROM $table_name WHERE $entry_name='$id') GROUP BY exerciseID ORDER BY exerciseID ASC");
			}
			else
			{
				$result = $this->query("SELECT sessionID, exerciseID, $stat_func(countCorrects) AS corrects, $stat_func(countFails) AS fails, $stat_func(countOmissions) AS omissions, $stat_func(finalScore) AS score, $stat_func(seconds) AS seconds FROM exerciseResult WHERE ".(($filter != null && $value != null)?"userID IN (SELECT userID FROM userProfile WHERE $filter$comp'$value') AND ":'')."userID IN (SELECT patient_id AS userID FROM $table_name WHERE $entry_name='$id') GROUP BY sessionID, exerciseID ORDER BY sessionID ASC, exerciseID ASC");
			}
			
			$res = array();
			
			if ($group_by_exercises)
			{
				foreach($result as $row)
				{
					$exercise = $row['exerciseID'];
					if (!array_key_exists($exercise, $res))
					{
						$res[$exercise] = array('corrects'=>$row['corrects'], 'fails'=>$row['fails'], 'omissions'=>$row['omissions'], 'score'=>$row['score'], 'seconds'=>$row['seconds']);
					}
				}
			}
			else
			{
				foreach($result as $row)
				{
					$session = $row['sessionID'];
					$exercise = $row['exerciseID'];
					if (!array_key_exists($session, $res))
					{
						$res[$session] = array();
					}
					if (!array_key_exists($exercise, $res[$session]))
					{
						$res[$session][$exercise] = array('corrects'=>$row['corrects'], 'fails'=>$row['fails'], 'omissions'=>$row['omissions'], 'score'=>$row['score'], 'seconds'=>$row['seconds']);
					}
				}
			}
			return $res;
		}
		
		public function last_session_log()
		{
			$id = $this->id;
			$result = $this->query("SELECT sessionID,exerciseID,repetitions,startTime,endTime,result FROM sessionlog WHERE userID='$id' and result!='1' ORDER BY endTime DESC");
			if (count($result) > 0)
			{
				return $result[0];
			}
			else return null;
		}
		
		public function results_for_session($session) {
			$id = $this->id;
			$result = $this->query("SELECT exerciseID,countCorrects,countFails,countOmissions,finalScore,seconds FROM exerciseResult WHERE userID='$id' AND sessionID='$session' ORDER BY sessionID ASC, exerciseID ASC");
			$res = array();
			foreach($result as $row)
			{
				$exercise = $row['exerciseID'];
				if (!array_key_exists($exercise, $res))
				{
					$res[$exercise] = array('corrects'=>$row['countCorrects'], 'fails'=>$row['countFails'], 'omissions'=>$row['countOmissions'], 'score'=>$row['finalScore'], 'seconds'=>$row['seconds']);
				}
			}
			return $res;
		}
		
		public function insert_object(){
			$username = $this->username;
			$name = $this->name;
			$surname = $this->surname;
			$password = $this->password;
			$superuser = $this->superuser?'1':'0';
			$therapist = $this->therapist?'1':'0';
			$activated = $this->activated?'1':'0'; //not activated by default
			$carer = $this->carer?'1':'0';
			
			$activation_key = sha1(mt_rand(10000,99999).time().$name);
		
			$this->query("INSERT INTO user VALUES ('', '$name', '$surname', '$username', '$password', '$superuser', '$therapist', '$carer', '$activation_key', '$activated')");
			$this->id = $this->link->get_last_insert_id();
		}
		
		public function update_object() {
			$id = $this->id;
			$username = $this->username;
			$name = $this->name;
			$surname = $this->surname;
			$password = $this->password;
			$superuser = $this->superuser?'1':'0';
			$therapist = $this->therapist?'1':'0';
			$activated = $this->activated?'1':'0';
			$carer = $this->carer?'1':'0';
		
			$this->query("UPDATE user SET name='$name', email='$username', surname='$surname', hashedPassword='$password', admin='$superuser', therapist='$therapist', carer='$carer', status='$activated' WHERE id='$id'");
		}
	}
?>