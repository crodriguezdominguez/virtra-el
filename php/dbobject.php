<?php
	class mysqliSingleton
	{
	    private static $instance;
	    private $connection;
	    private static $counter;
	
	    private function __construct()
	    {
		    require(__DIR__.'/../backend/config.php');
		    $this->counter = 0;
	        $this->connection = new mysqli($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_DBNAME);
	        $this->connection->set_charset('utf8');
	    }
	    
	    public function close(){
		    self::$counter--;
		    if (self::$counter <= 0 && !is_null(self::$instance))
		    {
			    $this->connection->close();
			    self::$instance = null;
			    self::$counter = 0;
		    }
	    }
	
	    public static function init()
	    {
	        if(is_null(self::$instance))
	        {
	            self::$instance = new mysqliSingleton();
	        }
	
			self::$counter++;
	        return self::$instance;
	    }
	    
	    public function get_last_insert_id() {
		    return $this->connection->insert_id;
	    }
	    
	    public function __call($name, $args)
	    {
	        if(method_exists($this->connection, $name))
	        {
	            return call_user_func_array(array($this->connection, $name), $args);
	        } else {
	            return false;
	        }
	    }
	}
	
	abstract class DBObject {
		protected $link;
		protected $id;
		protected $updated;
		
		function __construct($id=-1){
			$this->connect_link();
			$this->id=$id;
			
			if ($id==-1)
			{
				$this->updated = true;
				$this->default_builder();
			}
			else
			{
				$this->updated = false;
				$this->fetch_builder();
			}
		}
		
		function __destruct() {
			if ($this->link)
			{
				$this->link->close();
			}
		}
		
		abstract protected function insert_object();
		abstract protected function update_object();
		
		abstract protected function default_builder();
		abstract protected function fetch_builder();
		
		protected function connect_link(){
			$this->link = mysqliSingleton::init(); 
		}
		
		public function get_id(){
			return $this->id;
		}
		
		public function save() {
			if ($this->updated)
			{
				if ($this->id == -1)
				{
					$this->insert_object();
				}
				else
				{
					$this->update_object();
				}
				
				$this->updated = false;
			}
		}
		
		public function query($query){
			$ret = array();
			
			$result = $this->link->query($query);
			if ($result === FALSE || $result === NULL)
			{
				return $ret;
			}
			else
			{
				if ($result === TRUE) return $ret;
			
				while ($row = $result->fetch_assoc())
				{
					array_push($ret, $row);
				}
				
				$result->close();
				
				return $ret;
			}
		}
	}
?>