var numPages = $('.page').length;


function checkRadiobuttons(radioName){

	//Make the radio selected as class freqChecked
	$('input[type=radio]').parent('label').removeClass('freqChecked');
	$('input[type=radio]:checked').parent('label').addClass('freqChecked');
	
	checkPage();

}


function checkPage(){

	//alert($('.active').next('.page').prop('tagName'));

	if ($('.active').find('.food').length == $('.active').find('.freqChecked').length ){
		if ($('.active').next('.page').prop('tagName') !== undefined)		
			$('.next > button').removeClass('hidden');
		else{
			$("#psubmit").show();
			$('.previous > button').removeClass('hidden');
		}
	}
	else {
		$('.next > button').addClass('hidden');
	}


	//Páginas inicial y final	
	if ($('.active').prev('.page').prop('tagName') === undefined){
		$('.previous > button').addClass('hidden');
	}

	if ($('.active').next('.page').prop('tagName') === undefined){
		$('.next > button').addClass('hidden');
	}

}