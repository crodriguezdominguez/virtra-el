
<?php
	require_once('../locale/localization.php');
?>
<?php
	 $headers = array('Content-type: application/json');
	 $nutprofile_service_url = 'http://asistic.ugr.es:8080/NutElCare/rest/userController/getNutProfile';
	 $curl = curl_init($nutprofile_service_url);
     $curl_post_data = array(
            "idUser" => "100",
            );
     
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($curl, CURLOPT_POST, true);     
     curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
     $jsonData = json_encode($curl_post_data);
     curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
     $curl_response = curl_exec($curl);

     if($curl_response === false)
	{
    		echo 'Curl error: ' . curl_error($curl);
	}
	else
	{
		$nutProfile=$curl_response;	
		$nutProfile=json_decode($curl_response);
	}

     curl_close($curl);
  
	

	//echo $jsonData->idUser;
	
	//if (isset ($nutProfile)) echo "HOLA";
?>

<!DOCTYPE html>
<html lang="es">
		<head>
		<title>Perfile Nutricional</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<link rel="stylesheet" href="../css/sweet-alert.css">
	    <link rel="stylesheet" href="../css/bootstrap-combined.no-icons.min.css">
	    <link rel="stylesheet" href="../css/virtrael.css">
	    <link rel="stylesheet" href="css/nutelcare.css">
	    <link rel="stylesheet" href="../css/datepicker.css">
	    <link rel="stylesheet" href="../css/font-awesome.min.css">
	
			<!-- scripts -->
		<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="../js/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="../js/jquery.ui.touch-punch.min.js"></script>
	    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="../js/sweet-alert.min.js"></script>
	    <script type="text/javascript" src="js/nutelcare.js"></script>
    </head>
	<body>
	<div id="wrap">		
		<div id="bodyContainer" class="container" style="overflow:hidden;padding: 0;">		
		  <div style="border:1px solid #e5e5e5;margin-top:10px;">
		  <!-- CONTENTS -->
		  <div class="row-fluid">
			<div class="span6">
		
			<form method="POST" id="formProfile" class="form-horizontal nut-profile" style="padding-top:10px;">
				<input type="hidden" name="id" value="<?php if (isset($nutProfile)) echo  _($nutProfile->id)?>">
				<input type="hidden" name="idUser" value="<?php if (isset($nutProfile)) echo  $curl_post_data['idUser']?>">
			  <div class="form-group" style="width:100%; float:left">
			    <label class="col-sm-2 control-label" for="contrasena">Peso: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			    <div class="col-sm-10"><input type="number" class="form-control" name="weight" placeholder="0" value="<?php if (isset($nutProfile)) echo  _($nutProfile->weight)?>" required> (Kg)</div>
			  </div>
			   <div class="form-group" style="width:100%; float:left">
			    <label class="col-sm-2 control-label" for="height">Altura: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			    <div class="col-sm-10"><input type="text" step="any" class="form-control" name="height"  value="<?php if (isset($nutProfile)) echo _($nutProfile->height)?>" required> (m)</div>
			  </div>
			   <div class="form-group" style="width:100%; float:left">
			    <label class="col-sm-2 control-label" for="height">Cintura: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			    <div class="col-sm-10"><input type="number" class="form-control" name="waist" placeholder="0" value="<?php if (isset($nutProfile)) echo _($nutProfile->waist)?>" (cm)</div>
			  </div>
			   <div class="form-group" style="width:100%; float:left">
			   	 <label class="col-sm-2 control-label" for="masticacion">Masticación: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			     <div class="col-sm-10  inline">		      
			     	<?php if (isset($nutProfile)){?>     
				    	<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="chewing" value="4" <?php if ($nutProfile->chewingLevel==4) echo 'checked'; ?> required>Buena&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="chewing" value="3" <?php if ($nutProfile->chewingLevel==3) echo 'checked'; ?>>Regular&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="chewing" value="2" <?php if ($nutProfile->chewingLevel==2) echo 'checked'; ?>>Mala&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<?php }else{?>
				    	<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="chewing" value="4" required>Buena&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="chewing" value="3">Regular&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="chewing" value="2">Mala&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<?php } ?>
				</div>
			  </div>
			   <div class="form-group" style="width:100%; float:left">
			   	 <label class="col-sm-2 control-label" for="deglucion">Deglución: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			     <div class="col-sm-10  inline">		      
			     	<?php if (isset($nutProfile)){?>     
				    	<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="swallowing" value="4" <?php if ($nutProfile->swallowingLevel==4) echo 'checked'; ?> required>Buena&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="swallowing" value="3" <?php if ($nutProfile->swallowingLevel==3) echo 'checked'; ?>>Regular&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="swallowing" value="2" <?php if ($nutProfile->swallowingLevel==2) echo 'checked'; ?>>Mala&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<?php }else{?>
				    	<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="swallowing" value="4" required>Buena&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="swallowing" value="3">Regular&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Mastica sin dificultad alimentos DUROS" type="radio" name="swallowing" value="2">Mala&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<?php } ?>
				</div>
			  </div>
			   <div class="form-group" style="width:100%; float:left">
			   	 <label class="col-sm-2 control-label" for="ejercicio">Ejercicio: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			     <div class="col-sm-10  inline">		  
			     	<?php if (isset($nutProfile)){?>      
				    	<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Horas ejercicio fisico" type="radio" name="activity" value="4" <?php if ($nutProfile->activityLevel==4) echo 'checked'; ?> required>Buena&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Horas ejercicio fisico" type="radio" name="activity" value="3" <?php if ($nutProfile->activityLevel==3) echo 'checked'; ?>>Regular&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Horas ejercicio fisico" type="radio" name="activity" value="2" <?php if ($nutProfile->activityLevel==2) echo 'checked'; ?>>Mala&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<?php }else{?>
				     	<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Horas ejercicio fisico" type="radio" name="activity" value="2" required>Activo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>    			    
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Horas ejercicio fisico" type="radio" name="activity" value="3">Estándar&nbsp;&nbsp;</label>
						<label class="radio inline"><input class="nut-tooltip" data-toggle="tooltip" title="Horas ejercicio fisico" type="radio" name="activity" value="4">Sedentario</label>					
					<?php } ?>
				 </div>
			   </div>
			   <div class="form-group" style="width:100%; float:left">
			   	 <label class="col-sm-2 control-label" for="intoleracias">Intolerancias: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			     <div class="col-sm-10"><input type="text" class="form-control" name="intolerances" placeholder="alimento"> <a href="#" class="green-text"><strong>+</strong></a></div>
			   </div>
			   <div class="form-group" style="width:100%; float:left">
			   	 <label class="col-sm-2 control-label" for="alergias">Alergias: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			     <div class="col-sm-10"><input type="text" class="form-control" name="alergies" placeholder="alimento"> <a href="#" class="green-text"><strong>+</strong></a></div>
			   </div>
			   <div class="form-group" style="width:100%; float:left">
			   	 <label class="col-sm-2 control-label" for="dislikes">No me gusta: &nbsp;&nbsp;&nbsp;&nbsp;</label>
			     <div class="col-sm-10"><input type="text" class="form-control" name="dislikes" placeholder="alimento"> <a href="#" class="green-text"><strong>+</strong></a></div>
			   </div>
			   
			 
			  	<div class="form-group">
			  		<label class="col-sm-2 control-label"></label>
  					<div class="col-sm-10">
  						<button type="submit" class="btn btn-default"><strong>Guardar</strong></button>
  					</div>
  			  	</div>
		    	</form>
		    	
		    	<?php if (isset($message)){?>      
			    	<div class="alert alert-danger" role="alert">
	  					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
	  					<span class="sr-only"><?php echo $message?></span>
	  						
					</div>
				<?php } ?>
		    	
		    </div>
		   
		    <div class="span5 nut-file">
		    	<!--<h4 class="green-text"><strong><!--?php echo "$loggedUser.getName() $loggedUser.getSurname()" ?></strong></h4>
		    	<span class="green-text">Edad:</span><!--?php echo "$loggedUser.getUserProfile().getAge()" ?> años
		    	&nbsp;&nbsp;&nbsp;<span class="green-text">Sexo: </span><!--?php echo "$loggedUser.getUserProfile().getGenderString()"?><br/>
		    	<span class="green-text">Índice de Masa Corporal: </span><!--?php echo "$nutProfile!=null?Math.round(nutProfile.getBMI()):''"?><br/>
		    	<span class="green-text">Estado Nutricional: </span><!--?php echo "$nutProfile!=null?nutProfile.getNutritionalStateString():''"?><br/>
		    	-->
		   	    
		    </div>
			
			
			
		   </div>	
			
		<!-- END CONTENTS -->
		 </div>
		</div>
	</div>
	<div id="push"></div>
	
	<script type="text/javascript">
			$(document).ready(function() {
				 $("[data-toggle='tooltip']").tooltip();
				 
			});
			
			$("#formProfile").submit(function( event ) {	  			
		  			qry = $( this ).serializeArray(),
		  			$.ajax({
						type: 'POST',
						data: qry,
						jsonp: "callback",
						datatype: "jsonp",
						url: 'http://localhost:8080/NutElCare/rest/userController/modNutProfile', 
						success: function(data){	
							//endExercise();
							alert("OK: "+data);
						},
						error: function(jqXHR, textStatus, errorThrown) {
	  						console.log(textStatus, errorThrown);
	  						//event.preventDefault();
	  						alert("ERROR: "+textStatus+" "+errorThrown);

						},
					});
				});		

	    </script>
	</body>
</html>