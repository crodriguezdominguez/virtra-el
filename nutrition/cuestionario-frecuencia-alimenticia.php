<?php
	require_once('../locale/localization.php');
	
	
	session_start();
	
	
	if(isset($_SESSION['userID']))
	{
		require_once(__DIR__.'/../php/User.php');
		$user = new User($_SESSION['userID']);
		$easy_access = (!$user->get_therapist() && !$user->get_superuser() && !$user->get_carer());
?>

<!DOCTYPE html>
<html>
  <head>
    <title>CFCA</title>
    <meta charset="UTF-8">
    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/nutelcare.css" rel="stylesheet">

  </head>
<body>

<div id="cfca" class='container'>
	<div class="span12">		
				<div class="page-header">
				    <!--<p style="text-align:right"><span style="font-size:24px"><a href="#">A+</a></span>&nbsp;&nbsp;&nbsp;<span style="font-size:20px">A</span>&nbsp;&nbsp;&nbsp;<span style="font-size:16px"><a href="#">A-</a></span></p>-->
				    <h2>¿Cuántas veces consumiste la semana pasada...<!--<br/><small>Cuestionario de Frecuencia Alimenticia</small>--></h2>
		        </div>
					<form role="form" id="form-freq-alim" method="POST">
      					<div class="form-group" >
							    <div class="page active">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">CARNE ROJA (Ternera, Buey)</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/ternera.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="red-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="red-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="red-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="red-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="red-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="red-meat" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">POLLO o PAVO</div>
											<div class="span12">
											  <div class="span3"><img src="img/arasaac/pollo_3.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="chicken-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="chicken-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="chicken-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="chicken-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="chicken-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="chicken-meat" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">CARNE DE CERDO</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/lomo_de_cerdo.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="pork-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="pork-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="pork-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="pork-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="pork-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="pork-meat" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">CARNE DE CORDERO</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/carne_de_cordero_1.png"></div>	
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="lamb-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="lamb-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="lamb-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="lamb-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="lamb-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="lamb-meat" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>
								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">JAMÓN</div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											 	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/jamon.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="ham" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="ham" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="ham" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="ham" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="ham" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="ham" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
								    	<div class="span6 top">
											<div class="span12 food lead">EMBUTIDOS (Chorizo, Salchichón, Jamón dulce, ...)</div>
											<div class="span12">
											 	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/embutidos.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="cold-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="cold-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="cold-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="cold-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="cold-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="cold-meat" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">CARNE PICADA (Hamburguesa, Salchichas, Longaniza, ...)</div>
											<div class="span12">
											 	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/carne_picada.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="ground-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="ground-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="ground-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="ground-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="ground-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="ground-meat" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
									    <div class="span6">
											<div class="span12 food lead">HÍGADOS Y VÍSCERAS</div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/higado.png"></div>	
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="lever-meat" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="lever-meat" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="lever-meat" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="lever-meat" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="lever-meat" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="lever-meat" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>										
									</div>
								</div>
								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">PESCADO AZUL (Sardinas, Atún, Jureles, Pez Espada, Salmón, Anchoa, Caballa, Cazón)</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/sardina.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="blue-fish" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="blue-fish" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="blue-fish" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="blue-fish" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="blue-fish" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="blue-fish" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">PESCADO BLANCO (Rape, Bacalao, Merluza, Lenguado, Cabracho, Rodaballo, Gallo) </div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/lenguado_1.png"></div>	
											 	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="white-fish" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="white-fish" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="white-fish" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="white-fish" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="white-fish" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="white-fish" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">PESCADO SEMIGRASO (Lubina, Dorada, Salmonete, Besugo)</div>
											<div class="span12">
											 	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/dorada_1.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="midfat-fish" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="midfat-fish" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="midfat-fish" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="midfat-fish" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="midfat-fish" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="midfat-fish" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6">
											<div class="span12 food lead">MARISCOS (Marisco, Pulpo, Calamares, ...) </div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/marisco_2.png"></div>	
											 	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="seafood" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="seafood" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="seafood" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="seafood" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="seafood" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="seafood" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>									
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">HUEVOS FRITOS</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/huevo_frito.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="fried-eggs" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="fried-eggs" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="fried-eggs" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="fried-eggs" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="fried-eggs" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="fried-eggs" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">HUEVOS COCIDOS</div>
											<div class="span12">
											  	 <div class="span3"><img src="img/arasaac/huevo_duro.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="boiled-eggs" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="boiled-eggs" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="boiled-eggs" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="boiled-eggs" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="boiled-eggs" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="boiled-eggs" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">HUEVO EN TORTILLA</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/tortilla_francesa.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="omelette" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="omelette" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="omelette" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="omelette" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="omelette" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="omelette" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6">
											<div class="span12 food lead">HUEVOS REVUELTOS</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/huevos_revueltos.jpg"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="scrumbled-eggs" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="scrumbled-eggs" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="scrumbled-eggs" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="scrumbled-eggs" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="scrumbled-eggs" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="scrumbled-eggs" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">ARROZ (Cocido, Paella, al Horno, ...)</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/arroz_1.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="rices" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="rices" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="rices" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="rices" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="rices" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="rices" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">PASTA (Macarrones, Espaguetti, Lasaña, Canelones, ...)</div>
											<div class="span12">
											  	 <div class="span3"><img src="img/arasaac/pasta.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="pastas" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="pastas" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="pastas" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="pastas" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="pastas" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="pastas" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">LEGUMBRES (Garbanzos, Lentejas, Judías, Guisantes, ...)</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/lentejas.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="legumes" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="legumes" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="legumes" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="legumes" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="legumes" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="legumes" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
									    <div class="span6 even" >
											<div class="span12 food lead">SOPA DE FIDEOS</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/sopa_4.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="soup-pasta" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="soup-pasta" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="soup-pasta" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="soup-pasta" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="soup-pasta" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="soup-pasta" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>										
									</div>
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">VERDURA CRUDA O EN ENSALADA (Lechuga, Tomate, Zanahoria, ...)</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/ensalada_2.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="salad" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="salad" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="salad" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="salad" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="salad" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="salad" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">COCIDA, PLANCHA, HORNO (Espinacas, Brócoli, Judías, Calabacín, ...)</div>
											<div class="span12">
											  	 <div class="span3"><img src="img/arasaac/verduras_1.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="cooked-veget" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="cooked-veget" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="cooked-veget" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="cooked-veget" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="cooked-veget" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="cooked-veget" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">VERDURA FRITA O EN TEMPURA</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/verdura_frita.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="fried-veget" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="fried-veget" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="fried-veget" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="fried-veget" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="fried-veget" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="fried-veget" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">SOPA DE VERDURAS O CREMA DE VERDURAS</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/sopa_4.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="soup-veget" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="soup-veget" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="soup-veget" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="soup-veget" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="soup-veget" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="soup-veget" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
									<div class="row-fluid">
										<div class="span6 top">
											<div class="span12 food lead">PATATAS (COCIDAS, HORNO)</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/patata.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="potatoes" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="potatoes" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="potatoes" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="potatoes" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="potatoes" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="potatoes" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									    <div class="span6 top">
											<div class="span12 food lead">PATATAS FRITAS</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/patatas_fritas_1.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="fried_potatoes" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="fried_potatoes" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="fried_potatoes" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="fried_potatoes" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="fried_potatoes" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="fried_potatoes" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
										<div class="span6 ">
											<div class="span12 food lead">PATATAS EN TORTILLA</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/tortilla_de_patata.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="potatoes_ome" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="potatoes_ome" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="potatoes_ome" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="potatoes_ome" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="potatoes_ome" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="potatoes_ome" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									    <div class="span6 ">
											<div class="span12 food lead">PATATAS EN PURÉ O CREMA</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/pure.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="cream_potatoes" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="cream_potatoes" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="cream_potatoes" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="cream_potatoes" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="cream_potatoes" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="cream_potatoes" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">FRUTAS CÍTRICAS (Mandarina, Naranja, Limón, Pomelo, Lima)</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/fruta_citrica.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="citrus" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="citrus" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="citrus" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="citrus" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="citrus" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="citrus" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">OTRAS FRUTAS (Plátano, Piña, Manzana, Pera, Sandía, Fresa,....)</div>
											<div class="span12">
											  	 <div class="span3"><img src="img/arasaac/fruta_1.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="fruit" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="fruit" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="fruit" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="fruit" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="fruit" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="fruit" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">FRUTAS EN CONSERVA (Almíbar, Mermeladas)</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/frutas_en_conserva.png"></div>	
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="fruit_cons" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="fruit_cons" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="fruit_cons" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="fruit_cons" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="fruit_cons" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="fruit_cons" value="X"> No me acuerdo</label></div>
										        </div>
										    </div>
									    </div>
										<div class="span6" >
											<div class="span12 food lead">VASOS ZUMO DE FRUTA NATURAL</div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/zumo_natural.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="juice-fruit-nat" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="juice-fruit-nat" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="juice-fruit-nat" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="juice-fruit-nat" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="juice-fruit-nat" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
									<div class="row-fluid">
										<div class="span6 ">
											<div class="span12 food lead">VASOS ZUMO DE FRUTA ENVASADO</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/zumo_envasado.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="juice-fruit-bot" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="juice-fruit-bot" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="juice-fruit-bot" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="juice-fruit-bot" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="juice-fruit-bot" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>									
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">PAN BLANCO DE BARRA</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/pan_blanco.png">
											  </div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="white-bread" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="white-bread" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="white-bread" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="white-bread" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="white-bread" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="white-bread" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">PAN INTEGRAL DE BARRA</div>
											<div class="span12">
											  	 <div class="span3"><img src="img/arasaac/pan_integral.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="brown-bread" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="brown-bread" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="brown-bread" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="brown-bread" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="brown-bread" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="brown-bread" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">PAN DE MOLDE</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/pan_molde.png">
											  </div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="sandwich-bread" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="sandwich-bread" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="sandwich-bread" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="sandwich-bread" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="sandwich-bread" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="sandwich-bread" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">BISCOTES</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/biscote.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="toast-bread" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="toast-bread" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="toast-bread" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="toast-bread" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="toast-bread" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="toast-bread" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">CEREALES CON LECHE</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/cereales.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="grain-milk" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="grain-milk" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="grain-milk" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="grain-milk" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="grain-milk" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="grain-milk" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">PIZZA O SIMILAR</div>
											<div class="span12">
											  	 <div class="span3"><img src="img/arasaac/pizza.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="pizza" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="pizza" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="pizza" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="pizza" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="pizza" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="pizza" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">EMPANADOS (Croquetas, Empanadillas) </div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/croquetas.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="croquette" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="croquette" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="croquette" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="croquette" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="croquette" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="croquette" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>										
									</div>
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">
												VASOS DE LECHE
												<strong>Indicar si:</strong> <br/>
												<div class="food-types food">
													<label><input type="radio" name="milk-type" value="Entera">Entera</label>
													<label><input type="radio" name="milk-type" value="Semi">Semi-desnatada</label>
													<label><input type="radio" name="milk-type" value="Desnatada">Desnatada</label>
													<label><input type="radio" name="milk-type" value="NoTomoLeche">No tomo leche</label>
												</div>											
											</div>
											<div class="span12 ">
												<div class="span4">
													<img style="padding:0;margin:0" src="img/arasaac/leche.png">
												</div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="milk-glass" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="milk-glass" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="milk-glass" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="milk-glass" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="milk-glass" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">YOGUR</div>
											<div class="span12" style="padding-top:37px">
											  	 <div class="span3"><img src="img/arasaac/yogur.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="yogour" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="yogour" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="yogour" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="yogour" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="yogour" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="yogour" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">CUAJADA O REQUESON</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/cuajada.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="junket" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="junket" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="junket" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="junket" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="junket" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="junket" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">NATILLAS O FLAN</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/flan.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="jelly" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="jelly" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="jelly" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="jelly" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="jelly" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="jelly" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>
								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">QUESO FRESCO</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/queso_fresco.jpg"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="fresh-cheese" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="fresh-cheese" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="fresh-cheese" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="fresh-cheese" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="fresh-cheese" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="fresh-cheese" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">QUESO CURADO O SEMICURADO</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/queso_1.png"></div>	
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="cheese" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="cheese" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="cheese" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="cheese" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="cheese" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="cheese" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">QUESO DE UNTAR </div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/queso_untar.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="cream-cheese" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="cream-cheese" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="cream-cheese" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="cream-cheese" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="cream-cheese" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="cream-cheese" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>								    										
									</div>
								</div>
								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">ACEITES</div>
											<div class="span12"><div class="span3"><img src="img/arasaac/aceitera_1.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="oils" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="oils" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="oils" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="oils" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="oils" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="oils" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">ACEITUNAS</div>
											<div class="span12"><div class="span3"><img src="img/arasaac/aceitunas.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="olives" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="olives" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="olives" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="olives" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="olives" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="olives" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">MANTEQUILLA O MARGARINA</div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											 	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/mantequilla.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="butter" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="butter" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="butter" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="butter" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="butter" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="butter" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">CONSERVAS EN ACEITE (Atún, Anchoas, Sardinas, Espárragos, ...)</div>
											<div class="span12">
											  	 <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/lata_conserva.png"></div>	
											  <div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="oil-canned" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="oil-canned" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="oil-canned" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="oil-canned" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="oil-canned" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="oil-canned" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>

 								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">CHOCOLATES, BOMBONES</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/chocolate.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="chocolates" id="chocolates-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="chocolates" id="chocolates-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="chocolates" id="chocolates-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="chocolates" id="chocolates-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="chocolates" id="chocolates-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="chocolates" id="chocolates-5" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">GALLETAS TIPO MARÍA</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/galletas_1.png"></div>
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="biscuits-1" id="biscuits-1-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-1" id="biscuits-1-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-1" id="biscuits-1-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="biscuits-1" id="biscuits-1-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-1" id="biscuits-1-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-1" id="biscuits-1-5" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">GALLETAS CON CHOCOLATE O CREMA</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/galletas_2.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="biscuits-2" id="biscuits-2-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-2" id="biscuits-2-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-2" id="biscuits-2-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="biscuits-2" id="biscuits-2-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-2" id="biscuits-2-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="biscuits-2" id="biscuits-2-5" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">MAGDALENAS, BIZCOCHOS</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/magdalena.png"></div>
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="muffins" id="muffins-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="muffins" id="muffins-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="muffins" id="muffins-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="muffins" id="muffins-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="muffins" id="muffins-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="muffins" id="muffins-5" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>
								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">PASTELES</div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/pasteles.png"></div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="cakes" id="cakes-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="cakes" id="cakes-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="cakes" id="cakes-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="cakes" id="cakes-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="cakes" id="cakes-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="cakes" id="cakes-5" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">BOLLERÍA INDUSTRIAL (Donuts, Ensaimadas, Cruasanes, Napolitanas, ...)</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/donut.png"></div>
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="industrial-pastries" id="industrial-pastries-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="industrial-pastries" id="industrial-pastries-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="industrial-pastries" id="industrial-pastries-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="industrial-pastries" id="industrial-pastries-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="industrial-pastries" id="industrial-pastries-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="industrial-pastries" id="industrial-pastries-5" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">HELADOS</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/helado.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="icecreams" id="icecreams-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="icecreams" id="icecreams-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="icecreams" id="icecreams-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="icecreams" id="icecreams-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="icecreams" id="icecreams-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="icecreams" id="icecreams-5" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6">
											<div class="span12 food lead">MIEL</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/miel.png"></div>
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="honey" id="honey-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="honey" id="honey-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="honey" id="honey-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="honey" id="honey-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="honey" id="honey-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="honey" id="honey-5" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>									
								</div>

								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">PATATAS FRITAS DE BOLSA</div>
											<div class="span12">
											  <div class="row">&nbsp;</div>
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/bolsa_patatas_fritas.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="snacks" id="snacks-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="snacks" id="snacks-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="snacks" id="snacks-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="snacks" id="snacks-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="snacks" id="snacks-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="snacks" id="snacks-5" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">OTROS SNACKS DE BOLSA (Gusanitos, Ganchitos, ...)</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/bolsa_ganchitos.png">
											  	</div>
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="other-snacks" id="other-snacks-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="other-snacks" id="other-snacks-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="other-snacks" id="other-snacks-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="other-snacks" id="other-snacks-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="other-snacks" id="other-snacks-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="other-snacks" id="other-snacks-5" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">FRUTOS SECOS DE CÁSCARA DURA (Maní, Almendras, Nueces, Pistachos, Avellanas, ...)</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/frutos_secos.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="nuts" id="nuts-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="nuts" id="nuts-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="nuts" id="nuts-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											        <div class="row"><label><input type="radio" name="nuts" id="nuts-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="nuts" id="nuts-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="nuts" id="nuts-5" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6">
											<div class="span12 food lead">FRUTOS DESECADOS (Orejones, Pasas, Ciruelas pasas, Dátiles, ...)</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/fruta_seca.jpg">
											  	</div>
											  	<div class="span3">
											  		
											  		<div class="row"><label><input type="radio" name="dried-fruit" id="dried-fruit-0" value="0"> Ninguna</label></div>
											        <div class="row"><label><input type="radio" name="dried-fruit" id="dried-fruit-1" value="1"> 1 ó 2</label></div>
											        <div class="row"><label><input type="radio" name="dried-fruit" id="wdried-fruit-2" value="3"> 3 ó 4</label></div>
											    </div>
											    <div class="span4">
											    	
											      	<div class="row"><label><input type="radio" name="dried-fruit" id="dried-fruit-3" value="5"> 5 ó 6</label></div>
											        <div class="row"><label><input type="radio" name="dried-fruit" id="dried-fruit-4" value="7"> Todos los días</label></div>
											        <div class="row"><label><input type="radio" name="dried-fruit" id="dried-fruit-5" value="X"> No me acuerdo</label></div>
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">TÉ</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/te.png">
											  </div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="tea" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="tea" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="tea" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="tea" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="tea" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">OTRAS INFUSIONES (Manzanilla, Poleo, ...)</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/infusion.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="infusions" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="infusions" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="infusions" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="infusions" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="infusions" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">CAFÉ</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/cafe.png">
											  </div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="coffee" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="coffee" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="coffee" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="coffee" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="coffee" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">CAFÉ DESCAFEINADO</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/cafe_descafeinado.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="deca-coffee" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="deca-coffee" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="deca-coffee" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="deca-coffee" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="deca-coffee" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">VASOS DE AGUA DE GRIFO</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/agua_grifo.png">
											  </div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="tap-water" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="tap-water" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="tap-water" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="tap-water" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="tap-water" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">VASOS DE AGUA DE BOTELLA</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/agua_botella.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="bottle-water" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="bottle-water" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="bottle-water" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="bottle-water" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="bottle-water" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">BEBIDA AZUCARADA (Cocacola, Fanta)
												<div class="food-types food">
													<label><input type="radio" name="sweet_drink-type" value="light">Light</label>
													<label><input type="radio" name="sweet_drink-type" value="zero">Zero</label>
													<label><input type="radio" name="sweet_drink-type" value="normal">Normal</label>	
													<label><input type="radio" name="sweet_drink-type" value="nobebidasazucaradas">No tomo bebidas azucaradas</label>	
												</div>
											</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/bebida_azucarada.png">
											  </div>	
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="sweet_drink" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="sweet_drink" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="sweet_drink" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="sweet_drink" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="sweet_drink" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6">
											<div class="span12 food lead">VASOS DE CERVEZA SIN ALCOHOL</div>
											<div class="span12" style="padding-top:30px;">
											  	<div class="span3"><img style="padding:0px;margin:0" src="img/arasaac/cerveza_sin.png">
											  	</div>
											  	<div class="span3"  >
											  	 	
											  		<div class="row"><label><input type="radio" name="free-alc-beer" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="free-alc-beer" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="free-alc-beer" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="free-alc-beer" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="free-alc-beer" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>									
								</div>

								<div class="page hidden">
								    <div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">VASOS DE VINO BLANCO</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/vino_blanco.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="white-wine" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="white-wine" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="white-wine" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="white-wine" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="white-wine" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">VASOS DE VINO TINTO</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/vino_tinto.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="red-wine" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="red-wine" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="red-wine" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="red-wine" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="red-wine" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6 even" >
											<div class="span12 food lead">VASOS DE CERVEZA</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/cerveza.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="beer" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="beer" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="beer" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="beer" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="beer" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 ">
											<div class="span12 food lead">ESPUMOSOS (Champán, Cava, ...)</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/champan.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="sparkling" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="sparkling" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="sparkling" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="sparkling" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="sparkling" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
								</div>
								
								<div class="page hidden">
									<div class="row-fluid">
								    	<div class="span6 top">
											<div class="span12 food lead">VASOS DE SANGRÍA</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/arasaac/sangria.png"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="sangria" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="sangria" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="sangria" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="sangria" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="sangria" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6 top">
											<div class="span12 food lead">BEBIDAS DESTILADAS (Whisky, Ginebra, ...)</div>
											<div class="span12">
											  	<div class="span3"><img src="img/arasaac/whisky.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="distilled-drinks" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="distilled-drinks" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="distilled-drinks" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="distilled-drinks" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="distilled-drinks" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
									<div class="row-fluid">
								    	<div class="span6" >
											<div class="span12 food lead">VERMÚ</div>
											<div class="row">&nbsp;</div>
											<div class="span12">
											  <div class="span3"><img style="padding:0;margin:0" src="img/vermu.jpg"></div>	
											  <div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="vermu" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="vermu" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="vermu" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="vermu" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="vermu" value="X"> No me acuerdo</label></div> 
										        </div>
										    </div>
									    </div>
										<div class="span6">
											<div class="span12 food lead">AGUARDIENTES O ANISADOS (Pacharán, Anís, Licor de Hierbas, ...)</div>
											<div class="span12">
											  	<div class="span3"><img style="padding:0;margin:0" src="img/arasaac/pacharan.png">
											  	</div>
											  	<div class="span3">
											  	 	
											  		<div class="row"><label><input type="radio" name="liquor" value="0"> Ninguna</label></div>											       
											        <div class="row"><label><input type="radio" name="liquor" value="3"> 1 al día</label></div>
											    </div>
											    <div class="span4">
											    	
											    	<div class="row"><label><input type="radio" name="liquor" value="1"> Menos de 1 al día</label></div>
											        <div class="row"><label><input type="radio" name="liquor" value="5"> 2 o más al día</label></div>
											        <div class="row"><label><input type="radio" name="liquor" value="X"> No me acuerdo</label></div> 
										        </div>
										     </div>
									    </div>
									</div>
								</div>

								<ul class="pager pager-freq">
											<li class="previous"><button type="button" class="btn btn-primary hidden"><< Anterior</button></li>
								  			<li class="next"><button type="button" class="btn btn-primary hidden">Siguiente >></button></li>
								  			
							    </ul>
					
								<p id="psubmit" style="width:100%; margin-top:-30px" class="text-center hidden"><button id="submit" type="submit" class="btn btn-primary btn-large">Finalizar</button></p>
							
								
							
							
						</div>
						
					</form>
					<div id="result"> </div>
				</div>

			</section>

	</div>
</div>


	
	 <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="js/jquery.bootstrap.wizard.js"></script>
    <script src="js/nutelcare.js"></script>
    
    <script>
	$(document).ready(function() {

		$("input[type='radio']").click(function(){checkRadiobuttons($(this));});
		$('.next').click(function(){
					var active=$(this).closest('.form-group').children('.active');
					var next=$(this).closest('.form-group').children('.active').next();
					next.removeClass("hidden");
					next.addClass("active");
					active.removeClass("active");
					active.addClass("hidden");		
					//$(".previous > button").removeClass("hidden");

					checkPage();		
				});
		$('.previous').click(function(){
					var active=$(this).closest('.form-group').children('.active');
					var prev=$(this).closest('.form-group').children('.active').prev();
					prev.removeClass("hidden");
					prev.addClass("active");
					active.removeClass("active");
					active.addClass("hidden");	

					checkPage()			
				});




		$("#form-freq-alim").submit(function(){
    
			    data = $(this).serializeArray() ;
			   
			    $.ajax({
			      type: "POST",
			      dataType: "json",
			      url: "backend/save_cfca.php", //Relative or absolute path to response.php file			    
			      data:data,
			      success: function(data) {	
			    		      
			        $("#result").html('<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
		  							'<div class="modal-dialog">'+
		    						'<div class="modal-content">'+
		     							' <div class="modal-header">'+
									        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									        '<h3 class="modal-title" id="myModalLabel"></h3>'+
									      '</div>'+
									      '<div class="modal-body">'+
									        '<h3>'+data["result"]+'</h3>'+
									      '</div>'+
									      '<div class="modal-footer">'+
									        '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>'+	       
									      '</div>'+
									    '</div>'+
									  '</div>'+
									'</div>');
			        $("#myModal").modal();
			        $("#usability-form").hide();
			        $('#myModal').on('hidden', function () {
						   //window.location.href = "www.virtrael.es";
						   $("#cfca").hide();

						});
			       },

			       error: function(xhr, status, errorThrown){
			       		alert("Error: "+errorThrown);
			       }
			    });
			    return false;
			  });	


	});	//END



	</script>
</body>
</html>
<?php
}
?>