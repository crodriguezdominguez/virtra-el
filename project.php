<?php
	require_once('locale/localization.php');
?>

<div class="project-header">
<h1>VIRTRAEL</h1>
<p><?php echo _('Plataforma Virtual de Evaluación e Intervención Cognitiva en Mayores'); ?></p>
</div>

<div class="project-description">
	<?php echo _('PROJECT_DESCRIPTION'); ?>
</div>