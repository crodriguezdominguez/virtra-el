-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Servidor: db572913030.db.1and1.com
-- Tiempo de generación: 07-03-2017 a las 01:26:28
-- Versión del servidor: 5.1.73-log
-- Versión de PHP: 5.4.45-0+deb7u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db572913030`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carer_patient`
--

CREATE TABLE IF NOT EXISTS `carer_patient` (
  `carer_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`carer_id`,`patient_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exerciseError`
--

CREATE TABLE IF NOT EXISTS `exerciseError` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionID` int(11) NOT NULL,
  `exerciseID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `errorKey` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=165 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exerciseLevel`
--

CREATE TABLE IF NOT EXISTS `exerciseLevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `exerciseType` int(11) NOT NULL,
  `userLevel` int(11) NOT NULL,
  `userSubLevel` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1797 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exerciseResult`
--

CREATE TABLE IF NOT EXISTS `exerciseResult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionID` int(11) NOT NULL,
  `exerciseID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `countCorrects` int(11) NOT NULL,
  `countFails` int(11) NOT NULL,
  `countOmissions` int(11) NOT NULL,
  `finalScore` int(11) NOT NULL,
  `seconds` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14676 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medal`
--

CREATE TABLE IF NOT EXISTS `medal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kindOfMedal` tinyint(4) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sessionID` int(11) NOT NULL,
  `exerciseID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13283 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nutelcare_cfca`
--

CREATE TABLE IF NOT EXISTS `nutelcare_cfca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `result` text NOT NULL,
  `fdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=149 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partialResult`
--

CREATE TABLE IF NOT EXISTS `partialResult` (
  `userID` int(11) NOT NULL,
  `sessionID` int(11) NOT NULL,
  `exerciseID` int(11) NOT NULL,
  `exerciseType` int(11) NOT NULL,
  `repetition` int(11) NOT NULL,
  `entry` text NOT NULL,
  PRIMARY KEY (`userID`,`sessionID`,`exerciseID`,`exerciseType`,`repetition`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questionnaires`
--

CREATE TABLE IF NOT EXISTS `questionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `result` text NOT NULL,
  `fdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=177 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessionlog`
--

CREATE TABLE IF NOT EXISTS `sessionlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sessionID` int(11) NOT NULL,
  `exerciseID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `repetitions` smallint(11) NOT NULL,
  `result` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22175 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `therapist_patient`
--

CREATE TABLE IF NOT EXISTS `therapist_patient` (
  `therapist_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`therapist_id`,`patient_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hashedPassword` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `therapist` tinyint(1) NOT NULL,
  `carer` int(11) NOT NULL,
  `activationKey` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=479 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userProfile`
--

CREATE TABLE IF NOT EXISTS `userProfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `placeOfBirth` varchar(200) CHARACTER SET latin1 NOT NULL,
  `phoneNumber` varchar(30) CHARACTER SET latin1 NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `civilStatus` tinyint(4) NOT NULL,
  `partner` tinyint(4) NOT NULL,
  `levelOfStudies` tinyint(4) NOT NULL,
  `hoursReading` smallint(6) NOT NULL,
  `hoursWorkshop` smallint(6) NOT NULL,
  `hoursExercise` smallint(6) NOT NULL,
  `hoursComputer` smallint(6) NOT NULL,
  `eatingHelp` tinyint(4) NOT NULL,
  `washingHelp` tinyint(1) NOT NULL,
  `clothingHelp` tinyint(4) NOT NULL,
  `smartenUPHelp` tinyint(1) NOT NULL,
  `urinateHelp` tinyint(4) NOT NULL,
  `defecateHelp` tinyint(4) NOT NULL,
  `toiletHelp` tinyint(4) NOT NULL,
  `movingHelp` tinyint(4) NOT NULL,
  `walkingHelp` tinyint(4) NOT NULL,
  `stepsHelp` tinyint(4) NOT NULL,
  `phoneHelp` tinyint(4) NOT NULL,
  `shoppingHelp` tinyint(4) NOT NULL,
  `cookingHelp` tinyint(4) NOT NULL,
  `homecareHelp` tinyint(4) NOT NULL,
  `clothCleaningHelp` tinyint(4) NOT NULL,
  `transportHelp` tinyint(4) NOT NULL,
  `medicationHelp` tinyint(4) NOT NULL,
  `economyHelp` tinyint(4) NOT NULL,
  `computerUse` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=354 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_avatar`
--

CREATE TABLE IF NOT EXISTS `user_avatar` (
  `user_id` int(10) unsigned NOT NULL,
  `avatar` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `virtrael_usability`
--

CREATE TABLE IF NOT EXISTS `virtrael_usability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `result` text NOT NULL,
  `fdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=188 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carer_patient`
--
ALTER TABLE `carer_patient`
  ADD CONSTRAINT `carer_patient_ibfk_1` FOREIGN KEY (`carer_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `carer_patient_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `therapist_patient`
--
ALTER TABLE `therapist_patient`
  ADD CONSTRAINT `therapist_patient_ibfk_1` FOREIGN KEY (`therapist_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `therapist_patient_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_avatar`
--
ALTER TABLE `user_avatar`
  ADD CONSTRAINT `user_avatar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
