<?php
	require_once('locale/localization.php');
?>

<!-- Hero unit -->	    	
<!-- <div class="hero-unit">
	<h1>VIRTRA-EL</h1>
  	<p></p>
  	<p><a href="javascript:void(0);" onclick="javascript:$('#projectMenuItem').trigger('click');" class="btn btn-primary btn-large"><?php echo _('M&aacute;s informaci&oacute;n'); ?></a></p>
</div>-->

<div style="border:1px solid #e5e5e5;margin-top:-20px;">
<header class="masthead">
	<p><?php echo _('Plataforma Virtual de Evaluación e Intervención Cognitiva en Mayores'); ?></p>
</header>
<!--<div class="center-phrase">
	<h1><?php echo _('Bienvenido a VIRTRA-EL'); ?></h1>
	<p><?php echo _('La aplicaci&oacute;n web que te propondr&aacute; ejercicios para estimular tus capacidades cognitivas'); ?></p>
</div>-->
<div class="marketing row-fluid" style="width:98%;">
	<div class="span4">
		<img class="marketing-img" src="img/marketing1.png" alt="Sesiones programadas" />
		<h2><?php echo _('Sesiones programadas'); ?></h2>
		<p><?php echo _('VIRTRA-EL te propone 15 sesiones de ejercicios de aproximadamente una hora, para que las completes en diferentes días.'); ?></p>
	</div>
	<div class="span4">
		<img class="marketing-img" src="img/marketing2.png" alt="Múltiples ejercicios" />
		<h2><?php echo _('Múltiples ejercicios'); ?></h2>
		<p><?php echo _('En cada sesión realizarás ejercicios para estimular diferentes áreas: la memoria, la atención, el razonamiento y la planificación.'); ?></p>
	</div>
	<div class="span4">
		<img class="marketing-img" src="img/marketing3.png" alt="Hecho para todos" />
		<h2><?php echo _('Hecho para todos'); ?></h2>
		<p><?php echo _('VIRTRA-EL funciona en cualquier navegador, móvil o de escritorio. Además es una web totalmente accesible, basada solo en tecnologías estándares.'); ?></p>
	</div>
</div>
</div>
