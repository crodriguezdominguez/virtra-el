<?php
	require_once('locale/localization.php');
	session_start();
	
	$only_fade = false;
	$out_container = 'exercise-container';
	$in_container = 'exercise-container';
	
	if (isset($_GET['only_fade']))
	{
		if ($_GET['only_fade'] == '1')
		{
			$only_fade = true;
		}
	}
	
	if (isset($_GET['out']))
	{
		$out_container = $_GET['out'];
	}
	
	if (isset($_GET['in']))
	{
		$in_container = $_GET['in'];
	}
?>

<div style="background-color: white; position:fixed; top:0; bottom:0; left:0; height: 0; width: 100%; height: 100%; z-index: 1000000; display:table;">
	<div style="display:table-cell;vertical-align:middle;text-align:center">
		<div>
			<img src='<?php if(isset($_SESSION['userID'])){ require_once(__DIR__.'/php/User.php'); $user = new User($_SESSION['userID']); echo $user->get_avatar(); } ?>' style="height:180px;width:auto;" />
		</div>
		<h1><?php echo _('Vamos a comenzar el ejercicio en...'); ?></h1>
		<h1 id="enter-exercise-counter">3</h1>
	</div>
</div>

<script type="text/javascript">

var counter = 4;

function timer()
{
	counter--;
	
	if (counter < 0)
	{
		$('#<?php echo $out_container; ?>').fadeOut('fast', function(){
			<?php if (!$only_fade){ ?>
			$.ajax({
	        	url: exercise._UIurl,
	         	success: function(result) {
	         		$('#<?php echo $in_container; ?>').html(result);
	         		$('#<?php echo $in_container; ?>').fadeIn('fast');   
	            }
	    	});
			<?php } else { ?>
			$('#<?php echo $in_container; ?>').fadeIn('fast');
			<?php } ?>
		});
	}
	else
	{
		$("#enter-exercise-counter").html(counter);
		setTimeout(timer, 1000);
	}
}

timer();

</script>