<?php
	require_once('../locale/localization.php');

	session_start();
?>	
<?php	
	if(isset($_SESSION['userID']))
	{
		include_once('../php/User.php');
		$user = new User($_SESSION['userID']);
		$easy_access = (!$user->get_therapist() && !$user->get_superuser() && !$user->get_carer());
?>

 
<!DOCTYPE HTML>
<html lang="es">
	<head>
		<title>VIRTRA-EL - <?php echo _('Plataforma Virtual de Evaluaci&oacute;n e Intervenci&oacute;n Cognitiva en Mayores'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <meta charset="UTF-8">

		<!-- css styles -->
		<link rel="stylesheet" type="text/css" href="../css/sweet-alert.css">
	    <link rel="stylesheet" href="../css/bootstrap-combined.no-icons.min.css">
	    <link rel="stylesheet" href="../css/virtrael.css">
	    <link rel="stylesheet" href="../css/font-awesome.min.css">
	    <link rel="stylesheet" href="css/usability.css">
	   
	   <script src='https://www.google.com/jsapi?autoload={"modules":[{"name":"visualization","version":"1","packages":["corechart"]}]}'></script>

	   <script src="../js/jquery-1.9.1.min.js"></script> 	   
		<script src="../js/jquery-migrate-1.2.1.min.js"></script>
		<script src="../js/jquery-ui-1.10.1.custom.min.js"></script>
		<script src="../js/jquery.ui.touch-punch.min.js"></script>
	    <script src="../js/bootstrap.min.js"></script>
		<script src="js/usability.js"></script>	
	</head>
	<body>
		<div id="wrap">		
			<!-- Navigation bar -->
			
			<div id="bodyContainer" class="container" style="padding: 0;width: 100%;">



<!-- CONTENIDO -->
<script>
var questions = ["¿Sabes en cada momento qué ejercicio estás haciendo?", 
	"¿Sabes cuándo terminan las instrucciones y empieza cada ejercicio?", 
	"¿Entiendes las soluciones correctas de algunos ejercicios?", 
	"¿Los pasos para realizar los ejercicios están bien indicados?",
	"¿Sabes siempre lo qué estás haciendo o qué ocurre en el ejercicio?",
	"¿Entiendes el significado de las medallas de oro, plata y bronce?",
	"¿Entiendes para qué sirven los botones que hay en los ejercicios?",
	"¿Puedes empezar el ejercicio cuándo tu quieras?",
	"¿Tienes tiempo suficiente para leer las instrucciones?",
	"¿Sabes hacer los ejercicios?", //10
	"¿Se te avisa cuándo te has equivocado al realizar los ejercicios?",
	"Cuando te sale un mensaje de error, ¿se te indica cómo resolver el error?",
	"¿Entiendes las instrucciones que tienen los ejercicios?",
	"¿Entiendes todas las palabras que aparecen en las instrucciones?",
	"¿Las instrucciones explican bien cómo tienes que hacer cada ejercicio?",
	"Cuando las instrucciones son complicadas, ¿Eres capaz de recordarlas después?",
	"¿La colocación de las instrucciones (explicaciones) y los botones en la pantalla es similar o parecida en todos los ejercicios?",
	"¿Los botones están siempre visibles cuando los necesitas?",
	"¿Distingues los colores de la pantalla?",
	"¿Distingues los colores de las letras de las explicaciones (instrucciones)?",//20
	"¿Es adecuado el tamaño de las letras?",
	"¿Lees con facilidad el tipo de letra?",
	"¿Distingues las imágenes que aparecen en los ejercicios?",
	"¿Es adecuado el tamaño de las imágenes que aparecen en los ejercicios?",
	"¿Es adecuado el tamaño de los botones para pulsarlos?",
	"¿Es fácil pulsar un botón?",
	"¿En caso de no recordar las instrucciones, puedes acceder de nuevo a leerlas?",
	"¿Crees que se dan instrucciones suficientes para hacer los ejercicios?",
	"Cada día, ¿antes de hacer los ejercicios te sientes bien?",
	"Cada día, ¿después de hacer los ejercicios te sientes bien?", //30
	"¿Te ha gustado participar en este proyecto?",
	"¿Crees que te ha sido útil para mejorar tu actividad mental?"];

var qsize = questions.length;
var i=0;

var windowHeight=$(window).height()-$(".navbar-fixed-top").height()-70-50;

var page=1;
var newPage=true;

		document.write('<form id="usability-form" class="form-horizontal" method="post">'+
			'<div id="page-content">');
		
			for (i = 0; i < qsize; i++) {					
				if (newPage){/*principio de pagina*/
					document.write('<div id="page'+page+'" class="hidden">');	
					newPage=false;				 	
				}
				var html = '<div class="row qdiv"><div class="span5"><label class="lead">'+(i+1)+'- '+ questions[i]+'</label></div><div class="span7">';
				
				if (i != 30 && i != 31) {
					html = html + '<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="4">&nbsp;Siempre</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="3">&nbsp;Casi siempre</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="2">&nbsp;A veces si y a veces no</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="1">&nbsp;Pocas veces</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="0">&nbsp;Nunca</label>';
				}
				else{
					html = html + '<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="4">&nbsp;&iexcl;Muchísimo, ha sido genial!</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="3">&nbsp;Bastante</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="2">&nbsp;Normal</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="1">&nbsp;Poco</label>'+
							 		'<label class="radio inline lead"><input type="radio" name="qt'+(i+1)+'" value="0">&nbsp;Nada</label>';
				}
				
				document.write(html+'</div></div>');
						
				 if ($("#page"+page).height()>=windowHeight-100 || i==qsize-1){/*fin de pagina*/		
				 	//alert(("page"+page));		 
					document.write('<ul class="pager hidden">');
					if (i<qsize-1){document.write('<li class="next"><button type="button" class="btn btn-primary">Siguiente >></button></li>');}
					if (page!=1){ document.write('<li class="previous"><button type="button" class="btn btn-primary"><< Anterior</button></li>');}					 
				    document.write('</ul></div>');
				    page+=1;
				    newPage=true;
				}	
					
			}
				
				document.write('<p id="psubmit" style="width:100%; margin-top:-50px" class="text-center hidden"><button id="submit" type="submit" class="btn btn-primary btn-large">Siguiente Cuestionario</button></p>'+
						'</div></form>');	



		</script>
		</div>
<!-- fin CONTENIDO -->		
		<div id="push"></div>
		</div>

	    <script>
			$(document).ready(function() {

				$("input[type='radio']").click(function(){
					checkRadiobuttons($(this),qsize);
				}); 

				$('.next').click(function(){
					$(this).parent().parent().addClass("hidden");
					$(this).parent().parent().next().removeClass("hidden");
				});
				$('.previous').click(function(){
					$(this).parent().parent().addClass("hidden");
					$(this).parent().parent().prev().removeClass("hidden");
				});
				
				

				$("#page1").removeClass("hidden");
				
				 
				
			});


			$("#usability-form").submit(function(){
    
			    data = $(this).serializeArray() ;

			    $.ajax({
			      type: "POST",
			      dataType: "json",
			      url: "../backend/save_usability_form.php", //Relative or absolute path to response.php file
			    
			      data:data,
			      success: function(data) {	
			    		      
			       /* $("#push").html('<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
		  							'<div class="modal-dialog">'+
		    						'<div class="modal-content">'+
		     							' <div class="modal-header">'+
									        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									        '<h3 class="modal-title" id="myModalLabel"></h3>'+
									      '</div>'+
									      '<div class="modal-body">'+
									        '<h3>'+data["result"]+'</h3>'+
									      '</div>'+
									      '<div class="modal-footer">'+
									        '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>'+	       
									      '</div>'+
									    '</div>'+
									  '</div>'+
									'</div>');
			        $("#myModal").modal();
			        $("#usability-form").hide();
			        $('#myModal').on('hidden', function () {
						   window.location.href = "../personal-questionnaire/user-questionnaire-1.php";
						});*/
						window.location.href = "../personal-questionnaire/user-questionnaire-1.php";
			        
			       },

			       error: function(xhr, status, errorThrown){
			       		alert("Error: "+errorThrown);
			       }
			    });
			    return false;
			  });	


	    </script>
	  
	</body>
</html>
<?php
	} else {
?>

<script type="text/javascript">
$('#bodyContainer').load('../unregistered.php');
</script>

<?php
	}
?>