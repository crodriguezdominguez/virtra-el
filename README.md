# VIRTRAEL
-----------------------

El objetivo principal de este proyecto es desarrollar e implementar una plataforma virtual para la evaluación y estimulación cognitiva de personas mayores a través de Internet. Se trata de una estrategia destinada a la prevención e intervención sobre el deterioro cognitivo disponible para muchas personas y que será accesible a aquellas personas mayores que viven alejadas de los recursos asistenciales y con dificultadas de desplazamiento para acceder a ellos. Las pruebas de evaluación y las estrategias de estimulación cognitiva tendrán un enfoque ecológico con el fin de obtener una mejor estimación del grado de discapacidad y dependencia de la persona evaluada y como resultado final una disminución de éste.

Nuestra finalidad última es prevenir y retrasar la dependencia, así como intervenir en sus estadíos iniciales. Para alcanzar nuestro objetivo se ofrecen nuevos instrumentos de evaluación neuropsicológica y funcional que permitirán realizar una evaluación de la persona mayor a distancia, y nuevas tareas que permitirán realizar la estimulación o rehabilitación a distancia de esas personas. Para ello, VIRTRA-EL permite la configuración y administración de las tareas, supervisión y monitorización en línea de las actividades que realiza la persona mayor, así como el seguimiento de su evolución.

La plataforma software se ha desarrollado prestando especial atención al cumplimiento de ciertos atributos de calidad (escalabilidad, portabilidad, usabilidad, extensibilidad, etc.) que garanticen fundamentalmente:

* Adaptación a perfiles de usuario e incluso ciudadanos concretos
* Fácil implantación, configuración, mantenimiento y ampliación para asegurar la operatividad incluso en ausencia de personal experto, existiendo como única necesidad la disponibilidad de un ordenador con acceso a internet (domicilios particulares y centros de acceso público/privado a internet), con el propósito de abarcar todo el territorio andaluz

Además, en este proyecto se han incorporado tecnologías de realidad virtual que dotarán a las pruebas de evaluación y estimulación cognitiva de una mayor validez ecológica y de una mejor estimación del grado de discapacidad y dependencia de la persona evaluada.

## Instalación
-----------------------

Para desplegar este proyecto es necesario tener instalado un servidor Web Apache, una base de datos MySQL (versión 5.x) o MariaDB y PHP 5.6 o superior. La estructura de la base de datos se encuentra disponible en el fichero **database.sql**.

Además, será necesario crear un usuario con permisos de administración. Para ello será necesario insertar en la tabla **user** una entrada con los siguientes contenidos:

* `id` Se puede dejar vacío, ya que se autogenera por la base de datos
* `name` Nombre
* `surname` Apellidos
* `email` E-mail
* `hashedPassword` Password, codificado mediante el algoritmo md5
* `admin` Un valor 1 indicando que el usuario es administrador
* `therapist` Un valor 1 o 0 indicando si el administrador además es un terapeuta o no, respectivamente.
* `carer` Un valor 1 o 0 indicando si el administrador además es un cuidador o no, respectivamente.
* `activationKey` Se debe dejar vacío. Este campo solo se usa para usuarios registrados desde la Web.
* `status` Un valor 1 indicando que el usuario está activo.

## Estructura de directorios
-----------------------

```
backend                     contiene una serie de microservicios sobre los que se apoya la plataforma y los diferentes ejercicios
    ui/                     interfaz gráfica del backend de la plataforma
css/                        hojas de estilo para la plataforma Web
downloads/                  ficheros disponibles para descarga por parte de los usuarios de la plataforma
exercises/                  especificación de los ejercicios de los que consta la plataforma (ver siguiente sección)
    img/                    imágenes de las que hacen uso los ejercicios
    locale/                 ficheros de traducción para los diferentes ejercicios
    VRResources/            recursos para los ejercicios de realidad virtual
    xml/                    ficheros XML sobre los que se apoyan los diferentes ejercicios
facebook/                   ficheros relacionados con la SDK de Facebook (funcionalidad aún en desarrollo)
font/                       tipografías adicionales
img/                        imágenes de las que hace uso la plataforma Web
js/                         ficheros JavaScript
locale/                     ficheros de traducción para la plataforma Web
nutrition/                  carpeta que almacena formularios sobre nutrición para los usuarios
personal-questionnaire/     cuestionario personal para los pacientes
php/                        ficheros php auxiliares
usability/                  cuestionario sobre accesibilidad de la plataforma

```

## Desarrollo de nuevos ejercicios
-----------------------

Para desarrollar nuevos ejercicios es necesario crear nuevos ficheros con extensión php en la carpeta *exercises*. Dichos ficheros contendrán principalmente código JavaScript, haciendo uso de PHP para dar soporte a traducciones en múltiples idiomas, o para acceder a variables de sesión. El acceso a base de datos se deberá realizar (como recomendación) a través de microservicios php que deberán situarse en la carpeta *backend*.

Para cada ejercicio, al menos se deberán crear los siguientes ficheros (se recomienda revisar el ejemplo dado en los ficheros asociados a *DummyExercise*):

* `Ejercicio.php` Especificación del ejercicio en JavaScript. Se podrá especificar el título del ejercicio, si el ejercicio tiene tutorial, los ficheros donde estén definidas las interfaces gráficas y cualquier función auxiliar que se requiera para llevar a cabo el ejercicio.
* `EjercicioUI.php` Interfaz gráfica del ejercicio. Se dispone de un objeto JavaScript llamado **exercise** instanciado a partir de la especificación realizada en el fichero anterior. Además, se puede hacer uso de la función JavaScript **endExercise()** para finalizar el ejercicio y guardar toda la información asociada al mismo.
* `EjercicioDemoUI.php` (opcional) Interfaz gráfica del tutorial del ejercicio. Se dispone de un objeto JavaScript llamado **exercise** instanciado a partir de la especificación realizada en el fichero anterior. Además, se puede hacer uso de la función JavaScript **endDemo()** para finalizar el tutorial y guardar toda la información asociada al mismo.

Finalmente será necesario modificar los siguientes ficheros:

* `exercises/exercise-factory.js` Se deberá asociar un código numérico al ejercicio desarrollado, siguiendo el esquema que se puede observar en este fichero.
* `sessions.php` Se deberá modificar este esquema XML (se usa código PHP para dar soporte a traducciones) para especificar en qué sesión se incluirá el nuevo ejercicio.