<?php
	session_start();
	
	if (isset($_GET['lang'])) {
		$_SESSION['lang'] = $_GET['lang'];
	}
	
	require_once('locale/localization.php');
	require_once(__DIR__.'/php/User.php');
	
	if (strpos($_SERVER['HTTP_HOST'], 'www.') === FALSE){
		$host = str_replace('www.', '', $_SERVER['HTTP_HOST']);
		$host = str_replace('http://', '', $host);
		$host = str_replace('https://', '', $host);
		header('Location: http://www.'.$_SERVER['HTTP_HOST']);
	}
	
	$easy_access = false;
	if(isset($_SESSION['userID']))
	{
		$user = new User($_SESSION['userID']);
		$easy_access = (!$user->get_therapist() && !$user->get_superuser() && !$user->get_carer());
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
	<head>
		<title>VIRTRAEL - <?php echo _('Plataforma Virtual de Evaluaci&oacute;n e Intervenci&oacute;n Cognitiva en Mayores'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- css styles -->
		<link rel="stylesheet" type="text/css" href="css/sweet-alert.css">
	    <link rel="stylesheet" href="css/bootstrap-combined.no-icons.min.css">
	    <link rel="stylesheet" href="css/virtrael.css">
	    <link rel="stylesheet" href="css/datepicker.css">
	    <link rel="stylesheet" href="css/font-awesome.min.css">
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
		<![endif]-->
	    <!--<link href="css/jquery.toChecklist.min.css" rel="stylesheet">-->

	    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	
	    <!-- favicon and touch icons (for iPhone, iPad and iPod Touch)
	    <link rel="shortcut icon" href="img/ico/favicon.ico">
	    <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
	    <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72x72.png">
	    <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114x114.png">
	    <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144x144.png">
	    -->
	    
	    <script type="text/javascript" src='https://www.google.com/jsapi?autoload={"modules":[{"name":"visualization","version":"1","packages":["corechart"]}]}'></script>
	</head>
	<body>
		<div id="wrap">		
			<!-- Navigation bar -->
			<div class="navbar navbar-fixed-top">
				<div class="navbar-inner">
					<div class="container">
						<?php if (!$easy_access) { ?><a class="brand" href="javascript:void(0);" onclick="javascript:$('#homeMenuItem').trigger('click');">VIRTRAEL</a> <?php } ?>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-collapse">
						<?php if (!$easy_access){ ?>			
							<ul class="nav">
							  	<li id="homeMenuItem" class="active"><a href="javascript:void(0);"><!--<i class="icon-home"></i>&nbsp;--><?php echo _('Inicio'); ?></a></li>
							  	<li id="projectMenuItem"><a href="javascript:void(0);"><!--<i class="icon-info-sign"></i>&nbsp;--><?php echo _('Proyecto'); ?></a></li>
							  	<li id="membersMenuItem"><a href="javascript:void(0);"><!--<i class="icon-info-sign"></i>&nbsp;--><?php echo _('Participantes'); ?></a></li>
							  	<li id="exercisesMenuItem"><a href="javascript:void(0);"><!--<i class="icon-cogs"></i>&nbsp;--><?php echo _('Ejercicios'); ?></a></li>
								<li id="contactMenuItem"><a href="javascript:void(0);"><!--<i class="icon-envelope"></i>&nbsp;--><?php echo _('Contacto'); ?></a></li>
							</ul>
							<!-- The drop down menu -->
					        <ul class="nav pull-right">
					        <?php if(isset($_SESSION['userID']) == false){ ?>
								<li id="registerMenuItem"><a href="javascript:void(0);"><i class="icon-edit"></i>&nbsp;<?php echo _('Registrarse'); ?></a></li>
								<li class="divider-vertical"></li>
								<li id="accessMenuItem"><a href="javascript:void(0);"><i class="icon-signin"></i>&nbsp;<?php echo _('Iniciar Sesi&oacute;n'); ?></a></li>
								<!--<li><ul class="nav">
									<li class="dropdown">
										<a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown"><?php echo _('Iniciar sesi&oacute;n'); ?> <i class="icon-caret-down"></i></a>
										<ul class="dropdown-menu dropdown-login">
											<li><span class="login-error" id="login-error"></span></li>
											<li>
											<form action="javascript:login();" method="post" accept-charset="UTF-8">
												<label for="username" class="form-label login-label"><?php echo _('Correo Electr&oacute;nico'); ?></label>
													<input id="username" class="login-input" type="text" name="user[username]" size="30" />
												<label for="userpassword" class="form-label login-label"><?php echo _('Contrase&ntilde;a'); ?></label>
													<input id="userpassword" class="login-input" type="password" name="user[password]" size="30" />
												<button type="submit" class="btn btn-primary login-submit"><?php echo _('Acceder'); ?></button>
												<label class="checkbox remember" for="userremember">
													<input id="userremember" type="checkbox" name="user[remember_me]" value="1" /><span class="login-label"><?php echo _('Recordarme'); ?></span>
												</label><br />
												<p class="forgot-password-p"><a class="forgot-password" href="javascript:void(0);"><small><?php echo _('Olvid&eacute; mi contrase&ntilde;a'); ?></small></a></p>
											</form>
											</li>
										</ul>
									</li>
								</ul></li>-->
							<?php } else { ?>
								<li><a href="javascript:void(0);" id="user-profile"><i class="icon-cog"></i> <?php echo _('Gesti&oacute;n'); ?></a></li>
								<li class="divider-vertical"></li>
								<li><a href="backend/logout.php"><i class="icon-off"></i> <?php echo _('Salir'); ?></a></li>
								<!--<li><ul class="nav">
									<li class="dropdown">
										<a class="dropdown-toggle" id="dropMenu" href="javascript:void(0);" data-toggle="dropdown" role="button"><i class="icon-user"></i> <b class="caret"></b></a>
										<ul class="dropdown-menu dropdown-profile" role="menu" aria-labelledby="dropMenu">
											<li><a href="javascript:void(0);" id="user-profile"><i class="icon-cog"></i> <?php echo _('Gesti&oacute;n'); ?></a></li>
											<li class="divider"></li>
											<li><a href="backend/logout.php"><i class="icon-off"></i> <?php echo _('Salir'); ?></a></li>
										</ul>
									</li>
								</ul></li>-->
							<?php } ?>
					        </ul>
					    <?php } else { ?>
					    <ul class="nav">
							<li id="settings-avatar" class="easy-access-entry"><a href="javascript:void(0);" onclick="javascript:load_settings('avatar');"><i class="icon-comment"></i>&nbsp;<?php echo _('Mi Asistente'); ?></a></li>
							<li id="settings-therapist" class="easy-access-entry"><a href="javascript:void(0);" onclick="javascript:load_settings('therapist');"><i class="icon-user-md"></i>&nbsp;<?php echo _('Mi Terapeuta'); ?></a></li>
							<li id="settings-carer" class="easy-access-entry"><a href="javascript:void(0);" onclick="javascript:load_settings('carer');"><i class="icon-heart"></i>&nbsp;<?php echo _('Mi Cuidador'); ?></a></li>
							<!--<li id="settings-results" class="easy-access-entry"><a href="javascript:void(0);" onclick="javascript:load_settings('results');"><?php echo _('Mis Resultados'); ?></a></li>-->
							<li id="settings-medals" class="easy-access-entry"><a href="javascript:void(0);" onclick="javascript:load_settings('medals');"><i class="icon-trophy"></i>&nbsp;<?php echo _('Mis Medallas'); ?></a></li>
							<li id="settings-medals" class="easy-access-entry"><a href="javascript:void(0);" onclick="javascript:load_settings('questionnaires');"><i class="icon-question-sign"></i>&nbsp;<?php echo _('Cuestionarios'); ?></a></li>
							<li id="settings-social" class="easy-access-entry" disabled><a href="javascript:void(0);" onclick="javascript:load_settings('social');"><i class="icon-thumbs-up"></i>&nbsp;<?php echo _('Social'); ?></a></li>
						</ul>
						<ul class="nav pull-right">
							<li class="easy-access-entry"><a href="backend/logout.php"><i class="icon-off"></i>&nbsp;<?php echo _('Salir'); ?></a></li>
						</ul>
					    <?php } ?>
					    </div>
					</div>
				</div>
			</div>
			
			<div id="spinner" class="spinner" style="display:none;">
	    		<!--<i class="icon-spinner icon-spin icon-large"></i><br />--><div class="growingSpinner"></div><p><?php echo _('Cargando...'); ?></p>
			</div>
			
			<div id="bodyContainer" class="container">
			</div>
			
			<div id="push"></div>
		</div>
		
		<?php if (!$easy_access) { ?>
		<div class="footer container">
			<p class="pull-right footer-top"><a href="javascript:void(0);" onclick="javascript:scrollToTop();"><i class="icon-caret-up"></i>&nbsp;<?php echo _('Ir hacia arriba'); ?></a></p>
			<p class="pull-right footer-top"><a href="index.php?lang=es_ES#home"><img src="img/flags/spain.png" width="20" alt="España"></a>&nbsp;<a href="index.php?lang=pt_PT#home"><img width="20" src="img/flags/portugal.png" alt="Portugal"></a>&nbsp;<a href="index.php?lang=en_US#home"><img width="20" src="img/flags/uk.png" alt="UK"></a></p>
			<p class="pull-left footer-img"><img longdesc="http://www.juntadeandalucia.es" src="img/logojunta.png" width="50" alt="junta de andaluc&iacute;a" style='width:50px;' /></p>
			<p style="margin-left:100px;margin-top:25px;">VIRTRAEL &copy; 2015</p>
			<p style="margin-left:100px"><?php echo _('Proyecto de excelencia TIC-6600 de la Junta de Andaluc&iacute;a.'); ?></p>
			<p style="margin-left:100px;font-size:smaller;"><?php echo _('Los s&iacute;mbolos pictogr&aacute;ficos utilizados de ARASAAC (<a href="http://catedu.es/arasaac/" target="_blank">http://catedu.es/arasaac/</a>) son parte de una obra colectiva propiedad de la Diputaci&oacute;n General de Arag&oacute;n y han sido creados bajo licencia <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/es/" target="_blank">Creative Commons</a>.'); ?></p>
		</div>
		<?php } else { ?>
			<!-- Settings Modal -->
			<div id="settingsMenuModal" style="position: absolute;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="settingsMenuModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" style="opacity:1;" class="pull-right btn btn-danger" style="float:right;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove icon-white"></i> <?php echo _('Cerrar'); ?></button>
					<h3 id="settingsMenuModalLabel"></h3>
				</div>
				<div class="modal-body" id="settingsMenuModalContent" style="max-height: none;">
				</div>
			</div>
		<?php } ?>
		
		<!-- scripts -->
		<!--<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>-->
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
	    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="js/md5.min.js"></script>
	    <script type="text/javascript" src="js/sweet-alert.min.js"></script>
	    <script type="text/javascript">
			$(document).ready(function() {
				<?php if (!$easy_access){ ?>
		    	$('#homeMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#homeMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('home.php', function(data){
			    			window.location.hash = 'home';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	$('#projectMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#projectMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('project.php', function(data){
			    			window.location.hash = 'project';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	$('#membersMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#membersMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('members.php', function(data){
			    			window.location.hash = 'members';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	$('#exercisesMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#exercisesMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('exercises.php', function(data){
			    			window.location.hash = 'exercises';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	$('#contactMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#contactMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('contact.php', function(data){
			    			window.location.hash = 'contact';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	$('#registerMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#registerMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('register.php', function(data){
			    			window.location.hash = 'register';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	$('#accessMenuItem').click(function(event){
		    		event.preventDefault();
		    		$(".active").removeClass('active');
		    		$('#accessMenuItem').addClass('active');
		    		
		    		$('#bodyContainer').fadeOut('fast', function(){
			    		$.get('unregistered.php?init=1', function(data){
			    			window.location.hash = 'access';
			    			$('#bodyContainer').html(data).fadeIn('slow');
			    		});
		    		});
		    	});
		    	
		    	if (window.location.hash == 'project' || window.location.hash == '#project')
		    	{
		    		$('#projectMenuItem').click();
		    	}
		    	else if (window.location.hash == 'members' || window.location.hash == '#members')
		    	{
		    		$('#membersMenuItem').click();
		    	}
		    	else if (window.location.hash == 'exercises' || window.location.hash == '#exercises')
		    	{
		    		$('#exercisesMenuItem').click();
		    	}
		    	else if (window.location.hash == 'contact' || window.location.hash == '#contact')
		    	{
		    		$('#contactMenuItem').click();
		    	}
		    	else if (window.location.hash == 'register' || window.location.hash == '#register')
		    	{
		    		$('#registerMenuItem').click();
		    	}
		    	else if (window.location.hash == 'access' || window.location.hash == '#access')
		    	{
		    		$('#accessMenuItem').click();
		    	}
		    	else if (window.location.hash == 'home' || window.location.hash == '#home')
		    	{
		    		$('#homeMenuItem').click();
		    	}
		    	else
		    	{
		    		<?php if(isset($_SESSION['userID'])){ ?>
		    		$('#homeMenuItem').click();
		    		<?php } else { ?>
		    		$('#accessMenuItem').click();
		    		<?php } ?>
		    	}
		    	
		    	$(function() {
					// Setup drop down menu
					$('.dropdown-toggle').dropdown();
					
					// Fix input element click problem
					$('.dropdown input, .dropdown label, .dropdown button').click(function(e) {
						e.stopPropagation();
					});
				});
				
				$('#user-profile').click(function(event){
					$.get('profile.php', function(data){
						$(".active").removeClass('active');
						$('#user-profile').parent().addClass('active');
			    		$('#bodyContainer').html(data).fadeIn('slow');
			    	});
				});
				<?php } else { //easy access ?>
					$("#bodyContainer").css('padding-bottom', '0px');
										
					$.get('exercises.php', function(data){
			    		$('#bodyContainer').html(data).fadeIn('slow');
			    	});
				<?php } ?>
				
				//ajax loader
				$("#spinner").bind("ajaxSend", function() {
					$(this).show();
				}).bind("ajaxStop", function() {
					$(this).hide();
				}).bind("ajaxError", function() {
					$(this).hide();
				});
			});
			
			<?php if ($easy_access) { ?>
			function load_settings(tab_name)
			{
				$("li[id^='settings']").removeClass('active');
				if (tab_name == 'test_settings')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Ajustes de Prueba'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/test_settings.php');
				}
				else if (tab_name == 'access')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Acceso'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/access.php');
				}
				else if (tab_name == 'therapists')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Terapeutas'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/therapists.php');
				}
				else if (tab_name == 'therapist')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Mi Terapeuta'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/therapist.php');
				}
				else if (tab_name == 'carer')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Mi Cuidador'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/carer.php');
				}
				else if (tab_name == 'results')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Mis Resultados'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/results.php');
				}
				else if (tab_name == 'medals')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Mis Medallas'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/medals.php');
				}
				else if (tab_name == 'questionnaires')
				{
					//$("#settingsMenuModalLabel").html("<?php echo _('Mis Cuestionarios'); ?>");
					//$("#settingsMenuModalContent").load('backend/ui/patient_questionnaires.php');
					
					$('#exercise-title').html("<?php echo _('Cuestionarios'); ?>");
					var url = 'usability/usability-form.php';
					$('#exercise-description').html('<a class="btn btn-danger btn-large" href="javascript:void(0);" onclick="javascript:window.location.reload();"><i class="icon-rotate-left"></i>&nbsp;<?php echo _('Volver a los ejercicios'); ?></a>');
					$('#exercise-container').html('<iframe src="'+url+'" style="width:100%;height:600px;overflow:auto;border:0px;"></iframe>');
					
					toggleFullScreen();
				}
				else if (tab_name == 'avatar')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Mi Asistente'); ?>");
					$("#settingsMenuModalContent").load('backend/ui/avatar.php');
					
					$("#settingsMenuModal").on("hidden", function(){
						$("#img-avatar-id").attr('src', get_avatar());
						$("#settingsMenuModal").on("hidden", null);
					});
				}
				else if (tab_name == 'social')
				{
					$("#settingsMenuModalLabel").html("<?php echo _('Social'); ?>");
					$("#settingsMenuModalContent").load('facebook/facebook-login.php');
					
					/*
					$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
						function getUserContents(accessToken) {
							FB.api('/me/friends', 'get', {access_token : accessToken}, function(response){
								var friendsCount = response.summary.total_count;
								console.log(friendsCount);
								
								FB.api('/me/posts', 'get', {access_token : accessToken, fields:'created_time'}, function(response){
									var postCount = response.data.length;
									console.log(postCount);
								});
							});
						};
						
						FB.init({
						  appId: '728440753923116',
						  version: 'v2.5' // or v2.0, v2.1, v2.2, v2.3
						});
					*/
						/*
						$('#settings-social').removeAttr('disabled');
						FB.getLoginStatus(function(auth){*/
							/*if (auth.status === 'connected') {
								var accessToken = auth.authResponse.accessToken;
								getUserContents(accessToken);
							}
							else {*/
								/*FB.login(function(response){
									if (response.authResponse) {
										var accessToken = response.authResponse.accessToken;
										getUserContents(accessToken);
									}
									else{
										alert("<?php echo _('No nos has autorizado para obtener información sobre tu cuenta de Facebook'); ?>");
									}
								}, {scope: 'user_friends,user_posts'});*/
							//}
						/*});
					});*/
				}
				
				if (tab_name != 'questionnaires')
					$("#settingsMenuModal").modal('toggle');
			};
			<?php } ?>

			function scrollToTop(){
				$('html, body').animate({scrollTop: 0}, 700);
			};
			
			var RANDOM = (function() {
			  // Set two seed values.
			  var carry, x,
			      // Value of modulus and multiplier are chosen together
			      // 2^32 is chosen because it's similar to the others
			      // but we don't use bitwise operations to take advantage of this
			      max = Math.pow(2, 32),
			      a = 3636507990;
			  return {
			    setSeed : function(arr) {
			      var seed = arr || [0,1].map(function() {
			        return Math.round(Math.random() * max);
			      });
			      carry = seed[0];
			      x = seed[1];
			    },
			    getSeed : function() {
			      return [carry, x];
			    },
			    rand : function() {
			      // Two multiplications
			      // create carry with division, x with mod
			      // The first part is the "carry" where we're
			      // using both parts of the residue
			      carry = ((a * x) + carry) / max;
			      // subtracting from the max is what makes it the compliment
			      x = (max - 1) - ((a * x) + carry) % max;
			      return x / max;
			    }
			  };
			}());
			
			RANDOM.setSeed();
			
			function getRandom()
			{
				return RANDOM.rand();
			};
			
			function login()
			{
				var user = $("#username").val();
				var password = md5($("#userpassword").val());
				var remember = $("#userremember").prop('checked')?'1':'0';
				
				var qry = 'user='+ user + '&pass=' + password + '&rem=' + remember;
				
				$.ajax({
					type: "POST",
					url: "backend/login.php",
					data: qry,
					success: function(html) {
						if(html=='true')
						{
							$('#login-error').html("");
							window.location.reload();
						}
						else if (html == 'non-active')
						{
							$('#login-error').html("<?php echo _('Su acceso está desactivado. Contacte con el administrador'); ?>");
						}
						else
						{
							$('#login-error').html("<?php echo _('Usuario o contrase&ntilde;a no v&aacute;lidos'); ?>");
						}
					}
				});
			};
	    </script>
	</body>
</html>

