<?php
	require_once('locale/localization.php');
	require_once('php/User.php');
	session_start();
	
	$userName = '';
	if (isset($_SESSION['userID']))
	{
		$user = new User($_SESSION['userID']);
		$userName = ', '.$user->get_name();
	}
?>

<p class='lead'><?php echo _("<strong>&iexcl;Muy bien$userName!</strong>"); ?></p>
<p class='lead'><?php echo _('Este es el resumen de todas las medallas que has obtenido:'); ?></p>
<div id="medals" style="position:relative;"></div>
<div id="canvas-div" style="position:fixed; height:100%;top:0px;width:100%; left:0px;">
	<!--<canvas id="canvas-id" style="position:absolute; top:0px; left:0px; z-index:0;">-->
</div>

<script src="js/fireworks.js" type="text/javascript"></script>
<script type="text/javascript">	
	$("#medals").load('backend/ui/medals.php');
	Xteam.fireworkShow("#canvas-div", 500);
</script>