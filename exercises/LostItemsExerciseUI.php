<?php
	require_once('locale/localization.php');
?>

<div id="introExercise" class="hide">
	<p><a id="introNextStep" href="javascript:void(0);" onclick="javascript:nextStepIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
	<!--<p><img src='exercises/img/LostItemsExercise/hall/hall4doors.png' alt='Casa' style='width:50%' /></p>-->
</div>
<div id="replayExercise" class="hide">
	<p><a href="javascript:void(0);" onclick="javascript:replayExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<div id="mediaContainer" style="position:relative;">
	<div class="alert alert-block alert-success li-alert hide" id="mediaAlert">
	</div>
	<div id="itemsPanel" class="li-status-panel hide" style="z-index:1001;">
		<div class='li-itemsList' id="itemsList"></div>
	</div>
	<div id="mediaPanel" style="z-index: 1000;position:relative;margin-bottom:20px;top:20px;">
	</div>
</div>

<script type="text/javascript">
var repetitions = 0;
var sessionID = null;
var exerciseID = null;
var roomsXML = "exercises/xml/lost-items-rooms.php";
var rooms = [];
var cart = null;
var coinsFound = 0;
var totalCoins = 0;
var currentRoom = null;
var currentRoomIndex = -1;
var endTimer = false;
var timing = 0;
var level = 0;
var sublevel = 0;
var roomsPerLevel = [2, 4, 6, 8];
var itemsPerSubLevel = [1, 2, 4];
var allrooms = null;
var numberOfReplays = 0;
var roomWidth = 1100;
var roomHeight = 600;
var foundObjects = 0;
var unsortedItems = [];
var selectedCartObject = null;
var totalTimer = 0;
var MAX_TIMER = 60*20; //20 minutes
var endTotalTimer = false;

function showObjects(){
	selectedCartObject = null;
	var finalValue = "<table>";
	var col = 0;
	var row = 0;
	finalValue +="<tr>";
	for (var i=0; i<8; i++) {
		if (i<cart.length)
		{
			var item = cart[i];
			finalValue += "<td><div id='item"+i+"' class='thumbnail li-top-thumbs' onclick='javascript:toggleClass($(this));'><a href='javascript:void(0);' class='wordlist-exercise text-center'><img src='"+item.img+"' alt='"+item.name+"' ";
			if (parseInt(item.w) > parseInt(item.h)) finalValue += "style='width:80px; height:auto;";
			else finalValue += "style='height:80px; width:auto;";
			finalValue += "' /></a></div></td>";
		}
		else finalValue += "<td><span class='thumbnail' style='text-align:center;width:80px;height:80px;'></span></td>";
	}
	
	finalValue += "<td width='240px' style='text-align:center;'><p id='li-game-status'><?php echo _('Objetos desordenados:'); ?><br />";
	finalValue += "<span id='li-bad-objects'></span></p>";
	finalValue += "<p><?php echo _('Monedas encontradas:'); ?><br />";
	finalValue += "<span id='li-found-coins'></span> <?php echo _('de'); ?> <span id='li-total-coins'></span></p>";
	finalValue += "</td>";
	finalValue += "<td><a id='show-hall-button' class='hide' href='javascript:void(0);' onclick='javascript:showHall();'><img src='exercises/img/LostItemsExercise/hall/door.png' width='55' alt='<?php echo _('Volver al pasillo'); ?>' style='margin-left:5px;' /><br /><?php echo _('Volver al pasillo'); ?></a></td>"
	finalValue += "</tr>";
	finalValue += "</tr></table>";
	$("#itemsList").html(finalValue);
};

function toggleClass(element) {
	if (element.hasClass('li-toggle'))
	{
		element.removeClass('li-toggle');
		selectedCartObject = null;
		highlightCorrectPosition(false);
	}
	else
	{
		$(".li-toggle").removeClass('li-toggle');
		element.addClass('li-toggle');
		var index = parseInt(element.attr('id').split("item")[1]);
		selectedCartObject = cart[index];
		highlightCorrectPosition(true);
	}
}

function updateCounters(){
	var totalObjects = (roomsPerLevel[level]*itemsPerSubLevel[sublevel])-foundObjects;
	$("#li-bad-objects").html(totalObjects);
	$("#li-found-coins").html(coinsFound);
	$("#li-total-coins").html(totalCoins);
};

function timingFunction() {
	if (!endTimer){
		timing++;
		setTimeout(timingFunction, 1000);
	}
};

function totalTimerFunction(){
	if (!endTotalTimer){
		if (totalTimer < MAX_TIMER)
		{
			totalTimer++;
			setTimeout(totalTimerFunction, 1000);
		}
		else{
			endExercise();
		}
	}
};

function frameOfPlaceAtIndex(index) {
	return frameOfPlace(rooms.Room[index]);
};

function frameOfDoor(index) {
	var roomsCount = roomsPerLevel[level];
	var x =0;
	var y =0;
	var w = 190;
	var h = 300;
	var middleSeparator = 10;
	
	switch(roomsCount)
	{
		case 2: x = 76 + (index+1) * 180 + (index+1) * 76;
				y = 250;
		break;
		case 4: x = 76 + index * 180 + index * 76;
				y = 250;
		break;
		case 6: w = 170;
				h = 280;
				if (index == 3) middleSeparator = 10;
				x = 10 + index * w + index * 10 + middleSeparator;
				y = 270;
		break;
		case 8:
				w = 120;
				h = 220;
				if (index == 3) middleSeparator = 10;
				x = 6 + index * w + index * 6 + middleSeparator;
				y = 330;
		break;
	}
	
	return [x,y,w,h];
};

function itemAtRoomHTML(index, item) {
	var pos = item.position;
	var pHTML = "<div style='border:3px dashed red;position:absolute;left:"+pos.x+"px;top:"+pos.y+"px;width:"+pos.w+"px;height:"+pos.h+"px;' onclick='javascript:addToCart("+index+");'>";
	pHTML += "<div style='display:block;position:relative;width:"+item.w+"px;height:100%;margin-left:auto;margin-right:auto;'><img src='"+item.img+"' alt='"+item.name+"' style='position:absolute;bottom:0px;width:"+item.w+"px; height:"+item.h+"px;' /></div>";
	pHTML +="</div>";
	
	return pHTML;
};

function coinHTML(index, coin) {
	var pHTML = "<div id='coin"+index+"' style='position:absolute;left:"+coin.x+"px;top:"+coin.y+"px;width:"+coin.w+"px;height:"+coin.h+"px;' onclick='javascript:takeCoin("+index+");'>";
	pHTML += "<div style='display:block;position:relative;width:"+coin.w+"px;height:"+coin.h+"px;margin-left:auto;margin-right:auto;'><img src='exercises/img/LostItemsExercise/coin.png' alt='<?php echo _('Moneda'); ?>' style='position:absolute;bottom:0px;width:"+coin.w+"px; height:"+coin.h+"px;' /></div>";
	pHTML +="</div>";
	
	return pHTML;
};

function takeCoin(index) {
	coinsFound++;
	currentRoom.PositionedCoins.splice(index, 1);
	$("#coin"+index).remove();
	updateCounters();
	showRoomAtIndex(currentRoomIndex);
};

function showRoomAtIndex(index){
	var room = rooms[index];
	currentRoom = room;
	currentRoomIndex = index;
	var pHTML = "<img alt='"+room.name+"' src='"+room.background+"' width='100%' height='100%' />";
	if (typeof room.Decorations.Decoration != 'undefined')
	{
		if (typeof room.Decorations.Decoration.length != 'undefined')
		{
			$.each(room.Decorations.Decoration, function(index, dec){
				pHTML += "<img src='"+dec.img+"' alt='' style='position:absolute;left:"+dec.x+"px;top:"+dec.y+"px;' />";
			});
		}
		else
		{
			var dec = room.Decorations.Decoration;
			pHTML += "<img src='"+dec.img+"' alt='' style='position:absolute;left:"+dec.x+"px;top:"+dec.y+"px;' />";
		}
	}
	
	$.each(room.PositionedItems, function(index, item){
		pHTML += itemAtRoomHTML(index, item);
	});
	
	$.each(room.PositionedCoins, function(index, coin){
		pHTML += coinHTML(index, coin);
	});
	
	$("#mediaPanel").html(pHTML);
	$("#show-hall-button").removeClass('hide').show();
	
	if (selectedCartObject != null) highlightCorrectPosition(true);
};

function highlighCheckPosition()
{
	if (!existsItemAtPosition(currentRoom, selectedCartObject.position))
	{
		$.each(currentRoom.Positions.Position, function(index, pos){
			if (pos.id==selectedCartObject.position.id)
			{
				var newElement = "<div onclick='javascript:automaticRestoreObject();' class='li-highlight-restore' style='left:"+pos.x+"px; top:"+pos.y+"px; width:"+selectedCartObject.position.w+"px; height:"+selectedCartObject.position.h+"px;'>";
				newElement += "</div>";
				$("#mediaPanel").append(newElement);
				return false;
			}
		});
	}
}

function highlightCorrectPosition(highlight) {
	$(".li-highlight").remove();
	$(".li-highlight-restore").remove();
	
	if (highlight)
	{	
		//add a div at the correct position
		if (selectedCartObject.room == currentRoomIndex)
		{
			if (typeof selectedCartObject.originalPosition == 'undefined')
			{
				//it was ordered by mistake
				highlighCheckPosition()
			}
			else
			{
				if (!existsItemAtPosition(currentRoom, selectedCartObject.originalPosition))
				{
					$.each(currentRoom.Positions.Position, function(index, pos){
						if (pos.id==selectedCartObject.originalPosition.id)
						{
							var newElement = "<div onclick='javascript:restoreObject();' class='li-highlight' style='left:"+pos.x+"px; top:"+pos.y+"px; width:"+selectedCartObject.originalPosition.w+"px; height:"+selectedCartObject.originalPosition.h+"px;'>";
							newElement += "</div>";
							$("#mediaPanel").append(newElement);
							return false;
						}
					});
				}
				else
				{
					sweetAlert({title:"<?php echo _('Atento'); ?>", text:"<?php echo _('Si estás seguro que el objeto que deseas colocar en esta habitación es correcto, pero éste no se coloca, puede ser que su sitio esté ocupado por uno incorrecto. Intenta quitarlo primero para dejar espacio libre.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
				}
			}
		}
		else
		{
			highlighCheckPosition()
		}
	}
};

function automaticRestoreObject() {
	if (selectedCartObject != null && containsCart(selectedCartObject))
	{
		currentRoom.PositionedItems.push(selectedCartObject);
		var itemToRemove = -1;
		$.each(cart, function(index, it){
			if (it.id == selectedCartObject.id)
			{
				itemToRemove = index;
				return false;
			}
		});
		if (itemToRemove != -1) cart.splice(itemToRemove, 1);
		
		if (unsortedItems.indexOf(selectedCartObject) != -1 && (typeof unsortedItems.originalPosition == 'undefined') && currentRoom.Items.Item.indexOf(selectedCartObject) != -1)
		{
			foundObjects++; //it was disordered, then ordered and then disordered again, and then restored
		}
		
		selectedCartObject = null;
		
		showObjects();
		showRoomAtIndex(currentRoomIndex);
		updateCounters();
	}
};

function restoreObject(){
	if (selectedCartObject != null && containsCart(selectedCartObject))
	{
		selectedCartObject.position = selectedCartObject.originalPosition;
		currentRoom.PositionedItems.push(selectedCartObject);
		delete selectedCartObject.originalPosition;
		var itemToRemove = -1;
		$.each(cart, function(index, it){
			if (it.id == selectedCartObject.id)
			{
				itemToRemove = index;
				return false;
			}
		});
		if (itemToRemove != -1) cart.splice(itemToRemove, 1);
		
		selectedCartObject = null;
		foundObjects++;
		
		showObjects();
		showRoomAtIndex(currentRoomIndex);
		updateCounters();
	}
};

function showHall(){
	$("#mediaAlert").hide();
	$("#show-hall-button").addClass('hide').hide();
	$("#mediaPanel").html(placesHTML());
	currentRoom = null;
	currentRoomIndex = -1;
};

function placeHTML(place) {
	var index = rooms.indexOf(place);
	if (index != -1)
	{
		var placeHTML = "<div class='package-delivery-place' id='place"+index+"'>"
		placeHTML += "<img width='100%' height='100%' src='"+place.background+"' alt='"+place.name+"' /></div>";
		return placeHTML;
	}
	else return "";
};

function placeDoorHTML(place){
	var index = rooms.indexOf(place);
	if (index != -1)
	{
		var frame = frameOfDoor(index);
		var titleHTML = "<div class='li-door-title' style='width:"+frame[2]+"px; left:"+frame[0]+"px; top:"+(frame[1]-60)+"px;'>"+place.name.toUpperCase()+"</div>";
		var placeHTML = "<div class='li-room-door-img' id='place"+index+"' style='left:"+frame[0]+"px; top:"+frame[1]+"px; width:"+frame[2]+"px; height:"+frame[3]+"px;'>";
		placeHTML += "<img src='"+place.background+"' alt='"+place.name+"' /></div>";
		placeHTML += "<div onclick='javascript:showRoomAtIndex("+index+");' class='li-room-door-img-clickable' id='place"+index+"' style='left:"+frame[0]+"px; top:"+frame[1]+"px; width:"+frame[2]+"px; height:"+frame[3]+"px;'></div>"
		return titleHTML+placeHTML;
	}
	else return "";
};

function placesHTML() {
	var src = "exercises/img/LostItemsExercise/hall/hall"+roomsPerLevel[level]+"doors.png";
	var pHTML = "<img alt='<?php echo _('Casa'); ?>' src='"+src+"' width='100%' height='100%' />";
	$.each(rooms, function(index, element){
		pHTML = pHTML+placeDoorHTML(element);
	});
	
	return pHTML;
};

function nextStepIntro() {
	$("#introExercise").fadeOut('slow', function(evt){
		showMiniMenu(true);
		$("#exercise-description").html("<?php echo _('Visita todas las habitaciones para recoger los objetos desordenados y colocarlos en su habitación correspondiente. Acuérdate también de recoger las monedas.'); ?>");
		replayExercise();
	});
};

function checkExercise() {
	swal({
				title: "<?php echo _('¿Estás seguro?'); ?>",
				text: "<?php echo _('¿Seguro que deseas finalizar el ejercicio?'); ?>",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "<?php echo _('Si, terminar'); ?>",
				cancelButtonText: "<?php echo _('No terminar'); ?>",
				closeOnConfirm: true
			},
			function(isConfirm){
				if (isConfirm)
				{
					endTimer = true;

					if ( (foundObjects == unsortedItems.length) && (coinsFound == totalCoins) )
					{
						if (sublevel<2) sublevel++;
						else
						{
							level++;
							sublevel = 0;
						}
						
						exercise.setLevel(level);
						exercise.setSubLevel(sublevel);
						exercise.updateUserLevel();
					}
					
					var sim = (((foundObjects/unsortedItems.length)*0.6)+((coinsFound/totalCoins)*0.4))*100.0;
					
					exercise.setSeconds(exercise.seconds()+timing);
					exercise.setFinalScore(sim+exercise.finalScore());
					exercise.updateResults(coinsFound, totalCoins, foundObjects, unsortedItems.length);
					
					$("#mediaAlert").hide();
					$("#mediaContainer").fadeOut('slow', function(evt){
						showInstructions(true);
						$("#exercise-mini-description").html("");
						
						repetitions++;
						if (repetitions >= numberOfReplays)
						{	
							exercise.setLevel(level);
							exercise.setSubLevel(sublevel);
							exercise.setFinalScore(Math.ceil(exercise.finalScore()/numberOfReplays));
							endTotalTimer = true;
							endExercise();
						}
						else{
							$("#exercise-description").html("<?php echo _('Ya has terminado de ordenar los objetos y recoger las monedas. Ahora volverás a realizar el ejercicio en otro escenario.'); ?>");
							$("#replayExercise").fadeIn('slow');
						}
					});
				}
			});
};

function existsItemAtPosition(room, position){
	var result = false;
	$.each(room.PositionedItems, function(index, item){
		if (item.position.x == position.x && item.position.y == position.y)
		{
			result = true;
			return false;
		}
	});
	return result;
};

//if ( this.Width <= position.Width && this.Height <= position.Height ) {

function itemCanBePlaced(position, item) {
	var posTags = position.tags.split(",");
	var itemTags = item.tag.split(",");
	var w = parseInt(item.w);
	var h = parseInt(item.h);
	var wPos = parseInt(position.w);
	var hPos = parseInt(position.h);
	
	var result = false;
	if (w<=wPos && h<=hPos)
	{
		$.each(itemTags, function(index, tag){
			if (posTags.indexOf(tag) != -1)
			{
				result = true;
				return false;
			}
		});
	}
	return result;
};

function isAlreadyUnsorted(item)
{
	if (typeof item == 'undefined') return false;
	
	var result = false;
	$.each(unsortedItems, function(index, element){
		if (element.id==item.id)
		{
			result = true;
			return false;
		}
	});
	return result;
};

function itemCanReplace(itemSrc, itemDest) {
	var posTags = itemDest.tag.split(",");
	var w = parseInt(itemSrc.w);
	var h = parseInt(itemSrc.h);
	var wPos = parseInt(itemDest.position.w);
	var hPos = parseInt(itemDest.position.h);
	var result = false;
	if (w<=wPos && h<=hPos)
	{
		//check if the destination is already a disordered element
		if (isAlreadyUnsorted(itemDest)) result = false;
		else
		{
			$.each(itemSrc.tag.split(","), function(index, tag){
				if (posTags.indexOf(tag) != -1)
				{
					result = true;
					return false;
				}
			});
		}
	}
	return result;
};

function reorderObject(item) {
	if (typeof item == 'undefined' || typeof item.room == 'undefined') return false;
	
	var reordered = false;
	for (var i=0; i<rooms.length; i++)
	{
		var room = rooms[i];
		var added = false;
		if (item.room != i)
		{
			var pos = firstFreePosition(rooms[i], item);
			if (pos != null)
			{
				added = true;
				item.position = pos;
				rooms[i].PositionedItems.push(item);
				reordered = true;
				break;
			}
		}
		/*else if (item.room != i)
		{
			//decide if we can place it instead of another object
			var replaced = false;
			for (var k=0; k<room.PositionedItems.length; k++){
				var kItem = room.PositionedItems[k];
				if (typeof kItem.originalPosition == 'undefined') //it is not a disordered object
				{
					if (itemCanReplace(item, kItem))
					{
						replaced = true;
						item.position = kItem.position;
						room.PositionedItems[k] = item;
						reordered = true;
						break;
					}
				}
			}
			if (replaced) break;
		}*/
	}
	
	return reordered;
};

function disorderObjects(count){
	if (count == 0) return true;
	var itemsToDisorder = [];
	unsortedItems = [];
	
	//take random items
	for (var index = 0; index<rooms.length; index++){
		var room = rooms[index];
		
		for (var i=0; i<count; i++) //count indicates the amount of elements to take per room
		{
			if (i<room.PositionedItems.length)
			{
				var rIndex = Math.floor(getRandom()*room.PositionedItems.length);
				var obj = room.PositionedItems[rIndex];
				room.PositionedItems.splice(rIndex, 1);
				obj.room = index;
				obj.originalPosition = obj.position;
				itemsToDisorder.push(obj);
			}
		}
	};
	
	var total = count*rooms.length;
	while ((total-itemsToDisorder.length) > 0) //we may have remaining items to take
	{
		var kIndex = Math.floor(getRandom()*rooms.length); //take them randomly
		var room = rooms[kIndex];
		
		if (room.PositionedItems.length > 0)
		{
			var rIndex = Math.floor(getRandom()*room.PositionedItems.length);
			var obj = room.PositionedItems[rIndex];
			room.PositionedItems.splice(rIndex, 1);
			obj.room = kIndex;
			obj.originalPosition = obj.position;
			itemsToDisorder.push(obj);
		}
	}
	
	//pass several times, just in case one object leaves a space for other object
	while(unsortedItems.length < total)
	{
		var replaced = false;
		
		if (!isAlreadyUnsorted(itemsToDisorder[0]))
		{
			if (reorderObject(itemsToDisorder[0]))
			{
				replaced = true;
				unsortedItems.push(itemsToDisorder[0]);
				itemsToDisorder.splice(0, 1);
			}
		}
		
		if (!replaced)
		{
			var obj = itemsToDisorder[0];
			
			var lastIndex = 0;
			$.each(itemsToDisorder, function(index, otherObject){
				if (otherObject != obj && otherObject.room != obj.room)
				{
					if (itemCanReplace(otherObject, obj))
					{	
						var tmp = obj.position;
						obj.position = otherObject.position;
						otherObject.position = tmp;
						
						unsortedItems.push(obj);
						unsortedItems.push(otherObject);
						
						var otherRoom = rooms[otherObject.room];
						var room = rooms[obj.room];
						
						if (room.PositionedItems.indexOf(obj) != -1)
						{
							room.PositionedItems.splice(room.PositionedItems.indexOf(obj), 1);
						}
						if (otherRoom.PositionedItems.indexOf(otherObject) != -1)
						{
							otherRoom.PositionedItems.splice(otherRoom.PositionedItems.indexOf(otherObject), 1);
						}
						
						room.PositionedItems.push(otherObject);
						otherRoom.PositionedItems.push(obj);
						
						lastIndex = index;
						
						replaced = true;
						
						return false;
					}
				}
			});
			
			if (!replaced)
			{
				//the item can not be replaced, take another one
				if (typeof itemsToDisorder[0] == 'undefined')
				{
					return false;
				}
				else
				{
					var room = rooms[itemsToDisorder[0].room];
				
					//take 1 random item from that room
					var rIndex = Math.floor(getRandom()*room.PositionedItems.length);
					var obj = room.PositionedItems[rIndex];
					room.PositionedItems.splice(rIndex, 1);
					
					if (isAlreadyUnsorted(itemsToDisorder[0])) //just in case...
					{
						unsortedItems.splice(unsortedItems.indexOf(itemsToDisorder[0]), 1);
					}
					
					room.PositionedItems.push(itemsToDisorder[0]); //add again
					itemsToDisorder.push(obj);
					obj.room = itemsToDisorder[0].room;
					obj.originalPosition = obj.position;
					
					itemsToDisorder.splice(0, 1);
				}
			}
			else
			{
				itemsToDisorder.splice(lastIndex, 1);
				itemsToDisorder.splice(0, 1);
			}
		}
	}
	
	var ret = true;
	$.each(rooms, function(index, room){
		$.each(room.PositionedItems, function(i, item){
			$.each(room.PositionedItems, function(j, item2){
				if (item.id != item2.id)
				{
					if (item.position.id == item2.position.id)
					{
						ret = false;
						return false;
					}
				}
			});
			if (!ret) return false;
		});
		if (!ret) return false;
	});
	
	return ret;
	
	/*if (count == 0) return;
	var itemsToDisorder = [];
	unsortedItems = [];*/
	
	//include all objects in an array
	/*var allObjects = [];
	for (var index = 0; index<rooms.length; index++){
		var room = rooms[index];
		$.each(room.PositionedItems, function(objIndex, item){
			item.sortingRoom = index;
			allObjects.push(item);
		});
	}
	
	allObjects.sort(function(a,b){
		return 0.5-getRandom();
	});
	
	//take "count" objects
	var i=0;
	var ending = count;
	while(ending > 0)
	{
		var obj = allObjects[i];
		var room = rooms[obj.sortingRoom];
		
		obj.originalPosition = obj.position;
		
		if (reorderObject(obj))
		{
			unsortedItems.push(obj);
			obj.room = obj.sortingRoom;
			allObjects.splice(i, 1);
			ending--;
			room.PositionedItems.splice(room.PositionedItems.indexOf(obj), 1);
		}
		else //this item can't be placed, try to exchange with other
		{
			$.each(allObjects, function(index, otherObject){
				if (otherObject != obj)
				{
					if (itemCanReplace(otherObject, obj))
					{
						obj.originalPosition = obj.position;
						otherObject.originalPosition = otherObject.position;
						
						var tmp = obj.position;
						obj.position = otherObject.position;
						otherObject.position = tmp;
						
						unsortedItems.push(obj);
						unsortedItems.push(otherObject);
						obj.room = obj.sortingRoom;
						otherObject.room = otherObject.sortingRoom;
						
						var otherRoom = rooms[otherObject.sortingRoom];
						room.PositionedItems.splice(room.PositionedItems.indexOf(obj), 1);
						otherRoom.PositionedItems.splice(otherRoom.PositionedItems.indexOf(otherObject), 1);
						
						rooms[obj.sortingRoom].PositionedItems.push(otherObject);
						rooms[otherObject.sortingRoom].PositionedItems.push(obj);
						
						ending -= 2;
						
						allObjects.splice(i, 1);
						allObjects.splice(index, 1);
						
						return false;
					}
				}
			});
		}
	}
	
	var manolito = unsortedItems;
	console.log(manolito);
	
	return count-ending;*/
	
	/*
	for (var index = 0; index<rooms.length; index++){
		var room = rooms[index];
		
		for (var i=0; i<count; i++)
		{
			if (i<room.PositionedItems.length)
			{
				var rIndex = Math.floor(getRandom()*room.PositionedItems.length);
				var obj = room.PositionedItems[rIndex];
				room.PositionedItems.splice(rIndex, 1);
				itemsToDisorder.push(obj);
				obj.room = index;
				obj.originalPosition = obj.position;
			}
		}
	};
	
	//pass several times, just in case one object leaves a space for other object
	var iterateAgain = true;
	while(iterateAgain)
	{
		var replaced = false;
		var i;
		for (i=0; i<itemsToDisorder.length; i++)
		{
			if (reorderObject(itemsToDisorder[i]))
			{
				replaced = true;
				unsortedItems.push(itemsToDisorder[i]);
				itemsToDisorder.splice(i, 1);
				break;
			}
		}
		if (!replaced)
		{
			//the item can not be replaced, take another one
			if (i>=itemsToDisorder.length) i = itemsToDisorder.length-1;
			var room = rooms[itemsToDisorder[i].room];
			
			//take 1 random item
			var rIndex = Math.floor(getRandom()*room.PositionedItems.length);
			var obj = room.PositionedItems[rIndex];
			room.PositionedItems.splice(rIndex, 1);
			room.PositionedItems.push(itemsToDisorder[i]);
			itemsToDisorder.push(obj);
			obj.room = itemsToDisorder[i].room;
			itemsToDisorder.splice(i, 1);
			obj.originalPosition = obj.position;
		}
		
		if (itemsToDisorder.length == 0) iterateAgain = false;
		else iterateAgain = true;
	}*/
};

function firstFreePosition(room, item){
	var result = null;
	$.each(room.Positions.Position, function(index, position){
		if (itemCanBePlaced(position, item))
		{
			if (!existsItemAtPosition(room, position))
			{
				result = position;
				return false;
			}
		}
	});
	
	return result;
};

function replayExercise() {
	do{
		cart = [];
		totalCoins = roomsPerLevel[level]*itemsPerSubLevel[sublevel];
		coinsFound = 0;
		foundObjects = 0;
		
		//take elements
		rooms = [];
		allrooms.Room.sort(function(a,b){
			return 0.5-getRandom();
		});
		$.each(allrooms.Room, function(index, room){
			if (index<roomsPerLevel[level]) rooms.push(room);
			else return false;
		});
		
		$.each(rooms, function(index, room){
			room.PositionedItems = [];
			room.Items.Item.sort(function(a,b){
				return 0.5-getRandom();
			});
			$.each(room.Items.Item, function(j, item){
				var pos = firstFreePosition(room, item);
				if (pos != null)
				{
					if (typeof item.originalPosition != 'undefined') delete item.originalPosition;
					item.position = pos;
					room.PositionedItems.push(item);
				}
			});
			room.PositionedCoins = [];
			room.CoinPositions.Position.sort(function(a,b){
				return 0.5-getRandom();
			});
			for (var k=0; k<itemsPerSubLevel[sublevel] && k < room.CoinPositions.Position.length; k++){
				room.PositionedCoins.push(room.CoinPositions.Position[k]);
			}
		});

	}while(!disorderObjects(itemsPerSubLevel[sublevel]));
	
	showObjects();
	updateCounters();
	
	$("#mediaPanel").css('width', roomWidth);
	$("#mediaPanel").css('height', roomHeight);
	
	$("#mediaAlert").hide();
	$("#replayExercise").fadeOut('slow', function(f){
		$("#mediaContainer").fadeOut('slow', function(evt){
			$("#itemsPanel").removeClass('hide').show();
			$("#mediaPanel").html(placesHTML());
			
			showMiniMenu(true);
			$("#exercise-description").html("<?php echo _('Visita todas las habitaciones para recoger los objetos desordenados y colocarlos en su habitación correspondiente. Acuérdate también de recoger las monedas.'); ?>");
			$("#exercise-mini-description").append("<a class='btn btn-primary' href='javascript:void(0);' onclick='javascript:checkExercise();'><?php echo _('Finalizar'); ?></a>");
			$("#mediaContainer").fadeIn('slow', function(e){
				timing = 0;
				endTimer = false;
				timingFunction();
			});
		});
	});
};

function containsCart(item){
	for (var i=0; i<cart.length; i++)
	{
		if (cart[i].id == item.id) return true;
	}
	return false;
};

function addToCart(itemIndex) {
	var item = currentRoom.PositionedItems[itemIndex];
	if (!containsCart(item))
	{
		if (cart.length == 8)
		{
			$("#mediaAlert").html("<p class='lead'><?php echo _('No puedes llevar más de 8 objetos. Tendrás que soltar algunos objetos en sus habitaciones correspondientes antes de coger más.'); ?></p>");
			$("#mediaAlert").fadeIn('slow');
		}
		else
		{
			//check if the item was previously restored to a correct place
			$.each(unsortedItems, function(i, it){
				if (item.id == it.id && currentRoomIndex == item.room)
				{
					foundObjects--;
					return false;
				}
			});
		
			$("#mediaAlert").hide();
			cart.push(item);
			currentRoom.PositionedItems.splice(itemIndex, 1);
			
			showObjects();
			showRoomAtIndex(currentRoomIndex);
			updateCounters();
		}
	}
};

function removeFromCart(itemIndex) {
	cart.splice(itemIndex, 1);
};

function itemForID(idItem){
	var result = null;
	$.each(allitems.Item, function(index, element){
		if (element.id == idItem)
		{
			result = element;
			return false;
		}
	});
	return result;
};

function placeContainsItem(place, itemID) {
	var result = false;
	$.each(place.Items.Item, function(index, element){
		if (element.id == itemID)
		{
			result = true;
			return false;
		}
	});
	return result;
}

$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	$.ajax({
		type: "GET",
		url: roomsXML,
		dataType: "xml",
		success: function(roomsXMLResult) {
			allrooms = $.xml2json(roomsXMLResult);
			numberOfReplays = 5;
			
			level = 1;
			sublevel = 0;
			
			totalTimerFunction();
			
			//show the panels
			if (!exercise.afterDemo()) $("#introExercise").fadeIn('slow');
			else nextStepIntro();
		}
	});
});

</script>
