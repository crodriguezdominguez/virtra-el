<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!QuestionnaireExercise) {

var QuestionnaireExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Cuestionario!'); ?>', '<?php echo _('Ahora que sabes c&oacute;mo vamos a trabajar, nos gustar&iacute;a que contestases unas preguntas sobre ti.'); ?>', 'exercises/QuestionnaireExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
	}
});

}
</script>