<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<p id='corrects' class="lead hide"></p>
	<p id='omissions' class="lead hide"></p>
	<p id='fails' class="lead hide"></p>
	<p class="lead"></p>
	<a href="javascript:void(0);" onclick="javascript:nextIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<!--<div class="tasklist-introImg" id="introImg"></div>-->
</div>

<div id="wordlistExerciseFixed" class="hide">
	<div class="lead tasklist-fixedlist" id="fixedList"></div>
	<p class="lead tasklist-timer-p"><span id="timer">0</span>/<span id="maxtimer"></span> <?php echo _('Segundos'); ?></p>
</div>

<div id="wordlistExercise" class="hide" style="margin-left: 25%; margin-right: 25%;">
	<div class="pagination pagination-centered">
		<!--<ul>
			<li id='leftPage'><a href="javascript:void(0);" onclick="javascript:previousPage();">&larr;</a></li>
			<li id='page0'><a href="javascript:void(0);" onclick="javascript:loadList(0);">1</a></li>
			<li id='page1'><a href="javascript:void(0);" onclick="javascript:loadList(1);">2</a></li>
			<li id='rightPage'><a href="javascript:void(0);" onclick="javascript:nextPage();">&rarr;</a></li>
		</ul>
		<div class="pull-right"><p><a class="btn btn-primary" href="javascript:void(0);" onclick="javascript:checkExercise();" id="continue-button"><?php echo _('Listo'); ?></a></p></div>-->
	</div>
	<div style="font-size:48px;" id="WordList"></div>
	<div class="text-center" style="margin-top:70px;"><a class="btn btn-success btn-large" href="javascript:void(0);" onclick="javascript:accept();"><i class="icon-thumbs-up icon-white"></i> <?php echo _('Sí'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-danger btn-large" href="javascript:void(0);" onclick="javascript:decline();"><i class="icon-thumbs-down icon-white"></i> <?php echo _('No'); ?></a></div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var tasklistExercise = null;
var correctWords = null;
var checkedWords = null;
var timing = 0;
var resultTiming = 0;
var endTiming = false;
var blueColor = "#0000ff";
var executionNumber = 0;
var level = 1;
var currentIntroIndex = 0;
var maxTime = 60;
var currentPage = 0;
var numberOfSequentialErrors = 0;

function resultTimerFunction(){
	resultTiming += 1;
	if (endTiming == false)
	{
		setTimeout(resultTimerFunction, 1000);
	}
};

function nextIntro(){
	if (executionNumber>=5)
	{
		exercise.setSeconds(0);
		exercise.setFails(0);
		exercise.setCorrects(0);
		exercise.setOmissions(0);
	
		endExercise();
	}
	else
	{
		$("#exercise-description").fadeOut('slow', function(){
			if (!exercise.isLongTerm())
			{
				showInstructions(false);
				switch(currentIntroIndex)
				{
					case 0:
						$("#exercise-description").html("<?php echo _('En este ejercicio verás durante un tiempo limitado una lista de recados que tendrás que tratar de memorizar.'); ?>");
						break;
					case 1:
						$("#exercise-description").html("<?php echo _('Una vez transcurrido el tiempo, aparecerá una lista con muchos recados, entre los cuales estarán los que se te mostraron inicialmente y otros que no estaban.'); ?> <?php echo _('Deberás marcar solo los recados que se te mostraron pulsando sobre ellos. Si te equivocas, podrás pulsar de nuevo para desmarcarlos.'); ?>");
						break;
					default:
						$("#introImg").hide();
						showMiniMenu(false);
						$("#exercise-description").html("<?php echo _('Memoriza estos recados'); ?>");
						beginExercise();
						return; //avoid next operations
				}
			}
			else
			{
				showInstructions(false);
				switch(currentIntroIndex)
				{
					case 0:
						$("#exercise-description").html("<?php echo _('En este ejercicio deberás tratar de marcar los recados que se te mostraron al principio de esta sesión.'); ?>");
						break;
					default:
						$("#introImg").hide();
						$("#exercise-description").html('<?php echo _('Indica si los recados que se muestran estaban o no en la lista que se te mostró al principio de esta sesión. Para ello, deberás pulsar en los botones <strong>Sí</strong> o <strong>No</strong>.'); ?>');
						beginExercise();
						break;
				}
			}
			$("#exercise-description").fadeIn('slow');
			
			currentIntroIndex++;
		});
	}
}

function timerFunction(){
	timing+=1;
	$("#timer").html(""+timing);
	maxTime = (exercise.isLongTerm() || level > 1) ? 60 : 30;
	$("#maxtimer").html(""+maxTime);
	if (timing >= maxTime)
	{
		timing = 0;
		showInstructions(false);
		$("#exercise-description").html("<?php echo _('Pulsa en el botón <strong>Sí</strong> cuando aparezca un recado de la lista anterior. Pulsa el botón <strong>No</strong> en caso contrario.'); ?>");
		
		loadList(0);
		$("#wordlistExerciseFixed").fadeOut('slow', function (e){
			$("#wordlistExercise").fadeIn('slow', function(evt){
				resultTiming = 0;
				endTiming = false;
				resultTimerFunction();
			});
		});
	}
	else
	{
		setTimeout(timerFunction, 1000);
	}
};

function previousPage() {
	if ($("#page1").hasClass("active")) loadList(0);
};

function nextPage() {
	if ($("#page0").hasClass("active")) loadList(1);
};

function toggleClass(element){
	var index = parseInt(element.attr('id').split("word")[1]);
	checkedWords[index] = !checkedWords[index];
	
	if (element.hasClass("wordlist-toggle"))
	{
		element.removeClass("wordlist-toggle");
	}
	else
	{
		element.addClass("wordlist-toggle");
	}
};

function accept(){
	checkedWords[currentPage] = true;
	currentPage++;
	loadList(currentPage);
};

function decline(){
	checkedWords[currentPage] = false;
	currentPage++;
	loadList(currentPage);
};

function loadList(page) {
	/*var initialIndex = page*5;

	$("#WordList").css("color", blueColor);
	var finalValue = "<table style='margin-left:auto; margin-right:auto;'>";
	$.each(tasklistExercise, function(index, element){
		if (index >= initialIndex && index < initialIndex+5)
			finalValue += "<tr><td><a id='word"+index+"' href='javascript:void(0);' class='thumbnail wordlist-exercise text-center "+(checkedWords[index]?"wordlist-toggle":"")+"' onclick='javascript:toggleClass($(this));' style='text-align:left;width:400px;padding:10px;'>"+element+"</a></td></tr>";
	});
	finalValue += "</table>";
	
	$("#page0").removeClass('active');
	$("#page1").removeClass('active');
	
	$("#page"+page).addClass('active');
	if (page == 0) $("#leftPage").addClass('active');
	else $("#leftPage").removeClass('active');
	
	if (page == 2) $("#rightPage").addClass('active');
	else $("#rightPage").removeClass('active');
	
	$("#WordList").html(finalValue);*/
	
	if (page >= tasklistExercise.length)
	{
		checkExercise();
	}
	else
	{
		$("#WordList").css("color", blueColor);
		var finalValue = "<p class='wordlist-exercise text-center' id='word"+page+"'>"+tasklistExercise[page]+"</p>";
		
		$("#WordList").html(finalValue);
	}
}

function beginExercise()
{
	$("#finalExercise").fadeOut('slow');
	$("#introExercise").fadeOut('slow', function(){
		$("#exercise-description").fadeIn('slow');
		/*if (executionNumber==0)
		{*/
			if (executionNumber == 0)
			{
				repetitions = 0;
				sessionID = parseInt(lastSession['sessionID']);
				exerciseID = parseInt(lastSession['exerciseID']);
			}
			
			//retrieve current exercise plan
			$.ajax({
				type: "GET",
				url: "exercises/xml/tasklist-plan.xml",
				dataType: "xml",
				success: function(xml) {
					var plan = $.xml2json(xml);
					var numberOfExercise = -1;
					
					$.ajax({
						type: "GET",
						url: "exercises/xml/tasklist-series.php",
						dataType: "xml",
						success: function(result){
							var tasklistExercises = $.xml2json(result);
							$.each(plan.TaskListPlanEntry, function(index, value){
								if (value.exerciseID == exerciseID && value.sessionID == sessionID)
								{
									//the first are the corrects
									var correctWordsAux = tasklistExercises.TaskListExercise[value.list];
									
									correctWords = [];
									var maxCorrects = 5+((level-1)*2);
									if (maxCorrects > 15) maxCorrects = 15;
									
									$.each(correctWordsAux.Elements.Element, function(index, element){
										if (index<maxCorrects)
											correctWords.push(element.value);
									});
									
									var maxIndex = 10;
									var otherWords = [];
									$.each(tasklistExercises.TaskListExercise, function(index, element){
										if (element.id == "randomWords")
										{
											element.Elements.Element.sort(function(){
												return 0.5 - getRandom();
											});
											var count = 0;
											$.each(element.Elements.Element, function(index, element){
												if (count>=maxIndex) return false;
												if (correctWords.indexOf(element.value) == -1)
												{
													count++;
													otherWords.push(element.value);
												}
											});
											return false;
										}
									});
									
									//create a copy of the array
									tasklistExercise = correctWords.slice(0);
									tasklistExercise = correctWords.concat(otherWords);
									
									tasklistExercise.sort(function(){
										return 0.5 - getRandom();
									});
									
									checkedWords = [];
									$.each(tasklistExercise, function(index, element){
										checkedWords.push(false);
									});
														
									return false; //break the loop
								}
							});
							
							if (tasklistExercise != null){
								if (!exercise.isLongTerm())
								{
									reloadTasklist();
									$("#wordlistExerciseFixed").fadeIn('slow', function(evt){
										timing = 0;
										timerFunction();
									});
								}
								else
								{
									loadList(0);
									$("#wordlistExercise").fadeIn('slow', function(evt){
										resultTiming = 0;
										endTiming = false;
										resultTimerFunction();
									});
								}
							}
							
							else console.debug("error: task exercise not defined");
						}
					});
				}
			});
		/*}
		else
		{
			$.each(checkedWords, function(index, element){
				checkedWords[index] = false;
			});
			reloadTasklist();
			loadList(0);
			$("#wordlistExerciseFixed").fadeIn('slow', function(evt){
				timing = 0;
				timerFunction();
			});
		}*/
	});
};

function reloadTasklist() {
	currentPage = 0;
	var finalValue = "<ul>";
	var total = (2*(level-1))+5;
	if (total > 15) total = 15;	
	
	correctWords.sort(function(){
		return 0.5 - getRandom();
	});
	
	$.each(correctWords, function(index, element){
		if (index < total)
			finalValue += "<li class='tasklist-fixedlist-element'>"+element+"</li>";
	});
	finalValue += "</ul>";
	
	$("#fixedList").html(finalValue);
};

function checkExercise(){
	endTiming = true;
	
	//compare the written list with the original list
	var corrects = 0;
	var fails = 0;
	var omissions = 0;
	
	var rec = {};
	$.each(tasklistExercise, function(index, element){
		rec[element] = (checkedWords[index])?'1':'0';
		if (checkedWords[index]) //if checked
		{
			//check if the element is in correct words
			if (correctWords.indexOf(element) >= 0){ corrects++; }
			else{ fails++; };
		}
		else
		{
			//if not checked, but in correct words, then it's an omission
			//if (correctWords.indexOf(element) >= 0) omissions++;
			if (correctWords.indexOf(element) >= 0) omissions++;
			//else corrects++;
		}
	});
	
	//omissions = tasklistExercise.length-corrects-fails;
	
	exercise.setSeconds(resultTiming);
	exercise.setCorrects(corrects);
	exercise.setFails(fails);
	exercise.setOmissions(omissions);
	
	if (fails == 0 && omissions == 0)
	{
		numberOfSequentialErrors = 0;
		level++;
	}
	else
	{
		numberOfSequentialErrors++;
	}
	
	exercise.updateResults(correctWords, rec);
	
	executionNumber++;
	
	if (exercise.isLongTerm()) executionNumber = 1000; //force finish
	
	currentPage = 0;
	
	if (numberOfSequentialErrors > 4)
	{
		endExercise();
		return;
	}
	
	$('#wordlistExercise').fadeOut('slow', function(){
		$("#timer").html("1");
		showInstructions(true);
		if (executionNumber >= 5)
		{
			$("#exercise-description").html("<?php echo _('Estos son tus últimos resultados en este ejercicio'); ?>");
		}
		else $("#exercise-description").html("<?php echo _('Trata de memorizar de nuevo la lista de recados'); ?>");
		
		if (corrects == 0)
		{
			$("#corrects").html("<?php echo _('No has marcado ningún recado correcto'); ?>");
		}
		else $("#corrects").html("<?php echo _('Has marcado'); ?> "+corrects+(corrects==1?" <?php echo _('recado'); ?>":" <?php echo _('recados'); ?>")+" <?php echo _('correctamente'); ?>");
		
		if (fails == 0)
		{
			$("#fails").html("<?php echo _('No has marcado ningún recado mal'); ?>");
		}
		else $("#fails").html("<?php echo _('Has marcado'); ?> "+fails+(fails==1?" <?php echo _('recado incorrecto'); ?>":" <?php echo _('recados incorrectos'); ?>"));
		if (omissions == 0)
		{
			$("#omissions").html("<?php echo _('No se te ha olvidado marcar ningún recado'); ?>");
		}
		else $("#omissions").html("<?php echo _('Se te ha olvidado marcar'); ?> "+omissions+(omissions==1?" <?php echo _('recado'); ?>":" <?php echo _('recados'); ?>"));
		
		$("#corrects").show();
		$("#fails").show();
		$("#omissions").show();
		
		reloadTasklist();
		
		$("#introExercise").fadeIn('slow');
	});
};


$(function() {
	$("#exercise-description").hide();
	nextIntro();
});

</script>
