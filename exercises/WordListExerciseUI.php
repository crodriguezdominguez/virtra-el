<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<!--<div id="exerciseAgain" class="hide">
<p class="lead"><?php echo _('Se te va a mostrar de nuevo la lista inicial, para que trates de memorizarla otra vez.'); ?></p>
</div>-->

<!--<div id="finalExercise" class="hide">
<p class="lead"><?php echo _('Ahora se te va a mostrar una nueva lista, con los elementos en <span style="color:red;">rojo</span>. Trata de memorizarla. Solo tendrás una oportunidad para hacerlo.'); ?></p>
</div>-->

<div id="introExercise">
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="wordlistExercise" class="hide">
	<div class="lead wordlist-fixedlist" id="fixedList"></div>
	<p class="lead wordlist-timer-p"><span id="timer">0</span>/60 <?php echo _('Segundos'); ?></p>
</div>
<div id="wordlistExerciseAnswers" class="hide">
	<div style="margin-left:5px;text-align: center;"><input type="text" name="answersList" id="answersList" cols="50" class="input-large" style="font-size:24px; height:35px;" />&nbsp;<a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:nextWord();" style="margin-top:-10px;"><?php echo _('Escribir Siguiente Palabra'); ?></a></div>
	<div style="margin-top:80px;margin-left:5px; text-align: center;"><a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:checkExercise();" id="continue-button"><?php echo _('No recuerdo más palabras'); ?></a></div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var wordlistExercise = null;
var timing = 0;
var resultTiming = new Timer();
var blueColor = "#0000ff";
var redColor = "#ff0000";
var executionNumber = 0;
var totalAnswerList = "";
var shouldShow = true;
var shownWords = null;

function nextWord(){
	totalAnswerList += $("#answersList").val()+"\n";
	$("#answersList").val("");
	$("#answersList").focus();
}

function showWordListExercise(func){
	$('#wordlistExerciseAnswers').fadeOut('slow', function(evt){
		showMiniMenu(true);
		$('#exercise-description').html("<?php echo _('Trata de memorizar esta lista de palabras.'); ?>");
		$('#wordlistExercise').fadeIn('slow', func);
	});
}

function showWordListExerciseAnswers(func){
	$('#wordlistExercise').fadeOut('slow', function(evt){
		showInstructions(true);
		$('#exercise-description').html("<?php echo _('A continuación, escribe una a una las palabras que recuerdes sin importar el orden. Para ello, introduce cada palabra en el recuadro de abajo y pulsa <strong>Escribir Siguiente Palabra</strong>. Cuando hayas terminado pulsa <b>No recuerdo más palabras</b>.'); ?>");
		$('#wordlistExerciseAnswers').fadeIn('slow', function(){
			$("#answersList").focus();
			func();
		});
	});
}

function timerFunction(){
	timing+=1;
	$("#timer").html(""+timing);
	if (timing >= 60)
	{
		timing = 0;
		showWordListExerciseAnswers(function(){
            resultTiming.reset();
            resultTiming.start();
		});
	}
	else
	{
		setTimeout(timerFunction, 1000);
	}
}

function reloadExercise()
{
	totalAnswerList = "";
	
	if (wordlistExercise != null)
	{
		if (executionNumber < 3) $("#fixedList").css("color", blueColor);
		else $("#fixedList").css("color", redColor);
		var finalValue = "<ul>";
		
		//randomize words
		/*if (executionNumber == 0 || executionNumber >= 3)
		{
			wordlistExercise.Elements.Element.sort(function(){
				return .5 - getRandom();
			});
		}*/
		
		shownWords = [];
		$.each(wordlistExercise.Elements.Element, function(index, element){
			shownWords.push(element.value);
			finalValue += "<li class='wordlist-fixedlist-element'>"+element.value+"</li>";
		});
		finalValue += "</ul>";
		
		$("#fixedList").html(finalValue);
		showWordListExercise(function(){
			timing = 0;
			timerFunction();
		});
	}
	else
	{
		//error
		console.debug("error: wordlist exercise not defined");
	}
}

function beginExercise()
{
	if (!exercise.afterDemo() && shouldShow)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				$("#exerciseCounter").show();
				shouldShow = false;
				setTimeout(beginExercise, 4000);
			});
		});
	}
	else
	{
		$("#exercise-description").html("<?php echo _('Trata de memorizar esta lista de palabras.'); ?>");
		
		//$("#exerciseAgain").fadeOut('slow');
		//$("#finalExercise").fadeOut('slow');
		$("#introExercise").fadeOut('slow', function(){
			$("#answersList").val("");
			repetitions = 0;
			sessionID = parseInt(lastSession['sessionID']);
			exerciseID = parseInt(lastSession['exerciseID']);
			
			//retrieve current exercise plan
			
			if (executionNumber == 0 || executionNumber >= 3)
			{
				$.ajax({
					type: "GET",
					url: "exercises/xml/wordlist-plan.xml",
					dataType: "xml",
					success: function(xml) {
						var plan = $.xml2json(xml);
						var numberOfExercise = -1;
						
						$.ajax({
							type: "GET",
							url: "exercises/xml/wordlist-series.php",
							dataType: "xml",
							success: function(result){
								var wordlistExercises = $.xml2json(result);
								
								$.each(plan.WordListPlanEntry, function(index, value){
									if (value.exerciseID == exerciseID && value.sessionID == sessionID)
									{
										if (value.list == "random")
										{
											//take one random exercise
											numberOfExercise = Math.floor(getRandom()*wordlistExercises.WordListExercise.length);
										}
										else
										{
											var array = value.list.split(",");
											if (repetitions < array.length)
											{
												var rep = 0;
												if (executionNumber >= 3)
												{
													rep = 1;
												}
												numberOfExercise = parseInt(array[rep]);
											}
											else numberOfExercise = Math.floor(getRandom()*wordlistExercises.WordListExercise.length);
										}
										
										wordlistExercise = wordlistExercises.WordListExercise[numberOfExercise];
															
										return false; //break the loop
									}
								});
								
								reloadExercise();
							}
						});
					}
				});
			}
			else
			{
				reloadExercise();
			}
		});
	}
}

function checkExercise(){
    resultTiming.stop();

	if ($("#answersList").val() != "") //the user entered a word, but didn't click on next word button
	{
		nextWord();
	}
	
	//compare the written list with the original list
	var corrects = 0;
	var omissions = 0;
	var fails = 0;
	var lines = totalAnswerList.split("\n");
	
	var ansList = [];
	for (var i=0; i<lines.length; i++)
	{
		lines[i] = $.trim(lines[i]);
		
		if (lines[i].length > 0) ansList.push(lines[i]);
	}
	exercise.setWordlist(ansList.join("\n"));
	
	//check the correct ones
	$.each(wordlistExercise.Elements.Element, function(index, element){
		var entered = false;
		var trimmedValue = $.trim(element.value).toLowerCase();
		for (var i=0; i<lines.length; i++)
		{
			if (lines[i].toLowerCase()==trimmedValue)
			{
				corrects++;
				entered = true;
				break;
			}
		}
		if (!entered) omissions++;
	});
	
	//check the fails: one written element is not in the word list
	for (var i=0; i<lines.length; i++)
	{
		var comp = lines[i].toLowerCase();
		if (comp.length > 0)
		{
			var entered = false;
			$.each(wordlistExercise.Elements.Element, function(index, element){
				if (comp==$.trim(element.value).toLowerCase())
				{
					entered = true;
					return false;
				}
			});
			
			if (!entered) fails++;
		}
	}
	
	exercise.setSeconds(resultTiming.getSeconds());
	exercise.setCorrects(corrects);
	exercise.setOmissions(omissions);
	exercise.setFails(fails);
	
	exercise.updateResults(shownWords, null);
	
	executionNumber+=1;
	if (executionNumber>3)
	{
		exercise.setSeconds(0);
		exercise.setFails(0);
		exercise.setCorrects(0);
		exercise.setOmissions(0);


		endExercise();
	}
	else
	{
		$('#wordlistExerciseAnswers').fadeOut('slow', function(){
			if (executionNumber<3)
			{
				$("#exercise-description").html("<?php echo _('Se te va a mostrar de nuevo la lista inicial, para que trates de memorizarla otra vez.'); ?>");
				
				//$("#exerciseAgain").fadeIn('slow');
			}
			else
			{
				$("#exercise-description").html("<?php echo _('Ahora se te va a mostrar una nueva lista, con los elementos en <span style=\'color:red;\'>rojo</span>. Trata de memorizarla. Solo tendrás una oportunidad para hacerlo.'); ?>");
				
				//$("#finalExercise").fadeIn('slow');
			}
			$("#introExercise").fadeIn('slow');
		});
	}
}
</script>
