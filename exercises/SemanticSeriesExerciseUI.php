<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise" class="hide">
	<p class="lead"><?php echo _('Por ejemplo:'); ?> <?php echo _('Negro, Rojo, Verde, <b style="color:red">Mesa</b>, Azul, Amarillo.'); ?></p>
	<div>
	<ul class="thumbnails">
		<li class="span2"><span class="thumbnail semantic-exercise"><?php echo _('Negro'); ?></span></li>
		<li class="span2"><span class="thumbnail semantic-exercise"><?php echo _('Rojo'); ?></span></li>
		<li class="span2"><span class="thumbnail semantic-exercise"><?php echo _('Verde'); ?></span></li>
		<li class="span2"><span class="thumbnail semantic-exercise" style="font-weight: bold; color: red;"><?php echo _('Mesa'); ?></span></li>
		<li class="span2"><span class="thumbnail semantic-exercise"><?php echo _('Azul'); ?></span></li>
		<li class="span2"><span class="thumbnail semantic-exercise"><?php echo _('Amarillo'); ?></span></li>
	</ul>
	</div>
	<p class="lead"><?php echo _('En este grupo de palabras, <b style="color:red">Mesa</b> es la &uacute;nica palabra que no es un color, y, por tanto, es la palabra que sobra.'); ?></p>
	<a href="javascript:void(0);" onclick="javascript:showSemanticExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="semanticExercise" class="hide">
	<ul class="thumbnails">
		<li class="span2"><a id="answer0" href="javascript:void(0);" onclick="javascript:checkAnswer(0);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer1" href="javascript:void(0);" onclick="javascript:checkAnswer(1);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer2" href="javascript:void(0);" onclick="javascript:checkAnswer(2);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer3" href="javascript:void(0);" onclick="javascript:checkAnswer(3);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer4" href="javascript:void(0);" onclick="javascript:checkAnswer(4);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer5" href="javascript:void(0);" onclick="javascript:checkAnswer(5);" class="thumbnail hide"></a></li>
	</ul>
	<div class="lead" id="exercise-explanation"></div>
</div>

<div style="text-align:left; margin-bottom:40px; margin-top: 40px;"><a class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:checkExercise();" id="ready-button"><?php echo _('Listo'); ?></a></div>

<div style="text-align:left; margin-bottom: 40px;margin-top: 40px;"><a href="javascript:void(0);" onclick="javascript:endExercise();" class="hide btn btn-primary btn-large" id="continue-button"><?php echo _('Continuar'); ?></a></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var semanticExercise = null;
var answer = -1;

function showSemanticExercise(){
	if (repetitions == 0)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				$("#exerciseCounter").show();
				setTimeout(function(){
					showMiniMenu(true);
					$('#semanticExercise').fadeIn('slow');
					$('#continue-button').fadeOut('slow');
					//$('#ready-button').fadeIn('slow', function(){
						$('#exercise-title').html("<?php echo _('&iquest;Qu&eacute; palabra no est&aacute; relacionada?'); ?>");
						$('#exercise-description').html("<?php echo _('Selecciona la palabra que sobra y pulsa'); ?> <strong><?php echo _('Listo'); ?></strong>");
					//});
				}, 4000);
			});
		});
	}
	else
	{
		$('#introExercise').fadeOut('slow', function(evt){
			showMiniMenu(true);
			$('#semanticExercise').fadeIn('slow');
			$('#continue-button').fadeOut('slow');
			//$('#ready-button').fadeIn('slow', function(){
				$('#exercise-title').html("<?php echo _('&iquest;Qu&eacute; palabra no est&aacute; relacionada?'); ?>");
				$('#exercise-description').html("<?php echo _('Selecciona la palabra que sobra y pulsa'); ?> <strong><?php echo _('Listo'); ?></strong>");
			//});
		});
	}
};

function checkAnswer(num) {
	answer = num;
	
	$('#ready-button').fadeIn('fast');
	
	$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle');
	$('#answer'+num).addClass('semantic-exercise-toggle');
};

function checkExercise(){
	var initialText = null;
	if (answer == -1)
	{
		$('#exercise-explanation').html("<?php echo _('Debe seleccionar alguna respuesta'); ?>");
	}
	else
	{
		if (parseInt(semanticExercise.Answer.position) == answer) //win
		{
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-ok");
			initialText = "<span style='color:green;'><b><?php echo _('&iexcl;Correcto!'); ?></b></span><br />";
			exercise.setCorrect(true);
		}
		else //lose
		{
			var goodAnswer = semanticExercise.Answer.position;
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-bad");
			initialText = "<b><?php echo _('&iexcl;Lo siento!'); ?></b> <?php echo _('La palabra correcta es'); ?> <b>"+semanticExercise.Elements.Element[goodAnswer].value+"</b><br />";
			$('#answer'+goodAnswer).addClass("semantic-exercise-ok");
			exercise.setCorrect(false);
		}
		
		for (i=0; i<6; i++)
		{
			$('#answer'+i).addClass('disabled').attr('onclick', 'javascript:void(0);');
		}
		
		if (exercise.offerHelp())
		{
			$('#exercise-explanation').html(initialText+"<b><?php echo _('Explicaci&oacute;n: '); ?></b>"+semanticExercise.explanation);
		}
		
		$('#ready-button').fadeOut('fast', function(evt){
			$('#exercise-description').html('Cuando estés preparado pulsa en <strong>Continuar</strong>.');
			$('#continue-button').fadeIn('fast');
		});
	}
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (repetitions==0) //show intro
	{
		$('#introExercise').show();
	}
	else //show exercise
	{
		showSemanticExercise();
	}
	
	//retrieve current exercise plan
	$.ajax({
		type: "GET",
		url: "exercises/xml/semantic-series-plan.xml",
		dataType: "xml",
		success: function(xml) {
			var plan = $.xml2json(xml);
			var numberOfExercise = -1;
			
			$.ajax({
				type: "GET",
				url: "exercises/xml/semantic-series.php",
				dataType: "xml",
				success: function(result){
					var semanticExercises = $.xml2json(result);
					
					$.each(plan.SemanticSeriesPlanEntry, function(index, value){
						if (value.exerciseID == exerciseID && value.sessionID == sessionID)
						{
							if (value.exercises == "random")
							{
								//take one random exercise
								numberOfExercise = Math.floor(getRandom()*semanticExercises.SemanticSeriesExercise.length);
							}
							else
							{
								var array = value.exercises.split(",");
								if (repetitions < array.length)
									numberOfExercise = parseInt(array[repetitions]);
								else numberOfExercise = Math.floor(getRandom()*semanticExercises.SemanticSeriesExercise.length);
							}
							
							semanticExercise = semanticExercises.SemanticSeriesExercise[numberOfExercise];
												
							return false; //break the loop
						}
					});
					
					if (semanticExercise != null)
					{
						$.each(semanticExercise.Elements.Element, function(index, element){
							var idAnswer = '#answer'+element.position;
							$(idAnswer).html(element.value).removeClass('hide').addClass('semantic-exercise').show();
						});
					}
					else
					{
						//error
						console.debug("error: semantic exercise not defined");
					}
				}
			});
		}
	});
});
</script>