function mapExerciseTitle(exerciseType)
{
	var e = '';
	switch(parseInt(exerciseType))
	{
		// Memory
		case 1:
			e = '<?php echo _('Números y vocales'); ?>';
			break;
		case 2:
			e = '<?php echo _('Lista de recados'); ?>';
			break;
		case 3:
			e = '<?php echo _('Clasificación de imágenes'); ?>';
			break;
		case 18:
			e = '<?php echo _('Lista de recados (Largo Plazo)'); ?>';
			break;
		case 19:
			e = '<?php echo _('Lista de palabras'); ?>';
			break;
		case 7:
			e = '<?php echo _('Dictado de números'); ?>';
			break;
		case 8:
			e = '<?php echo _('Lista de palabras'); ?>';
			break;
		case 25:
			e = '<?php echo _('Test de posiciones'); ?>';
			break;
			
		// Attention
		case 4:
			e = '<?php echo _('Globos'); ?>';
			break;
		case 5:
			e = '<?php echo _('Bolsa de objetos'); ?>';
			break;
		case 6:
			e = '<?php echo _('Pirámides'); ?>';
			break;
		case 9:
			e = '<?php echo _('Objetos desordenados'); ?>';
			break;
			
		// Reasoning exercises
		case 11:
			e = '<?php echo _('Series semánticas'); ?>';
			break;
		case 12:
			e = '<?php echo _('Series lógicas'); ?>';
			break;				
		case 13:
			e = '<?php echo _('Piezas de puzzle'); ?>';
			break;				
		case 14:
			e = '<?php echo _('¿Cuál es diferente?'); ?>';
			break;
		case 15:
			e = '<?php echo _('Analogías semánticas'); ?>';
			break;
	
		// Planning
		case 16:
			e = '<?php echo _('Comprar regalos'); ?>';
			break;
		case 17:
			e = '<?php echo _('Reparto de paquetes'); ?>';
			break;
		case 20:
			e = '<?php echo _('Realidad virtual (pre)'); ?>';
			break;
		case 21:
			e = '<?php echo _('Realidad virtual (post)'); ?>';
			break;
			
		// Tests
		case 30:
			e = '<?php echo _('Cuestionario (I)'); ?>';
			break;
			
		case 32:
			e = '<?php echo _('Prueba de puntería'); ?>';
			break;
			
		case 33:
			e = '<?php echo _('Cuestionario (II)'); ?>';
			break;
			
		// Helps
		case 34:
			e = '<?php echo _('Ej. de Introducción'); ?>';
			break;
			
		case 101:
			e = '<?php echo _('Primera ayuda'); ?>';
			break;
	
		case 102:
			e = '<?php echo _('Segunda ayuda'); ?>';
			break;
	}
	
	return e;
}