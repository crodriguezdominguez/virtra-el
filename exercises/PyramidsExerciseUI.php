<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<a style="margin-bottom: 20px;" href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<p><img src="exercises/img/PyramidsExercise/c1.jpg" class="img-polaroid" alt="pirámide 1" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/c2.jpg" class="img-polaroid" alt="pirámide 1" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/c3.jpg" class="img-polaroid" alt="pirámide 1" width="105" height="105" /></p>
</div>

<div id="pyramidsPanel" class="hide">
	<table cellspacing="5" cellpadding="5" style="table-layout: fixed; width: 100%;">
		<tr>
		    <td><div id="start_arrow" style="margin-left:-80px;margin-top:20px;position:absolute;width:80px;height:auto;"><img src="exercises/img/PyramidsExercise/arrow-right.png" alt="<?php echo _('flecha derecha'); ?>" width="80" /><br /><span style="text-align:center; width:80px; display: block; font-weight: bold;"><?php echo _('Comienza aquí'); ?></span></div><a id="pyramid0" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 0);"></a></td>
		    <td><a id="pyramid1" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 1);"></a></td>
		    <td><a id="pyramid2" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 2);"></a></td>
		    <td><a id="pyramid3" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 3);"></a></td>
		    <td><a id="pyramid4" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 4);"></a></td>
		    <td><a id="pyramid5" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 5);"></a></td>
		    <td><a id="pyramid6" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 6);"></a></td>
		    <td><a id="pyramid7" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 7);"></a></td>
		    <td><a id="pyramid8" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 8);"></a></td>
		    <td><a id="pyramid9" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 9);"></a></td>
		</tr>
		<tr>
		    <td><a id="pyramid10" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 10);"></a></td>
		    <td><a id="pyramid11" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 11);"></a></td>
		    <td><a id="pyramid12" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 12);"></a></td>
		    <td><a id="pyramid13" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 13);"></a></td>
		    <td><a id="pyramid14" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 14);"></a></td>
		    <td><a id="pyramid15" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 15);"></a></td>
		    <td><a id="pyramid16" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 16);"></a></td>
		    <td><a id="pyramid17" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 17);"></a></td>
		    <td><a id="pyramid18" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 18);"></a></td>
		    <td><a id="pyramid19" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 19);"></a></td>
		</tr>
		<tr>
		    <td><a id="pyramid20" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 20);"></a></td>
		    <td><a id="pyramid21" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 21);"></a></td>
		    <td><a id="pyramid22" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 22);"></a></td>
		    <td><a id="pyramid23" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 23);"></a></td>
		    <td><a id="pyramid24" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 24);"></a></td>
		    <td><a id="pyramid25" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 25);"></a></td>
		    <td><a id="pyramid26" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 26);"></a></td>
		    <td><a id="pyramid27" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 27);"></a></td>
		    <td><a id="pyramid28" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 28);"></a></td>
		    <td><a id="pyramid29" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 29);"></a></td>
		</tr>
		<tr>
		    <td><a id="pyramid30" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 30);"></a></td>
		    <td><a id="pyramid31" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 31);"></a></td>
		    <td><a id="pyramid32" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 32);"></a></td>
		    <td><a id="pyramid33" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 33);"></a></td>
		    <td><a id="pyramid34" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 34);"></a></td>
		    <td><a id="pyramid35" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 35);"></a></td>
		    <td><a id="pyramid36" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 36);"></a></td>
		    <td><a id="pyramid37" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 37);"></a></td>
		    <td><a id="pyramid38" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 38);"></a></td>
		    <td><a id="pyramid39" href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 39);"></a></td>
		</tr>
	</table>
</div>

<div id="changePanel-pyramids" class="hide" style="background-color: white; position:absolute; top:0; bottom:0; left:0; height: 0; width: 100%; height: 100%; z-index: 1000000;">
	<h1 style="text-align: center; display:none; vertical-align: middle;"><?php echo _('Cambio de panel'); ?></h1>
</div>

<div class="pull-left lead" id="result-explanation"></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var lastCheckedIndex = -1;
var countFlashes = 0;

var changePanelCounter = 1;

var correctImgs = ["c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11", "c12"];
//var badImgs = ["d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "d11", "d12", "d13", "d14", "d15", "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23", "d24", "d25", "d26", "d27", "d28", "d29", "d30", "d31", "d6"];
var badImgs = ["d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "d11", "d12", "d13", "d14", "d15", "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23", "d24", "d25", "d26", "d27", "d28"];
var commonArray = null;

var resultTiming = new Timer();

function beginExercise() {
	countFlashes = 0;
	
	exercise.setHasBegan(true);
	$('#introExercise').fadeOut('slow', function(evt){
		replayExercise();
	});
};

function checkIndex(but, index) {
	lastCheckedIndex = index;
	
	if (but.hasClass("pyramid-toggle")){
		but.children().css('opacity', '1.0');
		but.removeClass("pyramid-toggle");
	}
	else {
		but.addClass("pyramid-toggle");
		but.children().css('opacity', '0.6');
	}
};

function timingFunction(t) {
	if (t<20)
	{
		var str = "<?php echo _('Rápido, ¡selecciona las pirámides correctas! Si te equivocas mientras seleccionas, pulsa de nuevo para desmarcar.'); ?>";
		var post = "";
		if ((20-t) == 1) post = " <strong><?php echo _('Queda 1 segundo'); ?></strong>";
		else post = " <strong><?php echo _('Quedan'); ?> "+(20-t).toString()+" <?php echo _('segundos'); ?></strong>";
		
		$('#timer').html(str+post);
		$('#mini-timer').html(post);
		setTimeout(function(){
			timingFunction(t+1);
		}, 1000);
	}
	else
	{
        resultTiming.stop();
		//replay exercise and update corrects
		var corrects = 0;
		var omissions = 0;
		var failures = 0;
		for (var i=0; i<commonArray.length; i++)
		{
			var pyramid = $("#pyramid"+i);
			var img = commonArray[i];
			if (pyramid.hasClass("pyramid-toggle"))
			{
				if (correctImgs.indexOf(img) == -1)
				{
					//it is not a correct image
					failures++;
				}
				else corrects++;
			}
			else //we didn't choose it
			{
				if (correctImgs.indexOf(img) >= 0)
				{
					//it is correct, but we didn't choose it
					omissions++;
				}
			}
		}
		
		exercise.setCorrects(exercise.corrects()+corrects);
		exercise.setFails(exercise.fails()+failures);
		exercise.setOmissions(exercise.omissions()+omissions);
		
		exercise.updatePartialResult(JSON.stringify({"corrects":corrects, "failures":failures, "omissions":omissions, "lastCheckedIndex": lastCheckedIndex}), repetitions);
		
		$("#pyramidsPanel").fadeOut('slow', function(evt){
			$("#changePanel-pyramids").show();
			$("#changePanel-pyramids").css('display', 'table');
			$("#changePanel-pyramids h1").css('display', 'table-cell');
			changePanelPyramids();
		});
	}
};

function flashArrow()
{
	$("#start_arrow").fadeOut('slow', function(){
		$("#start_arrow").fadeIn('slow', function(){
			countFlashes++;
			if (countFlashes < 3)
			{
				flashArrow();
			}
			else
			{
				$("#start_arrow").fadeOut('slow', function(){
                    resultTiming.start();
					timingFunction(0);
				});
			}
		});
	});
}

function changePanelPyramids()
{
	changePanelCounter--;
	
	if (changePanelCounter < 0)
	{
		$('#changePanel-pyramids').fadeOut('fast', function(){
			$("#changePanel-pyramids h1").css('display', 'none');
			changePanelCounter = 1;
			replayExercise();
		});
	}
	else
	{
		setTimeout(changePanelPyramids, 1000);
	}
};

function fillTable() {
	$(".pyramid-toggle").removeClass("pyramid-toggle");
	
	for (var i=0; i<commonArray.length; i++)
	{
		var img = commonArray[i];
		$("#pyramid"+i).html("<img src='exercises/img/PyramidsExercise/"+img+".jpg' alt='pirámide"+i+"' width='105' height='105' />");
	}
};

function replayExercise() {
	countFlashes = 0;
	
	$('#exercise-description').fadeOut('slow', function(evt){
		repetitions++;
		if (repetitions > 12)
		{
            exercise.setSeconds(resultTiming.getSeconds());
			$('#exercise-mini-description').html("");
			endExercise();
			return;
		}
		
		commonArray = correctImgs.concat(badImgs);
		commonArray.sort(function(){
			return 0.5 - getRandom();
		});
	
		showMiniMenu(false);
		$('#exercise-description').html("<?php echo _('Repetición:') ?> "+repetitions+"/12. <span id='timer'></span>");
		$('#exercise-mini-description').html("<?php echo _('Repetición:') ?> "+repetitions+"/12. <span id='mini-timer'></span>");
		$('#exercise-description').show();
		$('#timer').html("");
		$('#mini-timer').html("");
		$("#pyramidsPanel").fadeOut('slow', function(evt){
			fillTable();
			
			$("#pyramidsPanel").fadeIn('slow', function(evt){
				flashArrow();
			});
		});
	});
};

$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (exercise.afterDemo())
	{
		$("#introExercise").hide();
		beginExercise();
	}
});

</script>
