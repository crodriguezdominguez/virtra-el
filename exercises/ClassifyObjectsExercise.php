<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!ClassifyObjectsExercise) {

var ClassifyObjectsExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('Clasificación y memorización de imágenes'); ?>', '<?php echo _('En este ejercicio vamos a trabajar tu capacidad para memorizar imágenes. Para ello te mostraré un grupo de imágenes durante un tiempo determinado para que las memorices y luego deberás seleccionar aquellas que has aprendido. Pulsa <strong>Continuar</strong>.');?>', 'exercises/ClassifyObjectsExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._omissions = 0;
		this._seconds = 0;
		this._level = 0;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	demoUIurl : function() {
		//override to return the demo UI
		/*if (parseInt(this.sessionID())==5) return "exercises/ClassifyObjectsExerciseDemoUI.php";
		else return null;*/
		
		return "exercises/ClassifyObjectsExerciseDemoUI.php";
	},
	setOmissions: function(f){
		this._omissions = f;
	},
	omissions: function(f){
		return this._omissions;
	},
	updateResults : function() {
		this.finishExercise();
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	level : function() {
		return this._level;
	},
	setLevel : function(l) {
		this._level = l;
	},
	updateUserLevel : function() {
		var qry = 'exerciseType=3&userLevel='+this._level+'&userSubLevel=0';
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_level.php',
			success: function(data){	
			}
		});
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		this.updateUserLevel();
		
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_omissions_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	canAvoidHelp : function(){
		if (this._sessionID == 8 && !this._repeating)
		{
			return true;
		}
		else return false;
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
