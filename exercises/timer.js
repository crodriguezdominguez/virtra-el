var Timer = function () {
    var seconds = 0;
    Timer.instances = (!Timer.instances) ? [this] : Timer.instances.concat(this);

    this.start = function () {
        if (!this.interval) {
            this.interval = setInterval(function () {
                seconds += 1;
            }, 1000);
        }
    };

    this.stop = function () {
        clearInterval(this.interval);
        delete this.interval;
    };

    this.stopTimers = function() {
        for(var i = 0; i < Timer.instances.length; i++)
            Timer.instances[i].stop();
    };


    this.resume = function () {
        this.start();
    };

    this.reset = function () {
        seconds = 0;
    };

    this.getSeconds = function () {
        return seconds;
    };

    this.setSeconds = function (s) {
        seconds = s;
    };

    this.getInstances = function () {
        return Timer.instances;
    };
};
