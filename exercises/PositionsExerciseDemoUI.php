<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<a href="javascript:void(0);" onclick="javascript:nextInstructions();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<table id="table-intro" cellspacing="5" cellpadding="5" style="background-color:lightyellow;margin-bottom:20px; margin-top:20px;">
		<tr>
		    <td><img src="exercises/img/PositionsExercise/ventana-close.png" alt="<?php echo _('Ventana cerrada'); ?>" width="105" height="105" /></td>
		    <td><img src="exercises/img/PositionsExercise/ventana-open.png" alt="<?php echo _('Ventana abierta'); ?>" width="105" height="105" /></td>
		    <td><img src="exercises/img/PositionsExercise/ventana-close.png" alt="<?php echo _('Ventana cerrada'); ?>" width="105" height="105" /></td>
		</tr>
	</table>
</div>
<div id="positionsPanel" style="margin-top:10px;" class="hide">
	<p id="help" class="lead"></p>
	<p id="endDemo-p" class="hide"><a href="javascript:void(0);" onclick="javascript:endDemo();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
	<p id="replayDemo-p" class="hide"><a href="javascript:void(0);" onclick="javascript:replayExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
	<table cellspacing="5" cellpadding="5" style="background-color:lightyellow;">
		<tr>
		    <td><a id="position0" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 0);"></a></td>
		    <td><a id="position1" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 1);"></a></td>
		    <td><a id="position2" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 2);"></a></td>
		    <td><a id="position3" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 3);"></a></td>
		    <td><a id="position4" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 4);"></a></td>
		</tr>
		<tr>
		    <td><a id="position5" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 5);"></a></td>
		    <td><a id="position6" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 6);"></a></td>
		    <td><a id="position7" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 7);"></a></td>
		    <td><a id="position8" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 8);"></a></td>
		    <td><a id="position9" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 9);"></a></td>
		</tr>
		<tr>
			<td><a id="position10" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 10);"></a></td>
		    <td><a id="position11" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 11);"></a></td>
		    <td><a id="position12" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 12);"></a></td>
		    <td><a id="position13" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 13);"></a></td>
		    <td><a id="position14" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 14);"></a></td>
		</tr>
		<tr>
			<td><a id="position15" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 15);"></a></td>
		    <td><a id="position16" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 16);"></a></td>
		    <td><a id="position17" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 17);"></a></td>
		    <td><a id="position18" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 18);"></a></td>
		    <td><a id="position19" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 19);"></a></td>
		</tr>
		<tr>
			<td><a id="position20" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 20);"></a></td>
		    <td><a id="position21" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 21);"></a></td>
		    <td><a id="position22" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 22);"></a></td>
		    <td><a id="position23" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 23);"></a></td>
		    <td><a id="position24" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 24);"></a></td>
		</tr>
	</table>
</div>

<div class="pull-left lead" id="result-explanation"></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;

var positionsXML = "exercises/xml/positions-grids.xml";

var grids = null;
var currentGridN = 0;

var disabled = true;

var commonArray = null;
var selectedElements = null;

var currentInstructions = 0;
var currentHelp = 0;

var correctDemos = 0;

function nextHelp() {
	
	var help = null;
	switch(currentHelp)
	{
		case 0:
		break;
		case 1:
		break;
		case 2:
		break;
		case 3:
		break;
		case 4:
		break;
	}
	
	if (help!=null)
	{
		
	}
	
	currentHelp++;
}

function nextInstructions(){
	var instructions = null;
	switch(currentInstructions){
		case 0:
			instructions = "<?php echo _('Vamos a realizar una prueba, para que sepas cómo se lleva a cabo este ejercicio.'); ?>";
			break;
		case 1:
			instructions = "<?php echo _('El edificio será como el que se muestra abajo. Puedes ver como hay 25 ventanas distribuidas en filas y columnas.'); ?>";
			$(".position-exercise-window-disabled").html("<img src='exercises/img/PositionsExercise/ventana-close.png' alt='<?php echo _('Ventana abierta'); ?>' width='80' height='80' />");
			$("#table-intro").fadeOut('slow', function(){
				$("#positionsPanel").fadeIn('slow');
			});
			break;
		case 2:
			instructions = "<?php echo _('Este edificio es igual que el anterior, pero ahora hay algunas ventanas abiertas. Deberás fijarte muy bien en el lugar que ocupan las ventanas abiertas, porque luego tendrás que localizarlas. Vamos a hacer una prueba: ¿Cómo crees que te será más fácil recordar las ventanas abiertas, por filas o por columnas?'); ?>";
			commonArray = grids.grid[currentGridN].positions.split(',');
			fillTable();
			break;
		default:
			beginExercise();
			break;
	}
	
	if (instructions != null)
	{
		$("#exercise-description").fadeOut('slow', function(){
			$("#exercise-description").html(instructions);
			$("#exercise-description").fadeIn('slow');
		});
	}
	
	currentInstructions++;
}

function beginExercise() {
	$('#introExercise').fadeOut('slow', function(evt){
		replayExercise();
	});
};

function checkIndex(but, index) {

	if (!disabled)
	{
		if (!but.hasClass("position-exercise-window-selected")) but.addClass("position-exercise-window-selected");
		
		if (commonArray[index] != '1')
		{
			nextHelp();
		}
		
		if ($(".position-exercise-window-selected").length >= 5)
		{
			disabled = true;
			updateCorrects();
		}
	}
};

function updateCorrects()
{
	//replay exercise and update corrects
	var corrects = 0;
	var omissions = 0;
	var failures = 0;
	for (var i=0; i<commonArray.length; i++)
	{
		var position = $("#position"+i);
		var open = commonArray[i];
		if (position.hasClass("position-exercise-window-selected"))
		{
			if (open!='1')
			{
				//it is not a correct image
				failures++;
			}
			else corrects++;
		}
		else //we didn't choose it
		{
			if (open == '1')
			{
				//it is correct, but we didn't choose it
				omissions++;
			}
		}
	}
	
	if (corrects == 5)
	{
		correctDemos++;
	}
	else correctDemos = 0;
	
	if (correctDemos==2)
	{
		$("#exercise-description").fadeOut('slow', function(){
			$("#exercise-description").html("<?php echo _('¡Enhorabuena! Has completado la prueba correctamente. Ahora vamos a realizar el ejercicio. Pulsa en <strong>Continuar</strong> cuando estés listo'); ?>");
			$("#exercise-description").fadeIn('slow');
			$("#endDemo-p").fadeIn('slow');
		});
	}
	else if (correctDemos != 0)
	{
		$("#exercise-description").fadeOut('slow', function(){
			$("#exercise-description").html("<?php echo _('¡Perfecto! Lo has hecho muy bien. Vamos a ver si eres capaz de repetir la prueba de nuevo. Pulsa en <strong>Continuar</strong> cuando estés listo'); ?>");
			$("#exercise-description").fadeIn('slow');
			$("#replayDemo-p").fadeIn('slow');
		});
	}
	else
	{
		$("#exercise-description").fadeOut('slow', function(){
			$("#exercise-description").html("<?php echo _('Has cometido algún fallo. Vamos a repetir de nuevo la prueba, para ver si consigues memorizar las posiciones de las ventanas abiertas. Pulsa en <strong>Continuar</strong> cuando estés listo'); ?>");
			$("#exercise-description").fadeIn('slow');
			$("#replayDemo-p").fadeIn('slow');
		});
	}
}

function timingFunction(t) {
	if (t<10)
	{
		var secs = exercise.seconds();
		exercise.setSeconds(secs+1);
		var str = "<?php echo _('Trata de memorizar las posiciones de las ventanas abiertas.'); ?>";
		if ((10-t) == 1) $('#timer').html(str+" <strong><?php echo _('Queda 1 segundo'); ?></strong>");
		else $('#timer').html(str+" <strong><?php echo _('Quedan'); ?> "+(10-t).toString()+" <?php echo _('segundos'); ?></strong>");
		setTimeout(function(){
			timingFunction(t+1);
		}, 1000);
	}
	else
	{
		disabled = false;
	
		$(".position-exercise-window-disabled").html("<img src='exercises/img/PositionsExercise/ventana-close.png' alt='<?php echo _('Ventana abierta'); ?>' width='80' height='80' />");
	
		$(".position-exercise-window-disabled").addClass('position-exercise-window').removeClass("position-exercise-window-disabled");
	
		$('#exercise-description').html("<?php echo _('Marca ahora las posiciones de las ventanas que estaban abiertas.'); ?>");
		$('#timer').html("");
	}
};

function fillTable() {
	for (var i=0; i<commonArray.length; i++)
	{
		var open = commonArray[i]=='1';
		$("#position"+i).html("<img src='exercises/img/PositionsExercise/ventana-"+(open?'open':'close')+".png' alt='<?php echo _('Ventana'); ?> "+(open?'<?php echo _('abierta'); ?>':'<?php echo _('cerrada'); ?>')+"' width='80' height='80' />");
	}
};

function replayExercise() {
	disabled = true;

	$("#help").html("");
	currentHelp = 0;
	$("#replayDemo-p").fadeOut('slow');

	$(".position-exercise-window").addClass('position-exercise-window-disabled').removeClass("position-exercise-window").removeClass('position-exercise-window-selected');

	$('#exercise-description').fadeOut('slow', function(evt){
		fillTable();
	
		$('#exercise-description').html("<span id='timer'></span>");
		$('#exercise-description').show();
		$('#timer').html("");
		timingFunction(0);
	});
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	$.ajax({
		type: "GET",
		url: positionsXML,
		dataType: "xml",
		success: function(gridsXMLResult) {
			grids = $.xml2json(gridsXMLResult);
		}
	});
	
	/*
	if (repetitions >= 1)
	{
		$('#exercise-description').html("<?php echo _('Continuamos con el ejercicio. Recuerda que debes seleccionar aquellas imágenes que tengan <strong>una</strong> pirámide grande y <strong>dos</strong> pequeñas con puerta en el <strong>lado soleado</strong>. Debajo puedes ver varios ejemplos del tipo de imágenes que debes seleccionar.'); ?>");
	}*/
});

</script>
