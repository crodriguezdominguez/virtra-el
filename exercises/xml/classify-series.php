<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<ClassifyExercises>
	<ClassifyExercise id="1" difficulty="1" explanation="<?php echo _('Es el único que no está relleno.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="5" />
			<Element position="3" value="4" />
			<Element position="4" value="3" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="2" difficulty="1" explanation="<?php echo _('Los demás van emparejados y con los colores invertidos.'); ?>">
		<Elements>
			<Element position="0" value="5" />
			<Element position="1" value="2" />
			<Element position="2" value="4" />
			<Element position="3" value="1" />
			<Element position="4" value="3" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="3" difficulty="0" explanation="<?php echo _('Es el único donde el punto está fuera del círculo.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="4" difficulty="0" explanation="<?php echo _('Es el único que tiene un cuadrado en el exterior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="5" difficulty="0" explanation="<?php echo _('Es el único triángulo.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="6" difficulty="0" explanation="<?php echo _('Es el único que sólo tiene un elemento en el interior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="7" difficulty="2" explanation="<?php echo _('Es el único que sólo tiene tres puntos, los demás tienen 4 puntos.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="8" difficulty="2" explanation="<?php echo _('Es el único que tiene 4 líneas, los demás tienen 3 líneas.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="0" />
	</ClassifyExercise>
	<ClassifyExercise id="9" difficulty="0" explanation="<?php echo _('Es la única figura formada por 3 líneas.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="10" difficulty="1" explanation="<?php echo _('Es el único en el que las dos figuras exteriores no coinciden.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="5" />
			<Element position="2" value="1" />
			<Element position="3" value="4" />
			<Element position="4" value="3" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="11" difficulty="1" explanation="<?php echo _('Es el único donde no se superponen dos figuras.'); ?>">
		<Elements>
			<Element position="0" value="4" />
			<Element position="1" value="1" />
			<Element position="2" value="5" />
			<Element position="3" value="2" />
			<Element position="4" value="3" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="12" difficulty="1" explanation="<?php echo _('Es el único que tiene todas las figuras pequeñas en el interior.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="5" />
			<Element position="2" value="3" />
			<Element position="3" value="1" />
			<Element position="4" value="4" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="13" difficulty="1" explanation="<?php echo _('Es el único que está formado por dos figuras independientes.'); ?>">
		<Elements>
			<Element position="0" value="3" />
			<Element position="1" value="5" />
			<Element position="2" value="4" />
			<Element position="3" value="1" />
			<Element position="4" value="2" />
		</Elements>
		<Answer position="0" />
	</ClassifyExercise>
	<ClassifyExercise id="14" difficulty="1" explanation="<?php echo _('Es la única figura que está compuesta por 3 líneas y un punto, las demás están formadas por dos líneas y un punto.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="15" difficulty="1" explanation="<?php echo _('Es la única que tiene un trazo en el interior.'); ?>">
		<Elements>
			<Element position="0" value="4" />
			<Element position="1" value="1" />
			<Element position="2" value="2" />
			<Element position="3" value="5" />
			<Element position="4" value="3" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="16" difficulty="2" explanation="<?php echo _('Las demás van emparejadas y con los mismos colores invertidos.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="17" difficulty="2" explanation="<?php echo _('Es el único donde el rectángulo va delante de la raya.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="18" difficulty="1" explanation="<?php echo _('Es el único donde el relleno de la figura no es simétrico.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="5" />
			<Element position="2" value="1" />
			<Element position="3" value="3" />
			<Element position="4" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="19" difficulty="0" explanation="<?php echo _('Es el único donde la línea no va recta.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="20" difficulty="1" explanation="<?php echo _('Es la única figura no simétrica.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="5" />
			<Element position="3" value="3" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="21" difficulty="0" explanation="<?php echo _('Es el único que es verde.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="22" difficulty="0" explanation="<?php echo _('Es el único que tiene dos ángulos de 45º.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="23" difficulty="0" explanation="<?php echo _('Es el único que en su interior tiene siempre la misma figura.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="24" difficulty="0" explanation="<?php echo _('Es el único número, los demás son letras.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="25" difficulty="2" explanation="<?php echo _('Es el único que no va alternando con los colores.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="26" difficulty="0" explanation="<?php echo _('Es el único que tiene dos figuras distintas.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
			<Element position="4" value="5" />
		</Elements>
		<Answer position="4" />
	</ClassifyExercise>
	<ClassifyExercise id="27" difficulty="0" explanation="<?php echo _('Es el único que tiene 3 cuadrados coloreados en el interior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="28" difficulty="0" explanation="<?php echo _('Es el único cuadrado que tiene números en su interior, los demás tienen todos letras.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="29" difficulty="0" explanation="<?php echo _('Es el único que tiene 3 puntos negros en su interior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="30" difficulty="1" explanation="<?php echo _('Es el único que tiene dos líneas, las demás figuras tienen solamente una.'); ?>">
		<Elements>
			<Element position="0" value="3" />
			<Element position="1" value="2" />
			<Element position="2" value="1" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="31" difficulty="1" explanation="<?php echo _('Es el único que está relleno con líneas verticales, las demás figuras están rellenas con líneas horizontales.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="4" />
			<Element position="2" value="2" />
			<Element position="3" value="3" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="32" difficulty="0" explanation="<?php echo _('La figura está compuesta sólamente por una flecha.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="33" difficulty="0" explanation="<?php echo _('Es el único que está dividido por la mitad.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="34" difficulty="1" explanation="<?php echo _('Es el único que tiene dos cuadrados pequeños iguales.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="3" />
			<Element position="2" value="4" />
			<Element position="3" value="1" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="35" difficulty="1" explanation="<?php echo _('Es la única figura que está compuesta por 5 círculos.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="3" />
			<Element position="2" value="2" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="36" difficulty="1" explanation="<?php echo _('Todas las demás flechas señalan a las esquinas.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="3" />
			<Element position="2" value="1" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="37" difficulty="1" explanation="<?php echo _('Todos los demás círculos tocan la pared.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="3" />
			<Element position="2" value="1" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="38" difficulty="0" explanation="<?php echo _('Todas las demás figuras interiores tocan la pared.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="39" difficulty="1" explanation="<?php echo _('Es el único en el que no coincide la misma figura.'); ?>">
		<Elements>
			<Element position="0" value="3" />
			<Element position="1" value="1" />
			<Element position="2" value="2" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="40" difficulty="1" explanation="<?php echo _('Es el único cuadrado que está dividido en partes iguales.'); ?>">
		<Elements>
			<Element position="0" value="3" />
			<Element position="1" value="1" />
			<Element position="2" value="2" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="41" difficulty="0" explanation="<?php echo _('Es el único que tiene una línea en el interior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="0" />
	</ClassifyExercise>
	<ClassifyExercise id="42" difficulty="1" explanation="<?php echo _('Es el único que tiene un cuadrado pequeño en el interior, los demás tienen un círculo.'); ?>">
		<Elements>
			<Element position="0" value="2" />
			<Element position="1" value="3" />
			<Element position="2" value="4" />
			<Element position="3" value="1" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="43" difficulty="1" explanation="<?php echo _('Es la única figura que no está dividida exactamente por la mitad.'); ?>">
		<Elements>
			<Element position="0" value="4" />
			<Element position="1" value="2" />
			<Element position="2" value="1" />
			<Element position="3" value="3" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="44" difficulty="0" explanation="<?php echo _('Es el  único círculo que está dividido entero por la línea.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="0" />
	</ClassifyExercise>
	<ClassifyExercise id="45" difficulty="1" explanation="<?php echo _('Es el único donde los cuadrados pequeños no son simétricos.'); ?>">
		<Elements>
			<Element position="0" value="4" />
			<Element position="1" value="1" />
			<Element position="2" value="3" />
			<Element position="3" value="2" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="46" difficulty="0" explanation="<?php echo _('Es el único que está formado por una línea continua.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="47" difficulty="0" explanation="<?php echo _('Es el único que tiene 4 elementos en su interior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="48" difficulty="0" explanation="<?php echo _('Es el único que no lleva una figura negra en su interior.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="1" />
	</ClassifyExercise>
	<ClassifyExercise id="49" difficulty="0" explanation="<?php echo _('Es el único que tiene una línea recta, las demás son curvadas.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="2" />
	</ClassifyExercise>
	<ClassifyExercise id="50" difficulty="1" explanation="<?php echo _('Es el único que no tiene los dos círculos del mismo lado.'); ?>">
		<Elements>
			<Element position="0" value="4" />
			<Element position="1" value="3" />
			<Element position="2" value="2" />
			<Element position="3" value="1" />
		</Elements>
		<Answer position="0" />
	</ClassifyExercise>
	<ClassifyExercise id="51" difficulty="1" explanation="<?php echo _('Es la única figura dividida en cuatro partes iguales.'); ?>">
		<Elements>
			<Element position="0" value="4" />
			<Element position="1" value="1" />
			<Element position="2" value="2" />
			<Element position="3" value="3" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="52" difficulty="1" explanation="<?php echo _('Es el único que no repite la misma figura.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="4" />
			<Element position="2" value="2" />
			<Element position="3" value="3" />
		</Elements>
		<Answer position="3" />
	</ClassifyExercise>
	<ClassifyExercise id="53" difficulty="0" explanation="<?php echo _('Es el único donde el círculo está dentro del cuadrado.'); ?>">
		<Elements>
			<Element position="0" value="1" />
			<Element position="1" value="2" />
			<Element position="2" value="3" />
			<Element position="3" value="4" />
		</Elements>
		<Answer position="0" />
	</ClassifyExercise>	
</ClassifyExercises>

