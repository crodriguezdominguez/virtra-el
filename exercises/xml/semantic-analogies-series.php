<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<SemanticAnalogiesExercises>
	<SemanticAnalogiesExercise id="0" difficulty="0" help="<?php echo _('Lo opuesto de simple es complicado, ¿y lo opuesto de bonito?'); ?>" explanation="<?php echo _('Porque bonito es opuesto a feo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Amable'); ?>" />
			<Element position="1" value="<?php echo _('Feo'); ?>" />
			<Element position="2" value="<?php echo _('Complicado'); ?>" />
			<Element position="3" value="<?php echo _('Indicado'); ?>" />
		</Elements>
		<Answer position="1" sample="<?php echo _('Lo simple es para lo complicado igual que lo bonito es para lo...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="1" difficulty="2" help="<?php echo _('Son meses del año con dos meses de diferencia, igual que lunes y miércoles son días de la semana con dos días de diferencia.'); ?>" explanation="<?php echo _('Porque son meses del año con dos meses de diferencia, igual que lunes y miércoles son días de la semana con dos días de diferencia'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Febrero y abril'); ?>" />
			<Element position="1" value="<?php echo _('Mes y año'); ?>" />
			<Element position="2" value="<?php echo _('Enero y diciembre'); ?>" />
			<Element position="3" value="<?php echo _('Semana y mes'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('La relación que hay entre el lunes y el miércoles es igual que la relación que hay entre...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="2" difficulty="1" help="<?php echo _('El río está formado por agua.'); ?>" explanation="<?php echo _('El desierto está formado de arena, lo mismo que el río está formado por agua.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Arena'); ?>" />
			<Element position="1" value="<?php echo _('Agua'); ?>" />
			<Element position="2" value="<?php echo _('Rojo'); ?>" />
			<Element position="3" value="<?php echo _('Seco'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Río es a agua como desierto es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="5" difficulty="1" help="<?php echo _('El día es lo opuesto de la noche.'); ?>" explanation="<?php echo _('Porque la salud es lo opuesto la enfermedad.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Alegría'); ?>" />
			<Element position="1" value="<?php echo _('Vitalidad'); ?>" />
			<Element position="2" value="<?php echo _('Enfermedad'); ?>" />
			<Element position="3" value="<?php echo _('Tristeza'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('El día es a la noche como salud es a la...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="6" difficulty="0" help="<?php echo _('El amor es lo opuesto al odio, ¿y el opuesto de blanco?'); ?>" explanation="<?php echo _('Porque blanco es el opuesto de negro.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Amarillo'); ?>" />
			<Element position="1" value="<?php echo _('Rojo'); ?>" />
			<Element position="2" value="<?php echo _('Negro'); ?>" />
			<Element position="3" value="<?php echo _('Verde'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('La relación entre el amor y el odio es la misma que la relación entre el blanco y el...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="7" difficulty="0" help="<?php echo _('El sol forma parte de la naturaleza, ¿de qué forma parte el conejo?'); ?>" explanation="<?php echo _('El conejo forma parte de los seres vivos y el sol forma parte del universo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('La liebre'); ?>" />
			<Element position="1" value="<?php echo _('Las zanahorias'); ?>" />
			<Element position="2" value="<?php echo _('El caballo'); ?>" />
			<Element position="3" value="<?php echo _('Los seres vivos'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('La relación entre el sol y el universo es igual que la relación entre el conejo y...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="8" difficulty="1" help="<?php echo _('La armonía es lo opuesto al desequilibrio.'); ?>" explanation="<?php echo _('Porque el amor es lo opuesto al odio.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('El odio'); ?>" />
			<Element position="1" value="<?php echo _('La tristeza'); ?>" />
			<Element position="2" value="<?php echo _('La alegría'); ?>" />
			<Element position="3" value="<?php echo _('El equilibrio'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('La relación entre la armonía y el desequilibrio es igual que la relación entre el amor y...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="9" difficulty="1" help="<?php echo _('La inteligencia es una característica de la persona.'); ?>" explanation="<?php echo _('Porque el instinto es característico de los animales.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Cruel'); ?>" />
			<Element position="1" value="<?php echo _('Animal'); ?>" />
			<Element position="2" value="<?php echo _('Naturaleza'); ?>" />
			<Element position="3" value="<?php echo _('Bondad'); ?>" />
		</Elements>
		<Answer position="1" sample="<?php echo _('Inteligencia es a persona como el instinto es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="10" difficulty="0" help="<?php echo _('Los conejos comen zanahorias, ¿quién come queso?'); ?>" explanation="<?php echo _('Porque los ratones comen queso, como el conejo come zanahorias.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('El ratón'); ?>" />
			<Element position="1" value="<?php echo _('La leche'); ?>" />
			<Element position="2" value="<?php echo _('El perro'); ?>" />
			<Element position="3" value="<?php echo _('El gato'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Una zanahoria es para un conejo lo mismo que el queso es para...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="11" difficulty="1" help="<?php echo _('El trueno va antes que el relámpago.'); ?>" explanation="<?php echo _('Porque el trueno va antes que el relámpago.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Sol'); ?>" />
			<Element position="1" value="<?php echo _('Cielo'); ?>" />
			<Element position="2" value="<?php echo _('Trueno'); ?>" />
			<Element position="3" value="<?php echo _('Viento'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('La nube es a la lluvia lo que relámpago es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="12" difficulty="1" help="<?php echo _('La palma es característica de Semana Santa.'); ?>" explanation="<?php echo _('Porque las uvas son características de la noche vieja.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Nochevieja'); ?>" />
			<Element position="1" value="<?php echo _('San Valentín'); ?>" />
			<Element position="2" value="<?php echo _('Pentecostés'); ?>" />
			<Element position="3" value="<?php echo _('12 de Octubre'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('La palma es a Semana Santa como las uvas son a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="13" difficulty="1" help="<?php echo _('Un oasis se encuentra en un desierto.'); ?>" explanation="<?php echo _('Porque las islas se encuentran en el mar.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Mar'); ?>" />
			<Element position="1" value="<?php echo _('Tierra'); ?>" />
			<Element position="2" value="<?php echo _('Paraiso'); ?>" />
			<Element position="3" value="<?php echo _('Pescador'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Oasis es a desierto lo que isla es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="14" difficulty="1" help="<?php echo _('Un oasis se encuentra en un desierto.'); ?>" explanation="<?php echo _('Porque las islas se encuentran en el mar.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Mar'); ?>" />
			<Element position="1" value="<?php echo _('Tierra'); ?>" />
			<Element position="2" value="<?php echo _('Paraiso'); ?>" />
			<Element position="3" value="<?php echo _('Pescador'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Oasis es a desierto lo que isla es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="15" difficulty="0" help="<?php echo _('La cera es producida por las abejas, ¿quién produce la leche?'); ?>" explanation="<?php echo _('Porque la leche la produce la vaca.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('A la vaca'); ?>" />
			<Element position="1" value="<?php echo _('Al gato'); ?>" />
			<Element position="2" value="<?php echo _('Al niño'); ?>" />
			<Element position="3" value="<?php echo _('Al hombre'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('La cera es a la abeja lo que la leche es...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="16" difficulty="1" help="<?php echo _('Un conjunto de palabras componen una frase.'); ?>" explanation="<?php echo _('Porque la palabra está compuesta por letras.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Nombre'); ?>" />
			<Element position="1" value="<?php echo _('Alfabeto'); ?>" />
			<Element position="2" value="<?php echo _('Palabra'); ?>" />
			<Element position="3" value="<?php echo _('Número'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('Palabra es a frase lo que letra es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="17" difficulty="2" help="<?php echo _('El compás se utiliza para dibujar circunferencias.'); ?>" explanation="<?php echo _('Porque el ángulo recto se dibuja con la escuadra.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('El ángulo recto'); ?>" />
			<Element position="1" value="<?php echo _('La longitud'); ?>" />
			<Element position="2" value="<?php echo _('La altura'); ?>" />
			<Element position="3" value="<?php echo _('La superficie'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('El compás sirve para las circunferencias lo mismo que la escuadra sirve para...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="18" difficulty="1" help="<?php echo _('El café contiene cafeína.'); ?>" explanation="<?php echo _('Porque el tabaco contiene nicotina.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Tabaco'); ?>" />
			<Element position="1" value="<?php echo _('Alcohol'); ?>" />
			<Element position="2" value="<?php echo _('Chocolate'); ?>" />
			<Element position="3" value="<?php echo _('Té'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('La cafeína es al café lo que la nicotina es al...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="19" difficulty="1" help="<?php echo _('El trigo se muele en el molino.'); ?>" explanation="<?php echo _('Porque el pan se hace en el horno.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Horno'); ?>" />
			<Element position="1" value="<?php echo _('Árbol'); ?>" />
			<Element position="2" value="<?php echo _('Canasto'); ?>" />
			<Element position="3" value="<?php echo _('Plancha'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Trigo es a molino lo que pan es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="20" difficulty="0" help="<?php echo _('Una abeja vive en una colmena, ¿dónde vive el zorro?'); ?>" explanation="<?php echo _('Porque el zorro vive en la madriguera.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Capullo'); ?>" />
			<Element position="1" value="<?php echo _('Madriguera'); ?>" />
			<Element position="2" value="<?php echo _('Árbol'); ?>" />
			<Element position="3" value="<?php echo _('Nido'); ?>" />
		</Elements>
		<Answer position="1" sample="<?php echo _('Abeja es a colmena lo que zorro es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="21" difficulty="2" help="<?php echo _('El arma que lanza balas es el fusil.'); ?>" explanation="<?php echo _('Porque la catapulta lanza las piedras.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Catapulta'); ?>" />
			<Element position="1" value="<?php echo _('Jabalina'); ?>" />
			<Element position="2" value="<?php echo _('Arco'); ?>" />
			<Element position="3" value="<?php echo _('Cuchillo'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Bala es a fusil lo que piedra es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="22" difficulty="0" help="<?php echo _('Los segundos dan lugar a minutos, ¿a qué dan lugar las horas?'); ?>" explanation="<?php echo _('Porque los días están compuestos por horas.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Semana'); ?>" />
			<Element position="1" value="<?php echo _('Mes'); ?>" />
			<Element position="2" value="<?php echo _('Año'); ?>" />
			<Element position="3" value="<?php echo _('Día'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Los segundos son a los minutos lo que horas son a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="23" difficulty="0" help="<?php echo _('El agua sacia la sed, ¿qué sacia el pan?'); ?>" explanation="<?php echo _('Porque el hambre se sacia con el pan.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Molino'); ?>" />
			<Element position="1" value="<?php echo _('Trigo'); ?>" />
			<Element position="2" value="<?php echo _('Hambre'); ?>" />
			<Element position="3" value="<?php echo _('Comida'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('Agua es a sed lo que pan es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="24" difficulty="0" help="<?php echo _('En verano hay sol, ¿cuando hay nieve?'); ?>" explanation="<?php echo _('Porque en el invierno hay nieve.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Blanco'); ?>" />
			<Element position="1" value="<?php echo _('Frío'); ?>" />
			<Element position="2" value="<?php echo _('Otoño'); ?>" />
			<Element position="3" value="<?php echo _('Invierno'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Sol es a verano lo que nieve es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="25" difficulty="2" help="<?php echo _('La llave es el utensilio que abre la puerta, ¿qué afloja el destornillador?'); ?>" explanation="<?php echo _('Porque el destornillador es la herramienta que afloja al tornillo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Tornillo'); ?>" />
			<Element position="1" value="<?php echo _('Llave inglesa'); ?>" />
			<Element position="2" value="<?php echo _('Puerta'); ?>" />
			<Element position="3" value="<?php echo _('Caja'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Llave es a cerradura lo que destornillador es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="26" difficulty="0" help="<?php echo _('Una azada es una herramienta que usa el agricultor, ¿quién usa una jeringa?'); ?>" explanation="<?php echo _('Porque la jeringa es una herramienta que utilizan las enfermeras.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('El dolor'); ?>" />
			<Element position="1" value="<?php echo _('La enfermedad'); ?>" />
			<Element position="2" value="<?php echo _('La inyección'); ?>" />
			<Element position="3" value="<?php echo _('La enfermera'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('La azada es para el agricultor lo mismo que la jeringa es para...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="27" difficulty="2" help="<?php echo _('La vaca es un animal más grande que el perro.'); ?>" explanation="<?php echo _('Porque el mosquito es el único animal más pequeño que el escarabajo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Mosquito'); ?>" />
			<Element position="1" value="<?php echo _('León'); ?>" />
			<Element position="2" value="<?php echo _('Elefante'); ?>" />
			<Element position="3" value="<?php echo _('Animal'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Vaca es a perro como escarabajo es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="28" difficulty="1" help="<?php echo _('La pera y la manzana se asemejan porque ambas crecen en árboles, igual que la patata y el rábano que crecen bajo tierra.'); ?>" explanation="<?php echo _('Porque la patata y el rábano crecen bajo tierra.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Rábano'); ?>" />
			<Element position="1" value="<?php echo _('Fresa'); ?>" />
			<Element position="2" value="<?php echo _('Melocotón'); ?>" />
			<Element position="3" value="<?php echo _('Lechuga'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Pera es a manzana como patata es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="29" difficulty="0" help="<?php echo _('Alto es lo opuesto de bajo, ¿qué es lo opuesto de cielo?'); ?>" explanation="<?php echo _('El cielo es lo opuesto a la tierra.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Tierra'); ?>" />
			<Element position="1" value="<?php echo _('Malva'); ?>" />
			<Element position="2" value="<?php echo _('Selva'); ?>" />
			<Element position="3" value="<?php echo _('Sierra'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Alto es a bajo lo que cielo es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="30" difficulty="1" help="<?php echo _('Un remolino es parte del mar.'); ?>" explanation="<?php echo _('Un remolino es parte del mar como la montaña es parte de la sierra.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Fluido'); ?>" />
			<Element position="1" value="<?php echo _('Mojado'); ?>" />
			<Element position="2" value="<?php echo _('Mar'); ?>" />
			<Element position="3" value="<?php echo _('Lluvia'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('Montaña es a sierra como remolino es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="31" difficulty="1" help="<?php echo _('Un remolino es parte del mar.'); ?>" explanation="<?php echo _('Un remolino es parte del mar como la montaña es parte de la sierra.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Fluido'); ?>" />
			<Element position="1" value="<?php echo _('Mojado'); ?>" />
			<Element position="2" value="<?php echo _('Lluvia'); ?>" />
			<Element position="3" value="<?php echo _('Mar'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Montaña es a sierra como remolino es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="32" difficulty="2" help="<?php echo _('La energía potencial aumenta con la altura'); ?>" explanation="<?php echo _('Porque el estudio de la cinética es el movimiento.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Movimiento'); ?>" />
			<Element position="1" value="<?php echo _('Mecánica'); ?>" />
			<Element position="2" value="<?php echo _('Aceleración'); ?>" />
			<Element position="3" value="<?php echo _('Moción'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Potencial es a altura como cinética es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="33" difficulty="2" help="<?php echo _('La moneda de Japón es el yen.'); ?>" explanation="<?php echo _('La moneda de Argelia es el dinar.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Dinar'); ?>" />
			<Element position="1" value="<?php echo _('Dracma'); ?>" />
			<Element position="2" value="<?php echo _('Dólar'); ?>" />
			<Element position="3" value="<?php echo _('Peso'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Japón es a yen como Argelia es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="34" difficulty="2" help="<?php echo _('Un ramo está hecho con flores, ¿de qué está hecho el libro?'); ?>" explanation="<?php echo _('El libro está hecho de papel.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Lectura'); ?>" />
			<Element position="1" value="<?php echo _('Inteligencia'); ?>" />
			<Element position="2" value="<?php echo _('Papel'); ?>" />
			<Element position="3" value="<?php echo _('Estudios'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('Ramo es a flores lo que libro es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="35" difficulty="1" help="<?php echo _('Un cuaderno está unido por la espiral.'); ?>" explanation="<?php echo _('El libro está unido por el lomo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Libro'); ?>" />
			<Element position="1" value="<?php echo _('Tinta'); ?>" />
			<Element position="2" value="<?php echo _('Papel'); ?>" />
			<Element position="3" value="<?php echo _('Pequeño'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Espiral es a cuaderno como lomo es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="36" difficulty="1" help="<?php echo _('Un puente está formado por vigas.'); ?>" explanation="<?php echo _('La presa está formada por muros.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Presa'); ?>" />
			<Element position="1" value="<?php echo _('Altura'); ?>" />
			<Element position="2" value="<?php echo _('Cable'); ?>" />
			<Element position="3" value="<?php echo _('Forma'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Viga es a puente como muro es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="37" difficulty="0" help="<?php echo _('El paraguas sirve para evitar la lluvia, ¿qué evita el guante?'); ?>" explanation="<?php echo _('El guante es para evitar el frío.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Frío'); ?>" />
			<Element position="1" value="<?php echo _('Agua'); ?>" />
			<Element position="2" value="<?php echo _('Prenda'); ?>" />
			<Element position="3" value="<?php echo _('Mano'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Paraguas es a lluvia como guante es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="38" difficulty="1" help="<?php echo _('La ciencia que estudia los planetas es la astronomía.'); ?>" explanation="<?php echo _('La ciencia que estudia las aves es la ornitología.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Ornitología'); ?>" />
			<Element position="1" value="<?php echo _('Misterio'); ?>" />
			<Element position="2" value="<?php echo _('Universo'); ?>" />
			<Element position="3" value="<?php echo _('Avicultor'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Planeta es a astronomía como aves es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="39" difficulty="1" help="<?php echo _('El tapón cierra una botella.'); ?>" explanation="<?php echo _('La caja se cierra con la tapa.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Caja'); ?>" />
			<Element position="1" value="<?php echo _('Jarrón'); ?>" />
			<Element position="2" value="<?php echo _('Zapato'); ?>" />
			<Element position="3" value="<?php echo _('Garrafa'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Tapón es a botella como tapa es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="40" difficulty="1" help="<?php echo _('Las lágrimas dan lugar al llanto.'); ?>" explanation="<?php echo _('El fuego da lugar al humo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Humo'); ?>" />
			<Element position="1" value="<?php echo _('Frío'); ?>" />
			<Element position="2" value="<?php echo _('Tristeza'); ?>" />
			<Element position="3" value="<?php echo _('Árbol'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Lágrimas es a llanto como fuego es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="41" difficulty="0" help="<?php echo _('El sonido que emite el perro es el ladrido, ¿quién emite el maullido?'); ?>" explanation="<?php echo _('El sonido que emite el gato es el maullido.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Gato'); ?>" />
			<Element position="1" value="<?php echo _('Rana'); ?>" />
			<Element position="2" value="<?php echo _('Elefante'); ?>" />
			<Element position="3" value="<?php echo _('Lobo'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Ladrido es a perro como maullido es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="42" difficulty="1" help="<?php echo _('La piel recubre al animal.'); ?>" explanation="<?php echo _('La corteza recubre al árbol.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('La cáscara'); ?>" />
			<Element position="1" value="<?php echo _('La corteza'); ?>" />
			<Element position="2" value="<?php echo _('El hombre'); ?>" />
			<Element position="3" value="<?php echo _('Lo suave'); ?>" />
		</Elements>
		<Answer position="1" sample="<?php echo _('La relación entre un animal y la piel es la misma que entre un árbol y...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="43" difficulty="0" help="<?php echo _('El zapato se pone en el pie, ¿qué se pone en el cuello?'); ?>" explanation="<?php echo _('La corbata se pone en el cuello.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Corbata'); ?>" />
			<Element position="1" value="<?php echo _('Suelo'); ?>" />
			<Element position="2" value="<?php echo _('Jirafa'); ?>" />
			<Element position="3" value="<?php echo _('Pantalón'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Pie es a zapato como cuello es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="44" difficulty="0" help="<?php echo _('Cerca es lo contrario de lejos, ¿qué es lo contrario de encima?'); ?>" explanation="<?php echo _('Debajo es lo contrario de encima.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Debajo'); ?>" />
			<Element position="1" value="<?php echo _('Suelo'); ?>" />
			<Element position="2" value="<?php echo _('Jirafa'); ?>" />
			<Element position="3" value="<?php echo _('Pantalón'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Cerca es a lejos como encima es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="45" difficulty="0" help="<?php echo _('La cría del caballo es el potrillo, ¿cual es la cría de la vaca?'); ?>" explanation="<?php echo _('La cría de la vaca es el ternero.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Cabra'); ?>" />
			<Element position="1" value="<?php echo _('Cachorro'); ?>" />
			<Element position="2" value="<?php echo _('Buey'); ?>" />
			<Element position="3" value="<?php echo _('Ternero'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Caballo es a potrillo como vaca es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="46" difficulty="0" help="<?php echo _('El asno emite rebuznos, ¿qué emite el caballo?'); ?>" explanation="<?php echo _('El caballo relincha.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Coz'); ?>" />
			<Element position="1" value="<?php echo _('Trotar'); ?>" />
			<Element position="2" value="<?php echo _('Encabritarse'); ?>" />
			<Element position="3" value="<?php echo _('Relincho'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Asno es a rebuzno como caballo es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="47" difficulty="0" help="<?php echo _('La cena es la comida que se hace por la noche, ¿cuándo se hace el desayuno?'); ?>" explanation="<?php echo _('El desayuno es por la mañana.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Abundancia'); ?>" />
			<Element position="1" value="<?php echo _('Madrugar'); ?>" />
			<Element position="2" value="<?php echo _('Mañana'); ?>" />
			<Element position="3" value="<?php echo _('Café'); ?>" />
		</Elements>
		<Answer position="2" sample="<?php echo _('Cena es a noche como desayuno es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="48" difficulty="1" help="<?php echo _('Una característica de las liebres es que son rápidas.'); ?>" explanation="<?php echo _('Se dice que los zorros son muy astutos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Astuto'); ?>" />
			<Element position="1" value="<?php echo _('Voraz'); ?>" />
			<Element position="2" value="<?php echo _('Duro'); ?>" />
			<Element position="3" value="<?php echo _('Simple'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Liebre es a rápido como zorro es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="49" difficulty="0" help="<?php echo _('En una aldea habitan aldeanos, ¿dónde habitan los ciudadanos?'); ?>" explanation="<?php echo _('Los habitantes de la ciudad son los ciudadanos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Pueblo'); ?>" />
			<Element position="1" value="<?php echo _('Ciudad'); ?>" />
			<Element position="2" value="<?php echo _('Calle'); ?>" />
			<Element position="3" value="<?php echo _('Avenida'); ?>" />
		</Elements>
		<Answer position="1" sample="<?php echo _('Aldeanos es a aldea como ciudadanos es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="50" difficulty="1" help="<?php echo _('Pequinés es una raza de perro.'); ?>" explanation="<?php echo _('El siamés es una raza de gato.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Chino'); ?>" />
			<Element position="1" value="<?php echo _('Rabo'); ?>" />
			<Element position="2" value="<?php echo _('Galgo'); ?>" />
			<Element position="3" value="<?php echo _('Gato'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Pequinés es a perro como siamés es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="51" difficulty="0" help="<?php echo _('El pan se hace con harina, ¿qué se hace con almendra?'); ?>" explanation="<?php echo _('El turrón se hace con almendras.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Avellana'); ?>" />
			<Element position="1" value="<?php echo _('Bocadillo'); ?>" />
			<Element position="2" value="<?php echo _('Árbol'); ?>" />
			<Element position="3" value="<?php echo _('Turrón'); ?>" />
		</Elements>
		<Answer position="3" sample="<?php echo _('Harina es a pan como almendra es a...'); ?>" />
	</SemanticAnalogiesExercise>
	<SemanticAnalogiesExercise id="52" difficulty="1" help="<?php echo _('El guerrero utiliza la espada.'); ?>" explanation="<?php echo _('La herramienta del escritor es la pluma.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Escritor'); ?>" />
			<Element position="1" value="<?php echo _('Volar'); ?>" />
			<Element position="2" value="<?php echo _('Paloma'); ?>" />
			<Element position="3" value="<?php echo _('Tinta'); ?>" />
		</Elements>
		<Answer position="0" sample="<?php echo _('Espada es a guerrero como pluma es a...'); ?>" />
	</SemanticAnalogiesExercise>
</SemanticAnalogiesExercises>
