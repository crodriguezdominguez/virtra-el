<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<SpatialReasoningExercises>
	<SpatialReasoningExercise id="0" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/alce.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/reno.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="1" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/alhambra.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/ardilla.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="2" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/ardilla.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/cisne.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="3" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/alce.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/jupiter.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="4" difficulty="2">
		<Element img="exercises/img/SpatialReasoningExercise/casiopea.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/nebulosa_cangrejo7.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="5" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/cisne.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/alce.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="6" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/cuadro_ninio.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/vaqueros.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="7" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/esfinge.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/alhambra.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="8" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/geisha.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/cisne.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="9" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/gente_florida.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/paisaje_kentuky.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="10" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/leoncio.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/mirlo.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="11" difficulty="2">
		<Element img="exercises/img/SpatialReasoningExercise/mariposa.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/sunflower.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="12" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/mirlo.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/leoncio.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="13" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/mariposa.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/mirlo.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="14" difficulty="2">
		<Element img="exercises/img/SpatialReasoningExercise/mirlo.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/pajaro.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="15" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/playa.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/seurat.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="16" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/prado_flores.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/sunflower.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="17" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/reno.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/alce.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="18" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/seurat.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/cisne.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="19" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/sunflower.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/prado_flores.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="20" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/trajes_astronauta.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/vaqueros.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="21" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/vaqueros.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/cuadro_ninio.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="22" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/nebulosa_cangrejo7.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/casiopea.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="23" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/noche_estrellada.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/seurat.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="24" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/paisaje_kentuky.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/seurat.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="25" difficulty="0">
		<Element img="exercises/img/SpatialReasoningExercise/pajaro.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/cisne.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="26" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/arco_iris.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/arco_iris2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="27" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/babuchas.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/babuchas2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="28" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/curro1.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/curro2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="29" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/cuadro_colores.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/cuadro_paisaje.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="30" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/frutas1.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/frutas2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="31" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/hojas.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/hoja.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="32" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/libros1.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/libros2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="33" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/loros1.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/loros2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="34" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/rio1.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/rio2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="35" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/ninos1.png" />
		<Distractor img="exercises/img/SpatialReasoningExercise/ninos2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="36" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/tren1.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/tren2.jpg" />
	</SpatialReasoningExercise>
	<SpatialReasoningExercise id="37" difficulty="1">
		<Element img="exercises/img/SpatialReasoningExercise/alce.jpg" />
		<Distractor img="exercises/img/SpatialReasoningExercise/ardilla.jpg" />
	</SpatialReasoningExercise>
</SpatialReasoningExercises>
