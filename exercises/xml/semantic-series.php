<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<SemanticSeriesExercises>
	<SemanticSeriesExercise id="0" difficulty="0" explanation="<?php echo _('El resto de palabras son herramientas.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Martillo'); ?>" />
			<Element position="1" value="<?php echo _('Alicate'); ?>" />
			<Element position="2" value="<?php echo _('Manzana'); ?>" />
			<Element position="3" value="<?php echo _('Tornillo'); ?>" />
			<Element position="4" value="<?php echo _('Tuerca'); ?>" />
			<Element position="5" value="<?php echo _('Taladro'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="1" difficulty="0" explanation="<?php echo _('El resto de palabras son animales.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Perro'); ?>" />
			<Element position="1" value="<?php echo _('Gato'); ?>" />
			<Element position="2" value="<?php echo _('Canario'); ?>" />
			<Element position="3" value="<?php echo _('Caballo'); ?>" />
			<Element position="4" value="<?php echo _('Margarita'); ?>" />
			<Element position="5" value="<?php echo _('Conejo'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="2" difficulty="2" explanation="<?php echo _('El resto de palabras son valores de la persona.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Agresivo'); ?>" />
			<Element position="1" value="<?php echo _('Rencoroso'); ?>" />
			<Element position="2" value="<?php echo _('Sincero'); ?>" />
			<Element position="3" value="<?php echo _('Romántico'); ?>" />
			<Element position="4" value="<?php echo _('Bello'); ?>" />
			<Element position="5" value="<?php echo _('Honrado'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="3" difficulty="1" explanation="<?php echo _('El resto de palabras son ciudades de Andalucía.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Granada'); ?>" />
			<Element position="1" value="<?php echo _('Sevilla'); ?>" />
			<Element position="2" value="<?php echo _('Cádiz'); ?>" />
			<Element position="3" value="<?php echo _('Madrid'); ?>" />
			<Element position="4" value="<?php echo _('Jaén'); ?>" />
			<Element position="5" value="<?php echo _('Huelva'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="4" difficulty="2" explanation="<?php echo _('El resto de palabras son valores positivos de una persona.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Afectuoso'); ?>" />
			<Element position="1" value="<?php echo _('Valiente'); ?>" />
			<Element position="2" value="<?php echo _('Alegre'); ?>" />
			<Element position="3" value="<?php echo _('Honrado'); ?>" />
			<Element position="4" value="<?php echo _('Leal'); ?>" />
			<Element position="5" value="<?php echo _('Mentiroso'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="5" difficulty="1" explanation="<?php echo _('El resto forman parte de las extremidades superiores del cuerpo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Dedo'); ?>" />
			<Element position="1" value="<?php echo _('Muñeca'); ?>" />
			<Element position="2" value="<?php echo _('Codo'); ?>" />
			<Element position="3" value="<?php echo _('Brazo'); ?>" />
			<Element position="4" value="<?php echo _('Cadera'); ?>" />
			<Element position="5" value="<?php echo _('Antebrazo'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="6" difficulty="0" explanation="<?php echo _('El resto de palabras son colores.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Rojo'); ?>" />
			<Element position="1" value="<?php echo _('Azul'); ?>" />
			<Element position="2" value="<?php echo _('Verde'); ?>" />
			<Element position="3" value="<?php echo _('Hierro'); ?>" />
			<Element position="4" value="<?php echo _('Amarillo'); ?>" />
			<Element position="5" value="<?php echo _('Negro'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="7" difficulty="1" explanation="<?php echo _('El resto de palabras son países europeos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('España'); ?>" />
			<Element position="1" value="<?php echo _('Francia'); ?>" />
			<Element position="2" value="<?php echo _('Italia'); ?>" />
			<Element position="3" value="<?php echo _('Argentina'); ?>" />
			<Element position="4" value="<?php echo _('Alemania'); ?>" />
			<Element position="5" value="<?php echo _('Inglaterra'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="8" difficulty="2" explanation="<?php echo _('El resto de palabras son felinos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('León'); ?>" />
			<Element position="1" value="<?php echo _('Tigre'); ?>" />
			<Element position="2" value="<?php echo _('Gato'); ?>" />
			<Element position="3" value="<?php echo _('Zorro'); ?>" />
			<Element position="4" value="<?php echo _('Pantera'); ?>" />
			<Element position="5" value="<?php echo _('Leopardo'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="9" difficulty="0" explanation="<?php echo _('El resto de palabras son árboles.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Abeto'); ?>" />
			<Element position="1" value="<?php echo _('Pino'); ?>" />
			<Element position="2" value="<?php echo _('Olmo'); ?>" />
			<Element position="3" value="<?php echo _('Manzano'); ?>" />
			<Element position="4" value="<?php echo _('Peral'); ?>" />
			<Element position="5" value="<?php echo _('Rosa'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="10" difficulty="2" explanation="<?php echo _('El resto de palabras contienen una b.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Árbol'); ?>" />
			<Element position="1" value="<?php echo _('Bellota'); ?>" />
			<Element position="2" value="<?php echo _('Boca'); ?>" />
			<Element position="3" value="<?php echo _('Libro'); ?>" />
			<Element position="4" value="<?php echo _('Bisutería'); ?>" />
			<Element position="5" value="<?php echo _('Vocal'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="11" difficulty="1" explanation="<?php echo _('El resto de palabras son animales con 4 patas.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Gato'); ?>" />
			<Element position="1" value="<?php echo _('Perro'); ?>" />
			<Element position="2" value="<?php echo _('Gallina'); ?>" />
			<Element position="3" value="<?php echo _('Vaca'); ?>" />
			<Element position="4" value="<?php echo _('Caballo'); ?>" />
			<Element position="5" value="<?php echo _('Cabra'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="12" difficulty="2" explanation="<?php echo _('El resto de palabras son figuras planas.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Círculo'); ?>" />
			<Element position="1" value="<?php echo _('Triángulo'); ?>" />
			<Element position="2" value="<?php echo _('Rectángulo'); ?>" />
			<Element position="3" value="<?php echo _('Cuadrado'); ?>" />
			<Element position="4" value="<?php echo _('Rombo'); ?>" />
			<Element position="5" value="<?php echo _('Cubo'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="13" difficulty="2" explanation="<?php echo _('El resto de palabras son verduras.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Tomate'); ?>" />
			<Element position="1" value="<?php echo _('Zanahoria'); ?>" />
			<Element position="2" value="<?php echo _('Limón'); ?>" />
			<Element position="3" value="<?php echo _('Cebolla'); ?>" />
			<Element position="4" value="<?php echo _('Nabo'); ?>" />
			<Element position="5" value="<?php echo _('Puerro'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="14" difficulty="0" explanation="<?php echo _('El resto de palabras son meses del año.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Enero'); ?>" />
			<Element position="1" value="<?php echo _('Marzo'); ?>" />
			<Element position="2" value="<?php echo _('Diciembre'); ?>" />
			<Element position="3" value="<?php echo _('Tomate'); ?>" />
			<Element position="4" value="<?php echo _('Octubre'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="15" difficulty="2" explanation="<?php echo _('El resto de palabras son partes del cuerpo que van por pares.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Pie'); ?>" />
			<Element position="1" value="<?php echo _('Mejilla'); ?>" />
			<Element position="2" value="<?php echo _('Brazo'); ?>" />
			<Element position="3" value="<?php echo _('Pierna'); ?>" />
			<Element position="4" value="<?php echo _('Oreja'); ?>" />
			<Element position="5" value="<?php echo _('Nariz'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="16" difficulty="0" explanation="<?php echo _('El resto de palabras son flores.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Tulipán'); ?>" />
			<Element position="1" value="<?php echo _('Clavel'); ?>" />
			<Element position="2" value="<?php echo _('Olmo'); ?>" />
			<Element position="3" value="<?php echo _('Rosa'); ?>" />
			<Element position="4" value="<?php echo _('Margarita'); ?>" />
			<Element position="5" value="<?php echo _('Azucena'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="17" difficulty="0" explanation="<?php echo _('El resto de palabras son días de la semana.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Mediodía'); ?>" />
			<Element position="1" value="<?php echo _('Miércoles'); ?>" />
			<Element position="2" value="<?php echo _('Domingo'); ?>" />
			<Element position="3" value="<?php echo _('Lunes'); ?>" />
			<Element position="4" value="<?php echo _('Martes'); ?>" />
			<Element position="5" value="<?php echo _('Sábado'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="18" difficulty="2" explanation="<?php echo _('El resto de palabras empiezan por c.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Caballo'); ?>" />
			<Element position="1" value="<?php echo _('Cazuela'); ?>" />
			<Element position="2" value="<?php echo _('Cocina'); ?>" />
			<Element position="3" value="<?php echo _('Cinismo'); ?>" />
			<Element position="4" value="<?php echo _('Botella'); ?>" />
			<Element position="5" value="<?php echo _('Coche'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="19" difficulty="0" explanation="<?php echo _('El resto de palabras son instrumentos musicales.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Violín'); ?>" />
			<Element position="1" value="<?php echo _('Flauta'); ?>" />
			<Element position="2" value="<?php echo _('Trompeta'); ?>" />
			<Element position="3" value="<?php echo _('Tambor'); ?>" />
			<Element position="4" value="<?php echo _('Lápiz'); ?>" />
			<Element position="5" value="<?php echo _('Guitarra'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="20" difficulty="1" explanation="<?php echo _('El resto de palabras son aves que vuelan.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Gaviota'); ?>" />
			<Element position="1" value="<?php echo _('Garza'); ?>" />
			<Element position="2" value="<?php echo _('Cisne'); ?>" />
			<Element position="3" value="<?php echo _('Pato'); ?>" />
			<Element position="4" value="<?php echo _('Oca'); ?>" />
			<Element position="5" value="<?php echo _('Pingüino'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="21" difficulty="1" explanation="<?php echo _('El resto de palabras son piedras preciosas.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Esmeralda'); ?>" />
			<Element position="1" value="<?php echo _('Zafiro'); ?>" />
			<Element position="2" value="<?php echo _('Granito'); ?>" />
			<Element position="3" value="<?php echo _('Diamante'); ?>" />
			<Element position="4" value="<?php echo _('Rubí'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="22" difficulty="1" explanation="<?php echo _('El resto de animales son carnívoros.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Jirafa'); ?>" />
			<Element position="1" value="<?php echo _('Lobo'); ?>" />
			<Element position="2" value="<?php echo _('Gato'); ?>" />
			<Element position="3" value="<?php echo _('Leopardo'); ?>" />
			<Element position="4" value="<?php echo _('Perro'); ?>" />
			<Element position="5" value="<?php echo _('León'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="23" difficulty="2" explanation="<?php echo _('El resto de palabras son animales de mar.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Trucha'); ?>" />
			<Element position="1" value="<?php echo _('Tiburón'); ?>" />
			<Element position="2" value="<?php echo _('Calamar'); ?>" />
			<Element position="3" value="<?php echo _('Pulpo'); ?>" />
			<Element position="4" value="<?php echo _('Sardina'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="24" difficulty="1" explanation="<?php echo _('El resto de palabras son pájaros.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Águila'); ?>" />
			<Element position="1" value="<?php echo _('Pato'); ?>" />
			<Element position="2" value="<?php echo _('Canario'); ?>" />
			<Element position="3" value="<?php echo _('Gorrión'); ?>" />
			<Element position="4" value="<?php echo _('Buitre'); ?>" />
			<Element position="5" value="<?php echo _('Mariposa'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="25" difficulty="1" explanation="<?php echo _('El resto de frutas son cítricos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Naranja'); ?>" />
			<Element position="1" value="<?php echo _('Limón'); ?>" />
			<Element position="2" value="<?php echo _('Pomelo'); ?>" />
			<Element position="3" value="<?php echo _('Mandarina'); ?>" />
			<Element position="4" value="<?php echo _('Albaricoque'); ?>" />
			<Element position="5" value="<?php echo _('Lima'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="26" difficulty="0" explanation="<?php echo _('El resto de palabras indican una comida del día.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Almuerzo'); ?>" />
			<Element position="1" value="<?php echo _('Cena'); ?>" />
			<Element position="2" value="<?php echo _('Amanecer'); ?>" />
			<Element position="3" value="<?php echo _('Comida'); ?>" />
			<Element position="4" value="<?php echo _('Merienda'); ?>" />
			<Element position="5" value="<?php echo _('Desayuno'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="27" difficulty="0" explanation="<?php echo _('El resto de palabras son una medida de tiempo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Hora'); ?>" />
			<Element position="1" value="<?php echo _('Segundo'); ?>" />
			<Element position="2" value="<?php echo _('Día'); ?>" />
			<Element position="3" value="<?php echo _('Minuto'); ?>" />
			<Element position="4" value="<?php echo _('Semana'); ?>" />
			<Element position="5" value="<?php echo _('Mañana'); ?>" />
		</Elements>
		<Answer position="5" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="28" difficulty="1" explanation="<?php echo _('El resto de palabras son aparatos eléctricos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Nevera'); ?>" />
			<Element position="1" value="<?php echo _('Televisor'); ?>" />
			<Element position="2" value="<?php echo _('Aspirador'); ?>" />
			<Element position="3" value="<?php echo _('Mesa'); ?>" />
			<Element position="4" value="<?php echo _('Batidora'); ?>" />
			<Element position="5" value="<?php echo _('Lavadora'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="29" difficulty="2" explanation="<?php echo _('El resto de palabras son sustantivos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Cuchillo'); ?>" />
			<Element position="1" value="<?php echo _('Cisne'); ?>" />
			<Element position="2" value="<?php echo _('Sonrisa'); ?>" />
			<Element position="3" value="<?php echo _('Pluma'); ?>" />
			<Element position="4" value="<?php echo _('Hermoso'); ?>" />
			<Element position="5" value="<?php echo _('Pensamiento'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="30" difficulty="1" explanation="<?php echo _('El resto de palabras hacen referencia a algo negativo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Ilusión'); ?>" />
			<Element position="1" value="<?php echo _('Trampa'); ?>" />
			<Element position="2" value="<?php echo _('Timo'); ?>" />
			<Element position="3" value="<?php echo _('Estafa'); ?>" />
			<Element position="4" value="<?php echo _('Engaño'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="31" difficulty="1" explanation="<?php echo _('El resto de palabras son accidentes geográficos.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Valle'); ?>" />
			<Element position="1" value="<?php echo _('Llano'); ?>" />
			<Element position="2" value="<?php echo _('Mapa'); ?>" />
			<Element position="3" value="<?php echo _('Golfo'); ?>" />
			<Element position="4" value="<?php echo _('Montaña'); ?>" />
		</Elements>
		<Answer position="2" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="32" difficulty="1" explanation="<?php echo _('El resto de palabras son cosas que calientan.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Alimento'); ?>" />
			<Element position="1" value="<?php echo _('Horno'); ?>" />
			<Element position="2" value="<?php echo _('Parrilla'); ?>" />
			<Element position="3" value="<?php echo _('Tostadora'); ?>" />
			<Element position="4" value="<?php echo _('Vitrocerámica'); ?>" />
			<Element position="5" value="<?php echo _('Microondas'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="33" difficulty="0" explanation="<?php echo _('El resto de palabras son animales.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Gallina'); ?>" />
			<Element position="1" value="<?php echo _('Cerdo'); ?>" />
			<Element position="2" value="<?php echo _('Vaca'); ?>" />
			<Element position="3" value="<?php echo _('Granja'); ?>" />
			<Element position="4" value="<?php echo _('Pato'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="34" difficulty="0" explanation="<?php echo _('El resto de palabras son tipos de muebles.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Silla'); ?>" />
			<Element position="1" value="<?php echo _('Ventana'); ?>" />
			<Element position="2" value="<?php echo _('Banqueta'); ?>" />
			<Element position="3" value="<?php echo _('Sillón'); ?>" />
			<Element position="4" value="<?php echo _('Butaca'); ?>" />
			<Element position="5" value="<?php echo _('Sofá'); ?>" />
		</Elements>
		<Answer position="1" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="35" difficulty="1" explanation="<?php echo _('El resto de palabras son insectos que vuelan.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Libélula'); ?>" />
			<Element position="1" value="<?php echo _('Escarabajo'); ?>" />
			<Element position="2" value="<?php echo _('Mosquito'); ?>" />
			<Element position="3" value="<?php echo _('Mariposa'); ?>" />
			<Element position="4" value="<?php echo _('Abeja'); ?>" />
			<Element position="5" value="<?php echo _('Mosca'); ?>" />
		</Elements>
		<Answer position="1" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="36" difficulty="1" explanation="<?php echo _('El resto de palabras son una forma de pago.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Monedas'); ?>" />
			<Element position="1" value="<?php echo _('Pagaré'); ?>" />
			<Element position="2" value="<?php echo _('Cheque'); ?>" />
			<Element position="3" value="<?php echo _('Banco'); ?>" />
			<Element position="4" value="<?php echo _('Billete'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="37" difficulty="1" explanation="<?php echo _('El resto de palabras son peces.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Salmón'); ?>" />
			<Element position="1" value="<?php echo _('Merluza'); ?>" />
			<Element position="2" value="<?php echo _('Rape'); ?>" />
			<Element position="3" value="<?php echo _('Gamba'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="38" difficulty="0" explanation="<?php echo _('El resto de palabras tienen motor.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Coche'); ?>" />
			<Element position="1" value="<?php echo _('Bicicleta'); ?>" />
			<Element position="2" value="<?php echo _('Autobús'); ?>" />
			<Element position="3" value="<?php echo _('Camión'); ?>" />
		</Elements>
		<Answer position="1" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="39" difficulty="0" explanation="<?php echo _('El resto de palabras sirven para escribir.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Libro'); ?>" />
			<Element position="1" value="<?php echo _('Pluma'); ?>" />
			<Element position="2" value="<?php echo _('Rotulador'); ?>" />
			<Element position="3" value="<?php echo _('Bolígrafo'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="40" difficulty="0" explanation="<?php echo _('El resto de palabras hacen referencia al invierno.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Nieve'); ?>" />
			<Element position="1" value="<?php echo _('Frío'); ?>" />
			<Element position="2" value="<?php echo _('Hielo'); ?>" />
			<Element position="3" value="<?php echo _('Calor'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="41" difficulty="0" explanation="<?php echo _('El resto de animales tienen patas.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Gato'); ?>" />
			<Element position="1" value="<?php echo _('Serpiente'); ?>" />
			<Element position="2" value="<?php echo _('Perro'); ?>" />
			<Element position="3" value="<?php echo _('León'); ?>" />
		</Elements>
		<Answer position="1" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="42" difficulty="1" explanation="<?php echo _('El resto de deportes se juegan con pelota.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Natación'); ?>" />
			<Element position="1" value="<?php echo _('Tenis'); ?>" />
			<Element position="2" value="<?php echo _('Baloncesto'); ?>" />
			<Element position="3" value="<?php echo _('Fútbol'); ?>" />
		</Elements>
		<Answer position="0" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="43" difficulty="0" explanation="<?php echo _('El resto de palabras tienen relación con el agua.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Lago'); ?>" />
			<Element position="1" value="<?php echo _('Río'); ?>" />
			<Element position="2" value="<?php echo _('Mar'); ?>" />
			<Element position="3" value="<?php echo _('Cascada'); ?>" />
			<Element position="4" value="<?php echo _('Desierto'); ?>" />
		</Elements>
		<Answer position="4" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="44" difficulty="0" explanation="<?php echo _('El resto de palabras son adornos del pelo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Diadema'); ?>" />
			<Element position="1" value="<?php echo _('Horquilla'); ?>" />
			<Element position="2" value="<?php echo _('Peineta'); ?>" />
			<Element position="3" value="<?php echo _('Pelo'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="45" difficulty="0" explanation="<?php echo _('El resto de palabras son muebles de la casa.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Silla'); ?>" />
			<Element position="1" value="<?php echo _('Tejado'); ?>" />
			<Element position="2" value="<?php echo _('Alfombra'); ?>" />
			<Element position="3" value="<?php echo _('Mesa'); ?>" />
		</Elements>
		<Answer position="1" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="46" difficulty="1" explanation="<?php echo _('El resto de palabras son un tipo de arte.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Arquitectura'); ?>" />
			<Element position="1" value="<?php echo _('Escultura'); ?>" />
			<Element position="2" value="<?php echo _('Pintura'); ?>" />
			<Element position="3" value="<?php echo _('Rehabilitación'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="47" difficulty="0" explanation="<?php echo _('El resto de palabras son procesos donde interviene el calor.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Hervir'); ?>" />
			<Element position="1" value="<?php echo _('Salar'); ?>" />
			<Element position="2" value="<?php echo _('Tostar'); ?>" />
			<Element position="3" value="<?php echo _('Asar'); ?>" />
		</Elements>
		<Answer position="1" />
	</SemanticSeriesExercise>
	<SemanticSeriesExercise id="48" difficulty="0" explanation="<?php echo _('El resto de palabras son prendas que se ponen en la parte de superior del cuerpo.'); ?>">
		<Elements>
			<Element position="0" value="<?php echo _('Camisa'); ?>" />
			<Element position="1" value="<?php echo _('Blusa'); ?>" />
			<Element position="2" value="<?php echo _('Chaqueta'); ?>" />
			<Element position="3" value="<?php echo _('Pantalón'); ?>" />
		</Elements>
		<Answer position="3" />
	</SemanticSeriesExercise>
</SemanticSeriesExercises>

