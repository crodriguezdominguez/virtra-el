<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<Items>
	<Item id="medicina" genre="f" singularname="<?php echo _('caja de pastillas'); ?>" pluralname="<?php echo _('cajas de pastillas'); ?>" simplename="<?php echo _('cajas de pastillas'); ?>" img="exercises/img/BagItemsExercise/objects/medicina.png"/>
	<Item id="camisa" genre="f" singularname="<?php echo _('camisa'); ?>" pluralname="<?php echo _('camisas'); ?>" simplename="<?php echo _('camisas'); ?>" img="exercises/img/BagItemsExercise/objects/camisa.png"/>
	<Item id="filete" genre="m" singularname="<?php echo _('filete'); ?>" pluralname="<?php echo _('filetes'); ?>" simplename="<?php echo _('filetes'); ?>" img="exercises/img/BagItemsExercise/objects/filete.png"/>
	<Item id="flor" genre="f" singularname="<?php echo _('flor'); ?>" pluralname="<?php echo _('flores'); ?>" simplename="<?php echo _('flores'); ?>" img="exercises/img/BagItemsExercise/objects/flor.png"/>
	<Item id="huevo" genre="m" singularname="<?php echo _('huevo'); ?>" pluralname="<?php echo _('huevos'); ?>" simplename="<?php echo _('huevos'); ?>" img="exercises/img/BagItemsExercise/objects/huevo.png"/>
	<Item id="juguete" genre="m" singularname="<?php echo _('juguete'); ?>" pluralname="<?php echo _('juguetes'); ?>" simplename="<?php echo _('juguetes'); ?>" img="exercises/img/BagItemsExercise/objects/juguete.png"/>
	<Item id="leche" genre="f" singularname="<?php echo _('cartón de leche'); ?>" pluralname="<?php echo _('cartones de leche'); ?>" simplename="<?php echo _('leche'); ?>" img="exercises/img/BagItemsExercise/objects/leche.png"/>
	<Item id="libro" genre="m" singularname="<?php echo _('libro'); ?>" pluralname="<?php echo _('libros'); ?>" simplename="<?php echo _('libros'); ?>" img="exercises/img/BagItemsExercise/objects/libro.png"/>
	<Item id="manzana" genre="f" singularname="<?php echo _('manzana'); ?>" pluralname="<?php echo _('manzanas'); ?>" simplename="<?php echo _('manzanas'); ?>" img="exercises/img/BagItemsExercise/objects/manzana.png"/>
	<Item id="moneda" genre="f" singularname="<?php echo _('moneda'); ?>" pluralname="<?php echo _('monedas'); ?>" simplename="<?php echo _('monedas'); ?>" img="exercises/img/BagItemsExercise/objects/moneda.png"/>
	<Item id="pan" genre="f" singularname="<?php echo _('barra de pan'); ?>" pluralname="<?php echo _('barras de pan'); ?>" simplename="<?php echo _('panes'); ?>" img="exercises/img/BagItemsExercise/objects/pan.png"/>
	<Item id="pantalon" genre="m" singularname="<?php echo _('pantalón'); ?>" pluralname="<?php echo _('pantalones'); ?>" simplename="<?php echo _('pantalones'); ?>" img="exercises/img/BagItemsExercise/objects/pantalon.png"/>
	<Item id="panyuelo" genre="m" singularname="<?php echo _('paquete de pañuelos'); ?>" pluralname="<?php echo _('paquetes de pañuelos'); ?>" simplename="<?php echo _('pañuelos'); ?>" img="exercises/img/BagItemsExercise/objects/panyuelo.png"/>
	<Item id="paraguas" genre="m" singularname="<?php echo _('paraguas'); ?>" pluralname="<?php echo _('paraguas'); ?>" simplename="<?php echo _('paraguas'); ?>" img="exercises/img/BagItemsExercise/objects/paraguas.png"/>
	<Item id="pera" genre="f" singularname="<?php echo _('pera'); ?>" pluralname="<?php echo _('peras'); ?>" simplename="<?php echo _('peras'); ?>" img="exercises/img/BagItemsExercise/objects/pera.png"/>
	<Item id="periodico" genre="m" singularname="<?php echo _('periódico'); ?>" pluralname="<?php echo _('periódicos'); ?>" simplename="<?php echo _('periódicos'); ?>" img="exercises/img/BagItemsExercise/objects/periodico.png"/>
	<Item id="pescado" genre="m" singularname="<?php echo _('pescado'); ?>" pluralname="<?php echo _('pescados'); ?>" simplename="<?php echo _('pescados'); ?>" img="exercises/img/BagItemsExercise/objects/pescado.png"/>
	<Item id="platano" genre="m" singularname="<?php echo _('plátano'); ?>" pluralname="<?php echo _('plátanos'); ?>" simplename="<?php echo _('plátanos'); ?>" img="exercises/img/BagItemsExercise/objects/platano.png"/>
	<Item id="refresco" genre="m" singularname="<?php echo _('refresco'); ?>" pluralname="<?php echo _('refrescos'); ?>" simplename="<?php echo _('refrescos'); ?>" img="exercises/img/BagItemsExercise/objects/refresco.png"/>
	<Item id="revista" genre="f" singularname="<?php echo _('revista'); ?>" pluralname="<?php echo _('revistas'); ?>" simplename="<?php echo _('revistas'); ?>" img="exercises/img/BagItemsExercise/objects/revista.png"/>
	<Item id="sombrero" genre="m" singularname="<?php echo _('sombrero'); ?>" pluralname="<?php echo _('sombreros'); ?>" simplename="<?php echo _('sombreros'); ?>" img="exercises/img/BagItemsExercise/objects/sombrero.png"/>
	<Item id="telefono" genre="m" singularname="<?php echo _('teléfono móvil'); ?>" pluralname="<?php echo _('teléfonos móviles'); ?>" simplename="<?php echo _('teléfonos'); ?>" img="exercises/img/BagItemsExercise/objects/telefono.png"/>
</Items>
