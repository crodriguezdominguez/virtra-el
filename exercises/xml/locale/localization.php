<?php
/** Script that generates localized JS code 
* @author Carlos Rodriguez Dominguez
*/

$langs = array('es_ES', 'en_US', 'pt', 'pt_PT');

session_start();

if (isset($_SESSION['lang'])) {
    $locale = $_SESSION['lang'];
}
else {
    $locale = 'es_ES';
    if (array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER))
    {
        $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        if ($locale == null)
        {
            $locale = 'es_ES';
        }
    }

    if ($locale == 'pt') {
        $locale = 'pt_PT';
    }
}

if (!in_array($locale, $langs)) {
    $locale = 'es_ES';
}

putenv("LC_ALL=$locale");
setlocale(LC_ALL, $locale);
bindtextdomain('message', '../locale');
textdomain('message');
?>
