<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<LogicalSeriesExercises>
	<LogicalSeriesExercise id="1" difficulty="0" explanation="<?php echo _('Le sigue un triángulo, porque van alternativamente un triángulo y un círculo.'); ?>">
		<Elements>
			<Element position="0" figure="2" />
			<Element position="1" figure="1" />
			<Element position="2" figure="2" />
			<Element position="3" figure="1" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="2" difficulty="0" explanation="<?php echo _('La flecha sigue el sentido de las agujas de un reloj.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="5" />
			<Element position="2" figure="3" />
			<Element position="3" figure="6" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="3" difficulty="0" explanation="<?php echo _('Se le va restando un punto cada vez, hasta quedarse sin ningún punto.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="5" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="4" difficulty="1" explanation="<?php echo _('Le sigue un punto, porque la serie va aumentando de dos en dos hasta llegar a 5 puntos y luego restando de dos en dos.'); ?>">
		<Elements>
			<Element position="0" figure="4" />
			<Element position="1" figure="2" />
			<Element position="2" figure="6" />
			<Element position="3" figure="2" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="4" />
			<Element position="2" figure="3" />
			<Element position="3" figure="5" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="5" difficulty="0" explanation="<?php echo _('Le sigue el rombo dentro de un círculo, ya que cada figura va seguida de la misma dentro de un círculo.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="3" />
			<Element position="2" figure="6" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="2" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="6" difficulty="0" explanation="<?php echo _('Le sigue los dos círculos, porque se van sumando las dos figuras anteriores.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="3" />
			<Element position="2" figure="4" />
			<Element position="3" figure="6" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="7" difficulty="0" explanation="<?php echo _('Le sigue la figura inclinada hacia la derecha, porque van alternativamente.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="1" />
			<Element position="3" figure="2" />
		</Elements>
		<Options>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="8" difficulty="1" explanation="<?php echo _('Le sigue esta figura, porque se van sumando las dos figuras anteriores.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="6" />
			<Element position="4" figure="5" />
		</Elements>
		<Options>
			<Element position="0" figure="6" />
			<Element position="1" figure="7" />
			<Element position="2" figure="4" />
			<Element position="3" figure="8" />
		</Options>
		<Answer position="2" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="9" difficulty="0" explanation="<?php echo _('Le sigue esta figura, porque los palitos del lado izquierdo se va sumando al lado derecho.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="2" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="10" difficulty="1" explanation="<?php echo _('Le sigue el círculo, ya que se va descomponiendo la figura anterior de dentro hacia fuera.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="8" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="11" difficulty="0" explanation="<?php echo _('Le sigue esta figura, porque el triángulo pequeño va girando en sentido de las agujas del reloj.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="4" />
			<Element position="1" figure="6" />
			<Element position="2" figure="5" />
			<Element position="3" figure="1" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="12" difficulty="0" explanation="<?php echo _('Le sigue esta figura, porque sería el opuesto de la figura anterior.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="4" />
			<Element position="1" figure="6" />
			<Element position="2" figure="5" />
			<Element position="3" figure="2" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="13" difficulty="0" explanation="<?php echo _('Le sigue esta figura, ya que el criterio seguido es que se introducen las figuras pequeñas en el interior con la misma posición.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="4" />
			<Element position="1" figure="6" />
			<Element position="2" figure="5" />
			<Element position="3" figure="7" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="14" difficulty="1" explanation="<?php echo _('Le sigue esta figura, ya que el círculo desaparece y las otras dos figuras invierten su posición.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="4" />
			<Element position="1" figure="6" />
			<Element position="2" figure="5" />
			<Element position="3" figure="7" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="15" difficulty="2" explanation="<?php echo _('El círculo y el cuadrado giran en el sentido de las agujas del reloj y el triángulo gira en el sentido contrario a las agujas del reloj.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="4" />
			<Element position="1" figure="6" />
			<Element position="2" figure="5" />
			<Element position="3" figure="7" />
		</Options>
		<Answer position="2" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="16" difficulty="1" explanation="<?php echo _('El rectángulo, como el círculo va girando cada vez y los colores de las superficie se va invirtiendo.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="4" />
			<Element position="1" figure="6" />
			<Element position="2" figure="5" />
			<Element position="3" figure="7" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="17" difficulty="1" explanation="<?php echo _('Se van sumando las dos figuras anteriores.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
		</Elements>
		<Options>
			<Element position="0" figure="7" />
			<Element position="1" figure="6" />
			<Element position="2" figure="3" />
			<Element position="3" figure="8" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="18" difficulty="1" explanation="<?php echo _('Van en grupos de dos en dos y siempre se invierte el anterior.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
		</Elements>
		<Options>
			<Element position="0" figure="8" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="3" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="20" difficulty="0" explanation="<?php echo _('Le sigue esta figura, porque la imagen anterior se refleja como en un espejo.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="2" />
			<Element position="1" figure="1" />
			<Element position="2" figure="3" />
			<Element position="3" figure="5" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="21" difficulty="2" explanation="<?php echo _('El círculo se desplaza a razón de una casilla en diagonal hacia abajo; el cuadrado, una casilla horizontal hacia la izquierda.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="22" difficulty="0" explanation="<?php echo _('El ángulo formado por las líneas del interior del círculo gira cada vez 90 grados en el sentido de las agujas del reloj.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="7" />
			<Element position="1" figure="4" />
			<Element position="2" figure="5" />
			<Element position="3" figure="1" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="23" difficulty="2" explanation="<?php echo _('De una figura a la otra, el cuadrado y el triángulo avanzan sistemáticamente a razón de 90º en el sentido de las agujas del reloj; el círculo de un cuarto de vuelta en el sentido contrario.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="24" difficulty="1" explanation="<?php echo _('Se juntan las dos lineas en la parte opuesta a la figura.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="25" difficulty="2" explanation="<?php echo _('A partir de la figura de la izquierda, el círculo blanco, el cuadrado y el triángulo dan un cuarto de vuelta en el sentido de las agujas del reloj y  cambian de color en cada movimiento. El círculo negro realiza el mismo desplazamiento, pero sin cambiar de color.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="26" difficulty="2" explanation="<?php echo _('La raya de abajo a la izquierda gira de una figura a otra 45 grados en el sentido de las agujas del reloj; la raya de arriba a la derecha, 90 grados.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="27" difficulty="2" explanation="<?php echo _('El círculo avanza cada vez una casilla en sentido horizontal, de derecha a izquierda; el cuadrado, en diagonal, de abajo arriba.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="1" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="28" difficulty="2" explanation="<?php echo _('El círculo y el cuadrado se van desplazando 45 grados en el sentido de las agujas del reloj y en cambio el triángulo además de cambiar el color, se desplaza 45 grados en sentido contrario.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="29" difficulty="1" explanation="<?php echo _('La raya de la derecha gira 45 grados en el sentido de las agujas del reloj; la raya de la izquierda, en sentido contrario. El cuadrado se desplaza 90 grados en sentido contrario a las agujas del reloj.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="6" />
			<Element position="2" figure="7" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="30" difficulty="2" explanation="<?php echo _('El círculo blanco se desplaza cada vez una casilla en diagonal, de izquierda a derecha y de arriba abajo. El círculo negro se desplaza una casilla en diagonal, de derecha a izquierda y de arriba abajo.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
		</Elements>
		<Options>
			<Element position="0" figure="5" />
			<Element position="1" figure="7" />
			<Element position="2" figure="6" />
			<Element position="3" figure="4" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="31" difficulty="1" explanation="<?php echo _('Los dibujos se van sumando por columnas, los dos dibujos que van por encima.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="12" />
			<Element position="1" figure="14" />
			<Element position="2" figure="9" />
			<Element position="3" figure="10" />
			<Element position="4" figure="13" />
			<Element position="5" figure="11" />
		</Options>
		<Answer position="5" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="32" difficulty="0" explanation="<?php echo _('Los dibujos van por filas y centrando la figura pequeña en el centro en la última columna.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="14" />
			<Element position="1" figure="11" />
			<Element position="2" figure="12" />
			<Element position="3" figure="10" />
			<Element position="4" figure="9" />
			<Element position="5" figure="13" />
		</Options>
		<Answer position="5" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="33" difficulty="1" explanation="<?php echo _('Se van sumando las figuras por filas y columnas.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="10" />
			<Element position="2" figure="11" />
			<Element position="3" figure="12" />
			<Element position="4" figure="13" />
			<Element position="5" figure="14" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="34" difficulty="0" explanation="<?php echo _('Se van restando puntos tanto por columnas como por filas.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="10" />
			<Element position="2" figure="11" />
			<Element position="3" figure="12" />
			<Element position="4" figure="13" />
			<Element position="5" figure="14" />
		</Options>
		<Answer position="5" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="35" difficulty="1" explanation="<?php echo _('En la última columna la figura pequeña va debajo y alternando el color.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="12" />
			<Element position="1" figure="10" />
			<Element position="2" figure="9" />
			<Element position="3" figure="11" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="39" difficulty="1" explanation="<?php echo _('Se van sumando los palitos de las figuras que van en misma columna.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="10" />
			<Element position="2" figure="12" />
			<Element position="3" figure="11" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="40" difficulty="1" explanation="<?php echo _('Se va repitiendo el mismo dibujo en forma diagonal de izquierda a derecha.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="12" />
			<Element position="2" figure="11" />
			<Element position="3" figure="10" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="43" difficulty="1" explanation="<?php echo _('Se va repitiendo el mismo dibujo en forma diagonal, de izquierda a derecha.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="10" />
			<Element position="2" figure="12" />
			<Element position="3" figure="11" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="44" difficulty="2" explanation="<?php echo _('Por filas, cada figura gira 45 grados y se invierte el triángulo con respecto a la figura anterior.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="11" />
			<Element position="1" figure="10" />
			<Element position="2" figure="9" />
			<Element position="3" figure="12" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="46" difficulty="0" explanation="<?php echo _('Se van sumando los dos dibujos que van por encima.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="6" />
			<Element position="4" figure="5" />
			<Element position="5" figure="4" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="12" />
			<Element position="1" figure="13" />
			<Element position="2" figure="14" />
			<Element position="3" figure="10" />
			<Element position="4" figure="9" />
			<Element position="5" figure="11" />
		</Options>
		<Answer position="2" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="51" difficulty="0" explanation="<?php echo _('Se va aumentando progresivamente el número de aros.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="12" />
			<Element position="2" figure="10" />
			<Element position="3" figure="11" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="52" difficulty="0" explanation="<?php echo _('Se van sumando todos los trazos por filas.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="12" />
			<Element position="1" figure="10" />
			<Element position="2" figure="9" />
			<Element position="3" figure="11" />
		</Options>
		<Answer position="0" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="53" difficulty="0" explanation="<?php echo _('Por columnas, se va repitiendo la figura pequeña y en la última fila, la figura pequeña queda a la izquierda.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="9" />
			<Element position="1" figure="12" />
			<Element position="2" figure="11" />
			<Element position="3" figure="10" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="55" difficulty="1" explanation="<?php echo _('Por filas, cada figura gira 45 grados con respecto a la anterior figura.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="11" />
			<Element position="1" figure="12" />
			<Element position="2" figure="9" />
			<Element position="3" figure="10" />
		</Options>
		<Answer position="1" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="56" difficulty="1" explanation="<?php echo _('Por columnas, se va sumando las dos figuras de encima.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="11" />
			<Element position="1" figure="9" />
			<Element position="2" figure="10" />
			<Element position="3" figure="12" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="57" difficulty="1" explanation="<?php echo _('Por filas, cada figura gira 90 grados en el sentido de las agujas del reloj respecto a la figura anterior.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="11" />
			<Element position="1" figure="10" />
			<Element position="2" figure="12" />
			<Element position="3" figure="9" />
		</Options>
		<Answer position="3" />
	</LogicalSeriesExercise>
	<LogicalSeriesExercise id="58" difficulty="0" explanation="<?php echo _('Se va sumando las dos figuras por columnas y filas.'); ?>">
		<Elements>
			<Element position="0" figure="1" />
			<Element position="1" figure="2" />
			<Element position="2" figure="3" />
			<Element position="3" figure="4" />
			<Element position="4" figure="5" />
			<Element position="5" figure="6" />
			<Element position="6" figure="7" />
			<Element position="7" figure="8" />
		</Elements>
		<Options>
			<Element position="0" figure="12" />
			<Element position="1" figure="9" />
			<Element position="2" figure="11" />
			<Element position="3" figure="10" />
		</Options>
		<Answer position="2" />
	</LogicalSeriesExercise>
</LogicalSeriesExercises>
