<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<Criterias>
	<Criteria id="0" phrase="<?php echo _('A tu Nieto Daniel, de 7 años, le gusta mucho el ciclismo y el color amarillo.'); ?>" person="0" item="3" />
	<Criteria id="1" phrase="<?php echo _('A tu hermano Joaquín, de 54 años, le gusta mucho cazar y leer.'); ?>" person="1" item="18" />
	<Criteria id="2" phrase="<?php echo _('A tu sobrino José, de 20 años, le gusta mucho esquiar y el color negro.'); ?>" person="2" item="20" />
	<Criteria id="3" phrase="<?php echo _('A tu sobrina Pepa, de 60 años, le gustan mucho las flores rojas, pero es alérgica a las rosas.'); ?>" person="3" item="29" />
	<Criteria id="4" phrase="<?php echo _('A tu hija Manuela le gusta decorar su casa y el color morado.'); ?>" person="4" item="34" />
	<Criteria id="5" phrase="<?php echo _('A tu hermana Josefa le gustan los zapatos blancos y pasa mucho frío para ir al trabajo.'); ?>" person="5" item="38" />
	<Criteria id="6" phrase="<?php echo _('A tu sobrina Laura le gusta hacer puzzles y adora los gatos.'); ?>" person="6" item="48" />
	<Criteria id="7" phrase="<?php echo _('A tu nieta Lorena le gusta mucho la ropa y el color rosa.'); ?>" person="7" item="26" />
	<Criteria id="8" phrase="<?php echo _('A tu nieto Pablo le gustan mucho los coches y el color azul.'); ?>" person="8" item="51" />
	<Criteria id="9" phrase="<?php echo _('A tu nieta, de 3 años, le gustan las muñecas, pero no las que son pelirrojas o morenas y tienen el pelo corto.'); ?>" person="9" item="55" />
	<Criteria id="10" phrase="<?php echo _('A tu primo Vicente le gustan mucho los dulces, sobretodo los de navidad.'); ?>" person="10" item="112" />
	<Criteria id="11" phrase="<?php echo _('A tu hija se le ha roto la lavadora y tiene mucha ropa de sus 4 hijos.'); ?>" person="11" item="60" />
	<Criteria id="12" phrase="<?php echo _('A tu hijo le gusta echarse la siesta en un sillón cómodo y el color negro.'); ?>" person="12" item="63" />
	<Criteria id="13" phrase="<?php echo _('A tu nieto se le ha perdido el teléfono móvil y le gusta el color blanco.'); ?>" person="13" item="69" />
	<Criteria id="14" phrase="<?php echo _('Tu sobrino se acaba de sacar el carnet de moto y le gusta el color rojo.'); ?>" person="14" item="75" />
	<Criteria id="15" phrase="<?php echo _('A tu nieto le gusta mucho jugar a la videoconsola, sobretodo a los juegos de tiros.'); ?>" person="15" item="77" />
	<Criteria id="16" phrase="<?php echo _('A tu nieta le gusta mucho leer, y además le encantan los animales.'); ?>" person="16" item="113" />
	<Criteria id="17" phrase="<?php echo _('A tu sobrina le gustan las joyas de oro y los anillos.'); ?>" person="17" item="80" />
	<Criteria id="18" phrase="<?php echo _('A tu nieto le gusta mucho el fútbol y le hace falta ropa para hacer deporte.'); ?>" person="18" item="5" />
	<Criteria id="19" phrase="<?php echo _('A tu hermano le gustan mucho las tartas, pero es alérgico al chocolate, a la manzana y al merengue.'); ?>" person="19" item="88" />
	<Criteria id="20" phrase="<?php echo _('A tu sobrina le gusta oler bien, pero no le gustan los olores fuertes.'); ?>" person="20" item="93" />
	<Criteria id="21" phrase="<?php echo _('A tu amigo Luis le gusta mucho caminar por el campo y llevar zapatos adecuados.'); ?>" person="21" item="47" />
	<Criteria id="22" phrase="<?php echo _('A tu amiga María le gusta mucho que le regalen flores blancas, pero no le gustan las margaritas.'); ?>" person="22" item="31" />
	<Criteria id="23" phrase="<?php echo _('A tu amiga Ana, que sabe hablar francés, le gustaría ir de viaje a otro país, aunque no le gustan los viajes muy largos.'); ?>" person="23" item="94" />
	<Criteria id="24" phrase="<?php echo _('A tu nieto le gusta mucho ir de paseo y se le ha roto el carrito. No le gustan los colores rojo, verde y gris.'); ?>" person="24" item="97" />
	<Criteria id="25" phrase="<?php echo _('A tu nieta le gusta mucho disfrazarse y los cuentos de princesas.'); ?>" person="25" item="101" />
	<Criteria id="26" phrase="<?php echo _('A tu hermano le gusta hacer viajes y sacar muchas fotos.'); ?>" person="26" item="67" />
	<Criteria id="27" phrase="<?php echo _('Tu nieto está aprendiendo a tocar la guitarra y le gusta mucho el flamenco.'); ?>" person="27" item="105" />
	<Criteria id="28" phrase="<?php echo _('A tu amigo Carlos le gustan mucho los cuadros y los animales.'); ?>" person="28" item="108" />
     <Criteria id="29" phrase="<?php echo _('A tu primo le gusta mucho jugar al baloncesto y ya tiene la equipación y las zapatillas.'); ?>" person="29" item="9" />	
</Criterias>