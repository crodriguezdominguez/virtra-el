<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');

    $langs = array('es_ES', 'pt', 'pt_PT');

    session_start();

    $locale = '';
    if (isset($_SESSION['lang'])) {
        $locale = $_SESSION['lang'];
    }
    else {
        $locale = '';
        if (array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER))
        {
            $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
            if ($locale == null)
            {
                $locale = '';
            }
        }

        if ($locale == 'pt') {
            $locale = 'pt_PT';
        }
    }

    if (!in_array($locale, $langs)) {
        $locale = '';
    }

    if ($locale == 'es_ES') {
        $locale = '';
    }

    $langCode = $locale;
?>

<Places background="exercises/img/PackageDeliveryExercise/background.png" width="900" height="540">
	<Locations>
		<Location id="0" x="55" y="68" />
		<Location id="1" x="290" y="68" />
		<Location id="2" x="525" y="68" />
		<Location id="3" x="760" y="68" />
		<Location id="4" x="55" y="185" />
		<Location id="5" x="290" y="185" />
		<Location id="6" x="525" y="185" />
		<Location id="7" x="760" y="185" />
		<Location id="8" x="55" y="302" />
		<Location id="9" x="300" y="302" />
		<Location id="10" x="525" y="302" />
		<Location id="11" x="760" y="302" />
		<Location id="12" x="55" y="419" />
		<Location id="13" x="265" y="419" />
		<Location id="14" x="505" y="419" />
		<Location id="15" x="760" y="419" />
	</Locations>
	<Place id="0" name="<?php echo _('Tienda de deportes'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_deportes<?= $langCode ?>.png" w="100" h="100" />
	<Place id="1" name="<?php echo _('Librería'); ?>" img="exercises/img/PackageDeliveryExercise/places/library<?= $langCode ?>.png" w="100" h="100" />
	<Place id="2" name="<?php echo _('Tienda de ropa'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_ropa<?= $langCode ?>.png"  w="100" h="100" />
	<Place id="3" name="<?php echo _('Floristería'); ?>" img="exercises/img/PackageDeliveryExercise/places/floristeria<?= $langCode ?>.png"  w="100" h="100" />
	<Place id="4" name="<?php echo _('Agencia de viajes'); ?>" img="exercises/img/PackageDeliveryExercise/places/agencia_de_viajes<?= $langCode ?>.png" w="100" h="100" />
	<Place id="5" name="<?php echo _('Joyería'); ?>" img="exercises/img/PackageDeliveryExercise/places/joyeria<?= $langCode ?>.png" w="100" h="100" />
	<Place id="6" name="<?php echo _('Pastelería'); ?>" img="exercises/img/PackageDeliveryExercise/places/pasteleria<?= $langCode ?>.png" w="100" h="100" />
	<Place id="7" name="<?php echo _('Perfumería'); ?>" img="exercises/img/PackageDeliveryExercise/places/perfumeria<?= $langCode ?>.png" w="100" h="100" />
	<Place id="8" name="<?php echo _('Tienda de animales'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_animales<?= $langCode ?>.png" w="100" h="100" />
	<Place id="9" name="<?php echo _('Concesionario'); ?>" img="exercises/img/GiftShoppingExercise/concesionario<?= $langCode ?>.png" w="100" h="100" />
	<Place id="10" name="<?php echo _('Tienda de muebles'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_muebles<?= $langCode ?>.png" w="100" h="100" />
	<Place id="11" name="<?php echo _('Tienda de telefonía'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_telefonia<?= $langCode ?>.png" w="100" h="100" />
	<Place id="12" name="<?php echo _('Zapatería'); ?>" img="exercises/img/PackageDeliveryExercise/places/zapateria<?= $langCode ?>.png" w="100" h="100" />
	<Place id="13" name="<?php echo _('Tienda de decoración'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_decoracion<?= $langCode ?>.png" w="100" h="100" />
	<Place id="14" name="<?php echo _('Juguetería'); ?>" img="exercises/img/PackageDeliveryExercise/places/jugueteria<?= $langCode ?>.png" w="100" h="100" />
	<Place id="15" name="<?php echo _('Electrodomésticos'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_electrodomesticos<?= $langCode ?>.png" w="100" h="100" />
	<Place id="16" name="<?php echo _('Tienda de música'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_musica<?= $langCode ?>.png" w="100" h="100" />
	<Place id="17" name="<?php echo _('Tienda de bebés'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_bebes<?= $langCode ?>.png" w="100" h="100" />
</Places>
