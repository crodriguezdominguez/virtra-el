<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');

    $langs = array('es_ES', 'pt', 'pt_PT');

    session_start();

    $locale = '';
    if (isset($_SESSION['lang'])) {
        $locale = $_SESSION['lang'];
    }
    else {
        $locale = '';
        if (array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER))
        {
            $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
            if ($locale == null)
            {
                $locale = '';
            }
        }

        if ($locale == 'pt') {
            $locale = 'pt_PT';
        }
    }

    if (!in_array($locale, $langs)) {
        $locale = '';
    }

    if ($locale == 'es_ES') {
        $locale = '';
    }

    $langCode = $locale;
?>

<Places background="exercises/img/BagItemsExercise/fondo1024.jpg" width="1024" height="600">
	<Actions>
		<Action id="0" name="<?php echo _('Cojo'); ?>" add="1" />
		<Action id="1" name="<?php echo _('Doy'); ?>" add="0" />
		<Action id="2" name="<?php echo _('Compro'); ?>" add="1" />
		<Action id="3" name="<?php echo _('Pierdo'); ?>" add="0" />
		<Action id="4" name="<?php echo _('Encuentro'); ?>" add="1" />
	</Actions>
	<Characters>
		<Character id="panadero" name="<?php echo _('al panadero'); ?>" />
		<Character id="farmaceutico" name="<?php echo _('al farmaceutico'); ?>" />
		<Character id="patos" name="<?php echo _('a los patos'); ?>" />
		<Character id="librero" name="<?php echo _('al librero'); ?>" />
		<Character id="cajero" name="<?php echo _('al cajero'); ?>" />
		<Character id="carnicero" name="<?php echo _('al carnicero'); ?>" />
		<Character id="pescadero" name="<?php echo _('al pescadero'); ?>" />
		<Character id="frutero" name="<?php echo _('al frutero'); ?>" />
		<Character id="gorilas" name="<?php echo _('a los gorilas'); ?>" />
		<Character id="vendedor" name="<?php echo _('al vendedor'); ?>" />
	</Characters>	
	<Place id="casa" name="<?php echo _('casa'); ?>" img="exercises/img/BagItemsExercise/places/casa.png">
		<Item action="0" id="moneda" character=""/>
		<Item action="0" id="telefono" character=""/>
		<Item action="0" id="paraguas" character=""/>
		<Item action="0" id="sombrero" character=""/>
		<Item action="0" id="medicina" character=""/>
		<Item action="0" id="libro" character=""/>		
	</Place>
	<Place id="panaderia" name="<?php echo _('la panadería'); ?>" img="exercises/img/BagItemsExercise/places/panaderia<?= $langCode ?>.png">
		<Item action="2" id="pan" character="panadero"/>
		<Item action="1" id="moneda" character="panadero"/>
		<Item action="1" id="medicina" character="panadero"/>
	</Place>
	<Place id="parque" name="<?php echo _('el parque'); ?>" img="exercises/img/BagItemsExercise/places/parque.png">
		<Item action="4" id="flor" character=""/>
		<Item action="1" id="pera" character="patos"/>
		<Item action="1" id="platano" character="patos"/>
		<Item action="1" id="manzana" character="patos"/>
		<Item action="1" id="pan" character="patos"/>
	</Place>
	<Place id="farmacia" name="<?php echo _('la farmacia'); ?>" img="exercises/img/BagItemsExercise/places/farmacia<?= $langCode ?>.png">
		<Item action="2" id="medicina" character="farmaceutico"/>
		<Item action="1" id="moneda" character="farmaceutico"/>
	</Place>
	<Place id="libreria" name="<?php echo _('la librería'); ?>" img="exercises/img/BagItemsExercise/places/libreria<?= $langCode ?>.png">
		<Item action="2" id="libro" character="librero"/>
		<Item action="1" id="moneda" character="librero"/>
		<Item action="1" id="medicina" character="librero"/>
	</Place>
	<Place id="fruteria" name="<?php echo _('la frutería'); ?>" img="exercises/img/BagItemsExercise/places/fruteria<?= $langCode ?>.png">
		<Item action="2" id="manzana" character="frutero"/>
		<Item action="2" id="pera" character="frutero"/>
		<Item action="2" id="platano" character="frutero"/>
		<Item action="1" id="moneda" character="frutero"/>
		<Item action="1" id="medicina" character="frutero"/>
		<Item action="3" id="telefono" character=""/>
	</Place>
	<Place id="tiendaderopa" name="<?php echo _('la tienda de ropa'); ?>" img="exercises/img/BagItemsExercise/places/tienda_de_ropa<?= $langCode ?>.png">
		<Item action="2" id="pantalon" character="vendedor"/>
		<Item action="2" id="camisa" character="vendedor"/>
		<Item action="1" id="moneda" character="vendedor"/>
		<Item action="3" id="telefono" character=""/>		
	</Place>
	<Place id="supermercado" name="<?php echo _('el supermercado'); ?>" img="exercises/img/BagItemsExercise/places/supermercado<?= $langCode ?>.png">
		<Item action="2" id="leche" character=""/>
		<Item action="2" id="huevo" character=""/>
		<Item action="1" id="moneda" character="cajero"/>
		<Item action="3" id="telefono" character=""/>		
	</Place>
	<Place id="kiosko" name="<?php echo _('el kiosco'); ?>" img="exercises/img/BagItemsExercise/places/kiosco<?= $langCode ?>.png">
		<Item action="2" id="periodico" character="kioskero"/>
		<Item action="2" id="revista" character="kioskero"/>
		<Item action="1" id="moneda" character="kioskero"/>
		<Item action="1" id="medicina" character="kioskero"/>
		<Item action="1" id="libro" character="kioskero"/>	
	</Place>
	<Place id="jugueteria" name="<?php echo _('la juguetería'); ?>" img="exercises/img/BagItemsExercise/places/jugueteria<?= $langCode ?>.png">
		<Item action="2" id="juguete" character="vendedor"/>
		<Item action="1" id="moneda" character="vendedor"/>
	</Place>
	<Place id="pescaderia" name="<?php echo _('la pescadería'); ?>" img="exercises/img/BagItemsExercise/places/pescaderia<?= $langCode ?>.png">
		<Item action="2" id="pescado" character="pescadero"/>
		<Item action="1" id="moneda" character="pescadero"/>
		<Item action="1" id="medicina" character="pescadero"/>
	</Place>
	<Place id="carniceria" name="<?php echo _('la carnicería'); ?>" img="exercises/img/BagItemsExercise/places/carniceria<?= $langCode ?>.png">
		<Item action="2" id="filete" character="carnicero"/>
		<Item action="1" id="moneda" character="carnicero"/>
		<Item action="1" id="medicina" character="carnicero"/>
	</Place>
	<Place id="zoo" name="<?php echo _('el zoo'); ?>" img="exercises/img/BagItemsExercise/places/zoo.png">
		<Item action="1" id="platano" character="gorilas"/>
	</Place>
</Places>
