<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');

    $langs = array('es_ES', 'pt', 'pt_PT');

    session_start();

    $locale = '';
    if (isset($_SESSION['lang'])) {
        $locale = $_SESSION['lang'];
    }
    else {
        $locale = '';
        if (array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER))
        {
            $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
            if ($locale == null)
            {
                $locale = '';
            }
        }

        if ($locale == 'pt') {
            $locale = 'pt_PT';
        }
    }

    if (!in_array($locale, $langs)) {
        $locale = '';
    }

    if ($locale == 'es_ES') {
        $locale = '';
    }

    $langCode = $locale;
?>

<Places background="exercises/img/PackageDeliveryExercise/background.png" width="900" height="540">
	<Place id="0" name="<?php echo _('Tienda de deportes'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_deportes<?= $langCode ?>.png" x="55" y="68" w="100" h="100" />
	<Place id="1" name="<?php echo _('Librería'); ?>" img="exercises/img/PackageDeliveryExercise/places/library<?= $langCode ?>.png" x="290" y="68" w="100" h="100" />
	<Place id="2" name="<?php echo _('Tienda de ropa'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_ropa<?= $langCode ?>.png" x="525" y="68" w="100" h="100" />
	<Place id="3" name="<?php echo _('Floristería'); ?>" img="exercises/img/PackageDeliveryExercise/places/floristeria<?= $langCode ?>.png" x="760" y="68" w="100" h="100" />
	<Place id="4" name="<?php echo _('Agencia de viajes'); ?>" img="exercises/img/PackageDeliveryExercise/places/agencia_de_viajes<?= $langCode ?>.png" x="55" y="185" w="100" h="100" />
	<Place id="5" name="<?php echo _('Joyería'); ?>" img="exercises/img/PackageDeliveryExercise/places/joyeria<?= $langCode ?>.png" x="290" y="185" w="100" h="100" />
	<Place id="6" name="<?php echo _('Pastelería'); ?>" img="exercises/img/PackageDeliveryExercise/places/pasteleria<?= $langCode ?>.png" x="525" y="185" w="100" h="100" />
	<Place id="7" name="<?php echo _('Perfumería'); ?>" img="exercises/img/PackageDeliveryExercise/places/perfumeria<?= $langCode ?>.png" x="760" y="185" w="100" h="100" />
	<Place id="8" name="<?php echo _('Tienda de animales'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_animales<?= $langCode ?>.png" x="55" y="302" w="100" h="100" />
	<Place id="9" name="<?php echo _('Concesionario'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_coches<?= $langCode ?>.png" x="300" y="302" w="100" h="100" />
	<Place id="10" name="<?php echo _('Tienda de muebles'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_muebles<?= $langCode ?>.png" x="525" y="302" w="100" h="100" />
	<Place id="11" name="<?php echo _('Tienda de telefonía'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_telefonia<?= $langCode ?>.png" x="760" y="302" w="100" h="100" />
	<Place id="12" name="<?php echo _('Zapatería'); ?>" img="exercises/img/PackageDeliveryExercise/places/zapateria<?= $langCode ?>.png" x="55" y="419" w="100" h="100" />
	<Place id="13" name="<?php echo _('Tienda de decoración'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_decoracion<?= $langCode ?>.png" x="265" y="419" w="100" h="100" />
	<Place id="14" name="<?php echo _('Juguetería'); ?>" img="exercises/img/PackageDeliveryExercise/places/jugueteria<?= $langCode ?>.png" x="505" y="419" w="100" h="100" />
	<Place id="15" name="<?php echo _('Correos'); ?>" img="exercises/img/PackageDeliveryExercise/places/postoffice<?= $langCode ?>.png" x="760" y="419" w="100" h="100" />
	<!--<Place id="16" name="<?php echo _('Electrodomésticos'); ?>" img="exercises/img/PackageDeliveryExercise/places/tienda_de_electrodomesticos<?= $langCode ?>.png" x="0" y="0" w="100" h="100" />-->
</Places>
