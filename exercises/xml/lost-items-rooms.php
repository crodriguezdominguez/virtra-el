<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<LostItemsExercise>
	<Room id="0" name="<?php echo _('Baño'); ?>" background="exercises/img/LostItemsExercise/rooms/bathroom.png">
		<Decorations>
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/bath.png" x="40" y="332" />
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/wc.png" x="859" y="325" />
		</Decorations>
		<Positions>
			<Position id="0" x="390" y="170" w="120" h="50" tags="shelf" />
			<Position id="1" x="390" y="255" w="120" h="100" tags="shelf" />
			<Position id="2" x="390" y="375" w="120" h="50" tags="shelf" />
			<Position id="3" x="390" y="445" w="120" h="50" tags="shelf" />
			<Position id="4" x="770" y="170" w="120" h="50" tags="shelf" />
			<Position id="5" x="770" y="240" w="120" h="50" tags="shelf" />
			<Position id="6" x="770" y="325" w="120" h="100" tags="shelf" />
			<Position id="7" x="770" y="445" w="120" h="50" tags="shelf" />
			<Position id="8" x="980" y="480" w="50" h="115" tags="floor" />
			<Position id="9" x="545" y="395" w="75" h="130" tags="floor" />
		</Positions>
		<CoinPositions>
			<Position id="10" x="320" y="530" w="35" h="35" tags="coin" />
			<Position id="11" x="1060" y="560" w="35" h="35" tags="coin" />
			<Position id="12" x="701" y="472" w="35" h="35" tags="coin" />
			<Position id="13" x="75" y="505" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="0" img="exercises/img/LostItemsExercise/items/comb.png" w="120" h="50" tag="shelf" name="<?php echo _('el peine'); ?>" />
			<Item id="1" img="exercises/img/LostItemsExercise/items/shampoo.png" w="55" h="100" tag="shelf" name="<?php echo _('el champú'); ?>" />
			<Item id="2" img="exercises/img/LostItemsExercise/items/toothbrush.png" w="120" h="50" tag="shelf" name="<?php echo _('el cepillo de dientes'); ?>" />
			<Item id="3" img="exercises/img/LostItemsExercise/items/bin.png" w="50" h="100" tag="floor" name="<?php echo _('la papelera'); ?>" />
			<Item id="4" img="exercises/img/LostItemsExercise/items/sponge.png" w="120" h="50" tag="shelf" name="<?php echo _('la esponja'); ?>" />
			<Item id="5" img="exercises/img/LostItemsExercise/items/razor.png" w="120" h="100" tag="shelf" name="<?php echo _('la cuchilla de afeitar'); ?>" />
			<Item id="6" img="exercises/img/LostItemsExercise/items/toothpaste.png" w="120" h="50" tag="shelf" name="<?php echo _('la pasta de dientes'); ?>" />
			<Item id="7" img="exercises/img/LostItemsExercise/items/soap.png"  w="120" h="50" tag="shelf" name="<?php echo _('la pastilla de jabón'); ?>" />
			<Item id="8" img="exercises/img/LostItemsExercise/items/gel.png" w="55" h="100" tag="shelf" name="<?php echo _('el gel'); ?>" />
			<Item id="9" img="exercises/img/LostItemsExercise/items/dryer.png" w="60" h="68" tag="shelf" name="<?php echo _('el secador'); ?>" />
		</Items>
	</Room>
	<Room id="1" name="<?php echo _('Dormitorio'); ?>" background="exercises/img/LostItemsExercise/rooms/bedroom.png">
		<Decorations>
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/bed.png" x="723" y="380" />
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/furniture.png" x="35" y="405" />
		</Decorations>
		<Positions>
			<Position id="20" x="488" y="180" w="120" h="100" tags="shelf" />
			<Position id="21" x="488" y="280" w="120" h="70" tags="shelf" />
			<Position id="22" x="488" y="370" w="120" h="50" tags="shelf" />
			<Position id="23" x="488" y="443" w="120" h="50" tags="shelf" />
			<Position id="24" x="640" y="320" w="80" h="120" tags="table" />
			<Position id="25" x="920" y="315" w="80" h="120" tags="table" />
			<Position id="26" x="35" y="300" w="140" h="120" tags="table" />
			<Position id="27" x="980" y="500" w="100" h="100" tags="floor" />
			<Position id="28" x="80" y="500" w="100" h="100" tags="floor" />
			<Position id="29" x="225" y="370" w="100" h="50" tags="table" />
			<Position id="30" x="225" y="250" w="100" h="100" tags="wardrobe" />
		</Positions>
		<CoinPositions>
			<Position id="35" x="190" y="465" w="35" h="35" tags="coin" />
			<Position id="36" x="170" y="515" w="35" h="35" tags="coin" />
			<Position id="37" x="430" y="485" w="35" h="35" tags="coin" />
			<Position id="38" x="940" y="565" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="10" img="exercises/img/LostItemsExercise/items/pending.png" w="40" h="50" tag="shelf" name="<?php echo _('los pendientes'); ?>" />
			<Item id="11" img="exercises/img/LostItemsExercise/items/houseslippers.png" w="100" h="50" tag="floor" name="<?php echo _('las zapatillas de casa'); ?>" />
			<Item id="13" img="exercises/img/LostItemsExercise/items/nightgown.png" w="80" h="120" tag="shelf" name="<?php echo _('el camisón'); ?>" />
			<Item id="14" img="exercises/img/LostItemsExercise/items/jeweller.png" w="120" h="100" tag="shelf" name="<?php echo _('el joyero'); ?>" />
			<Item id="15" img="exercises/img/LostItemsExercise/items/perch.png" w="100" h="50" tag="wardrobe" name="<?php echo _('la percha'); ?>" />
			<Item id="16" img="exercises/img/LostItemsExercise/items/collar.png" w="50" h="50" tag="shelf" name="<?php echo _('el collar'); ?>" />
			<Item id="17" img="exercises/img/LostItemsExercise/items/shirt.png" w="100" h="100" tag="wardrobe" name="<?php echo _('la camisa'); ?>" />
			<Item id="18" img="exercises/img/LostItemsExercise/items/jeans.png" w="60" h="100" tag="wardrobe" name="<?php echo _('el pantalón'); ?>" />
		</Items>
	</Room>
	<Room id="2" name="<?php echo _('Salón'); ?>" background="exercises/img/LostItemsExercise/rooms/livingroom.png">
		<Decorations>
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/sofa1.png" x="390" y="365" />
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/sofa2.png" x="53" y="336" />
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/table.png" x="190" y="426" />
		</Decorations>
		<Positions>
			<Position id="40" x="635" y="185" w="100" h="120" tags="shelf" />
			<Position id="41" x="745" y="185" w="100" h="120" tags="shelf" />
			<Position id="42" x="635" y="315" w="100" h="50" tags="shelf" />
			<Position id="43" x="745" y="315" w="100" h="50" tags="shelf" />
			<Position id="44" x="635" y="380" w="100" h="50" tags="shelf" />
			<Position id="45" x="745" y="380" w="100" h="50" tags="shelf" />
			<Position id="46" x="635" y="440" w="100" h="50" tags="shelf" />
			<Position id="47" x="745" y="440" w="100" h="50" tags="shelf" />
			<Position id="48" x="910" y="315" w="100" h="150" tags="table" />
			<Position id="49" x="210" y="350" w="120" h="150" tags="table" />
			<Position id="54" x="980" y="470" w="120" h="130" tags="floor" />
			<Position id="55" x="90" y="460" w="120" h="130" tags="floor" />
		</Positions>
		<CoinPositions>
			<Position id="50" x="50" y="515" w="35" h="35" tags="coin" />
			<Position id="51" x="250" y="560" w="35" h="35" tags="coin" />
			<Position id="52" x="380" y="465" w="35" h="35" tags="coin" />
			<Position id="53" x="903" y="478" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="21" img="exercises/img/LostItemsExercise/items/vase.png" w="100" h="120" tag="table" name="<?php echo _('el florero'); ?>" />
			<Item id="22" img="exercises/img/LostItemsExercise/items/tv.png" w="100" h="100" tag="table" name="<?php echo _('la televisión'); ?>" />
			<Item id="23" img="exercises/img/LostItemsExercise/items/phone.png" w="40" h="48" tag="shelf,table" name="<?php echo _('el teléfono'); ?>" />
			<Item id="24" img="exercises/img/LostItemsExercise/items/dvd.png" w="120" h="50" tag="shelf" name="<?php echo _('el DVD'); ?>" />
			<Item id="25" img="exercises/img/LostItemsExercise/items/pictureframe.png" w="50" h="50" tag="shelf" name="<?php echo _('la fotografía'); ?>" />
			<Item id="26" img="exercises/img/LostItemsExercise/items/remotecontrol.png" w="50" h="50" tag="shelf" name="<?php echo _('el mando a distancia'); ?>" />
		</Items>
	</Room>
	<Room id="3" name="<?php echo _('Garaje'); ?>" background="exercises/img/LostItemsExercise/rooms/garage.png">
		<Decorations>
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/car.png" x="65" y="248" />
		</Decorations>
		<Positions>
			<Position id="60" x="610" y="160" w="120" h="50" tags="shelf" />
			<Position id="61" x="610" y="230" w="120" h="60" tags="shelf" />
			<Position id="62" x="610" y="300" w="120" h="60" tags="shelf" />
			<Position id="63" x="610" y="380" w="120" h="120" tags="shelf" />
			<Position id="64" x="770" y="160" w="120" h="50" tags="shelf" />
			<Position id="65" x="770" y="230" w="120" h="60" tags="shelf" />
			<Position id="66" x="770" y="300" w="120" h="60" tags="shelf" />
			<Position id="67" x="770" y="380" w="120" h="120" tags="shelf" />
			<Position id="68" x="895" y="470" w="120" h="120" tags="floor" />
		</Positions>
		<CoinPositions>
			<Position id="70" x="65" y="515" w="35" h="35" tags="coin" />
			<Position id="71" x="430" y="530" w="35" h="35" tags="coin" />
			<Position id="72" x="525" y="475" w="35" h="35" tags="coin" />
			<Position id="73" x="940" y="110" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="30" img="exercises/img/LostItemsExercise/items/toolbox.png" w="100" h="100" tag="floor,shelf" name="<?php echo _('la caja de herramientas'); ?>" />
			<Item id="31" img="exercises/img/LostItemsExercise/items/pliers.png" w="120" h="50" tag="shelf" name="<?php echo _('los alicates'); ?>" />
			<Item id="32" img="exercises/img/LostItemsExercise/items/hammer.png" w="100" h="50" tag="shelf" name="<?php echo _('el martillo'); ?>" />
			<Item id="33" img="exercises/img/LostItemsExercise/items/paintbucket.png" w="100" h="100" tag="shelf,floor" name="<?php echo _('el cubo de pintura'); ?>" />
			<Item id="34" img="exercises/img/LostItemsExercise/items/hose.png" w="120" h="100" tag="shelf" name="<?php echo _('la manguera'); ?>" />
			<Item id="35" img="exercises/img/LostItemsExercise/items/tool.png" w="50" h="100" tag="shelf" name="<?php echo _('el gato del coche'); ?>" />
			<Item id="36" img="exercises/img/LostItemsExercise/items/blade.png" w="100" h="50" tag="shelf" name="<?php echo _('la segueta'); ?>" />
			<Item id="37" img="exercises/img/LostItemsExercise/items/brush.png" w="120" h="50" tag="shelf" name="<?php echo _('la brocha'); ?>" />
			<Item id="38" img="exercises/img/LostItemsExercise/items/smallbrush.png" w="120" h="50" tag="shelf" name="<?php echo _('la brocha pequeña'); ?>" />
			<Item id="39" img="exercises/img/LostItemsExercise/items/fuel.png" w="100" h="120" tag="shelf,floor" name="<?php echo _('la gasolina'); ?>" />
		</Items>
	</Room>
	<Room id="4" name="<?php echo _('Dormitorio de los niños'); ?>" background="exercises/img/LostItemsExercise/rooms/bedroomchildren.png">
		<Decorations>
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/childrenbed.png" x="-20" y="338" />
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/childrentable.png" x="757" y="310" />
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/childrenchair.png" x="735" y="396" />
		</Decorations>
		<Positions>
			<Position id="80" x="425" y="167" w="100" h="120" tags="shelf" />
			<Position id="81" x="425" y="308" w="100" h="50" tags="shelf" />
			<Position id="82" x="425" y="378" w="100" h="50" tags="shelf" />
			<Position id="83" x="425" y="440" w="100" h="50" tags="shelf" />
			<Position id="84" x="526" y="167" w="100" h="120" tags="shelf" />
			<Position id="85" x="526" y="308" w="100" h="50" tags="shelf" />
			<Position id="86" x="526" y="378" w="100" h="50" tags="shelf" />
			<Position id="87" x="526" y="440" w="100" h="50" tags="shelf" />
			<Position id="88" x="280" y="310" w="120" h="120" tags="table" />
		</Positions>
		<CoinPositions>
			<Position id="90" x="160" y="555" w="35" h="35" tags="coin" />
			<Position id="91" x="390" y="475" w="35" h="35" tags="coin" />
			<Position id="92" x="690" y="475" w="35" h="35" tags="coin" />
			<Position id="93" x="920" y="500" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="50" img="exercises/img/LostItemsExercise/items/teddybear.png" w="50" h="50" tag="shelf" name="<?php echo _('el osito de peluche'); ?>" />
			<Item id="51" img="exercises/img/LostItemsExercise/items/teddybearbig.png" w="100" h="100" tag="shelf" name="<?php echo _('el oso de peluche'); ?>" />
			<Item id="52" img="exercises/img/LostItemsExercise/items/comic.png" w="100" h="100" tag="shelf" name="<?php echo _('el tebeo'); ?>" />
			<Item id="53" img="exercises/img/LostItemsExercise/items/yoyo.png" w="50" h="50" tag="shelf" name="<?php echo _('el yo-yó'); ?>" />
			<Item id="54" img="exercises/img/LostItemsExercise/items/footballball.png" w="50" h="50" tag="shelf,floor" name="<?php echo _('la pelota de fútbol'); ?>" />
			<Item id="55" img="exercises/img/LostItemsExercise/items/wrist.png" w="25" h="50" tag="shelf" name="<?php echo _('la muñeca'); ?>" />
			<Item id="56" img="exercises/img/LostItemsExercise/items/toy.png" w="100" h="50" tag="shelf" name="<?php echo _('el tren de juguete'); ?>" />
		</Items>
	</Room>
	<Room id="5" name="<?php echo _('Lavadero'); ?>" background="exercises/img/LostItemsExercise/rooms/washingroom.png">
		<Decorations>
			<Decoration img="exercises/img/LostItemsExercise/rooms/decoration/washer.png" x="70" y="280" />
		</Decorations>
		<Positions>
			<Position id="100" x="330" y="165" w="115" h="50" tags="shelf" />
			<Position id="101" x="330" y="235" w="115" h="120" tags="shelf" />
			<Position id="102" x="330" y="375" w="115" h="50" tags="shelf" />
			<Position id="103" x="330" y="445" w="115" h="50" tags="shelf" />
			<Position id="104" x="446" y="165" w="115" h="50" tags="shelf" />
			<Position id="105" x="446" y="235" w="115" h="120" tags="shelf" />
			<Position id="106" x="446" y="375" w="115" h="50" tags="shelf" />
			<Position id="107" x="446" y="446" w="120" h="50" tags="shelf" />
			<Position id="108" x="760" y="270" w="120" h="220" tags="shelf" />
			<Position id="109" x="881" y="270" w="50" h="220" tags="shelf" />
			<Position id="110" x="931" y="270" w="50" h="220" tags="shelf" />
			<Position id="111" x="590" y="310" w="170" h="200" tags="floor" />
		</Positions>
		<CoinPositions>
			<Position id="115" x="280" y="480" w="35" h="35" tags="coin" />
			<Position id="116" x="350" y="109" w="35" h="35" tags="coin" />
			<Position id="117" x="930" y="205" w="35" h="35" tags="coin" />
			<Position id="118" x="50" y="505" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="40" img="exercises/img/LostItemsExercise/items/detergent.png" w="80" h="100" tag="shelf,floor" name="<?php echo _('el detergente'); ?>" />
			<Item id="41" img="exercises/img/LostItemsExercise/items/clothesbox.png" w="100" h="120" tag="floor" name="<?php echo _('la cesta de la ropa'); ?>" />
			<Item id="42" img="exercises/img/LostItemsExercise/items/iron.png" w="100" h="100" tag="shelf" name="<?php echo _('la plancha'); ?>" />
			<Item id="43" img="exercises/img/LostItemsExercise/items/mopbucket.png" w="100" h="100" tag="floor" name="<?php echo _('el cubo de la fregona'); ?>" />
			<Item id="44" img="exercises/img/LostItemsExercise/items/mop.png" w="70" h="180" tag="shelf,floor" name="<?php echo _('la fregona'); ?>" />
			<Item id="45" img="exercises/img/LostItemsExercise/items/broom.png" w="70" h="180" tag="shelf,floor" name="<?php echo _('el cepillo de barrer'); ?>" />
			<Item id="46" img="exercises/img/LostItemsExercise/items/mop2.png" w="78" h="170" tag="floor" name="<?php echo _('la mopa'); ?>" />
			<Item id="47" img="exercises/img/LostItemsExercise/items/bleach.png" w="35" h="90" tag="floor" name="<?php echo _('la lejía'); ?>" />
			<Item id="48" img="exercises/img/LostItemsExercise/items/cleaner.png" w="48" h="90" tag="shelf,floor" name="<?php echo _('el amoniaco'); ?>" />
			<Item id="49" img="exercises/img/LostItemsExercise/items/dustpan.png" w="62" h="130" tag="shelf,floor" name="<?php echo _('el recogedor'); ?>" />
			<Item id="150" img="exercises/img/LostItemsExercise/items/softener.png" w="53" h="90" tag="shelf,floor" name="<?php echo _('el suavizante'); ?>" />
		</Items>
	</Room>
	<Room id="6" name="<?php echo _('Cocina'); ?>" background="exercises/img/LostItemsExercise/rooms/kitchen.png">
		<Decorations>
		</Decorations>
		<Positions>
			<Position id="120" x="78" y="385" w="112" h="136" tags="shelf,floor,wardrobe" />
			<Position id="121" x="310" y="385" w="115" h="70" tags="shelf" />
			<Position id="122" x="310" y="450" w="115" h="70" tags="shelf" />
			<Position id="123" x="435" y="385" w="115" h="70" tags="shelf" />
			<Position id="124" x="435" y="450" w="115" h="70" tags="shelf" />
			<Position id="125" x="676" y="385" w="115" h="70" tags="shelf" />
			<Position id="126" x="676" y="450" w="115" h="70" tags="shelf" />
			<Position id="127" x="791" y="385" w="110" h="70" tags="shelf" />
			<Position id="128" x="791" y="450" w="110" h="70" tags="shelf" />
			<Position id="129" x="759" y="144" w="100" h="50" tags="shelf" />
			<Position id="130" x="860" y="144" w="100" h="50" tags="shelf" />
			<Position id="131" x="759" y="197" w="100" h="75" tags="shelf" />
			<Position id="132" x="860" y="197" w="100" h="75" tags="shelf" />
			<Position id="133" x="759" y="274" w="100" h="70" tags="shelf" />
			<Position id="134" x="860" y="274" w="100" h="70" tags="shelf" />
			<Position id="135" x="125" y="255" w="125" h="125" tags="table" />
			<Position id="136" x="350" y="255" w="125" h="125" tags="table" />
			<Position id="137" x="985" y="340" w="100" h="120" tags="table" />
		</Positions>
		<CoinPositions>
			<Position id="140" x="215" y="270" w="35" h="35" tags="coin" />
			<Position id="141" x="260" y="121" w="35" h="35" tags="coin" />
			<Position id="142" x="935" y="545" w="35" h="35" tags="coin" />
			<Position id="143" x="593" y="546" w="35" h="35" tags="coin" />
		</CoinPositions>
		<Items>
			<Item id="60" img="exercises/img/LostItemsExercise/items/pan.png" w="70" h="48" tag="shelf" name="<?php echo _('la sartén'); ?>" />
			<Item id="61" img="exercises/img/LostItemsExercise/items/pot.png"  w="69" h="43" tag="shelf" name="<?php echo _('la olla'); ?>" />
			<Item id="62" img="exercises/img/LostItemsExercise/items/dipper.png" w="84" h="38" tag="shelf" name="<?php echo _('el cazo'); ?>" />
			<Item id="63" img="exercises/img/LostItemsExercise/items/dish1.png" w="70" h="48" tag="shelf" name="<?php echo _('el plato'); ?>" />
			<Item id="64" img="exercises/img/LostItemsExercise/items/dish2.png" w="67" h="34" tag="shelf" name="<?php echo _('el plato llano'); ?>" />
			<Item id="65" img="exercises/img/LostItemsExercise/items/fork.png" w="93" h="16" tag="shelf" name="<?php echo _('el tenedor'); ?>" />
			<Item id="66" img="exercises/img/LostItemsExercise/items/knife.png" w="91" h="11" tag="shelf" name="<?php echo _('el cuchillo'); ?>" />
			<Item id="67" img="exercises/img/LostItemsExercise/items/spoon.png" w="77" h="17" tag="shelf" name="<?php echo _('la cuchara'); ?>" />
			<Item id="68" img="exercises/img/LostItemsExercise/items/cup.png" w="51" h="48" tag="shelf" name="<?php echo _('la taza'); ?>" />
			<Item id="69" img="exercises/img/LostItemsExercise/items/microwave.png" w="113" h="75" tag="table" name="<?php echo _('el microondas'); ?>" />
			<Item id="70" img="exercises/img/LostItemsExercise/items/teapot.png" w="90" h="90" tag="table" name="<?php echo _('la tetera'); ?>" />
		</Items>
	</Room>
</LostItemsExercise>
