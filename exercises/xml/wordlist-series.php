<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<WordListExercises>
	<WordListExercise id="0">
		<Elements>
			<Element value="<?php echo _('Buitre'); ?>" />
			<Element value="<?php echo _('Clavel'); ?>" />
			<Element value="<?php echo _('Licor'); ?>" />
			<Element value="<?php echo _('Silla'); ?>" />
			<Element value="<?php echo _('Orqu�dea'); ?>" />
			<Element value="<?php echo _('�guila'); ?>" />
			<Element value="<?php echo _('L�mpara'); ?>" />
			<Element value="<?php echo _('An�s'); ?>" />
			<Element value="<?php echo _('Pavo'); ?>" />
			<Element value="<?php echo _('Armario'); ?>" />
			<Element value="<?php echo _('Jazm�n'); ?>" />
			<Element value="<?php echo _('Co�ac'); ?>" />
		</Elements>
	</WordListExercise>
	<WordListExercise id="1">
		<Elements>
			<Element value="<?php echo _('Falda'); ?>" />
			<Element value="<?php echo _('Huevos'); ?>" />
			<Element value="<?php echo _('Mesita'); ?>" />
			<Element value="<?php echo _('Cuervo'); ?>" />
			<Element value="<?php echo _('Queso'); ?>" />
			<Element value="<?php echo _('Calcet�n'); ?>" />
			<Element value="<?php echo _('B�ho'); ?>" />
			<Element value="<?php echo _('Vitrina'); ?>" />
			<Element value="<?php echo _('Pantal�n'); ?>" />
			<Element value="<?php echo _('Garza'); ?>" />
			<Element value="<?php echo _('Arroz'); ?>" />
			<Element value="<?php echo _('C�moda'); ?>" />
		</Elements>
	</WordListExercise>
	<WordListExercise id="2">
		<Elements>
			<Element value="<?php echo _('Gaviota'); ?>" />
			<Element value="<?php echo _('Azucena'); ?>" />
			<Element value="<?php echo _('Leche'); ?>" />
			<Element value="<?php echo _('Sof�'); ?>" />
			<Element value="<?php echo _('Lirio'); ?>" />
			<Element value="<?php echo _('Pato'); ?>" />
			<Element value="<?php echo _('Cuna'); ?>" />
			<Element value="<?php echo _('Zumo'); ?>" />
			<Element value="<?php echo _('Loro'); ?>" />
			<Element value="<?php echo _('Sill�n'); ?>" />
			<Element value="<?php echo _('Tulip�n'); ?>" />
			<Element value="<?php echo _('Infusi�n'); ?>" />
		</Elements>
	</WordListExercise>
	<WordListExercise id="3">
		<Elements>
			<Element value="<?php echo _('Tenedor'); ?>" />
			<Element value="<?php echo _('Librer�a'); ?>" />
			<Element value="<?php echo _('Halc�n'); ?>" />
			<Element value="<?php echo _('Piano'); ?>" />
			<Element value="<?php echo _('Aparador'); ?>" />
			<Element value="<?php echo _('Taza'); ?>" />
			<Element value="<?php echo _('Viol�n'); ?>" />
			<Element value="<?php echo _('Cisne'); ?>" />
			<Element value="<?php echo _('Sart�n'); ?>" />
			<Element value="<?php echo _('Guitarra'); ?>" />
			<Element value="<?php echo _('Butaca'); ?>" />
			<Element value="<?php echo _('Perdiz'); ?>" />
		</Elements>
	</WordListExercise>
	<WordListExercise id="randomWords">
		<Elements>
			<Element value="<?php echo _('Tomate'); ?>" />
			<Element value="<?php echo _('Pelota'); ?>" />
			<Element value="<?php echo _('Elemento'); ?>" />
			<Element value="<?php echo _('Dinosaurio'); ?>" />
			<Element value="<?php echo _('Autob�s'); ?>" />
			<Element value="<?php echo _('Electricidad'); ?>" />
			<Element value="<?php echo _('Conejo'); ?>" />
			<Element value="<?php echo _('Factura'); ?>" />
			<Element value="<?php echo _('Compra'); ?>" />
			<Element value="<?php echo _('Farmacia'); ?>" />
			<Element value="<?php echo _('M�dico'); ?>" />
			<Element value="<?php echo _('Paseo'); ?>" />
			<Element value="<?php echo _('Vecina'); ?>" />
			<Element value="<?php echo _('Cumplea�os'); ?>" />
			<Element value="<?php echo _('Horario'); ?>" />
			<Element value="<?php echo _('Hija'); ?>" />
			<Element value="<?php echo _('Caseta'); ?>" />
			<Element value="<?php echo _('Teclado'); ?>" />
			<Element value="<?php echo _('Macetas'); ?>" />
			<Element value="<?php echo _('Pescader�a'); ?>" />
			<Element value="<?php echo _('Comprar'); ?>" />
			<Element value="<?php echo _('Aguador'); ?>" />
			<Element value="<?php echo _('Libro'); ?>" />
			<Element value="<?php echo _('Pasatiempos'); ?>" />
			<Element value="<?php echo _('Telediario'); ?>" />
			<Element value="<?php echo _('Carta'); ?>" />
			<Element value="<?php echo _('Taladro'); ?>" />
			<Element value="<?php echo _('Comida'); ?>" />
			<Element value="<?php echo _('Peri�dico'); ?>" />
			<Element value="<?php echo _('Botella'); ?>" />
			<Element value="<?php echo _('Jardines'); ?>" />
			<Element value="<?php echo _('Memoria'); ?>" />
			<Element value="<?php echo _('Gimnasia'); ?>" />
			<Element value="<?php echo _('Viajero'); ?>" />
			<Element value="<?php echo _('Lavadora'); ?>" />
			<Element value="<?php echo _('Dinero'); ?>" />
			<Element value="<?php echo _('Jirafa'); ?>" />
			<Element value="<?php echo _('S�bana'); ?>" />
			<Element value="<?php echo _('Pepino'); ?>" />
			<Element value="<?php echo _('Ternera'); ?>" />
		</Elements>
	</WordListExercise>
</WordListExercises>

