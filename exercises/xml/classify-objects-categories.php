<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<ClassifyObjects>
	<Category id="0" name="<?php echo _('Comidas'); ?>" folder="exercises/img/ClassifyObjectsExercise/comida/">
		<Item id="0" img="2316.png" />
		<Item id="1" img="2494.png" />
		<Item id="2" img="2838.png" />
		<Item id="3" img="2865.png" />
		<Item id="4" img="6646.png" />
		<Item id="5" img="6918.png" />
		<Item id="6" img="7000.png" />
		<Item id="7" img="7130.png" />
		<Item id="8" img="tomatefrito.png" />
		<Item id="9" img="2446.png" />
		<Item id="10" img="2594.png" />
		<Item id="11" img="2839.png" />
		<Item id="12" img="6244.png" />
		<Item id="13" img="6911.png" />
		<Item id="14" img="6976.png" />
		<Item id="15" img="7001.png" />
		<Item id="16" img="8600.png" />
	</Category>
	<Category id="1" name="<?php echo _('Partes del cuerpo'); ?>" folder="exercises/img/ClassifyObjectsExercise/cuerpo/">
		<Item id="0" img="12218.png" />
		<Item id="1" img="2663.png" />
		<Item id="2" img="2669.png" />
		<Item id="3" img="2841.png" />
		<Item id="4" img="2851.png" />
		<Item id="5" img="2871.png" />
		<Item id="6" img="2887.png" />
		<Item id="7" img="2928.png" />
		<Item id="8" img="3298.png" />
		<Item id="9" img="6573.png" />
		<Item id="10" img="8666.png" />
		<Item id="11" img="cuello.png" />
	</Category>
	<Category id="2" name="<?php echo _('Herramientas'); ?>" folder="exercises/img/ClassifyObjectsExercise/herramientas/">
		<Item id="0" img="2706.png" />
		<Item id="1" img="2788.png" />
		<Item id="2" img="2837.png" />
		<Item id="3" img="2937.png" />
		<Item id="4" img="3392.png" />
		<Item id="5" img="5586.png" />
		<Item id="6" img="brocha.png" />
		<Item id="7" img="sierra.png" />
		<Item id="8" img="2736.png" />
		<Item id="9" img="2809.png" />
		<Item id="10" img="2922.png" />
		<Item id="11" img="2938.png" />
		<Item id="12" img="5499.png" />
		<Item id="13" img="alicates.png" />
		<Item id="14" img="clavo.png" />
	</Category>
	<Category id="3" name="<?php echo _('Transporte'); ?>" folder="exercises/img/ClassifyObjectsExercise/transporte/">
		<Item id="0" img="2263.png" />
		<Item id="1" img="2264.png" />
		<Item id="2" img="2273.png" />
		<Item id="3" img="2277.png" />
		<Item id="4" img="2339.png" />
		<Item id="5" img="2407.png" />
		<Item id="6" img="2506.png" />
		<Item id="7" img="2508.png" />
		<Item id="8" img="monopatin.png" />
		<Item id="9" img="parapente.png" />
		<Item id="10" img="taxi.png" />
		<Item id="11" img="tren.png" />
	</Category>
	<Category id="4" name="<?php echo _('Deportes'); ?>" folder="exercises/img/ClassifyObjectsExercise/deportes/">
		<Item id="0" img="10159.png" />
		<Item id="1" img="2889.png" />
		<Item id="2" img="3097.png" />
		<Item id="3" img="3260.png" />
		<Item id="4" img="5917.png" />
		<Item id="5" img="8544.png" />
		<Item id="6" img="9189.png" />
		<Item id="7" img="9699.png" />
		<Item id="8" img="10167.png" />
		<Item id="9" img="3090.png" />
		<Item id="10" img="3199.png" />
		<Item id="11" img="5398.png" />
		<Item id="12" img="6513.png" />
		<Item id="13" img="8591.png" />
		<Item id="14" img="9190.png" />
	</Category>
	<Category id="5" name="<?php echo _('Frutas'); ?>" folder="exercises/img/ClassifyObjectsExercise/fruta/">
		<Item id="0" img="13644.png" />
		<Item id="1" img="2400.png" />
		<Item id="2" img="2483.png" />
		<Item id="3" img="2955.png" />
		<Item id="4" img="ciruela.png" />
		<Item id="5" img="uvas.png" />
		<Item id="6" img="13645.png" />
		<Item id="7" img="2462.png" />
		<Item id="8" img="2525.png" />
		<Item id="9" img="2983.png" />
		<Item id="10" img="mandarina.png" />
		<Item id="11" img="2329.png" />
		<Item id="12" img="2468.png" />
		<Item id="13" img="2561.png" />
		<Item id="14" img="3022.png" />
		<Item id="15" img="platano.png" />
	</Category>
	<Category id="6" name="<?php echo _('Instrumentos'); ?>" folder="exercises/img/ClassifyObjectsExercise/instrumentos/">
		<Item id="0" img="11121.png" />
		<Item id="1" img="17332.png" />
		<Item id="2" img="2495.png" />
		<Item id="3" img="2604.png" />
		<Item id="4" img="5974.png" />
		<Item id="5" img="8315.png" />
		<Item id="6" img="8599.png" />
		<Item id="7" img="12088.png" />
		<Item id="8" img="2318.png" />
		<Item id="9" img="2521.png" />
		<Item id="10" img="2607.png" />
		<Item id="11" img="6234.png" />
		<Item id="12" img="8340.png" />
		<Item id="13" img="12119.png" />
		<Item id="14" img="2396.png" />
		<Item id="15" img="2559.png" />
		<Item id="16" img="2615.png" />
		<Item id="17" img="6235.png" />
		<Item id="18" img="8341.png" />
		<Item id="19" img="12150.png" />
		<Item id="20" img="2417.png" />
		<Item id="21" img="2578.png" />
		<Item id="22" img="5909.png" />
		<Item id="23" img="6249.png" />
		<Item id="24" img="8493.png" />
	</Category>
	<Category id="7" name="<?php echo _('Muebles'); ?>" folder="exercises/img/ClassifyObjectsExercise/muebles/">
		<Item id="0" img="11376.png" />
		<Item id="1" img="2304.png" />
		<Item id="2" img="2570.png" />
		<Item id="3" img="2917.png" />
		<Item id="4" img="4960.png" />
		<Item id="5" img="lampara.png" />
		<Item id="6" img="silla.png" />
		<Item id="7" img="2258.png" />
		<Item id="8" img="2360.png" />
		<Item id="9" img="2571.png" />
		<Item id="10" img="4937.png" />
		<Item id="11" img="escritorio.png" />
		<Item id="12" img="pupitre.png" />
	</Category>
	<Category id="8" name="<?php echo _('Profesiones'); ?>" folder="exercises/img/ClassifyObjectsExercise/profesiones/">
		<Item id="0" img="11165.png" />
		<Item id="1" img="11342.png" />
		<Item id="2" img="2636.png" />
		<Item id="3" img="2740.png" />
		<Item id="4" img="3008.png" />
		<Item id="5" img="5603.png" />
		<Item id="6" img="11265.png" />
		<Item id="7" img="2457.png" />
		<Item id="8" img="2642.png" />
		<Item id="9" img="2921.png" />
		<Item id="10" img="3321.png" />
		<Item id="11" img="8631.png" />
		<Item id="12" img="11341.png" />
		<Item id="13" img="2467.png" />
		<Item id="14" img="2733.png" />
		<Item id="15" img="2969.png" />
		<Item id="16" img="3358.png" />
		<Item id="17" img="9196.png" />
	</Category>
	<Category id="9" name="<?php echo _('Ropa'); ?>" folder="exercises/img/ClassifyObjectsExercise/ropa/">
		<Item id="0" img="11402.png" />
		<Item id="0" img="2308.png" />
		<Item id="0" img="2391.png" />
		<Item id="0" img="2622.png" />
		<Item id="0" img="4872.png" />
		<Item id="0" img="12176.png" />
		<Item id="0" img="2309.png" />
		<Item id="0" img="2565.png" />
		<Item id="0" img="2775.png" />
		<Item id="0" img="7123.png" />
		<Item id="0" img="16585.png" />
		<Item id="0" img="2310.png" />
		<Item id="0" img="2613.png" />
		<Item id="0" img="3296.png" />
		<Item id="0" img="8353.png" />
	</Category>
</ClassifyObjects>
