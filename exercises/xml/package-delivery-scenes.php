<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<Scenes>
	<Scene id="0">
		<Places ids="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15" />
		<Paths>
			<Path from="0" to="4" />
			<Path from="1" to="2,4,5" />
			<Path from="2" to="1,6,7" />
			<Path from="3" to="7" />
			<Path from="4" to="0,1,8,9" />
			<Path from="5" to="1" />
			<Path from="6" to="2,9,11" />
			<Path from="7" to="2,3" />
			<Path from="8" to="4,12" />
			<Path from="9" to="4,6,12,13" />
			<Path from="10" to="11" />
			<Path from="11" to="6,10,14,15" />
			<Path from="12" to="8,9" />
			<Path from="13" to="9,11,14" />
			<Path from="14" to="11,13" />
			<Path from="15" to="11" />
		</Paths>
		<Boxes>
			<Box id="0" name="<?php echo _('Paquete rojo'); ?>" inshop="1" initialplace="11" goalplace="-1" img="exercises/img/PackageDeliveryExercise/boxred.png" />
			<Box id="1" name="<?php echo _('Paquete azul'); ?>" inshop="1" initialplace="6" goalplace="-1" img="exercises/img/PackageDeliveryExercise/boxblue.png" />
			<Box id="2" name="<?php echo _('Paquete marrón'); ?>" inshop="1" initialplace="0" goalplace="-1" img="exercises/img/PackageDeliveryExercise/boxbrown.png" />
			<Box id="3" name="<?php echo _('Paquete amarillo'); ?>" inshop="0" initialplace="-1" goalplace="2" img="exercises/img/PackageDeliveryExercise/boxyellow.png" />
			<Box id="4" name="<?php echo _('Paquete negro'); ?>" inshop="0" initialplace="-1" goalplace="12" img="exercises/img/PackageDeliveryExercise/boxblack.png" />
			<Box id="5" name="<?php echo _('Paquete blanco'); ?>" inshop="0" initialplace="-1" goalplace="1" img="exercises/img/PackageDeliveryExercise/boxwhite.png" />
		</Boxes>
	</Scene>
	<Scene id="1">
		<Places ids="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15" />
		<Paths>
			<Path from="0" to="4" />
			<Path from="1" to="2,4,5" />
			<Path from="2" to="1,6,7" />
			<Path from="3" to="7" />
			<Path from="4" to="0,1,8,9" />
			<Path from="5" to="1" />
			<Path from="6" to="2,9,11" />
			<Path from="7" to="2,3" />
			<Path from="8" to="4,12" />
			<Path from="9" to="4,6,12,13" />
			<Path from="10" to="11" />
			<Path from="11" to="6,10,14,15" />
			<Path from="12" to="8,9" />
			<Path from="13" to="9,11,14" />
			<Path from="14" to="11,13" />
			<Path from="15" to="11" />
		</Paths>
		<Boxes>
			<Box id="0" name="<?php echo _('Paquete rojo'); ?>" inshop="1" initialplace="8" goalplace="-1" img="exercises/img/PackageDeliveryExercise/boxred.png" />
			<Box id="1" name="<?php echo _('Paquete azul'); ?>" inshop="1" initialplace="10" goalplace="-1" img="exercises/img/PackageDeliveryExercise/boxblue.png" />
			<Box id="2" name="<?php echo _('Paquete marrón'); ?>" inshop="1" initialplace="6" goalplace="-1" img="exercises/img/PackageDeliveryExercise/boxbrown.png" />
			<Box id="3" name="<?php echo _('Paquete amarillo'); ?>" inshop="0" initialplace="-1" goalplace="12" img="exercises/img/PackageDeliveryExercise/boxyellow.png" />
			<Box id="4" name="<?php echo _('Paquete negro'); ?>" inshop="0" initialplace="-1" goalplace="5" img="exercises/img/PackageDeliveryExercise/boxblack.png" />
			<Box id="5" name="<?php echo _('Paquete blanco'); ?>" inshop="0" initialplace="-1" goalplace="1" img="exercises/img/PackageDeliveryExercise/boxwhite.png" />
		</Boxes>
	</Scene>
</Scenes>
