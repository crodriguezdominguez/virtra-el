<?php
	header('Content-type: text/xml; charset=ISO-8859-1');
	require_once('locale/localization.php');
?>

<TaskListExercises>
	<TaskListExercise id="0">
		<Elements>
			<Element value="<?php echo _('Ir al banco'); ?>" />
			<Element value="<?php echo _('Dar de comer a los peces'); ?>" />
			<Element value="<?php echo _('Recoger un paquete en correos'); ?>" />
			<Element value="<?php echo _('Ir a la farmacia'); ?>" />
			<Element value="<?php echo _('Ir al médico'); ?>" />
			<Element value="<?php echo _('Recoger ropa de la tintorería'); ?>" />
			<Element value="<?php echo _('Lavar el coche'); ?>" />
			<Element value="<?php echo _('Limpiar la casa'); ?>" />
			<Element value="<?php echo _('Llevar el regalo al nieto'); ?>" />
			<Element value="<?php echo _('Poner la mesa'); ?>" />
			<Element value="<?php echo _('Salir a andar durante una hora'); ?>" />
			<Element value="<?php echo _('Ir a comer a casa de mi hija'); ?>" />
			<Element value="<?php echo _('Ir a desayunar a casa de la vecina'); ?>" />
			<Element value="<?php echo _('Regar las macetas'); ?>" />
			<Element value="<?php echo _('Comprar en la pescadería'); ?>" />
		</Elements>
	</TaskListExercise>
	<TaskListExercise id="1">
		<Elements>
			<Element value="<?php echo _('Comprar el periódico'); ?>" />
			<Element value="<?php echo _('Arreglar el jardín'); ?>" />
			<Element value="<?php echo _('Apuntarse a un viaje'); ?>" />
			<Element value="<?php echo _('Visitar a mi mejor amiga'); ?>" />
			<Element value="<?php echo _('Devolver dinero a tu hermano'); ?>" />
			<Element value="<?php echo _('Llevar el regalo al nieto'); ?>" />
			<Element value="<?php echo _('Ir al banco'); ?>" />
			<Element value="<?php echo _('Dar de comer a los peces'); ?>" />
			<Element value="<?php echo _('Recoger un paquete en correos'); ?>" />
			<Element value="<?php echo _('Ir a la farmacia'); ?>" />
			<Element value="<?php echo _('Ir al médico'); ?>" />
			<Element value="<?php echo _('Poner la mesa'); ?>" />
			<Element value="<?php echo _('Llamar a la vecina'); ?>" />
			<Element value="<?php echo _('Felicitar a la prima'); ?>" />
			<Element value="<?php echo _('Salir a andar durante una hora'); ?>" />

		</Elements>
	</TaskListExercise>
	<TaskListExercise id="2">
		<Elements>
			<Element value="<?php echo _('Sacar al perro'); ?>" />
			<Element value="<?php echo _('Recoger ropa de la tintorería'); ?>" />
			<Element value="<?php echo _('Lavar el coche'); ?>" />
			<Element value="<?php echo _('Limpiar la casa'); ?>" />
			<Element value="<?php echo _('Llevar el regalo al nieto'); ?>" />
			<Element value="<?php echo _('Comprar el pan'); ?>" />
			<Element value="<?php echo _('Hacer sopa de letras o pasatiempos'); ?>" />
			<Element value="<?php echo _('Dar de comer a los peces'); ?>" />
			<Element value="<?php echo _('Recoger un paquete en correos'); ?>" />
			<Element value="<?php echo _('Visitar a mi mejor amiga'); ?>" />
			<Element value="<?php echo _('Devolver dinero a tu hermano'); ?>" />
			<Element value="<?php echo _('Poner la mesa'); ?>" />
			<Element value="<?php echo _('Llamar a la vecina'); ?>" />
			<Element value="<?php echo _('Ir a gimnasia'); ?>" />
			<Element value="<?php echo _('Apuntarse a un viaje'); ?>" />
		</Elements>
	</TaskListExercise>
	<TaskListExercise id="randomWords">
		<Elements>
			<Element value="<?php echo _('Sacar al perro'); ?>" />
			<Element value="<?php echo _('Comprar el pan'); ?>" />
			<Element value="<?php echo _('Recoger ropa de la tintorería'); ?>" />
			<Element value="<?php echo _('Lavar el coche'); ?>" />
			<Element value="<?php echo _('Limpiar la casa'); ?>" />
			<Element value="<?php echo _('Llevar el regalo al nieto'); ?>" />
			<Element value="<?php echo _('Ir al banco'); ?>" />
			<Element value="<?php echo _('Dar de comer a los peces'); ?>" />
			<Element value="<?php echo _('Recoger un paquete en correos'); ?>" />
			<Element value="<?php echo _('Ir a la farmacia'); ?>" />
			<Element value="<?php echo _('Ir al médico'); ?>" />
			<Element value="<?php echo _('Poner la mesa'); ?>" />
			<Element value="<?php echo _('Llamar a la vecina'); ?>" />
			<Element value="<?php echo _('Felicitar a la prima'); ?>" />
			<Element value="<?php echo _('Salir a andar durante una hora'); ?>" />
			<Element value="<?php echo _('Ir a comer a casa de mi hija'); ?>" />
			<Element value="<?php echo _('Ir a desayunar a casa de la vecina'); ?>" />
			<Element value="<?php echo _('Regar las macetas'); ?>" />
			<Element value="<?php echo _('Comprar en la pescadería'); ?>" />
			<Element value="<?php echo _('Comprar en la frutería'); ?>" />
			<Element value="<?php echo _('Comprar un libro'); ?>" />
			<Element value="<?php echo _('Hacer sopa de letras o pasatiempos'); ?>" />
			<Element value="<?php echo _('Ver el telediario'); ?>" />
			<Element value="<?php echo _('Jugar a las cartas'); ?>" />
			<Element value="<?php echo _('Hacer la comida'); ?>" />
			<Element value="<?php echo _('Leer el periódico'); ?>" />
			<Element value="<?php echo _('Comprar el periódico'); ?>" />
			<Element value="<?php echo _('Arreglar el jardín'); ?>" />
			<Element value="<?php echo _('Ir al taller de memoria'); ?>" />
			<Element value="<?php echo _('Pagar la factura del agua'); ?>" />
			<Element value="<?php echo _('Ir a gimnasia'); ?>" />
			<Element value="<?php echo _('Apuntarse a un viaje'); ?>" />
			<Element value="<?php echo _('Visitar a mi mejor amiga'); ?>" />
			<Element value="<?php echo _('Devolver dinero a tu hermano'); ?>" />
		</Elements>
	</TaskListExercise>
</TaskListExercises>

