<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!SpatialReasoningExercise) {

var SpatialReasoningExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Piezas de Puzzle!'); ?>', '<?php echo _('En este ejercicio vamos a trabajar tu razonamiento a través de fotografías. Para ello, junto con cada fotografía te mostraré varios trozos que son parte de la fotografía y otros que no pertenecen a ella, como si fuese una especie de puzzle. Deberás pulsar sobre aquellos que NO forman parte de la imagen, y luego darle al botón <strong>Listo</strong>.  Cuando estés preparado pulsa <strong>Continuar</strong>.'); ?>', 'exercises/SpatialReasoningExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(cor) {
		this._corrects = cor;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = this._corrects;
		var countFails = this._fails;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_reasoning_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		else return {medal: false, type:0};
	}
});

}
</script>
