<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript" src="js/jquery.path.js"></script>

<div id="introExercise">
	<a id="introButton" href="javascript:void(0);" class="btn btn-primary btn-large" style="margin-bottom:20px;"><?php echo _('Continuar'); ?></a>
</div>

<div id="balloons">
	<p id="speedP" class="lead hide"><strong><?php echo _('Nivel'); ?>: </strong><span id="level"></span>&nbsp;&nbsp;<strong><?php echo _('Velocidad'); ?>: </strong><span id="speed"></span></p>
	<div id="balloonsContainer" tabindex="0">
		<div id="balloon">A</div>
	</div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var level = 1;
var sublevel = 1;
var currentIntroIndex = 0;
var clicked = false;
var demoCorrects = 0;
var consecutiveNumber = 0;
var backgroundBalloonsContainer = $('#balloonsContainer').css('background-color');
var MAX_BALLONS = 40;
var countBalloons = 0;
var lastLetters = [];

var SineWave = function() {
  this.css = function(p) {
    var s = Math.sin(p*10);
    var x = 500 - p * 500;
    var y = s * 10 + 100;
    return {top: y + "px", left: x + "px"};
  };
};

function randomLetter() {
	var letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
	if (lastLetters.length > level)
	{
		if (getRandom()<0.4) //a 40% percent that a correct result appears
		{
			var letter = lastLetters[lastLetters.length-level];
			lastLetters.push(letter);
			return letter.toUpperCase();
		}
	}
	
	letters.sort(function(a,b){
		return 0.5-getRandom();
	});
	
	lastLetters.push(letters[0]);
	
	return letters[0].toUpperCase();
};

function animateBallon() {
	clicked = false;	
	var currentLetter = randomLetter();
	var end = '450px';
	var time = 3000-((sublevel-1)*500);
	if (time < 1000) time = 1000;
	$("#balloon").css('left', '-10px');
	$("#balloon").html(currentLetter);
	$("#balloon").stop().animate({path: new SineWave}, time, 'linear', function(){
		countBalloons++;
		if (lastLetters.length > level)
		{
			if (currentLetter.toUpperCase() == lastLetters[lastLetters.length-level-1].toUpperCase())
			{
				if (clicked)
				{
					demoCorrects++;
					var substraction = 3-demoCorrects;
					if (substraction == 1) $("#exercise-description").html("<?php echo _('<strong>¡Muy bien!</strong> Ya solo te queda 1 prueba.'); ?>");
					else if (substraction>0) $("#exercise-description").html("<?php echo _('<strong>¡Muy bien!</strong> Ya solo te quedan'); ?> "+substraction+" <?php echo _('pruebas'); ?>.");
				}
			}
			else if (clicked)
			{
				if (level == 1)
				{
					$("#exercise-description").html("<?php echo _('<strong>Recuerda: </strong> Pulsa en el cuadrado azul cuando veas dos letras iguales seguidas (o pulsa espacio). Ten en cuenta que la letra de un globo puede servir para secuencias diferentes.'); ?>");
				}
				else
				{
					$("#exercise-description").html("<?php echo _('<strong>Recuerda: </strong> Pulsa en el cuadrado azul cuando se repita la letra del globo que apareció en los 2 lugares anteriores al actual, es decir, dejando 1 globo entre dos globos con la misma letra. Por ejemplo, debes pulsar cuando aparezca una secuencia como: A-B-A.'); ?>");
				}
			}
			
			if (demoCorrects >= 3) nextIntro();
		}
		
		$("#balloonsContainer").css('background-color', backgroundBalloonsContainer);
		$("#level").html(level);
		$("#speed").html(sublevel);
		
		if (demoCorrects<3) animateBallon();
	});
};

function nextIntro(){
	$("#exercise-description").fadeOut('slow', function(){
		switch(currentIntroIndex)
		{
			case 0:
				$("#exercise-description").html("<?php echo _('Vamos a practicar un poco antes de comenzar para comprobar que lo has entendido. Pulsa <strong>Continuar</strong>.'); ?>");
				break;
			case 1:
				$("#exercise-description").html("<?php echo _('Recuerda que debes pulsar el recuadro azul, o la tecla espacio, cuando veas dos globos seguidos con la misma letra. Ten en cuenta que la letra de un globo puede servir para secuencias diferentes.'); ?>");
				lastLetters = [];
				$("#introButton").hide();
				animateBallon();
				$("#balloonsContainer").bind('click', checkExercise);
				$(document).keypress(function(e){
					var key = e.which || e.keyCode;
					if (key == 13 || key == 32) //enter or space
					{
						checkExercise();
					}
				});
				break;
			/*case 2:
				$("#exercise-description").html("<?php echo _('Ahora vamos a aumentar al nivel de dificultad 2. Pulsa en el cuadrado azul cuando se repita la letra del globo que apareció en los 2 lugares anteriores al actual, es decir, dejando 1 globo entre dos globos con la misma letra. Por ejemplo, debes pulsar cuando aparezca una secuencia como: A-B-A.'); ?>");
				level = 2;
				lastLetters = [];
				demoCorrects = 0;
				$("#introButton").hide();
				animateBallon();
				break;*/
			case 2:
				$("#introButton").html("<?php echo _('Comenzar ejercicio'); ?>");
				$("#exercise-description").html("<?php echo _('<strong>¡Muy bien, has superado la prueba!</strong> Ahora comenzaremos el ejercicio. Cuando estés listo pulsa en <strong>Comenzar ejercicio</strong>.'); ?>");
				/*$("#exercise-description").html("<?php echo _('<strong>¡Muy bien, has superado la prueba!</strong> Ahora comenzaremos el ejercicio. Ten en cuenta que según el nivel de dificultad, podrá haber globos intermedios entre dos globos repetidos. Por ejemplo, en el nivel 3, deberás pulsar cuando aparezca un globo repetido en los 3 lugares anteriores al actual, es decir, dejando 2 globos intermedios entre dos globos con la misma letra. Por ejemplo, en dicho nivel, deberás pulsar en secuencias como: A-Z-Y-A. Cuando estés listo pulsa en <strong>Continuar</strong>.'); ?>");*/
				$("#balloonsContainer").unbind('click');
				$(document).unbind('keypress');
				$("#introButton").show();
				break;
			default:
				$("#exercise-description").html(exercise.description());
				endDemo();
				return; //avoid next operations
		}
		$("#exercise-description").fadeIn('slow');
		
		currentIntroIndex++;
	});
};

function checkExercise(){
	clicked = true;
	$("#balloonsContainer").css('background-color', '#cccccc');
};


$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	$("#balloonsContainer").unbind('click');
	$(document).unbind('keypress');
	
	$('#introButton').bind('click', nextIntro);
});

</script>
