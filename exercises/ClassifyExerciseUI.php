<?php
	require_once('locale/localization.php');
?>

<div id="introExercise" class="hide">
	<!--<p class="lead"><?php echo _('Cuando comiences el ejercicio, pulsa el dibujo que no encaje con el resto. A continuación, pulsa el botón <strong>Listo</strong>. Dispones de un botón <strong>Ayuda</strong>, por si necesitas alguna pista para completar el ejercicio.'); ?></p>-->
	<a href="javascript:void(0);" onclick="javascript:showSemanticExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="semanticExercise" class="hide">
	<ul class="thumbnails">
		<li id="li-answer0" class="span2 hide"><a id="answer0" href="javascript:void(0);" onclick="javascript:checkAnswer(0);" class="thumbnail logical-exercise-element"></a></li>
		<li id="li-answer1" class="span2 hide"><a id="answer1" href="javascript:void(0);" onclick="javascript:checkAnswer(1);" class="thumbnail logical-exercise-element"></a></li>
		<li id="li-answer2" class="span2 hide"><a id="answer2" href="javascript:void(0);" onclick="javascript:checkAnswer(2);" class="thumbnail logical-exercise-element"></a></li>
		<li id="li-answer3" class="span2 hide"><a id="answer3" href="javascript:void(0);" onclick="javascript:checkAnswer(3);" class="thumbnail logical-exercise-element"></a></li>
		<li id="li-answer4" class="span2 hide"><a id="answer4" href="javascript:void(0);" onclick="javascript:checkAnswer(4);" class="thumbnail logical-exercise-element"></a></li>
	</ul>
	<a class="btn btn-large btn-info" href="javascript:void(0);" onclick="javascript:showHelp();" id="help-button"><i class="icon-question-sign"></i> <?php echo _('Ayuda'); ?></a>&nbsp;&nbsp;<span class="lead" id="exercise-explanation"></span>
	<div style="margin-top:40px;margin-bottom:40px;"><a class="btn btn-large btn-primary hide" href="javascript:void(0);" onclick="javascript:checkExercise();" id="continue-button"><?php echo _('Listo'); ?></a></div>
	<div style="margin-top:40px;margin-bottom:40px;"><a class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:endExercise();" id="end-button"><?php echo _('Continuar'); ?></a></div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var classifyExercise = null;
var answer = -1;
var difficulty = 0;

function exercisesWithCurrentDifficulty(classifyExercises) {
	var result = [];
	$.each(classifyExercises.ClassifyExercise, function(index, item){
		if (parseInt(item.difficulty)==difficulty) result.push(item);
	});
	return result;
};

function allExercises(classifyExercises) {
	var result = [];
	$.each(classifyExercises.ClassifyExercise, function(index, item){
		result.push(item);
	});
	return result;
};

function showHelp() {
	$("#exercise-explanation").html("<strong><?php echo _('Pista') ?>:</strong> "+classifyExercise.explanation);
};

function showSemanticExercise(){
	$('#introExercise').fadeOut('slow', function(evt){
		showMiniMenu(true);
		$('#semanticExercise').fadeIn('slow', function(){
			$('#exercise-title').html("<?php echo _('&iquest;Qu&eacute; imagen no encaja?'); ?>");
			$('#exercise-description').html("<?php echo _('Selecciona la imagen que no encaje y pulsa <strong>Listo</strong>.'); ?>");
		});
	});
};

function checkAnswer(num) {
	answer = num;
	
	$("#continue-button").fadeIn('fast');
	
	$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle');
	$('#answer'+num).addClass('semantic-exercise-toggle');
};

function checkExercise(){
	var initialText = null;
	if (answer == -1)
	{
		$('#exercise-explanation').html("<?php echo _('Debe seleccionar alguna respuesta'); ?>");
	}
	else
	{
		if (parseInt(classifyExercise.Answer.position) == answer) //win
		{
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-ok");
			initialText = "<span style='color:green;'><b><?php echo _('&iexcl;Correcto!'); ?></b></span><br />";
			exercise.setCorrect(true);
		}
		else //lose
		{
			var goodAnswer = classifyExercise.Answer.position;
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-bad");
			initialText = "<b><?php echo _('&iexcl;Lo siento!'); ?></b> <?php echo _('La respuesta correcta es la'); ?> <b>"+(parseInt(goodAnswer)+1)+"</b><br />";
			$('#answer'+goodAnswer).addClass("semantic-exercise-ok");
			exercise.setCorrect(false);
		}
		
		for (var i=0; i<5; i++)
		{
			$('#answer'+i).addClass('disabled').attr('onclick', 'javascript:void(0);');
		}
		
		$('#exercise-explanation').html(initialText+"<b><?php echo _('Explicaci&oacute;n: '); ?></b>"+classifyExercise.explanation);
		$('#help-button').fadeOut('fast');
		$('#continue-button').fadeOut('fast', function(evt){
			$('#exercise-description').html('<?php echo _('Cuando estés preparado pulsa en <strong>Continuar</strong>.'); ?>');
			//$('#continue-button').fadeOut('fast');
			$('#end-button').fadeIn('fast');
		});
	}
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (repetitions==0) //show intro
	{
		$('#introExercise').show();
	}
	else //show exercise
	{
		showSemanticExercise();
	}
	
	//retrieve current exercise plan
	$.ajax({
		type: "GET",
		url: "exercises/xml/classify-plan.xml",
		dataType: "xml",
		success: function(xml) {
			var plan = $.xml2json(xml);
			var numberOfExercise = -1;
			
			$.ajax({
				type: "GET",
				url: "exercises/xml/classify-series.php",
				dataType: "xml",
				success: function(result){
					var classifyExercises = $.xml2json(result);
					
					$.each(plan.ClassifyPlanEntry, function(index, value){
						if (value.exerciseID == exerciseID && value.sessionID == sessionID)
						{
							if (value.exercises == "random")
							{
								classifyExercises = exercisesWithCurrentDifficulty(classifyExercises);
								
								//take one random exercise
								numberOfExercise = Math.floor(getRandom()*classifyExercises.length);
							}
							else
							{
								classifyExercises = allExercises(classifyExercises);
								
								var array = value.exercises.split(",");
								if (repetitions < array.length)
									numberOfExercise = parseInt(array[repetitions])-1;
								else numberOfExercise = Math.floor(getRandom()*classifyExercises.length);
							}
							
							classifyExercise = classifyExercises[numberOfExercise];
												
							return false; //break the loop
						}
					});
					
					if (classifyExercise != null)
					{
						$.each(classifyExercise.Elements.Element, function(index, element){
							var idAnswer = '#answer'+element.position;
							$(idAnswer).html("<img src='exercises/img/ClassifyExercise/series"+classifyExercise.id+"/figure"+element.value+".png' alt='<?php echo _('Respuesta'); ?> "+ element.value+"' width='150px' />");
							$("#li-answer"+element.position).removeClass('hide').show();
						});
					}
					else
					{
						//error
						console.debug("error: classify exercise not defined");
					}
				}
			});
		}
	});
});
</script>