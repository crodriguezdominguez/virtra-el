<?php
require_once('locale/localization.php');
?>

<!-- Questionnaire intro -->
<!-- <div id="questIntro">
<a href="javascript:void(0);" onclick="javascript:showQuestionnaire();" class="btn btn-primary btn-large"><?php /*echo _('Responder cuestionario');*/ ?></a>
<a href="javascript:void(0);" onclick="javascript:endExercise();" class="btn btn-large"><?php /*echo _('Omitir cuestionario');*/ ?></a>
</div>
-->

<p class="lead"
   id="long_intro"><?php echo _('Tus respuestas serán totalmente confidenciales. Servir&aacute;n para mejorar el funcionamiento de los ejercicios en el futuro y adaptarlos a las necesidades de todas las personas. Tambi&eacute;n se utilizar&aacute;n para elaborar estad&iacute;sticas que permitan comprobar c&oacute;mo es de efectivo VIRTRA-EL. En la mayor&iacute;a de los casos, deber&aacute;s seleccionar una respuesta de entre las que te ofrecemos.'); ?></p>
<!-- Questionnaire -->
<div id="questionnaire">
    <form method="post" action="javascript:updateUser();" class="form-horizontal" id="contact-form"
          style="font-size:18px;">
        <fieldset>
            <div id="nextQuestionnaire0">
                <div class="control-group">
                    <label class="control-label" for="sex"><?php echo _('Sexo'); ?></label>
                    <div class="controls">
                        <select class="input-xlarge" id="sex" name="sex"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire0').fadeOut('fast'); else $('#button-questionnaire0').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="2"><?php echo _('Masculino'); ?></option>
                            <option value="1"><?php echo _('Femenino'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a id="button-questionnaire0" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:{$('#long_intro').fadeOut('fast');$('#nextQuestionnaire0').fadeOut('fast', function(){$('#nextQuestionnaire0b').fadeIn('fast');});}"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="nextQuestionnaire0b" class="hide">
                <div class="control-group" id="placeOfBirthGroup">
                    <label class="control-label" for="placeOfBirth"><?php echo _('Lugar de nacimiento'); ?></label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="placeOfBirth" name="placeOfBirth">
                    </div>
                </div>
                <div class="control-group" id="yearInputGroup">
                    <label class="control-label" for="yearInput"><?php echo _('Año de nacimiento'); ?></label>
                    <div class="controls">
                        <!--<div class="input-append date" id="birthPicker" data-date="01-01-1950" data-date-format="dd-mm-yyyy">-->
                        <!--<input class="input-small" type="text" id="dateOfBirth" data-format="DD-MM-YYYY" data-template="D MMM YYYY" value="01-01-1950" name="dateOfBirth" data-date-format="dd-mm-yyyy">-->
                        <input type="text" class="input-small" placeholder="<?php echo _('Año'); ?>" name="yearInput"
                               id="yearInput"/>
                        <span
                            class="help-block"><?php echo _('Introduce el <strong>número</strong> de año usando cuatro números (por ejemplo, 1920).'); ?></span>
                        <!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                        <!--</div>-->
                        <!--<span class="help-block"><?php echo _('Pulsa en el botón con el icono <i class="icon-calendar"></i> para seleccionar tu año, mes y día de nacimiento.'); ?></span>-->
                    </div>
                </div>
                <div class="control-group" id="monthInputGroup">
                    <label class="control-label" for="monthInput"><?php echo _('Mes de nacimiento'); ?></label>
                    <div class="controls">
                        <input type="text" class="input-small" placeholder="<?php echo _('Mes'); ?>" name="monthInput"
                               id="monthInput"/>
                        <span
                            class="help-block"><?php echo _('Introduce el <strong>número</strong> de mes (por ejemplo, en vez de mayo pon 5).'); ?></span>
                    </div>
                </div>
                <div class="control-group" id="dayInputGroup">
                    <label class="control-label" for="dayInput"><?php echo _('Día de nacimiento'); ?></label>
                    <div class="controls">
                        <input type="text" class="input-small" placeholder="<?php echo _('Día del mes'); ?>"
                               name="dayInput" id="dayInput"/>
                        <span
                            class="help-block"><?php echo _('Introduce el <strong>número</strong> del día del mes en el que naciste (por ejemplo, 31).'); ?></span>
                    </div>
                </div>
                <!--<div class="control-group hide" id="finalDate">
			<label class="control-label" for="dateInput"><?php echo _('Naciste el'); ?></label>
			<div class="controls">
					<input type="text" class="input-xlarge" placeholder="<?php echo _('Fecha completa'); ?>" name="dateInput" id="dateInput" readonly="1" />
			</div>
	    </div>-->
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire0b').fadeOut('fast', function(){$('#nextQuestionnaire0').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                class="btn btn-primary" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire0b').fadeOut('fast', function(){$('#nextQuestionnaire1').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire1">
                <div class="control-group">
                    <label class="control-label" for="civilStatus"><?php echo _('Estado civil'); ?></label>
                    <div class="controls">
                        <select class="input-xlarge" id="civilStatus" name="civilStatus"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire1').fadeOut('fast'); else $('#button-questionnaire1').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('Soltero/a'); ?></option>
                            <option value="2"><?php echo _('Casado/a'); ?></option>
                            <option value="3"><?php echo _('Viudo/a'); ?></option>
                            <option value="4"><?php echo _('Divorciado/a'); ?></option>
                            <option value="5"><?php echo _('Otro/a'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire1').fadeOut('fast', function(){$('#nextQuestionnaire0b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire1" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire1').fadeOut('fast', function(){$('#nextQuestionnaire2').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire2">
                <div class="control-group">
                    <label class="control-label" for="partner"><?php echo _('Convivo con'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="partner" name="partner"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire2').fadeOut('fast'); else $('#button-questionnaire2').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('Esposo/a o pareja'); ?></option>
                            <option value="2"><?php echo _('Solo/a'); ?></option>
                            <option value="3"><?php echo _('Vivo en una residencia'); ?></option>
                            <option value="4"><?php echo _('Hijo/a'); ?></option>
                            <option value="5"><?php echo _('Otros familiares'); ?></option>
                            <option value="6"><?php echo _('Otros'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire2').fadeOut('fast', function(){$('#nextQuestionnaire1').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire2" class="btn btn-primary" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire2').fadeOut('fast', function(){$('#nextQuestionnaire3').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire3">
                <div class="control-group">
                    <label class="control-label"
                           for="washingHelp"><?php echo _('Necesito ayuda para ba&ntilde;arme'); ?></label>
                    <div class="controls">
                        <select class="input-xlarge" id="washingHelp" name="washingHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire3').fadeOut('fast'); else $('#button-questionnaire3').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No'); ?></option>
                            <option value="2"><?php echo _('S&iacute;'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire3').fadeOut('fast', function(){$('#nextQuestionnaire2').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire3" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire3').fadeOut('fast', function(){$('#nextQuestionnaire3b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire3b">
                <div class="control-group">
                    <label class="control-label" for="clothingHelp"><?php echo _('A la hora de vestirme'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="clothingHelp" name="clothingHelp"
                            onchange="javascript:{if (value == 0) $('#button-questionnaire3b').fadeOut('fast'); else $('#button-questionnaire3b').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No necesito ayuda'); ?></option>
                            <option value="2"><?php echo _('Necesito un poco de ayuda'); ?></option>
                            <option value="3"><?php echo _('Necesito ser vestido por otra persona'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire3b').fadeOut('fast', function(){$('#nextQuestionnaire3').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire3b" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire3b').fadeOut('fast', function(){$('#nextQuestionnaire4').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire4">
                <div class="control-group">
                    <label class="control-label"
                           for="smartenUPHelp"><?php echo _('Necesito ayuda para arreglarme'); ?></label>
                    <div class="controls">
                        <select class="input-xlarge" id="smartenUPHelp" name="smartenUPHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire4').fadeOut('fast'); else $('#button-questionnaire4').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No'); ?></option>
                            <option value="2"><?php echo _('S&iacute;'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire4').fadeOut('fast', function(){$('#nextQuestionnaire3b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire4" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire4').fadeOut('fast', function(){$('#nextQuestionnaire4b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire4b">
                <div class="control-group">
                    <label class="control-label" for="eatingHelp"><?php echo _('A la hora de comer'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="eatingHelp" name="eatingHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire4b').fadeOut('fast'); else $('#button-questionnaire4b').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No necesito ayuda'); ?></option>
                            <option value="2"><?php echo _('Necesito un poco de ayuda'); ?></option>
                            <option value="3"><?php echo _('Necesito ser alimentado por otra persona'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire4b').fadeOut('fast', function(){$('#nextQuestionnaire4').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire4b" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire4b').fadeOut('fast', function(){$('#nextQuestionnaire5').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire5">
                <div class="control-group">
                    <label class="control-label" for="urinateHelp"><?php echo _('A la hora de orinar'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="urinateHelp" name="urinateHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire5').fadeOut('fast'); else $('#button-questionnaire5').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No sufro episodios de incontinencia'); ?></option>
                            <option
                                value="2"><?php echo _('Sufro ocasionalmente de alg&uacute;n episodio de incontinencia'); ?></option>
                            <option value="3"><?php echo _('Soy incontinente'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire5').fadeOut('fast', function(){$('#nextQuestionnaire4b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire5" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire5').fadeOut('fast', function(){$('#nextQuestionnaire5b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire5b">
                <div class="control-group">
                    <label class="control-label" for="defecateHelp"><?php echo _('A la hora de defecar'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="defecateHelp" name="defecateHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire5b').fadeOut('fast'); else $('#button-questionnaire5b').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="2"><?php echo _('No sufro episodios de incontinencia'); ?></option>
                            <option
                                value="1"><?php echo _('No sufro episodios de incontinencia. Si tengo sonda, puedo cambiar la bolsa solo'); ?></option>
                            <option value="3"><?php echo _('Soy incontinente'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire5b').fadeOut('fast', function(){$('#nextQuestionnaire5').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire5b" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire5b').fadeOut('fast', function(){$('#nextQuestionnaire6').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire6">
                <div class="control-group">
                    <label class="control-label" for="toiletHelp"><?php echo _('A la hora de ir al aseo'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="toiletHelp" name="toiletHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire6').fadeOut('fast'); else $('#button-questionnaire6').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No necesito ayuda'); ?></option>
                            <option value="2"><?php echo _('Necesito un poco de ayuda'); ?></option>
                            <option value="3"><?php echo _('Necesito mucha ayuda'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire6').fadeOut('fast', function(){$('#nextQuestionnaire5b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire6" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire6').fadeOut('fast', function(){$('#nextQuestionnaire6b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire6b">
                <div class="control-group">
                    <label class="control-label"
                           for="movingHelp"><?php echo _('A la hora de ir a la cama/sill&oacute;n'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="movingHelp" name="movingHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire6b').fadeOut('fast'); else $('#button-questionnaire6b').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No necesito ayuda'); ?></option>
                            <option value="2"><?php echo _('Necesito un poco de ayuda'); ?></option>
                            <option value="3"><?php echo _('Necesito mucha ayuda'); ?></option>
                            <option
                                value="4"><?php echo _('Necesito ayuda de una gr&uacute;a o de al menos dos personas fuertes'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire6b').fadeOut('fast', function(){$('#nextQuestionnaire6').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire6b" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire6b').fadeOut('fast', function(){$('#nextQuestionnaire7').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire7">
                <div class="control-group">
                    <label class="control-label" for="walkingHelp"><?php echo _('A la hora de andar'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="walkingHelp" name="walkingHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire7').fadeOut('fast'); else $('#button-questionnaire7').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('Puedo andar 50 metros o m&aacute;s sin ayuda'); ?></option>
                            <option
                                value="2"><?php echo _('Para andar 50 metros necesito algo de ayuda (por ejemplo, de un andador)'); ?></option>
                            <option value="3"><?php echo _('Soy independiente en silla de ruedas'); ?></option>
                            <option value="4"><?php echo _('Soy incapaz de andar'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire7').fadeOut('fast', function(){$('#nextQuestionnaire6b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire7" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire7').fadeOut('fast', function(){$('#nextQuestionnaire7b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire7b">
                <div class="control-group">
                    <label class="control-label"
                           for="stepsHelp"><?php echo _('A la hora de subir y bajar escaleras'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="stepsHelp" name="stepsHelp"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire7b').fadeOut('fast'); else $('#button-questionnaire7b').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('No necesito ayuda'); ?></option>
                            <option value="2"><?php echo _('Necesito ayuda'); ?></option>
                            <option value="3"><?php echo _('No soy capaz'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire7b').fadeOut('fast', function(){$('#nextQuestionnaire7').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire7b" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire7b').fadeOut('fast', function(){$('#nextQuestionnaire8').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire8">
                <div class="control-group">
                    <label class="control-label" for="levelOfStudies"><?php echo _('Nivel de estudios'); ?></label>
                    <div class="controls">
                        <select class="input-xxlarge" id="levelOfStudies" name="levelOfStudies"
                                onchange="javascript:{if (value == 0) $('#button-questionnaire8').fadeOut('fast'); else $('#button-questionnaire8').fadeIn('fast');}">
                            <option value="0">-</option>
                            <option value="1"><?php echo _('Leer/escribir'); ?></option>
                            <option value="2"><?php echo _('Estudios primarios'); ?></option>
                            <option value="3"><?php echo _('Graduado escolar/EGB'); ?></option>
                            <option value="4"><?php echo _('Bachillerato/COU'); ?></option>
                            <option value="5"><?php echo _('Formaci&oacute;n profesional'); ?></option>
                            <option value="6"><?php echo _('Diplomatura'); ?></option>
                            <option value="7"><?php echo _('Licenciatura'); ?></option>
                            <option value="8"><?php echo _('Doctorado'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div><a class="btn" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire8').fadeOut('fast', function(){$('#nextQuestionnaire7b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                                id="button-questionnaire8" class="btn btn-primary hide" href="javascript:void(0);"
                                onclick="javascript:$('#nextQuestionnaire8').fadeOut('fast', function(){$('#nextQuestionnaire9').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide" id="nextQuestionnaire9">
                <div class="control-group">
                    <label class="control-label"
                           for="hoursReading"><?php echo _('Horas semanales dedicadas a...'); ?></label>
                    <div class="controls docs-input-sizes">
                        <?php echo _('Lectura'); ?> <input type="text" placeholder="0" onfocus="this.type='number';"
                                                           class="input-small" id="hoursReading" name="hoursReading"
                                                           min="0">
                        <?php echo _('Talleres (memoria, pintura, teatro, etc.)'); ?> <input placeholder="0"
                                                                                             onfocus="this.type='number';"
                                                                                             type="text"
                                                                                             class="input-small"
                                                                                             id="hoursWorkshop"
                                                                                             name="hoursWorkshop"
                                                                                             min="0">
                        <?php echo _('Ejercicio f&iacute;sico (caminar, gimnasia, etc.)'); ?> <input placeholder="0"
                                                                                                     onfocus="this.type='number';"
                                                                                                     type="text"
                                                                                                     class="input-small"
                                                                                                     id="hoursExercise"
                                                                                                     name="hoursExercise"
                                                                                                     min="0">
                        <?php echo _('Utilizar el ordenador'); ?> <input placeholder="0" onfocus="this.type='number';"
                                                                         type="text" class="input-small"
                                                                         id="hoursComputer" name="hoursComputer"
                                                                         min="0">
                    </div>
                </div>
                <div class="control-group" id="phoneNumberGroup">
                    <label class="control-label" for="phoneNumber"><?php echo _('Tel&eacute;fono'); ?></label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="phoneNumber" name="phoneNumber">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <a class="btn" href="javascript:void(0);"
                           onclick="javascript:$('#nextQuestionnaire9').fadeOut('fast', function(){$('#nextQuestionnaire8').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-primary"><?php echo _('Terminar cuestionario'); ?></button>
                        <!-- <a href="javascript:void(0);" onclick="javascript:endExercise();" class="btn btn-large"><?php /*echo _('Omitir cuestionario');*/ ?></a>-->
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<!--<script type="text/javascript" src="js/jquery.toChecklist.min.js"></script>-->
<link href="css/listbox.css" rel="stylesheet">
<script type="text/javascript" src="js/listbox.js"></script>
<!--<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>-->
<script type="text/javascript" src="js/moment-with-locales.min.js"></script>
<!--<script type="text/javascript" src="js/combodate.js"></script>-->
<script type="text/javascript">
    $(function () {
        //poner $('#birthPicker') para cambiar a modo componente
        moment.locale('es');
        $('#exercise-container select').listbox({'searchbar': false});
        //$('#dateOfBirth').combodate({smartDays:true, minYear:1890});
        /*$('#dateOfBirth').datepicker({
         weekStart: 1,
         viewMode: 'years',
         dates : {
         days: ["<?php echo _('Domingo'); ?>", "<?php echo _('Lunes'); ?>", "<?php echo _('Martes'); ?>", "<?php echo _('Mi&eacute;rcoles'); ?>", "<?php echo _('Jueves'); ?>", "<?php echo _('Viernes'); ?>", "<?php echo _('S&aacute;bado'); ?>", "<?php echo _('Domingo'); ?>"],
         daysShort: ["<?php echo _('Dom'); ?>", "<?php echo _('Lun'); ?>", "<?php echo _('Mar '); ?>", "<?php echo _('Mi&eecute;'); ?>", "<?php echo _('Jue'); ?>", "<?php echo _('Vie'); ?>", "<?php echo _('S&aacute;b'); ?>", "<?php echo _('Dom'); ?>"],
         daysMin: ["<?php echo _('Do'); ?>", "<?php echo _('Lu'); ?>", "<?php echo _('Ma'); ?>", "<?php echo _('Mi'); ?>", "<?php echo _('Ju'); ?>", "<?php echo _('Vi'); ?>", "<?php echo _('S&aacute;'); ?>", "<?php echo _('Do'); ?>"],
         months: ["<?php echo _('Enero'); ?>", "<?php echo _('Febrero'); ?>", "<?php echo _('Marzo'); ?>", "<?php echo _('Abril'); ?>", "<?php echo _('Mayo'); ?>", "<?php echo _('Junio'); ?>", "<?php echo _('Julio'); ?>", "<?php echo _('Agosto'); ?>", "<?php echo _('Septiembre'); ?>", "<?php echo _('Octubre'); ?>", "<?php echo _('Noviembre'); ?>", "<?php echo _('Diciembre'); ?>"],
         monthsShort: ["<?php echo _('Ene'); ?>", "<?php echo _('Feb'); ?>", "<?php echo _('Mar'); ?>", "<?php echo _('Abr'); ?>", "<?php echo _('May'); ?>", "<?php echo _('Jun'); ?>", "<?php echo _('Jul'); ?>", "<?php echo _('Ago'); ?>", "<?php echo _('Sep'); ?>", "<?php echo _('Oct'); ?>", "<?php echo _('Nov'); ?>", "<?php echo _('Dic'); ?>"]
         }
         });

         $('#dateOfBirth').on('changeDate', function(evt){
         //console.log(evt);
         if (evt.viewMode == 'days')
         {
         $('#dateOfBirth').datepicker('hide');
         }
         });*/
    });

    function validatePhone(txtPhone) {
        var a = document.getElementById(txtPhone).value;
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }

    // Validates that the input string is a valid date formatted as "yyyy-mm-dd"
    function validateDate(dateString) {
        // First check for the pattern
        if (!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("-");
        var year = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var day = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if (year < 1850 || year > 2500 || month == 0 || month > 12)
            return false;

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }
    ;

    function getDateValue(dateString) { //format yyyy-mm-dd
        var parts = dateString.split("-");
        var year = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var day = parseInt(parts[2], 10);

        var dateStr = "" + year + "-" + month + "-" + day;
        return moment(moment(dateStr, "YYYY-M-D")).format("YYYY-MM-DD");
    }
    ;

    function updateUser() {
        var finish = true;
        var comeback = false;

        var dateVal = $("#yearInput").val() + "-" + $("#monthInput").val() + "-" + $("#dayInput").val();

        if (!$('#placeOfBirth').val() || $('#placeOfBirth').val() == '') {
            finish = false;
            comeback = true;
            $('#placeOfBirthGroup').addClass('error');
            $('#placeOfBirth').focus();
        }
        else {
            $('#placeOfBirthGroup').removeClass('error');
        }

        if (!dateVal || dateVal == '' || !validateDate(dateVal)) {
            finish = false;
            comeback = true;

            sweetAlert({
                title: "<?php echo _('Error'); ?>",
                text: "<?php echo _('Te has equivocado al introducir tu fecha de nacimiento, arréglalo.'); ?>",
                type: "error",
                confirmButtonText: "<?php echo _('Continuar'); ?>"
            });
            $('#yearInputGroup').addClass('error');
            $('#dayInputGroup').addClass('error');
            $('#monthInputGroup').addClass('error');
            $('#yearInputGroup').focus();
        }
        else {
            $('#yearInputGroup').removeClass('error');
            $('#dayInputGroup').removeClass('error');
            $('#monthInputGroup').removeClass('error');
        }

        var phoneNumber = $('#phoneNumber').val();

        if (!phoneNumber || phoneNumber == '' || !validatePhone("phoneNumber")) {
            finish = false;
            sweetAlert({
                title: "<?php echo _('Error'); ?>",
                text: "<?php echo _('Te has equivocado en el teléfono, arréglalo.'); ?>",
                type: "error",
                confirmButtonText: "<?php echo _('Continuar'); ?>"
            });
            $('#phoneNumberGroup').addClass('error');
            $('#phoneNumber').focus();
        }
        else {
            $('#phoneNumberGroup').removeClass('error');
        }

        if (comeback) {
            $("div[id^=nextQuestionnaire]").fadeOut('slow', function () {
                $("#nextQuestionnaire0b").fadeIn('slow');
            });
        }
        else {
            $('#contact-form').find('select').each(function () {
                if ($(this).val() == '0' && !$(this).hasClass('day') && !$(this).hasClass('month') && !$(this).hasClass('year')) {
                    $(this).focus();
                    sweetAlert({
                        title: "<?php echo _('Error'); ?>",
                        text: "<?php echo _('Debes responder todas las preguntas.'); ?>",
                        type: "error",
                        confirmButtonText: "<?php echo _('Continuar'); ?>"
                    });
                    finish = false;
                    return false;
                }
            });
        }

        if (finish) {
            //change date to mysql format
            var mysqlDate = getDateValue(dateVal);

            var qry = 'placeOfBirth=' + encodeURIComponent($('#placeOfBirth').val()) + '&' +
                'phoneNumber=' + encodeURIComponent($('#phoneNumber').val()) + '&' +
                'eatingHelp=' + $('#eatingHelp').val() + '&' +
                'sex=' + $('#sex').val() + '&' +
                'dateOfBirth=' + mysqlDate + '&' +
                'civilStatus=' + $('#civilStatus').val() + '&' +
                'partner=' + $('#partner').val() + '&' +
                'levelOfStudies=' + $('#levelOfStudies').val() + '&' +
                'hoursReading=' + $('#hoursReading').val() + '&' +
                'hoursWorkshop=' + $('#hoursWorkshop').val() + '&' +
                'hoursExercise=' + $('#hoursExercise').val() + '&' +
                'washingHelp=' + $('#washingHelp').val() + '&' +
                'clothingHelp=' + $('#clothingHelp').val() + '&' +
                'smartenUPHelp=' + $('#smartenUPHelp').val() + '&' +
                'urinateHelp=' + $('#urinateHelp').val() + '&' +
                'defecateHelp=' + $('#defecateHelp').val() + '&' +
                'toiletHelp=' + $('#toiletHelp').val() + '&' +
                'movingHelp=' + $('#movingHelp').val() + '&' +
                'walkingHelp=' + $('#walkingHelp').val() + '&' +
                'stepsHelp=' + $('#stepsHelp').val() + '&' +
                'hoursComputer=' + $('#hoursComputer').val();

            $.ajax({
                type: "POST",
                url: 'backend/update_userprofile.php',
                data: qry,
                success: function (html) {
                    endExercise();
                }
            });
        }
    }

    /*
     function showQuestionnaire()
     {
     $('#questIntro').fadeOut('slow', function(){
     $('#questionnaire').fadeIn('slow');
     });
     }*/

</script>