<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!LogicalSeriesExercise) {

var LogicalSeriesExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Completa la serie!'); ?>', '<?php echo _('En este ejercicio te mostraré una serie de dibujos que siguen un orden determinado. Deberás averiguar cómo van cambiando y añadir el dibujo que falta en el lugar de la señal de interrogación. Para ello tendrás que pulsar sobre una de las imágenes numeradas que aparecerán en la parte inferior de la pantalla.'); ?>', 'exercises/LogicalSeriesExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._correct = false;
	},
	correct : function() {
		return this._correct;
	},
	setCorrect : function(cor) {
		this._correct = cor;
	},
	offerHelp : function(){
		return this._sessionID != 1 && this._sessionID != 2 && this._sessionID != 12 && this._sessionID != 13;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = 0;
		var countFails = 0;
		
		if (this._correct)
		{
			countCorrects = 1;
		}
		else countFails = 1;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_reasoning_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		else return {medal: false, type:0};
	}
});

}
</script>
