<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!NumbersAndVowelsExercise) {

var NumbersAndVowelsExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Dictado de Números y Vocales!'); ?>', '<?php echo _('En este ejercicio te mostraré una pizarra donde aparecerán varias vocales y números que tendrás que memorizar. Te los mostraré de uno en uno y cuando termine tendrás que introducir siempre en primer lugar los números <strong>del más pequeño al más grande</strong>, y después las vocales siguiendo el orden <strong>a, e, i, o, u</strong>. Si en algún momento te equivocas podrás pulsar en el botón <strong>Borrar</strong>.'); ?>', 'exercises/NumbersAndVowelsExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._seconds = 0;
		this._level = 2;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	demoUIurl : function() {
		//override to return the demo UI
		/*if (parseInt(this.sessionID())==2) return "exercises/NumbersAndVowelsExerciseDemoUI.php";
		else return null;*/
		return "exercises/NumbersAndVowelsExerciseDemoUI.php";
	},
	updateResults : function() {
		this.finishExercise();
	},
	level : function() {
		return this._level;
	},
	setLevel : function(l) {
		this._level = l;
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	updateUserLevel : function() {
		var qry = 'exerciseType=1&userLevel='+this._level+'&userSubLevel=0';
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_level.php',
			success: function(data){	
			}
		});
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		this.updateUserLevel();
		
		var countCorrects = this._corrects;
		var countFails = this._fails;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
