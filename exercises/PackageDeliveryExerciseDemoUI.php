<?php
	require_once('locale/localization.php');
?>

<div id="introExercise" class="hide">
	<p><a id="introNextStep" href="javascript:void(0);" onclick="javascript:nextStepIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<table id="vanStatusPanel" class="well package-delivery-van-status-panel hide">
	<tr>
		<td class="package-delivery-van-status">
			<p>
				<span style="position:absolute;text-align:center;width:180px;margin-top:8px;font-size:24px;"><?php echo _('Gasolina'); ?></span>
				<img style="height:35px" width='180px' height='40px' id='fuelStatus' src='exercises/img/PackageDeliveryExercise/fuel100.png' alt='<?php echo _('Nivel de gasolina al ') ?> 100%' />
            </p>
            <p style="text-align:left;margin-bottom:1px">
                <span style="margin-left:8px;font-size:18px;"><b><?php echo _('Maletero:'); ?></b></span>
            </p>
			<div class="package-delivery-van-box"><a src="javascript:void(0);" id="box0" onclick="javascript:dropBox(0);"><img id="boxImg0" class="thumbnail" src='exercises/img/PackageDeliveryExercise/boxempty.png' alt='' width="50px" height="50px"></a></div>
			<div class="package-delivery-van-box"><a src="javascript:void(0);" id="box1" onclick="javascript:dropBox(1);"><img id="boxImg1" class="thumbnail" src='exercises/img/PackageDeliveryExercise/boxempty.png' alt='' width="50px" height="50px"></a></div>
			<div class="package-delivery-van-box"><a src="javascript:void(0);" id="box2" onclick="javascript:dropBox(2);"><img id="boxImg2" class="thumbnail" src='exercises/img/PackageDeliveryExercise/boxempty.png' alt='' width="50px" height="50px"></a></div>
		</td>
		<!--<td class="package-delivery-game-description-title"><?php echo _('<strong>Entregas</strong>'); ?></td>-->
		<td id="deliveries" class="package-delivery-game-description-entries"></td>
		<!--<td class="package-delivery-game-description-title"><?php echo _('<strong>Recogidas</strong>'); ?></td>-->
		<td id="pickups" class="package-delivery-game-description-entries"></td>
	</tr>
</table>
<div id="mediaContainer" style="position:relative;">
	<div class="modal hide fade" id="mediaAlert">
		<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h3><?php echo _('Error'); ?></h3>
	  	</div>
	  	<div class="modal-body lead" id="mediaBody">
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo _('Cerrar'); ?></button>
		</div>
	</div>
	<div id="mediaPanel" style="position:relative;margin-bottom:10px;overflow: hidden;">
	</div>
</div>


<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var placesXML = "exercises/xml/package-delivery-places.php";
var planXML = "exercises/xml/package-delivery-plan.xml";
var scenesXML = "exercises/xml/package-delivery-scenes.php";
var places = null;
var scene = null;
var currentIntroStep = 0;
var currentVanPlace = null;
var checkForId = null;
var deliveries = null;
var pickups = null;
var boxesInVan = [];
var boxesAtPlaces = [];
var waitForBoxPlacement = false;
var waitForBoxPickup = false;
var VAN_DISPLACEMENT_Y = 25;
var currentFuelSteps = 0;
var MAX_FUEL_STEPS = 30;
var allowVanMovements = true;

function buildDeliveries() {
	deliveries = [];
	pickups = [];
	boxesAtPlaces = [];
	boxesInVan = [];
	$.each(scene.Boxes.Box, function(index, element){
		if (element.inshop=="1"){
			boxesAtPlaces.push({id:element.id, name:element.name, initialplace:element.initialplace, goalplace:element.goalplace, img:element.img, actualplace:element.initialplace});
			pickups.push(element);
		}
		else {
			boxesInVan.push({id:element.id, name:element.name, initialplace:element.initialplace, goalplace:element.goalplace, img:element.img, actualplace:element.initialplace});
			deliveries.push(element);
		}
	});
	
	var descriptionResult = "<h3><?php echo _('Paquetes para entregar'); ?></h3><ul class='unstyled'>";
	$.each(deliveries, function(index, element){
		var goalPlaceName = "";
		for (var i=0; i<places.Place.length; i++)
		{
			if (places.Place[i].id==element.goalplace)
			{
				goalPlaceName = places.Place[i].name;
				break;
			}
		}
		descriptionResult += "<li>"+element.name+" <?php echo _('en'); ?> <strong>"+goalPlaceName+"</strong></li>";
	});
	
	$("#deliveries").html(descriptionResult+"</ul>");
	
	descriptionResult = "<h3><?php echo _('Paquetes para recoger'); ?></h3><ul class='unstyled'>";
	$.each(pickups, function(index, element){
		var initialPlaceName = "";
		for (var i=0; i<places.Place.length; i++)
		{
			if (places.Place[i].id==element.initialplace)
			{
				initialPlaceName = places.Place[i].name;
				break;
			}
		}
		descriptionResult += "<li>"+element.name+" <?php echo _('en'); ?> <strong>"+initialPlaceName+"</strong></li>";
	});
	
	$("#pickups").html(descriptionResult+"</ul>");
};

function dropBox(index) {
	$('#mediaAlert').modal('hide');
	
	if (index < boxesInVan.length)
	{	
		var box = boxesInVan[index];
		var put = true;
		$.each(boxesAtPlaces, function(index, element){
			if (element.actualplace == currentVanPlace.id)
			{
				$('#mediaAlert').modal({show:false});
				$("#mediaBody").html("<?php echo _('No puedes repartir en un mismo lugar más de un paquete.'); ?>");
				$('#mediaAlert').modal('show');
				put = false;
				return false;
			}
		});
	
		if (put)
		{
			box.actualplace = currentVanPlace.id;
			boxesAtPlaces.push(box);
			boxesInVan.splice(index, 1);
			
			buildBoxes();
			
			if (waitForBoxPlacement)
			{
				waitForBoxPlacement = false;
				waitForBoxPickup = true;
				showInstructions(false);
				$("#exercise-description").html("<?php echo _('<strong>¡Muy bien!</strong> Observa como el paquete se ha puesto en la <strong>Librer&iacute;a</strong>. Ahora pulsa sobre el paquete de nuevo para recogerlo.'); ?>");
			}
		}
	}
};

function pickupBox(placeID) {
	if (currentVanPlace.id == placeID)
	{
		if (boxesInVan.length >= 3)
		{
			$('#mediaAlert').modal({show:false});
			$("#mediaBody").html("<?php echo _('No puedes llevar en la furgoneta más de tres paquetes.'); ?>");
			$('#mediaAlert').modal('show');
			return;
		}
		
		$('#mediaAlert').modal('hide');
		
		for (var i=0; i<boxesAtPlaces.length; i++)
		{
			if (boxesAtPlaces[i].actualplace == placeID)
			{
				boxesAtPlaces[i].actualplace = "-1";
				boxesInVan.push(boxesAtPlaces[i]);
				boxesAtPlaces.splice(i, 1);
				break;
			}
		}
		
		buildBoxes();
		
		if (waitForBoxPickup)
		{
			waitForBoxPickup = false;
			showInstructions(false);
			$("#exercise-description").html("<?php echo _('<strong>¡Muy bien!</strong> Observa como has recogido el paquete y ahora lo tienes en la furgoneta. Debes tener en cuenta que solo podrás llevar tres paquetes al mismo tiempo. Finalmente, junto al medidor de gasolina tienes toda la lista de paquetes que tienes que entregar o recoger para completar correctamente el ejercicio. Pulsa <strong>Continuar</strong>.'); ?>");
			$("#introNextStep").show();
		}
	}
};

function buildBoxes() {
	//remove all the existing boxes
	$('[id^="placedBox"]').remove();
	$('[id^="boxImg"]').attr('src', 'exercises/img/PackageDeliveryExercise/boxempty.png');
	$('[id^="boxImg"]').attr('alt', '');
	
	//place the boxes in the van
	$.each(boxesInVan, function(index, box){
		$("#boxImg"+index).attr('src', box.img);
		$("#boxImg"+index).attr('alt', box.name);
	});
	
	//place the boxes in the scene
	$.each(boxesAtPlaces, function(index, box){
		$.each(places.Place, function(j, place){
			if (box.actualplace == place.id)
			{
				var frame = frameOfPlace(place);
				var boxSize = 50;
				var boxHTML = "<div onclick='javascript:pickupBox("+place.id+");' id='placedBox"+index+"' style='margin:0;padding:0;position:absolute; left:"+(frame[0]+frame[2]-boxSize/2)+"px; top:"+(frame[1]+frame[3]-boxSize)+"px; width:"+boxSize+"px; height:"+boxSize+"px;'>"
				boxHTML += "<img width='"+boxSize+"px' height='"+boxSize+"px' src='"+box.img+"' alt='"+box.name+"' /></div>";
				$("#mediaPanel").append(boxHTML);
			}
		});
	});
};

function frameOfPlaceAtIndex(index) {
	return frameOfPlace(places.Place[index]);
};

function frameOfPlace(place) {
	var x = parseInt(place.x);
	var y = parseInt(place.y);
	var w = parseInt(place.w);
	var h = parseInt(place.h);
	return [x,y,w,h];
};

function placeHTML(place) {
	var index = places.Place.indexOf(place);
	if (index != -1)
	{
		var frame = frameOfPlace(place);
		var placeHTML = "<div onclick='javascript:moveVanToPlaceAtIndex("+index+");' class='package-delivery-place' id='place"+index+"' style='left:"+frame[0]+"px; top:"+frame[1]+"px; width:"+frame[2]+"px; height:"+frame[3]+"px;'>"
		placeHTML += "<img width='"+frame[2]+"px' height='"+frame[3]+"px' src='"+place.img+"' alt='"+place.name+"' /></div>";
		return placeHTML;
	}
	else return "";
};

function placesHTML() {
	var pHTML = "<img alt='<?php echo _('Ciudad'); ?>' src='"+places.background+"' width='100%' height='100%' />";
	$.each(places.Place, function(index, element){
		pHTML = pHTML+placeHTML(element);
	});
	
	return pHTML;
};

function vanPositionForPlace(place) {
	var placeElement = $("#place"+places.Place.indexOf(place));
	
	var left = placeElement.css('left');
	var top = parseInt(placeElement.css('top'))+VAN_DISPLACEMENT_Y;
	return [left, ""+top+"px"];
}

function isAdjacentPlace(place, destinationPlace) {
	var result = false;
	$.each(scene.Paths.Path, function(index, element){
		if (element.from == place.id)
		{
			//look in "to"
			var adjacents = element.to.split(",");
			$.each(adjacents, function(j, adj){
				if (adj==destinationPlace.id)
				{
					result = true;
					return false;
				}
			});
			if (result) return false;
		}
	});
	
	return result;
};

function hightlightPlace(place, highlight){
	var placeElement = $("#place"+place.id);
	var border = '0px';
	if (highlight)
	{
		border = '4px dashed red';
	}
	placeElement.css('outline', border);
};

function highlightAdjacentsToVan(){
	$.each(places.Place, function(index, element){
		hightlightPlace(element, isAdjacentPlace(currentVanPlace, element));
	});
};

function moveVanToPlaceAtIndex(index) {
	if (!allowVanMovements) return;
	moveVanToPlace(places.Place[index]);
};

function forceMoveVanToPlace(place) {
	currentVanPlace = place;
	var vanElement = $('#vanImage');
	vanElement.fadeOut('slow', function(evt){
		var pos = vanPositionForPlace(place);
		vanElement.css('left', pos[0]);
		vanElement.css('top', pos[1]);
		vanElement.fadeIn('slow', function(evt2){
			if (checkForId == place.id)
			{
				nextStepIntro();
				checkForId = null;
			}
		});
		highlightAdjacentsToVan();
	});
};

function moveVanToPlace(place){
	if (isAdjacentPlace(currentVanPlace, place))
	{
		$('#mediaAlert').modal('hide');
		forceMoveVanToPlace(place);
	}
	else{
		$('#mediaAlert').modal({show:false});
		$("#mediaBody").html("<?php echo _('<strong>Recuerda:</strong> Solo puedes moverte por los lugares por los que va pasando la furgoneta para llegar al sitio al que debes ir. Están marcados con un borde <strong>rojo</strong>.'); ?>");
		$('#mediaAlert').modal('show');
	}
};

function vanHTMLForPlaceIndex(index) {
	currentVanPlace = places.Place[index];
	var pos = vanPositionForPlace(currentVanPlace);
	var width=100;
	var height=100;
	var vanHTML = "<div id='vanImage' class='package-delivery-van' style='left:"+pos[0]+"; top:"+pos[1]+";width:"+width+"px; height:"+height+"px;'><img src='exercises/img/PackageDeliveryExercise/orangevan4.png' width='100%' height='100%'/></div>";
	return vanHTML;
};

function nextStepIntro() {
	$("#introExercise").fadeOut('slow', function(evt){
		currentIntroStep++;
		var showIntro = false;
		
		showInstructions(false);
		switch(currentIntroStep)
		{
			case 1:{
				$("#exercise-description").html('<?php echo _('Vamos a practicar para que lo entiendas mejor. La furgoneta ahora mismo está en la <strong>Tienda de Deportes</strong> y debes ir a la <strong>Librería</strong>. Recuerda que debes pasar antes por la <strong>Agencia de Viajes</strong>. Pulsa en los edificios marcados con <span style="color:red">borde rojo</span> para desplazarte. Recuerda que la furgoneta puede moverse hacia delante y hacia atrás.'); ?>');
				showIntro = true;
				$("#mediaPanel").html($("#mediaPanel").html()+vanHTMLForPlaceIndex(0));
				if (currentVanPlace != places.Place[0]) forceMoveVanToPlace(places.Place[0]);
				$("#introNextStep").hide();
				
				checkForId = "1";
				highlightAdjacentsToVan();
				break;
			}
			case 2:{
				if (currentVanPlace != places.Place[1]) forceMoveVanToPlace(places.Place[1]);
				allowVanMovements = false;
				$("#exercise-description").html("<?php echo _('¡Genial! Ya sabes moverte por la ciudad. Ahora fíjate, en la parte de arriba de la ciudad podrás ver el nivel de gasolina que tienes y los paquetes que llevas en la furgoneta. Deberás pensar muy bien a qué lugares ir antes y a cuales después para gastar la menor gasolina posible. Pulsa <strong>Continuar</strong>.'); ?>");
				buildBoxes();
				$("#introNextStep").show();
				$("#vanStatusPanel").show();
			
				showIntro = true;
				break;
			}
			case 3:{
				if (currentVanPlace != places.Place[1]) forceMoveVanToPlace(places.Place[1]);
				$("#exercise-description").html("<?php echo _('Para entregar y recoger los paquetes, deberás pulsar sobre ellos. Prueba a entregar <strong>uno de los paquetes</strong> que están en la furgoneta.'); ?>");
				waitForBoxPlacement = true;
				$("#introNextStep").hide();
			
				showIntro = true;
				break;
			}
			case 4:{
				$("#exercise-description").html("<?php echo _('&iexcl;Muy bien! Observa que el paquete se ha puesto en el lugar donde está la furgoneta. Ya estás listo para iniciar el ejercicio. Pulsa sobre el botón <strong>Comenzar ejercicio</strong> para comenzarlo.'); ?>");
				$("#introNextStep").html("<?php echo _('Comenzar ejercicio'); ?>");
				$("#introNextStep").show();
				showIntro = true;
				break;
			}
			default: endDemo(); break;
		}
		
		if (showIntro) $("#introExercise").fadeIn('slow');
	});
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	$.ajax({
		type: "GET",
		url: placesXML,
		dataType: "xml",
		success: function(placesXMLResult) {
			places = $.xml2json(placesXMLResult);
			$.ajax({
				type: "GET",
				url: planXML,
				dataType: "xml",
				success: function(planXMLResult) {
					var plans = $.xml2json(planXMLResult);
					$.ajax({
						type: "GET",
						url: scenesXML,
						dataType: "xml",
						success: function(scenesXMLResult) {
							var scenes = $.xml2json(scenesXMLResult);
							
							//look for the actual plan
							var plan = null;
							$.each(plans.PackageDeliveryPlanEntry, function(index, element){
								if (element.sessionID == sessionID && element.exerciseID == exerciseID)
								{
									plan = element;
									return false;
								}
							});
							
							//take the scene
							$.each(scenes.Scene, function(index, element){
								if (element.id == plan.scene)
								{
									scene = element;
									return false;
								}
							});
							
							//takeout the not used places
							var actualPlaces = scene.Places.ids.split(",");
							var finalPlacesArray = [];
							$.each(places.Place, function(index, element){
								if (actualPlaces.indexOf(element.id) != -1)
								{
									finalPlacesArray.push(element);
								}
							});
							
							places.Place = finalPlacesArray;
							currentFuelSteps = MAX_FUEL_STEPS;
							
							buildDeliveries();
							
							//show the panels
							$("#mediaPanel").css('width', places.width);
							$("#mediaPanel").css('height', 300); //cut out more or less about a half
							$("#mediaPanel").html(placesHTML());
							$("#vanStatusPanel").css('width', places.width);
							$("#introExercise").fadeIn('slow');
						}
					});
				}
			});
			
		}
	});
});

</script>
