<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!PositionsExercise) {

var PositionsExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Test de Posiciones!'); ?>', '<?php echo _('&iexcl;Estupendo! En este ejercicio te voy a mostrar un edificio con filas de ventanas. En algunas de ellas aparecerán personas y deberás memorizar en qué lugar están. Una vez que se cierren de nuevo todas las ventanas deberás marcar dónde crees que aparecieron estas personas.'); ?>', 'exercises/PositionsExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._omissions = 0;
		this._seconds = 0;
		this._testResults = [];
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	setOmissions: function(f){
		this._omissions = f;
	},
	omissions: function(f){
		return this._omissions;
	},
	updateResults : function() {
		this.finishExercise();
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	demoUIurl : function() {
		//override to return the demo UI
		/*if (parseInt(this.sessionID())==2) return "exercises/PositionsExerciseDemoUI.php";
		else return null;*/
		return null;
	},
	repetition : function() {
		return this._repetition;
	},
	setRepetition : function(rep){
		this._repetition = rep;
	},
	updateHelpLevel : function(level, seqCorrects, elems){
		this._testResults.push(JSON.stringify({"helpLevel":level, "time": this._seconds, "corrects": this._corrects, "fails": this._fails, "omissions":this._omissions, "sequentialCorrects": seqCorrects, "fullmap":elems.join("|")}));
		//
		this.partialCommitHelpLevel();
	},
	partialCommitHelpLevel : function(){
		this.updatePartialResult(JSON.stringify(this._testResults), this._repetition);
	},
	commitHelpLevel : function(){
		this.updatePartialResult(JSON.stringify(this._testResults), this._repetition);
		this._testResults = [];
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_omissions_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
