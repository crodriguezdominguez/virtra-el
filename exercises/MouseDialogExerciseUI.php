<?php
	require_once('locale/localization.php');
?>


<a id="finalizeButton" href="javascript:void(0);" onclick="javascript:endExercise();" class="btn btn-primary btn-large hide"><?php echo _('Continuar'); ?></a>

<a id="continueButton" href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>

<div id="testArea" class="hide" style="position:relative;width:100%; margin:0; height:250px; padding:0px;">
	<div id="errorArea" style="position:absolute;z-index:0;width:100%; margin:0; height:100%; padding:0px; background-color:white;" onclick="javascript:increaseError();"></div>
	<a id="clickMe" style="z-index: 10; position: absolute;" href="javascript:void(0);" onclick="javascript:checkExercise();" class="btn btn-primary btn-large"><?php echo _('&iexcl;P&uacute;lsame!'); ?></a>
</div>

<script type="text/javascript">
	var repetitions = null;
	var sessionID = null;
	var exerciseID = null;

	var consecutiveCorrects = 0;

    var timer = new Timer();

	function generateRandomLocation()
	{
		var x = Math.floor(getRandom()*80);
		var y = Math.floor(getRandom()*80);
		
		$("#clickMe").css('left', ''+x+'%');
		$("#clickMe").css('top', ''+y+'%');
	}
	
	function checkExercise()
	{
		exercise.increaseCorrects();
		consecutiveCorrects++;
		
		if (consecutiveCorrects >= 6)
		{
			timer.stop();
			exercise.setSeconds(timer.getSeconds());
			
			$('#continueButton').hide();
			$('#testArea').fadeOut('slow');
			$('#exercise-description').fadeOut('slow', function(){
				$('#exercise-description').html("<?php echo _('<strong>&iexcl;Enhorabuena! &iexcl;Lo has hecho muy bien!</strong>  Ahora continuaremos con VIRTRA-EL. Pulsa en el bot&oacute;n <strong>Continuar</strong> cuando est&eacute;s listo.'); ?>");
				$('#exercise-description').fadeIn('slow');
				$('#finalizeButton').fadeIn('slow');
			});
			
		}
		else
		{
			$("#clickMe").fadeOut('fast', function(){
				generateRandomLocation();
				$("#clickMe").fadeIn('fast');
			});
		}
	}
	
	function increaseError(){
		exercise.increaseFailures();
		consecutiveCorrects = 0;
		
		$("#errorArea").animate({"background-color":"red"}, 'fast', function(){
			$("#errorArea").animate({"background-color":"white"}, 'fast');
		});
	}
	
	function beginExercise()
	{
		$("#continueButton").fadeOut('slow', function(){
			$("#exercise-description").html('<?php echo _('Pulsa en el bot&oacute;n que pone <strong>&iexcl;P&uacute;lsame!</strong> Trata de no pulsar fuera de ese bot&oacute;n.'); ?>');
			
			$("#testArea").fadeIn('slow', function(){
				timer.start();
			})
		});
	}
	
	$(function() {
		repetitions = parseInt(lastSession['repetitions']);
		sessionID = parseInt(lastSession['sessionID']);
		exerciseID = parseInt(lastSession['exerciseID']);
		
		if (exercise.repetition() > 0)
		{
			$("#exercise-description").html("<?php echo _('Continuamos con el ejercicio. Recuerda que debes pulsar los botones que aparezcan en pantalla.'); ?>");
		}
		
		/*
		if (repetitions >= 1)
		{
			$('#exercise-description').html("<?php echo _('Continuamos con el ejercicio. Recuerda que debes seleccionar aquellas imágenes que tengan <strong>una</strong> pirámide grande y <strong>dos</strong> pequeñas con puerta en el <strong>lado soleado</strong>. Debajo puedes ver varios ejemplos del tipo de imágenes que debes seleccionar.'); ?>");
		}*/
	});
</script>