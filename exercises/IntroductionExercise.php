<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!IntroductionExercise) {

var IntroductionExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('Introducción a VIRTRA-EL'); ?>', '<?php echo _('Hoy es tu primer d&iacute;a, as&iacute; que te explicaremos cómo funciona VIRTRA-EL y cu&aacute;l ser&aacute; tu plan de trabajo.'); ?>', 'exercises/IntroductionExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
	}
});

}
</script>