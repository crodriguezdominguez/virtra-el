<?php
require_once('locale/localization.php');
?>

<div id="introExercise" class="hide">
    <p><a id="introNextStep" href="javascript:void(0);" onclick="javascript:nextStepIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<table id="vanStatusPanel" class="well package-delivery-van-status-panel hide">
    <tr>
        <td class="package-delivery-van-status">
            <p>
                <span style="position:absolute;text-align:center;width:180px;margin-top:8px;font-size:24px;"><?php echo _('Gasolina'); ?></span>
                <img style="height:35px" width='180px' height='40px' id='fuelStatus' src='exercises/img/PackageDeliveryExercise/fuel100.png' alt='<?php echo _('Nivel de gasolina al ') ?> 100%' />
            </p>
            <p style="text-align:left;margin-bottom:1px">
                <span style="margin-left:8px;font-size:18px;"><b><?php echo _('Maletero:'); ?></b></span>
            </p>
            <div class="package-delivery-van-box"><a src="javascript:void(0);" id="box0" onclick="javascript:dropBox(0);"><img id="boxImg0" class="thumbnail" src='exercises/img/PackageDeliveryExercise/boxempty.png' alt='' width="50px" height="50px"></a></div>
            <div class="package-delivery-van-box"><a src="javascript:void(0);" id="box1" onclick="javascript:dropBox(1);"><img id="boxImg1" class="thumbnail" src='exercises/img/PackageDeliveryExercise/boxempty.png' alt='' width="50px" height="50px"></a></div>
            <div class="package-delivery-van-box"><a src="javascript:void(0);" id="box2" onclick="javascript:dropBox(2);"><img id="boxImg2" class="thumbnail" src='exercises/img/PackageDeliveryExercise/boxempty.png' alt='' width="50px" height="50px"></a></div>
        </td>
        <!--<td class="package-delivery-game-description-title"><?php echo _('<strong>Entregas</strong>'); ?></td>-->
        <td id="deliveries" class="package-delivery-game-description-entries"></td>
        <!--<td class="package-delivery-game-description-title"><?php echo _('<strong>Recogidas</strong>'); ?></td>-->
        <td id="pickups" class="package-delivery-game-description-entries"></td>
    </tr>
</table>
<div id="mediaContainer" style="position:relative;">
    <div class="modal hide fade" id="mediaAlert">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3><?php echo _('Error'); ?></h3>
        </div>
        <div class="modal-body lead" id="mediaBody">
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo _('Cerrar'); ?></button>
        </div>
    </div>
    <div id="mediaPanel" style="position:relative;margin-bottom:10px;">
    </div>
</div>


<script type="text/javascript">
    var repetitions = null;
    var sessionID = null;
    var exerciseID = null;
    var placesXML = "exercises/xml/package-delivery-places.php";
    var planXML = "exercises/xml/package-delivery-plan.xml";
    var scenesXML = "exercises/xml/package-delivery-scenes.php";
    var places = null;
    var scene = null;
    var currentIntroStep = 0;
    var currentVanPlace = null;
    var checkForId = null;
    var deliveries = null;
    var pickups = null;
    var boxesInVan = [];
    var boxesAtPlaces = [];
    var VAN_DISPLACEMENT_Y = 25;
    var timing = new Timer();
    var currentFuelSteps = 0;
    var MAX_FUEL_STEPS = 30;
    var timeUntilFirst = 0;
    var vanMovements = 0;
    var packageMovements = 0;

    function checkExercise()
    {
        swal({
                title: "<?php echo _('¿Estás seguro?'); ?>",
                text: "<?php echo _('¿Seguro que deseas finalizar el ejercicio?'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo _('Si, terminar'); ?>",
                cancelButtonText: "<?php echo _('No terminar'); ?>",
                closeOnConfirm: true
            },
            function(isConfirm){
                if (isConfirm)
                {
                    $("#exercise-mini-description").html("");
                    timing.stop();
                    exercise.setSeconds(timing.getSeconds());

                    var corrects = 0;
                    var fails = 0;
                    var omissions = 0;

                    var allBoxes = boxesAtPlaces.concat(boxesInVan);

                    $.each(allBoxes, function(index, box){
                        if (box.actualplace == box.goalplace)
                        {
                            corrects++;
                        }
                        else if (box.actualplace == box.initialplace) omissions++;
                        else fails++;
                    });

                    exercise.setCorrects(corrects);
                    exercise.setFails(fails);
                    exercise.setOmissions(omissions);
                    var score = 0;
                    if (fails>0 || omissions>0) score = 0;
                    else
                    {
                        var totalSteps = MAX_FUEL_STEPS-currentFuelSteps;
                        if (totalSteps<15) score = 100;
                        else if (totalSteps<20) score = 60;
                        else score = 1;
                    }

                    exercise.setFinalScore(score);
                    exercise.updatePartialResult(JSON.stringify({"time":timeUntilFirst, "vanMovements":vanMovements, "packageMovements":packageMovements}), repetitions);

                    timeUntilFirst = 0;

                    endExercise();
                }
            });
    };

    function buildDeliveries() {
        deliveries = [];
        pickups = [];
        boxesAtPlaces = [];
        boxesInVan = [];
        $.each(scene.Boxes.Box, function(index, element){
            if (element.inshop=="1"){
                boxesAtPlaces.push({id:element.id, name:element.name, initialplace:element.initialplace, goalplace:element.goalplace, img:element.img, actualplace:element.initialplace});
                pickups.push(element);
            }
            else {
                boxesInVan.push({id:element.id, name:element.name, initialplace:element.initialplace, goalplace:element.goalplace, img:element.img, actualplace:element.initialplace});
                deliveries.push(element);
            }
        });

        var descriptionResult = "<h3><?php echo _('Paquetes para entregar'); ?></h3><ul id='deliveriesList' class='unstyled'>";
        $.each(deliveries, function(index, element){
            var goalPlaceName = "";
            for (var i=0; i<places.Place.length; i++)
            {
                if (places.Place[i].id==element.goalplace)
                {
                    goalPlaceName = places.Place[i].name;
                    break;
                }
            }
            descriptionResult += "<li>"+element.name+" <?php echo _('en'); ?> <strong>"+goalPlaceName+"</strong></li>";
        });

        $("#deliveries").html(descriptionResult+"</ul>");

        descriptionResult = "<h3><?php echo _('Paquetes para recoger'); ?></h3><ul id='pickupsList' class='unstyled'>";
        $.each(pickups, function(index, element){
            var initialPlaceName = "";
            for (var i=0; i<places.Place.length; i++)
            {
                if (places.Place[i].id==element.initialplace)
                {
                    initialPlaceName = places.Place[i].name;
                    break;
                }
            }
            descriptionResult += "<li>"+element.name+" <?php echo _('en'); ?> <strong>"+initialPlaceName+"</strong></li>";
        });

        $("#pickups").html(descriptionResult+"</ul>");
    };

    function markListElementByBoxAndPlace(boxName, placeID, mark, listID)
    {
        var placeName = "";

        for (var i=0; i<places.Place.length; i++)
        {
            if (places.Place[i].id==placeID)
            {
                placeName = places.Place[i].name;
                break;
            }
        }

        $.each($(listID).children(), function(i,e){
            if ($(e).html() == boxName+" <?php echo _('en'); ?> <strong>"+placeName+"</strong>")
            {
                var decoration = mark?'line-through':'none';
                $(e).css('text-decoration', decoration);

                if (decoration == 'line-through')
                {
                    /*$(e).css('text-decoration-style', 'double');
                     $(e).css('-webkit-text-decoration-style', 'double');
                     $(e).css('-moz-text-decoration-style', 'double');
                     $(e).css('-ms-text-decoration-style', 'double');
                     $(e).css('-ie-text-decoration-style', 'double');
                     $(e).css('-o-text-decoration-style', 'double');*/

                    $(e).css('text-decoration-color', 'blue');
                    $(e).css('-webkit-text-decoration-color', 'blue');
                    $(e).css('-moz-text-decoration-color', 'blue');
                    $(e).css('-ms-text-decoration-color', 'blue');
                    $(e).css('-ie-text-decoration-color', 'blue');
                    $(e).css('-o-text-decoration-color', 'blue');
                }
                else
                {
                    /*$(e).css('text-decoration-style', 'none');
                     $(e).css('-webkit-text-decoration-style', 'none');
                     $(e).css('-moz-text-decoration-style', 'none');
                     $(e).css('-ms-text-decoration-style', 'none');
                     $(e).css('-ie-text-decoration-style', 'none');
                     $(e).css('-o-text-decoration-style', 'none');*/

                    $(e).css('text-decoration-color', 'none');
                    $(e).css('-webkit-text-decoration-style', 'none');
                    $(e).css('-moz-text-decoration-color', 'none');
                    $(e).css('-ms-text-decoration-color', 'none');
                    $(e).css('-ie-text-decoration-color', 'none');
                    $(e).css('-o-text-decoration-color', 'none');
                }
            }
        });
    };

    function dropBox(index) {
        $('#mediaAlert').modal('hide');

        if (index < boxesInVan.length)
        {
            var box = boxesInVan[index];
            var put = true;
            $.each(boxesAtPlaces, function(index, element){
                if (element.actualplace == currentVanPlace.id)
                {
                    $('#mediaAlert').modal({show:false});
                    $("#mediaBody").html("<?php echo _('No puedes repartir en un mismo lugar más de un paquete.'); ?>");
                    $('#mediaAlert').modal('show');
                    put = false;
                    return false;
                }
            });

            if (put)
            {
                packageMovements++;

                box.actualplace = currentVanPlace.id;
                boxesAtPlaces.push(box);
                boxesInVan.splice(index, 1);

                markListElementByBoxAndPlace(box.name, box.actualplace, false, "#pickupsList");
                markListElementByBoxAndPlace(box.name, box.actualplace, true, "#deliveriesList");

                buildBoxes();
            }
        }
    };

    function pickupBox(placeID) {
        if (currentVanPlace.id == placeID)
        {
            if (boxesInVan.length >= 3)
            {
                $('#mediaAlert').modal({show:false});
                $("#mediaBody").html("<?php echo _('No puedes llevar en la furgoneta más de tres paquetes.'); ?>");
                $('#mediaAlert').modal('show');
                return;
            }

            $('#mediaAlert').modal('hide');

            packageMovements++;

            for (var i=0; i<boxesAtPlaces.length; i++)
            {
                if (boxesAtPlaces[i].actualplace == placeID)
                {
                    boxesAtPlaces[i].actualplace = "-1";

                    markListElementByBoxAndPlace(boxesAtPlaces[i].name, placeID, false, "#deliveriesList");
                    markListElementByBoxAndPlace(boxesAtPlaces[i].name, placeID, true, "#pickupsList");

                    boxesInVan.push(boxesAtPlaces[i]);
                    boxesAtPlaces.splice(i, 1);
                    break;
                }
            }

            buildBoxes();
        }
    };

    function buildBoxes() {
        //remove all the existing boxes
        $('[id^="placedBox"]').remove();
        $('[id^="boxImg"]').attr('src', 'exercises/img/PackageDeliveryExercise/boxempty.png');
        $('[id^="boxImg"]').attr('alt', '');

        //place the boxes in the van
        $.each(boxesInVan, function(index, box){
            $("#boxImg"+index).attr('src', box.img);
            $("#boxImg"+index).attr('alt', box.name);
        });

        //place the boxes in the scene
        $.each(boxesAtPlaces, function(index, box){
            $.each(places.Place, function(j, place){
                if (box.actualplace == place.id)
                {
                    var frame = frameOfPlace(place);
                    var boxSize = 50;
                    var boxHTML = "<div onclick='javascript:pickupBox("+place.id+");' id='placedBox"+index+"' style='margin:0;padding:0;position:absolute; left:"+(frame[0]+frame[2]-boxSize/2)+"px; top:"+(frame[1]+frame[3]-boxSize)+"px; width:"+boxSize+"px; height:"+boxSize+"px;'>"
                    boxHTML += "<img width='"+boxSize+"px' height='"+boxSize+"px' src='"+box.img+"' alt='"+box.name+"' /></div>";
                    $("#mediaPanel").append(boxHTML);
                }
            });
        });
    };

    function frameOfPlaceAtIndex(index) {
        return frameOfPlace(places.Place[index]);
    };

    function frameOfPlace(place) {
        var x = parseInt(place.x);
        var y = parseInt(place.y);
        var w = parseInt(place.w);
        var h = parseInt(place.h);
        return [x,y,w,h];
    };

    function placeHTML(place) {
        var index = places.Place.indexOf(place);
        if (index != -1)
        {
            var frame = frameOfPlace(place);
            var placeHTML = "<div onclick='javascript:moveVanToPlaceAtIndex("+index+");' class='package-delivery-place' id='place"+index+"' style='left:"+frame[0]+"px; top:"+frame[1]+"px; width:"+frame[2]+"px; height:"+frame[3]+"px;'>"
            placeHTML += "<img width='"+frame[2]+"px' height='"+frame[3]+"px' src='"+place.img+"' alt='"+place.name+"' /></div>";
            return placeHTML;
        }
        else return "";
    };

    function placesHTML() {
        var pHTML = "<img alt='<?php echo _('Ciudad'); ?>' src='"+places.background+"' width='100%' height='100%' />";
        $.each(places.Place, function(index, element){
            pHTML = pHTML+placeHTML(element);
        });

        return pHTML;
    };

    function vanPositionForPlace(place) {
        var placeElement = $("#place"+places.Place.indexOf(place));

        var left = placeElement.css('left');
        var top = parseInt(placeElement.css('top'))+VAN_DISPLACEMENT_Y;
        return [left, ""+top+"px"];
    }

    function isAdjacentPlace(place, destinationPlace) {
        var result = false;
        $.each(scene.Paths.Path, function(index, element){
            if (element.from == place.id)
            {
                //look in "to"
                var adjacents = element.to.split(",");
                $.each(adjacents, function(j, adj){
                    if (adj==destinationPlace.id)
                    {
                        result = true;
                        return false;
                    }
                });
                if (result) return false;
            }
        });

        return result;
    };

    function hightlightPlace(place, highlight){
        var placeElement = $("#place"+place.id);
        var border = '0px';
        if (highlight)
        {
            border = '4px dashed red';
        }
        placeElement.css('outline', border);
    };

    function highlightAdjacentsToVan(){
        $.each(places.Place, function(index, element){
            hightlightPlace(element, isAdjacentPlace(currentVanPlace, element));
        });
    };

    function moveVanToPlaceAtIndex(index) {
        if (timeUntilFirst <= 0)
        {
            timeUntilFirst = timing.getSeconds();
        }

        vanMovements++;

        moveVanToPlace(places.Place[index]);
    };

    function forceMoveVanToPlace(place) {
        if (currentFuelSteps <= 0)
        {
            $('#mediaAlert').modal({show:false});
            $("#mediaBody").html("<?php echo _('No tienes más gasolina, por lo que no puedes desplazarte a otros lugares. Pulsa en <strong>finalizar ejercicio</strong> cuando lo desees.'); ?>");
            $('#mediaAlert').modal('show');

            return;
        }
        currentVanPlace = place;
        var vanElement = $('#vanImage');
        vanElement.fadeOut('slow', function(evt){
            var pos = vanPositionForPlace(place);
            vanElement.css('left', pos[0]);
            vanElement.css('top', pos[1]);
            vanElement.fadeIn('slow');
            highlightAdjacentsToVan();

            currentFuelSteps--;
            //calculate fuel level
            var perc = currentFuelSteps/MAX_FUEL_STEPS;
            var img = "exercises/img/PackageDeliveryExercise/fuel100.png";
            if (perc <= 0.8 && perc > 0.6) img = "exercises/img/PackageDeliveryExercise/fuel80.png";
            else if (perc <= 0.6 && perc > 0.4) img = "exercises/img/PackageDeliveryExercise/fuel60.png";
            else if (perc <= 0.4 && perc > 0.2) img = "exercises/img/PackageDeliveryExercise/fuel40.png";
            else if (perc <= 0.2 && perc > 0) img = "exercises/img/PackageDeliveryExercise/fuel20.png";
            else if (perc <= 0) img = "exercises/img/PackageDeliveryExercise/fuel0.png";

            $("#fuelStatus").attr('src', img);
        });
    };

    function moveVanToPlace(place){
        if (isAdjacentPlace(currentVanPlace, place))
        {
            $('#mediaAlert').modal('hide');
            forceMoveVanToPlace(place);
        }
        else{
            $('#mediaAlert').modal({show:false});
            $("#mediaBody").html("<?php echo _('<strong>Recuerda:</strong> Solo puedes moverte por los lugares por los que va pasando la furgoneta para llegar al sitio al que debes ir. Están marcados con un borde <strong>rojo</strong>.'); ?>");
            $('#mediaAlert').modal('show');
        }
    };

    function vanHTMLForPlaceIndex(index) {
        currentVanPlace = places.Place[index];
        var pos = vanPositionForPlace(currentVanPlace);
        var width=100;
        var height=100;
        var vanHTML = "<div id='vanImage' class='package-delivery-van' style='left:"+pos[0]+"; top:"+pos[1]+";width:"+width+"px; height:"+height+"px;'><img src='exercises/img/PackageDeliveryExercise/orangevan4.png' width='100%' height='100%'/></div>";
        return vanHTML;
    };

    function nextStepIntro() {
        $("#introExercise").fadeOut('slow', function(evt){
            showMiniMenu(false);
            $("#exercise-description").html("<?php echo _('Reparte todos los paquetes con tu furgoneta. Recuerda que la furgoneta puede moverse hacia delante y hacia atrás. Pulsa en el botón <strong>Finalizar ejercicio</strong> cuando hayas terminado.'); ?>");
            $("#exercise-mini-description").html("<a class='btn btn-primary' href='javascript:void(0);' onclick='javascript:checkExercise();'><?php echo _('Finalizar ejercicio'); ?></a>");
            $("#finalizeExercise").show();
            $("#mediaPanel").html(placesHTML()+vanHTMLForPlaceIndex(0));
            buildDeliveries();
            buildBoxes();
            highlightAdjacentsToVan();
            $("#vanStatusPanel").show();
            timing.start();
        });
    };

    function replayExercise() {
        repetitions++;
    };

    $(function() {
        repetitions = parseInt(lastSession['repetitions']);
        sessionID = parseInt(lastSession['sessionID']);
        exerciseID = parseInt(lastSession['exerciseID']);

        $.ajax({
            type: "GET",
            url: placesXML,
            dataType: "xml",
            success: function(placesXMLResult) {
                places = $.xml2json(placesXMLResult);
                $.ajax({
                    type: "GET",
                    url: planXML,
                    dataType: "xml",
                    success: function(planXMLResult) {
                        var plans = $.xml2json(planXMLResult);
                        $.ajax({
                            type: "GET",
                            url: scenesXML,
                            dataType: "xml",
                            success: function(scenesXMLResult) {
                                var scenes = $.xml2json(scenesXMLResult);

                                //look for the actual plan
                                var plan = null;
                                $.each(plans.PackageDeliveryPlanEntry, function(index, element){
                                    if (element.sessionID == sessionID && element.exerciseID == exerciseID)
                                    {
                                        plan = element;
                                        return false;
                                    }
                                });

                                //take the scene
                                $.each(scenes.Scene, function(index, element){
                                    if (element.id == plan.scene)
                                    {
                                        scene = element;
                                        return false;
                                    }
                                });

                                //takeout the not used places
                                var actualPlaces = scene.Places.ids.split(",");
                                var finalPlacesArray = [];
                                $.each(places.Place, function(index, element){
                                    if (actualPlaces.indexOf(element.id) != -1)
                                    {
                                        finalPlacesArray.push(element);
                                    }
                                });

                                places.Place = finalPlacesArray;
                                currentFuelSteps = MAX_FUEL_STEPS;

                                buildDeliveries();

                                //show the panels
                                $("#mediaPanel").css('width', places.width);
                                $("#mediaPanel").css('height', places.height);
                                $("#mediaPanel").html(placesHTML());
                                $("#vanStatusPanel").css('width', places.width);
                                if (!exercise.afterDemo())
                                {
                                    $("#introExercise").fadeIn('slow');
                                }
                                else
                                {
                                    nextStepIntro();
                                }
                            }
                        });
                    }
                });

            }
        });
    });

</script>
