<?php
	require_once('locale/localization.php');
?>

<!-- Thumbnails -->
<div id="thumbs" class="row-fluid">
	<ul class="thumbnails">
		<li class="span12">
			<div class="thumbnail">
				<!--<img src="exercises/img/IntroductionExercise/12-sessions.png" alt="<?php echo _('sesiones de una hora'); ?>" class="pull-left" style="margin-top:20px;width:25%;height:auto;" />-->
				<div class="caption lead">
					<h1><?php echo _('Organizaci&oacute;n'); ?></h1>
					<p><?php echo _('Durante 15 sesiones, de aproxim&aacute;damente una hora cada una, realizar&aacute;s ejercicios para estimular diferentes habilidades: memoria, atenci&oacute;n, razonamiento y planificaci&oacute;n. Una vez terminadas las sesiones, tendrás acceso a ejercicios colaborativos, para hacerlos con otras personas que usen VIRTRA-EL.'); ?></p>
					<a onclick="javascript:changeReadButton($(this));" class="btn btn-info btn-large" href="javascript:void(0);"><?php echo _('Le&iacute;do'); ?></a>
				</div>
			</div>
		</li>
		<li class="span12 hide">
			<div class="thumbnail">
				<!--<img src="exercises/img/IntroductionExercise/dont-stop.png" alt="<?php echo _('no interrumpir las sesiones'); ?>" class="pull-left" style="margin-top:20px;width:25%;height:auto;" />-->
				<div class="caption lead">
					<h1><?php echo _('Planificaci&oacute;n'); ?></h1>
					<p><?php echo _('Puedes interrumpir las sesiones en cualquier momento, aunque se te recomienda que si comienzas una, intentes completarla en el mismo d&iacute;a. Por favor, trata de realizar cada <strong>sesión al completo</strong>, sin interrumpirlas. Por ejemplo, puedes hacer cada sesión en un día distinto.'); ?></p>
					<a onclick="javascript:changeReadButton($(this));" class="btn btn-info btn-large" href="javascript:void(0);"><?php echo _('Le&iacute;do'); ?></a>
				</div>
			</div>
		</li>
		<li class="span12 hide">
			<div class="thumbnail">
				<!--<img src="exercises/img/IntroductionExercise/execution.png" alt="<?php echo _('ayuda antes de cada ejercicio, prueba antes del ejercicio y ejecucion real del ejercicio'); ?>" class="pull-left" style="margin-top:20px;width:25%;height:auto;" />-->
				<div class="caption lead">
					<h1><?php echo _('Ejecuci&oacute;n'); ?></h1>
					<p><?php echo _('Antes de hacer cada ejercicio se te explicar&aacute; en qu&eacute; consiste, se te har&aacute; una prueba para que practiques, y despu&eacute;s tendr&aacute;s que hacer el ejercicio t&uacute; solo.'); ?></p>
					<a onclick="javascript:changeReadButton($(this));" class="btn btn-info  btn-large" href="javascript:void(0);"><?php echo _('Le&iacute;do'); ?></a>
				</div>
			</div>
		</li>
	</ul>
</div>

<div id="knowledge-id" class="hide">
	<div style="width:100%">
		<center><p class="lead"><?php echo _('&iquest;Alguna vez has usado un ordenador?'); ?></p></center>
	</div>
    <div style="width: 100%; margin-top: 20px;">
		<center><a href="javascript:void(0);" onclick="javascript:nextButton(true);" class="btn btn-success btn-large"><i class="icon-thumbs-up icon-white"></i> <?php echo _('S&iacute;'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="javascript:nextButton(false);" class="btn btn-danger btn-large"><i class="icon-thumbs-down icon-white"></i> <?php echo _('No'); ?></a></center>
	</div>
</div>

<div id="agreement-id" class="hide">
	<p class="lead">
		<p class="lead"><?php echo _('He le&iacute;do el consentimiento informado y estoy de acuerdo en que se utilicen los datos recopilados en VIRTRA-EL con fines estad&iacute;sticos o para ayudar en mi terapia.'); ?></p>
		<br />
		<center><button id="consent-true" class="btn btn-success btn-large" onclick="javascript:toogleCheckbox(true);"><i class="icon-thumbs-up icon-white"></i> <?php echo _('Sí, estoy de acuerdo'); ?></button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="consent-false" class="btn btn-danger btn-large" onclick="javascript:toogleCheckbox(false);"><i class="icon-thumbs-down icon-white"></i> <?php echo _('No estoy de acuerdo'); ?></button></center>
		<br />
		<div id="continueButton" class="hide" style="margin-bottom: 20px;text-align: center;">
			<a href="javascript:void(0);" onclick="javascript:endExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
		</div>
		<br />
		<center><iframe src="js/pdfjs/web/viewer.html?file=../../../downloads/<?php echo _('consentimientoinformado.pdf'); ?>" width="95%" height="920" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe></center>
	</p>
</div>

<script type="text/javascript">
var readCount = 0;
var acceptedConsent = false;

$("#exercise-description").html("<?php echo _('Lee atentamente la siguiente información. Deberás pulsar sobre el botón <strong>Leído</strong> para poder continuar.'); ?>");

function toogleCheckbox(accept)
{
	if (accept)
	{
		$("#consent-true").addClass('disabled');
		$("#consent-false").removeClass('disabled');
		$('#continueButton').fadeIn('slow');
	}
	else
	{
		$("#consent-false").addClass('disabled');
		$("#consent-true").removeClass('disabled');
		$('#continueButton').fadeOut('slow');
	}
};

function nextButton(ok)
{
	var check = ok?'1':'0';
	
	var qry = 'computerUse='+check;
	var obj = this;
	
	$.ajax({
		type: 'POST',
		data: qry,
		async: true,
		url: 'backend/update_computer_use.php',
		success: function(data){
		}
	});
	
	$("#knowledge-id").fadeOut('slow', function() {
		$("#exercise-description").fadeOut('slow', function(){
			$("#exercise-description").html("<?php echo _('Para participar en este proyecto debes leer el documento que se te muestra abajo, es un consentimiento informado.'); ?>");
			$("#exercise-description").fadeIn('slow');
			$("#agreement-id").fadeIn('slow');
		});
	});
}


function changeReadButton(but)
{
	but.removeClass('btn-info').addClass('btn-success');
	but.addClass('disabled');
	but.attr('onclick', 'javascript:void(0);');
	var html = but.html();
	but.html("<i class='icon-ok icon-white'></i>"+" "+html);
	
	var parents = but.parents('li');
	if (parents != null && parents.length > 0)
	{
		var nextSib = parents.next();
		
		if (nextSib != null && nextSib.length > 0)
		{
			parents.fadeOut('slow', function(){
				nextSib.css('margin-left', '0px').fadeIn('fast');
			});
		}
		else
		{
			parents.fadeOut('slow');
		}
	}
	
	readCount++;
	if (readCount >= 3)
	{
		$("#thumbs").fadeOut('slow', function(){
			$("#exercise-description").fadeOut('slow', function(){
				$("#exercise-description").html("<?php echo _('Por favor, responde la siguiente pregunta antes de continuar'); ?>");
				$("#exercise-description").fadeIn('slow');
				$("#knowledge-id").fadeIn('slow');
			});
		});
	}
};
</script>