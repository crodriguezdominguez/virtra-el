<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<a id="introExerciseButton" href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<!--<p><img src="exercises/img/DirectNumbersExercise/pizarraIntro.png" width="400" alt="<?php echo _('Pizarra'); ?>" /></p>-->
</div>
<div id="introDemo" class="hide">
	<a href="javascript:void(0);" onclick="javascript:beginDemo();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="keypad" style="overflow:auto; width:100%;" class="hide">
	<div id="keypadHeader" style="position:relative; margin-top:0px;" class="pull-left">
      <img src="exercises/img/DirectNumbersExercise/pizarra.png" width="400" alt="<?php echo _('Pizarra'); ?>" />
      <span id="blackboardKeypad" style="position:absolute; top:120px; text-align:center; left:40px; width:320px; height:200px; font-size:90px; color:white;"></span>
	</div>
	<table cellspacing="5" cellpadding="5" style="margin-left:35px;margin-top:0px;" class="pull-left">
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="1" onclick="javascript:checkValue('1');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="2" onclick="javascript:checkValue('2');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="3" onclick="javascript:checkValue('3');" /></td>
		</tr>
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="4" onclick="javascript:checkValue('4');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="5" onclick="javascript:checkValue('5');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="6" onclick="javascript:checkValue('6');" /></td>
		</tr>
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="7" onclick="javascript:checkValue('7');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="8" onclick="javascript:checkValue('8');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="9" onclick="javascript:checkValue('9');" /></td>
		</tr>
		<tr>
		    <!--<td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="0" onclick="javascript:checkValue('0');" /></td>-->
		    <td colspan="3" style="height: 50px; width: 300px"><input id="clearButton" type="button" class="btn" style="height: 50px; width: 100%; font-family: Arial; font-size:32px; color:#555;" value="<?php echo _('BORRAR'); ?>" onclick="javascript:eraseLastCharacter();" disabled /></td>
		</tr>
		<tr>
			<td colspan="3"><input type="button" class="btn" id="demoButton" style="margin-left:0px; margin-top:20px; height: 50px; width: 100%; font-family: Arial; font-size:44px; color:#555;" value="<?php echo _('Ver de nuevo'); ?>" onclick="javascript:replayNumber();" /></td>
		</tr>
	</table>
</div>

<div id="blackboard" class="hide">
	<div style="position:relative; width:100%;">
      <img src="exercises/img/DirectNumbersExercise/pizarra.png" width="400" alt="<?php echo _('Pizarra'); ?>" />
      <span id="blackboardNumber" style="position:absolute; top:130px; left:180px; width:80%; font-size:80px; color:white;"></span>
	</div>
</div>

<div style="margin-top:20px; margin-left:20px;"><a id="continue-evaluation-btn" class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:continueEvaluation();"><?php echo _('Listo'); ?></a></div>

<div class="pull-left hide" id="result-explanation"><p id="result-explanation-span" class="lead"></p><br /><br /><a id="button-explanation" href="javascript:void(0);" onclick="javascript:replayExercise();" class="btn btn-primary btn-large hide"><?php echo _('Continuar'); ?></a></div>

<div class="pull-left hide" id="force-finalize"><a href="backend/logout.php" class="btn btn-primary btn-large"><?php echo _('Terminar por hoy'); ?></a></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var executionNumber = 0;
var exerciseLevel = 2;
var currentNumberStr = null;
var currentKeypadIndex = 0;
var typedNumber = null;
var numberOfSequentialErrors = 0;
var numberOfSequentialCorrects = 0;
var finalizeDemo = false;
var shownSequences = [];

function beginExercise() {
	if (!finalizeDemo)
	{
		$("#result-explanation").hide();
		$('#introExercise').fadeOut('slow', function(evt){
		$('#exercise-description').fadeOut('slow', function(evt){
				$('#introDemo').fadeIn('slow');
				showInstructions(false);
				$('#exercise-description').html("<?php echo _('Ahora vamos a hacer tres pruebas. Se escribirán secuencias de números y se mostrará un panel para que los escribas en el mismo orden en el que se te mostraron. Cuando estés listo, pulsa en <strong>Continuar</strong>.'); ?>");
				$('#exercise-description').fadeIn('slow');
			});
		});
	}
	else
	{
		endDemo();
	}
};

function beginDemo() {
	$('#introDemo').fadeOut('slow', function(evt){
		replayExercise();
	});
};

function eraseLastCharacter() {
	$('#exercise-description').html("<?php echo _('Introduce los números en el mismo orden en el que se te mostraron en la pizarra.'); ?>");
	//$('#result-explanation').html("");
	$("#continue-evaluation-btn").hide();
	$("#demoButton").show();
	if (currentKeypadIndex>0)
	{
		currentKeypadIndex--;
		
		$('input[type="button"][value="'+(typedNumber.charAt(currentKeypadIndex))+'"]').css('color', '#555');
		
		if (currentKeypadIndex == 0)
		{
			typedNumber = "";
		}
		else
		{
			typedNumber = typedNumber.slice(0, currentKeypadIndex);
		}
		
		if (currentKeypadIndex==0)
		{
			$("#clearButton").attr('disabled', 'disabled');
		}
		
		//put an square in the deleted character
		var stubStr = $('#blackboardKeypad').html();
		//stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9744;" + stubStr.substr(currentKeypadIndex+1);
		stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9633;" + stubStr.substr(currentKeypadIndex+1);
		$('#blackboardKeypad').html(stubStr);
	}
	else
	{
		$('input[type="button"][value="'+typedNumber+'"]').css('color', '#555');
		typedNumber = "";
	}
};

function continueEvaluation()
{
	$("#continue-evaluation-btn").fadeOut('fast', function(){
		//$('#result-explanation').html("");
		$("#blackboardNumber").html("");
		$('#keypad').hide();
	
		if (typedNumber == currentNumberStr)
		{
			numberOfSequentialCorrects++;
			numberOfSequentialErrors = 0;
			executionNumber++;
			
			if (executionNumber == 2)
			{
				exerciseLevel++;
			}
		}
		else{
			numberOfSequentialCorrects = 0;
			numberOfSequentialErrors++;
			if (numberOfSequentialErrors >= 2)
			{
				shownSequences = [];
				numberOfSequentialErrors = 0;
				if (typeof exercise._demoRepetitions == 'undefined')
				{
					exercise._demoRepetitions = 1;
				}
				else
				{
					exercise._demoRepetitions++;
				}
				
				if (exercise._demoRepetitions >= 4)
				{
					$("#result-explanation").hide();
					$('#exercise-description').html("<?php echo _('Lo siento, has fallado demasiadas veces la prueba. Inténtalo el próximo día. Pulsa el botón <strong>Terminar por hoy</strong> para finalizar tu sesión en VIRTRA-EL.'); ?>");
					exercise._demoRepetitions = 0;
					$("#force-finalize").show();
				}
				else
				{
					$("#result-explanation").hide();
					forceRepeatExercise();
				}
				
				return;
			}
		}
		
		$('#button-explanation').removeClass('hide').show();
		if (typedNumber == currentNumberStr)
		{
			$('#result-explanation-span').html("<?php echo _('Es <strong>correcto</strong>, pulsa el botón <strong>Continuar</strong> cuando estés preparado'); ?>");
		}
		else
		{
			var htmlTyped = "";
			for (var i=0; i<typedNumber.length; i++)
			{
				htmlTyped += ' <span class="number-highlight">'+typedNumber.charAt(i)+'</span> ';
			}
			
			var htmlCurrent = "";
			for (var i=0; i<currentNumberStr.length; i++)
			{
				htmlCurrent += ' <span class="number-highlight">'+currentNumberStr.charAt(i)+'</span> ';
			}
			
			$('#result-explanation-span').html("<?php echo _('Has pulsado'); ?>"+" <strong>"+htmlTyped+"</strong><?php echo _(', pero deberías de haber pulsado'); ?>"+" <strong>"+htmlCurrent+"</strong>. "+"<?php echo _('Pulsa el botón <strong>Continuar</strong> cuando estés preparado');?>");
			executionNumber--;
		}
		
		$('#result-explanation').show();
		$('#result-explanation-span').show();
		$('#result-explanation-span').css('background-color', 'white');
		$('#result-explanation-span').css('width', '100%');
		
		//replayExercise();
	});
};

function checkValue(value) {
	if (currentKeypadIndex >= currentNumberStr.length)
	{
		sweetAlert({title:"<?php echo _('Error'); ?>", text:"<?php echo _('No puedes escribir más hasta que no le des al botón Listo, o bien le des al botón Borrar para escribir de nuevo si crees que te has equivocado.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
		return;
	}
	
	var isCorrect = (currentNumberStr.charAt(currentKeypadIndex) == value);
	typedNumber += value;
	
	/*if (isCorrect)
	{*/
		var stubStr = $('#blackboardKeypad').html();
		stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9632;" + stubStr.substr(currentKeypadIndex+1);
		//stubStr = stubStr.substr(0, currentKeypadIndex) + value + stubStr.substr(currentKeypadIndex+1);
		$('#blackboardKeypad').html(stubStr);
		
		//$('#result-explanation').html("<?php echo _('Es <strong><span style=\'color:green;\'>correcto</span></strong>, pulsa el siguiente número.'); ?>");
		currentKeypadIndex++;
		
		//$('input[type="button"][value="'+value+'"]').css('color', 'blue');
	/*}
	else
	{
		$('#result-explanation').html("<?php echo _('&Eacute;se no es el número mostrado. Inténtalo de nuevo. Si no lo recuerdas, pulsa el botón <strong>Ver de nuevo</strong>.'); ?>");
		
		$('input[type="button"][value="'+value+'"]').css('color', '#555');
	}*/
	
	if (currentKeypadIndex>0)
	{
		$("#clearButton").removeAttr('disabled');
	}
	
	if (currentKeypadIndex >= currentNumberStr.length)
	{
		//$("#clearButton").attr('disabled', 'disabled');
		$("#demoButton").hide();
		$('#exercise-description').html("<?php echo _('Pulsa en el botón <strong>Listo</strong> para continuar, o pulsa el botón <strong>Borrar</strong> si quieres corregir los números que hayas marcado.'); ?>");
		//$('#result-explanation').html("");
		$("#continue-evaluation-btn").fadeIn('fast');
	}
}

function clean_css()
{
	for (var i=0; i<=9; i++)
	{
		$('input[type="button"][value="'+i+'"]').css('color', '#555');
	}
}

function replayNumber() {
	//$('#result-explanation').html("");
	$('#keypad').fadeOut('slow', function(evt){
		
		while(currentKeypadIndex > 0)
		{
			eraseLastCharacter();
		}
		
		$('#blackboardNumber').html("");
		$('#blackboard').fadeIn('slow', function(evt){
			showNumber(currentNumberStr, 0);
		});
	});
};

function replayExercise() {
	clean_css();
	$("#result-explanation").hide();
	$("#continue-evaluation-btn").fadeOut('fast');
	
	$('#exercise-description').fadeOut('slow', function(evt){
		if (executionNumber>=3)
		{
			finalizeDemo = true;
			$("#introExerciseButton").html("<?php echo _('Comenzar ejercicio'); ?>");
			$("#introExercise").fadeIn('slow', function(evt){
				showInstructions(false);
				$('#exercise-description').html("<?php echo _('Has finalizado la prueba correctamente, ¡Enhorabuena! Ahora comenzarás el ejercicio. Cuando estés listo, pulsa el botón <strong>Comenzar ejercicio</strong>.'); ?>");
				$('#exercise-description').fadeIn('slow'); 
			});
			
			return;
		}
		
		configureKeypad();
		
		typedNumber = "";
		currentKeypadIndex = 0;
		var n = 0;
		do{
			n = randomNumber(exerciseLevel);
		}while(shownSequences.indexOf(numberToString(n)) !== -1);
		currentNumberStr = numberToString(n);
		
		shownSequences.push(currentNumberStr);
		
		var stubStr = "";
		for (var i=0; i<currentNumberStr.length; i++) stubStr += "&#9633;"; //stubStr += "&#9744;";
		$('#blackboardKeypad').html(stubStr);
		
		replayNumber();
	});
};

function configureKeypad() {
	$("#demoButton").removeClass("hide").show();
	//$("#clearButton").addClass("hide").hide();
	
	$("#clearButton").attr('disabled', 'disabled');
};

function showNumber(nStr, currentIndex){
	showInstructions(false);
	$('#exercise-description').html("<?php echo _('Introduce los números en el mismo orden en el que se te mostraron en la pizarra.'); ?>");
	if (currentIndex < nStr.length)
	{
		$("#blackboardNumber").fadeOut('fast', function(evt){
			$("#blackboardNumber").html(nStr.charAt(currentIndex));
			$("#blackboardNumber").fadeIn('fast', function(evt){
				setTimeout(function(){
					showNumber(nStr, currentIndex+1);
				}, 1500);
			});
		});
	}
	else
	{
		$('#blackboard').fadeOut('fast', function(evt){
			typedNumber = "";
			
			//font size decreases by 10px each 2 numbers (log2(nStr.len))
			var divisions = Math.ceil(Math.log(nStr.length)/Math.log(2));
			var total = 70-(divisions*10);
			if (divisions == 1) total = 70;
			if (total <= 0) total = 1;
			total = total.toString()+"px";
			$('#blackboardKeypad').css('font-size', total);
			
			$('#keypad').fadeIn('fast', function(evt){
				$('#exercise-description').fadeIn('slow');
			});
		});
	}
};

function randomNumber(n) {
	//var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
	var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
	numbers.sort(function(){
			return 0.5 - getRandom();
	});
	if (n>0)
	{
		var result = "";
		var lastNumber = "0";
		for (var i=0; i<n; i++)
		{
			var r = Math.floor(getRandom() * 9) % 9;
			var rStr = numbers[r];
			if (rStr == "0") rStr = "1";
			if (rStr == lastNumber)
			{
				r = (r+1)%10;
				rStr = numbers[r];
			}
			result += rStr;
			lastNumber = rStr;
		}
		
		return parseInt(result);
	}
	else return parseInt(numbers[0]);
};

function numberToString(n) {
	return n.toString();
};

$(function() {
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	executionNumber = 0;
});

</script>
