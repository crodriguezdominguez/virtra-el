<iframe id="VRiFrame" style="width:1024px;height:550px;margin-bottom:20px;display:none;"></iframe>

<div id="introExercise">
	<p class="lead"><?php echo _('Deberás recorrer diferentes lugares dentro de una ciudad, realizando tareas cotidianas. Podrás moverte pulsando sobre las flechas que aparecerán en pantalla. Al hacer click sobre un objeto, podrás interactuar con él (cogerlo, tocarlo, abrirlo, etc.). Las puertas de entrada a los edificios se señalarán en la calle con colores muy llamativos. Pulsa sobre ellas para acceder a los edificios que necesites.'); ?></p>
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<script type="text/javascript">
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

// Listen to message from child window
eventer(messageEvent,function(e) {
  if (e.data == 'endExercise')
  {
	  endExercise();
  }
},false);

function beginExercise()
{
	$("#introExercise").fadeOut('slow');
	$("#exercise-description").fadeOut('slow', function(){
		showMiniMenu(true);
		$("#exercise-description").html("<?php echo _('Pulsa las flechas para moverte y haz click para interactuar con los objetos que hay en pantalla'); ?>.");
		$('#VRiFrame').attr('src', "http://www.virtrael.es/exercises/VRResources/exerciseVR<?php if (isset($_GET['pre']) && $_GET['pre'] == '1') echo 'Pre'; else echo 'Post'; ?>.html?sessionID="+lastSession['sessionID']+"&exerciseID="+lastSession['exerciseID']);
		$("#exercise-description").fadeIn('slow', function(){
			$('#VRiFrame').css('display', 'block');
		});
	});
}

</script>