<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!DummyExercise) {

var DummyExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('Ejercicio no disponible'); ?>', '<?php echo _('Sesi&oacute;n'); ?> '+sessionID+', <?php echo _('ejercicio'); ?> '+exerciseID+', <?php echo _('repetici&oacute;n'); ?> '+repetition, 'exercises/DummyExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
	}
});

}
</script>
