<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript" src="js/jquery.path.js"></script>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise">
	<a id="introButton" href="javascript:void(0);" class="btn btn-primary btn-large" style="margin-bottom:20px;"><?php echo _('Continuar'); ?></a>
</div>

<div id="balloons">
	<p id="speedP" class="lead hide"><strong><?php echo _('Nivel'); ?>: </strong><span id="level"></span>&nbsp;&nbsp;<strong><?php echo _('Velocidad'); ?>: </strong><span id="speed"></span><br /><span id="remTime"></span></p>
	<div id="balloonsContainer" tabindex="0">
		<div id="balloon">A</div>
	</div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var resultTiming = 0;
var level = 1;
var sublevel = 1;
var currentIntroIndex = 0;
var clicked = false;
var endTiming = false;
var MAX_TIMER = 10*60; //10 minutes
var backgroundBalloonsContainer = $('#balloonsContainer').css('background-color');
var MAX_BALLONS = 50; //40;
var MAX_CORRECTS = 15;
var countCorrects = 0;
var countBalloons = 0;
var lastLetters = [];
var isDemo = false;
var demoCorrects = 0;
var pause = false;

function resultTimerFunction(){
	if (pause) return;
	resultTiming++;
	if (!endTiming)
	{
		var totalTime = MAX_TIMER-resultTiming;
		var minutes = Math.floor(totalTime/60);
		var seconds = totalTime%60;
		
		if (resultTiming > MAX_TIMER && !isDemo)
		{
			$("#remTime").html("(<?php echo _('Se va a terminar el ejercicio en 5 segundos'); ?>)");
			endTiming = true;
			setTimeout(resultTimerFunction, 5000);
		}
		else
		{
			/*var msgMinutes = minutes>1?("<?php echo _('Quedan'); ?> "+minutes+" <?php echo _('minutos'); ?>"):("<?php echo _('Queda 1 minuto'); ?>");
			var msgSeconds = seconds>0?(seconds+" <?php echo _('segundos'); ?>"):("<?php echo _('1 segundo') ?>");
			if (minutes > 0)
			{
				msgMinutes = msgMinutes+" <?php echo _('y'); ?> ";
			}
			else
			{
				msgMinutes = "";
				if (seconds > 1)
				{
					msgSeconds = "<?php echo _('Quedan'); ?> "+msgSeconds;
				}
				else{
					msgSeconds = "<?php echo _('Queda'); ?> "+msgSeconds;
				}
			}*/
			
			//$("#remTime").html("("+msgMinutes+msgSeconds+" <?php echo _('para terminar el ejercicio'); ?>)");
			
			setTimeout(resultTimerFunction, 1000);
		}
	}
	else
	{
		exercise.setSeconds(resultTiming);
		exercise.setLevel(level);
		exercise.setSubLevel(sublevel);
		
		$("#balloonsContainer").unbind('click');
		$(document).unbind('keypress');
		
		endExercise();
	}
};

var SineWave = function() {
  this.css = function(p) {
    var s = Math.sin(p*10);
    var x = 500 - p * 500;
    var y = s * 10 + 100;
    return {top: y + "px", left: x + "px"};
  };
};

function randomLetter() {
	var letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
	if (lastLetters.length > level)
	{
		if (getRandom()<=0.3) //a 40% percent that a correct result appears
		{
			var letter = lastLetters[lastLetters.length-level];
			lastLetters.push(letter);
			return letter.toUpperCase();
		}
	}
	
	letters.sort(function(a,b){
		return 0.5-getRandom();
	});
	
	lastLetters.push(letters[0]);
	
	return letters[0].toUpperCase();
};

function animateBallon() {
	if (isDemo) pause = true;
	
	if (!endTiming)
	{
		clicked = false;	
		var currentLetter = randomLetter();
		var end = '450px';
		var time = 3000-((sublevel-1)*500);
		if (time < 1000) time = 1000;
		$("#balloon").css('left', '-10px');
		$("#balloon").html(currentLetter);
		$("#balloon").stop().animate({path: new SineWave}, time, 'linear', function(){
			if (!isDemo)
			{
				if (!showInstructionsFullScreen) showMiniMenu(true);
				countBalloons++;
				if (lastLetters.length > level)
				{
					if (currentLetter.toUpperCase() == lastLetters[lastLetters.length-level-1].toUpperCase())
					{
						if (!clicked) exercise.increaseOmissions();
						else{
							exercise.increaseCorrects();
							countCorrects++;
						}
					}
					else{
						if (clicked) exercise.increaseFails();
						else {
							exercise.increaseCorrects(); // not clicked on two consecutive letters that are different
							countCorrects++;
						}
					}
				}
				else
				{
					if (clicked) exercise.increaseFails();
				}
			}
			else
			{
				if (lastLetters.length > level)
				{
					if (currentLetter.toUpperCase() == lastLetters[lastLetters.length-level-1].toUpperCase())
					{
						if (!clicked){
							//omission message
							showInstructions(true);
							$("#exercise-description").html("<?php echo _('<strong>Recuerda:</strong> Pulsa espacio o en el cuadrado azul cuando se repita la letra del globo que apareció en los'); ?> "+level+" <?php echo _('lugares anteriores al actual, es decir, dejando'); ?> "+(level-1)+((level-1 == 1)?" <?php echo _('globo entre dos globos con la misma letra.'); ?> ":" <?php echo _('globos entre dos globos con la misma letra.'); ?>"));
						}
						else{
							demoCorrects++;
							if (demoCorrects == 1)
							{
								showInstructions(true);
								$("#exercise-description").html("<?php echo _('<strong>&iexcl;Genial!</strong> Trata de acertar de nuevo.'); ?>");
							}
							else if (demoCorrects == 2)
							{
								showInstructions(true);
								$("#exercise-description").html("<?php echo _('<strong>&iexcl;Muy bien!</strong> Ya solo tienes que acertar otra vez para superar la prueba.'); ?>");
							}
						} //corrects message
					}
					else if (clicked){
						showInstructions(true);
						$("#exercise-description").html("<?php echo _('<strong>Recuerda:</strong> Pulsa espacio o en el cuadrado azul cuando se repita la letra del globo que apareció en los'); ?> "+level+" <?php echo _('lugares anteriores al actual, es decir, dejando'); ?> "+(level-1)+((level-1 == 1)?" <?php echo _('globo entre dos globos con la misma letra.'); ?> ":" <?php echo _('globos entre dos globos con la misma letra.'); ?>"));
					} //fails message
				}
				else
				{
					if (clicked){
						showInstructions(true);
						$("#exercise-description").html("<?php echo _('<strong>Recuerda:</strong> Pulsa espacio o en el cuadrado azul cuando se repita la letra del globo que apareció en los'); ?> "+level+" <?php echo _('lugares anteriores al actual, es decir, dejando'); ?> "+(level-1)+((level-1 == 1)?" <?php echo _('globo entre dos globos con la misma letra.'); ?> ":" <?php echo _('globos entre dos globos con la misma letra.'); ?>"));
					}
				}
				
				pause = true;
			}
			
			if ( (exercise.fails()+exercise.omissions()) <= 7 && countBalloons >= MAX_BALLONS && !isDemo) //(countCorrects >= MAX_CORRECTS && !isDemo)
			{
				sublevel++;
				if (sublevel>3)
				{
					sublevel = 1;
					level++;
					MAX_BALLONS = MAX_BALLONS*level;
					isDemo = true;
					if (level>3)
					{
						level = 3;
						sublevel = 3;
						endTiming = true;
						return;
					}
					
					var strIns = "<?php echo _('<strong>&iexcl;Genial!</strong> Has pasado al nivel'); ?> "+level+". ";
					
					if (level == 1)
					{
						strIns += "<?php echo _('Ahora debes pulsar espacio o en el cuadrado azul cuando veas <strong>dos iguales seguidos</strong>.'); ?> ";
					}
					else if (level == 2)
					{
						strIns = "<?php echo _('<strong>&iexcl;Estupendo! &iexcl;Has pasado de nivel!</strong> Ahora lo que tendrás que hacer es pulsar sobre el recuadro azul cuando aparezca un globo igual al que apareció dos lugares antes, es decir, dejando un globo en medio que puede ser distinto. Por ejemplo, deberás pulsar cuando aparezca una secuencia de globos con las letras: A-B-A.'); ?>";
					}
					else
					{
						//strIns += "<?php echo _('Ahora debes pulsar espacio o en el cuadrado azul cuando se repita la letra del globo que apareció en los'); ?> "+level+" <?php echo _('lugares anteriores al actual, es decir, dejando'); ?> "+(level-1)+((level-1 == 1)?" <?php echo _('globo entre dos globos con la misma letra.'); ?> ":" <?php echo _('globos entre dos globos con la misma letra.'); ?>"+"<?php echo _('<strong>Vamos a realizar una prueba para practiques</strong>'); ?>.");
						strIns = "<?php echo _('<strong>&iexcl;Estupendo! &iexcl;Has pasado al último nivel!</strong> Ahora lo que tendrás que hacer es pulsar sobre el recuadro azul cuando aparezca un globo igual al que apareció tres lugares antes, es decir, dejando dos globos en medio que pueden ser distintos. Por ejemplo, deberás pulsar cuando aparezca una secuencia de globos con las letras: A-Z-Y-A.'); ?>";
					}
					
					countBalloons = 0;
					
					showInstructions(true);
					$("#exercise-description").html(strIns+" <?php echo _('La velocidad de aparición de los globos se ha reiniciado. <strong>Vamos a hacer una prueba para comprobar si entiendes ahora el ejercicio</strong>.'); ?>");
				}
				else
				{
					showInstructions(true);
					$("#exercise-description").html("<?php echo _('Lo has hecho muy bien. Como consecuencia, se aumentará la velocidad de aparición de los globos en la siguiente tanda'); ?>.");
					$("#balloonsContainer").css('background-color', backgroundBalloonsContainer);
				}
				countBalloons = 0;
				pause = true;
			
				$("#balloons").hide();
				$("#introButton").unbind('click');
				$("#introButton").bind('click', showCounter);
				demoCorrects = 0;
				$("#introButton").show();
				$("#introExercise").fadeIn('slow');
			}
			else if (countBalloons >= MAX_BALLONS && countCorrects == 0 && !isDemo)
			{
				if (level == 1)
				{
					showInstructions(true);
					$("#exercise-description").html("<?php echo _('Parece que no has entendido el ejercicio muy bien. Debes pulsar espacio o en el cuadrado azul cuando aparezcan dos globos iguales seguidos con la misma letra, es decir, dos globos repetidos uno después de otro. Vamos a realizar otra prueba para que lo comprendas bien.'); ?>.");
				}
				else
				{
					showInstructions(true);
					$("#exercise-description").html("<?php echo _('Parece que no has entendido el ejercicio muy bien. Debes pulsar espacio o en el cuadrado azul cuando se repita la letra del globo que apareció en los'); ?> "+level+" <?php echo _('lugares anteriores al actual, es decir, dejando'); ?> "+(level-1)+((level-1 == 1)?" <?php echo _('globo entre dos globos con la misma letra.'); ?> ":" <?php echo _('globos entre dos globos con la misma letra. Vamos a realizar otra prueba para que lo comprendas bien.'); ?>."));
				}
				
				countBalloons = 0;
				pause = true;
			
				$("#balloons").hide();
				$("#introButton").unbind('click');
				$("#introButton").bind('click', showCounter);
				isDemo = true;
				demoCorrects = 0;
				$("#introButton").show();
				$("#introExercise").fadeIn('slow');
			}
			else
			{
				if (isDemo && demoCorrects >= 3)
				{
					showInstructions(true);
					$("#exercise-description").html("<?php echo _('<strong>&iexcl;Muy bien, has superado la prueba!</strong>. Ahora comenzaremos el ejercicio en el nivel '); ?>"+level+". <?php echo _('Cuando estés listo pulsa en el botón <strong>Continuar</strong>.') ?>");
					countBalloons = 0;
					pause = true;
				
					$("#balloons").hide();
					$("#introButton").unbind('click');
					$("#introButton").bind('click', showCounter);
					isDemo = false;
					demoCorrects = 0;
					$("#introButton").show();
					$("#introExercise").fadeIn('slow');
				}
				else
				{
					if (isDemo){
						countBalloons = 0;
						showInstructions(true);
					}
					
					$("#balloonsContainer").css('background-color', backgroundBalloonsContainer);
					$("#level").html(level);
					$("#speed").html(sublevel);
					animateBallon();
				}
			}
			
			//$("#exercise-mini-description").html($("#exercise-description").html());
		});
	}
};

function showCounter(){
	$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
		$("#exerciseCounter").show();
		setTimeout(beginExercise, 4000);
	});
}

function nextIntro(){
	$("#exercise-description").fadeOut('slow', function(){
		$("#exercise-description").html(exercise.description());
		beginExercise();
		
		$("#exercise-description").fadeIn('slow');
		
		currentIntroIndex++;
	});
};

function beginExercise()
{
	var strIns = "";
	lastLetters = [];
	countBalloons = 0;
	
	countCorrects = 0;
	
	if (level == 1)
	{
		showMiniMenu(true);
		strIns += "<?php echo _('Pulsa espacio o en el cuadrado azul cuando veas 2 globos iguales seguidos.'); ?>";
	}
	else
	{
		showMiniMenu(true);
		strIns += "<?php echo _('Pulsa espacio o en el cuadrado azul cuando se repita la letra del globo que apareció en los'); ?> "+level+" <?php echo _('lugares anteriores al actual, es decir, dejando'); ?> "+(level-1)+((level-1 == 1)?" <?php echo _('globo entre dos globos con la misma letra.'); ?> ":" <?php echo _('globos entre dos globos con la misma letra.'); ?>");
	}
	
	$("#exercise-description").html(strIns);
	//$("#exercise-mini-description").html(strIns);
	countBalloons = 0;
	$("#balloonsContainer").bind('click', checkExercise);
	$(document).keypress(function(e){
		var key = e.which || e.keyCode;
		if (key == 13 || key == 32) //enter or space
		{
			checkExercise();
		}
	});
	$("#introExercise").fadeOut('slow', function(){
		$("#exercise-description").fadeIn('slow', function(evt){
			$("#level").html(level);
			$("#speed").html(sublevel);
			$("#speedP").removeClass('hide').show();
			$("#balloons").fadeIn('slow', function(e){
				pause = false;
				resultTimerFunction();
				animateBallon();
			});
		});
	});
};

function checkExercise(){
	clicked = true;
	$("#balloonsContainer").css('background-color', '#cccccc');
};


$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	$("#balloonsContainer").unbind('click');
	$(document).unbind('keypress');
	
	$('#introButton').bind('click', nextIntro);
	if (exercise.afterDemo())
	{
		$("#introExercise").hide();
		beginExercise();
	}
});

</script>
