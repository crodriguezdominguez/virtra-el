<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise" class="hide">
	<p class="lead"><?php echo _('Para que entiendas como realizar el ejercicio, te proponemos un ejemplo. Los dibujos de abajo son cuadrados y círculos que van cambiando siguiendo el orden: cuadrado, luego círculo, cuadrado de nuevo, otro círculo y por último otro cuadrado. &iquest;Adivinas que iría después del último cuadrado?'); ?></p>
	<ul id="linear-layout-test" class="thumbnails">
		<li class="span2"><div class="logical-exercise-element"><img src="exercises/img/LogicalSeriesExercise/series1/figure4.png" width="100px"></div></li>
		<li class="span2"><div class="logical-exercise-element"><img src="exercises/img/LogicalSeriesExercise/series1/figure1.png" width="100px"></div></li>
		<li class="span2"><div class="logical-exercise-element"><img src="exercises/img/LogicalSeriesExercise/series1/figure4.png" width="100px"></div></li>
		<li class="span2"><div class="logical-exercise-element"><img src="exercises/img/LogicalSeriesExercise/series1/figure1.png" width="100px"></div></li>
		<li class="span2"><div class="logical-exercise-element"><img src="exercises/img/LogicalSeriesExercise/series1/figure4.png" width="100px"></div></li>
		<li class="span2"><div class="logical-exercise-element"><img id="intro-placeholder" src="exercises/img/LogicalSeriesExercise/placeholder.png" width="100px" style="background-color: lightblue;"></div></li>
	</ul>
	<span id="firstIntroStep">
		<a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:showNextIntroStep();"><?php echo _('Continuar'); ?></a>
	</span>
	<span id="secondIntroStep" class="hide">
        <p class="lead"><?php echo _('De las respuestas de abajo, pulsa cuál es la correcta:'); ?></p>
        <ul id="linear-layout-test" class="thumbnails">
            <li class="span2">
                <a id="intro-answer0"
                   onclick="javascript:checkAnswerIntro(0);"
                   href="javascript:void(0);"
                   style="text-decoration:none;"
                   class="thumbnail">
                    <img src="exercises/img/LogicalSeriesExercise/series1/figure4.png" width="100px"/>
                    <h3 align="center">1</h3>
                </a>
            </li>
            <li class="span2">
                <a id="intro-answer1"
                   onclick="javascript:checkAnswerIntro(1);"
                   href="javascript:void(0);"
                   style="text-decoration:none;"
                   class="thumbnail">
                    <img src="exercises/img/LogicalSeriesExercise/series1/figure2.png" width="100px"/>
                    <h3 align="center">2</h3>
                </a>
            </li>
            <li class="span2">
                <a id="intro-answer2"
                   onclick="javascript:checkAnswerIntro(2);"
                   href="javascript:void(0);"
                   style="text-decoration:none;"
                   class="thumbnail">
                    <img src="exercises/img/LogicalSeriesExercise/series1/figure1.png" width="100px"/>
                    <h3 align="center">3</h3>
                </a>
            </li>
            <li class="span2">
                <a id="intro-answer3"
                   onclick="javascript:checkAnswerIntro(3);"
                   href="javascript:void(0);"
                   style="text-decoration:none;"
                   class="thumbnail">
                    <img src="exercises/img/LogicalSeriesExercise/series5/figure5.png" width="100px"/>
                    <h3 align="center">4</h3>
                </a>
            </li>
		</ul>

		<a id="ready-button-intro" href="javascript:void(0);" onclick="javascript:checkExerciseIntro();" class="btn btn-primary btn-large hide"><?php echo _('Listo'); ?></a>
		<a id="continue-button-intro" href="javascript:void(0);" onclick="javascript:showLogicalExercise();" class="btn btn-primary btn-large hide"><?php echo _('Continuar'); ?></a>

	</span>
</div>

<div id="logicalExercise" class="hide">
	<div id="squared-layout" class="hide">
		<ul class="thumbnails">
			<li class="span2"><div id="option0" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
			<li class="span2"><div id="option1" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
			<li class="span2"><div id="option2" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		</ul>
		<ul class="thumbnails">
			<li class="span2"><div id="option3" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
			<li class="span2"><div id="option4" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
			<li class="span2"><div id="option5" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		</ul>
		<ul class="thumbnails">
			<li class="span2"><div id="option6" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
			<li class="span2"><div id="option7" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
			<li class="span2"><div id="option8" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		</ul>
	</div>
	
	<ul id="linear-layout" class="thumbnails hide">
		<li class="span2"><div id="option2_0" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		<li class="span2"><div id="option2_1" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		<li class="span2"><div id="option2_2" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		<li class="span2"><div id="option2_3" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		<li class="span2"><div id="option2_4" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
		<li class="span2"><div id="option2_5" href="javascript:void(0);" class="logical-exercise-element hide"></div></li>
	</ul>
	<hr />
	<p class="lead" id="help-below" style="font-weight: bold;margin-top:20px; font-size: 32px;"><?php echo _('De las respuestas de abajo, pulsa cuál es la correcta:'); ?></p>
	<ul class="thumbnails">
		<li class="span2"><a id="answer0" href="javascript:void(0);" onclick="javascript:checkAnswer(0);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer1" href="javascript:void(0);" onclick="javascript:checkAnswer(1);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer2" href="javascript:void(0);" onclick="javascript:checkAnswer(2);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer3" href="javascript:void(0);" onclick="javascript:checkAnswer(3);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer4" href="javascript:void(0);" onclick="javascript:checkAnswer(4);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer5" href="javascript:void(0);" onclick="javascript:checkAnswer(5);" class="thumbnail hide"></a></li>
	</ul>
	<div class="lead" id="exercise-explanation"></div>
</div>

<div style="text-align:left;margin-bottom:40px;margin-top: 40px;"><a class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:checkExercise();" id="ready-button"><?php echo _('Listo'); ?></a></div>

<div style="text-align:left;margin-bottom:40px;margin-top: 40px;"><a href="javascript:void(0);" onclick="javascript:endExercise();" class="hide btn btn-primary btn-large" id="continue-button"><?php echo _('Continuar'); ?></a></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var logicalExercise = null;
var answer = -1;
var answerIntro = -1;
var placeHolderID = null;

function showLogicalExercise(){
	if (repetitions == 0)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				$("#exerciseCounter").show();
				setTimeout(function(){
					showMiniMenu(true);
					$('#logicalExercise').fadeIn('slow');
					$('#continue-button').fadeOut('slow');
					//$('#ready-button').fadeIn('slow', function(){
						$('#exercise-title').html("<?php echo _('Completa la serie'); ?>");
						$('#exercise-description').html("<?php echo _('Selecciona el siguiente dibujo de la secuencia y pulsa'); ?> <strong><?php echo _('Listo'); ?></strong>");
					//});
				}, 4000);
			});
		});
	}
	else
	{
		$('#introExercise').fadeOut('slow', function(evt){
			showMiniMenu(true);
			$('#logicalExercise').fadeIn('slow');
			$('#continue-button').fadeOut('slow');
				//$('#ready-button').fadeIn('slow', function(){
					$('#exercise-title').html("<?php echo _('Completa la serie'); ?>");
					$('#exercise-description').html("<?php echo _('Selecciona el siguiente dibujo de la secuencia y pulsa'); ?> <strong><?php echo _('Listo'); ?></strong>");
				//});
		});
	}
};

function showNextIntroStep() {
    collapseMenu(true);
	$("#firstIntroStep").fadeOut('fast', function(){
		$("#secondIntroStep").fadeIn('fast');
	});
};

function checkAnswerIntro(num) {
    answerIntro = num;
    $("#ready-button-intro").fadeIn('fast');

    $(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle');
    $('#intro-answer'+num).addClass('semantic-exercise-toggle');
};

function checkExerciseIntro() {
    if (answerIntro != 2) { // lose
        $(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-bad");
        $("#intro-answer2").addClass('semantic-exercise-ok');
    }
    else { // win
        $(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-ok");
    }

    for (i = 0; i < 4; i++) {
        $('#intro-answer'+i).addClass('disabled').attr('onclick', 'javascript:void(0);');
    }

    $("#intro-placeholder").attr("src","exercises/img/LogicalSeriesExercise/series1/figure1.png");
    $("#intro-placeholder").css('border', '5px solid green');
    $("#intro-placeholder").css('background-color', 'white');

    $('#ready-button-intro').fadeOut('fast', function(evt){
        $('#continue-button-intro').fadeIn('fast');
    });
}

function checkAnswer(num) {
	answer = num;
	$("#ready-button").fadeIn('fast');
	
	$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle');
	$('#answer'+num).addClass('semantic-exercise-toggle');
};

function checkExercise(){
	var initialText = null;
	if (answer == -1)
	{
		$('#exercise-explanation').html("<?php echo _('Debe seleccionar alg&uacute;n dibujo'); ?>");
	}
	else
	{
		$("#help-below").hide();
		if (parseInt(logicalExercise.Answer.position) == answer) //win
		{
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-ok");
			initialText = "<span style='color:green;'><b><?php echo _('&iexcl;Correcto!'); ?></b></span><br />";
			exercise.setCorrect(true);
		}
		else //lose
		{
			var goodAnswer = logicalExercise.Answer.position;
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-bad");
			initialText = "<b><?php echo _('&iexcl;Lo siento, no es correcto!'); ?></b><br />";
			$('#answer'+goodAnswer).addClass("semantic-exercise-ok");
			exercise.setCorrect(false);
		}
		
		$(placeHolderID).children("img")[0].src = $("#answer"+logicalExercise.Answer.position).children("img")[0].src;
		$(placeHolderID).css('border', '5px solid green');
		
		for (i=0; i<6; i++)
		{
			$('#answer'+i).addClass('disabled').attr('onclick', 'javascript:void(0);');
		}
		
		if (exercise.offerHelp())
		{
			$('#exercise-explanation').html(initialText+"<b><?php echo _('Explicaci&oacute;n: '); ?></b>"+logicalExercise.explanation);
		}
		
		$('#ready-button').fadeOut('fast', function(evt){
			$('#exercise-description').html('Cuando estés preparado pulsa en <strong>Continuar</strong>.');
			$('#continue-button').fadeIn('fast');
		});
	}
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (repetitions==0) //show intro
	{
		$('#introExercise').show();
	}
	else //show exercise
	{
		showLogicalExercise();
	}
	
	//retrieve current exercise plan
	$.ajax({
		type: "GET",
		url: "exercises/xml/logical-series-plan.xml",
		dataType: "xml",
		success: function(xml) {
			var plan = $.xml2json(xml);
			var numberOfExercise = -1;
			
			$.ajax({
				type: "GET",
				url: "exercises/xml/logical-series.php",
				dataType: "xml",
				success: function(result){
					var logicalExercises = $.xml2json(result);
					
					$.each(plan.LogicalSeriesPlanEntry, function(index, value){
						if (value.exerciseID == exerciseID && value.sessionID == sessionID)
						{
							if (value.exercises == "random")
							{
								//take one random exercise
								numberOfExercise = Math.floor(getRandom()*logicalExercises.LogicalSeriesExercise.length);
								numberOfExercise = parseInt(logicalExercises.LogicalSeriesExercise[numberOfExercise].id);
							}
							else
							{
								var array = value.exercises.split(",");
								if (repetitions < array.length)
									numberOfExercise = parseInt(array[repetitions]);
								else
								{
									numberOfExercise = Math.floor(getRandom()*logicalExercises.LogicalSeriesExercise.length);
									numberOfExercise = parseInt(logicalExercises.LogicalSeriesExercise[numberOfExercise].id);
								}
							}
							
							$.each(logicalExercises.LogicalSeriesExercise, function(index, value){
								if (value.id == numberOfExercise)
								{
									logicalExercise = value;
									return false;
								}
							});
												
							return false; //break the loop
						}
					});
					
					if (logicalExercise != null)
					{
						$.each(logicalExercise.Options.Element, function(index, element){
							var idAnswer = '#answer'+element.position;
							$(idAnswer).html("<img src='exercises/img/LogicalSeriesExercise/series"+logicalExercise.id+"/figure"+element.figure+".png'' width='150px' /><center><h1>"+(parseInt(element.position)+1)+"</h1></center>").removeClass('hide').show();
						});
						
						var preOption = '#option';
						var width = '150px';
						if (logicalExercise.Elements.Element.length > 5)
						{
							$('#squared-layout').show();
							width = '100px';
						}
						else
						{
							$('#linear-layout').show();
							preOption = '#option2_';
						}
						
						$.each(logicalExercise.Elements.Element, function(index, element){
							var idOption = preOption+element.position;
							$(idOption).html("<img src='exercises/img/LogicalSeriesExercise/series"+logicalExercise.id+"/figure"+element.figure +".png'' width='"+width+"' />").removeClass('hide').show();
						});
						
						var idOption = preOption+logicalExercise.Elements.Element.length;
						
						placeHolderID = idOption;
						$(idOption).html("<img src='exercises/img/LogicalSeriesExercise/placeholder.png' width='"+width+"' />").removeClass('hide').show();
					}
					else
					{
						//error
						console.debug("error: logical exercise not defined");
					}
				}
			});
		}
	});
});
</script>