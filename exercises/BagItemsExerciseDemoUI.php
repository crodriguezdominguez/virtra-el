<?php
require_once('locale/localization.php');
?>

<div id="introExercise">
    <p><a id="introNextStep" href="javascript:void(0);" onclick="javascript:nextStepIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<div id="beginExercise" class="hide">
    <p><a href="javascript:void(0);" onclick="javascript:endDemo();" class="btn btn-primary btn-large"><?php echo _('Comenzar ejercicio'); ?></a></p>
</div>

<div id="replayExercise" class="hide">
    <p><a href="javascript:void(0);" onclick="javascript:replayExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<div class="hide" id="mediaContainer" style="position:relative;">
    <div class="alert alert-block alert-error package-delivery-movement-alert hide" id="mediaAlert">
    </div>
    <div id="bagitems-bag" class="hide">
    </div>
    <div id="mediaPanel" style="position:relative;margin-bottom:10px;">
    </div>
</div>

<script type="text/javascript">
    var repetitions = 0;
    var sessionID = null;
    var exerciseID = null;
    var placesXML = "exercises/xml/bagitems-places.php";
    var itemsXML = "exercises/xml/bagitems-items.php";
    var places = [];
    var workingPlaces = null;
    var level = 0;
    var placesByLevel = [2, 4, 6, 8];
    var itemsByLevel = [4, 6, 12, 15];
    var allplaces = null;
    var allitems = null;
    var numberOfReplays = 0;
    var positions = [[220,160],[410,160],[600,160],[790,160],[820,390],[630,390],[440,390],[250,390]];
    var initialPosition = [200, 340];
    var currentPlace = null;
    var currentPlaceIndex = -1;
    var counterAnimation = 0;
    var bag = null;
    var homePlace = null;
    var selectedItems = null;
    var repetitionsInLevel = 0;
    var currentIntroIndex = 0;

    function frameOfPlaceAtIndex(index) {
        return [positions[index][0], positions[index][1], 150, 150];
    };

    function frameOfPlace(place) {
        var index = places.indexOf(place);
        return frameOfPlaceAtIndex(index);
    };

    function placeHTML(place) {
        var index = places.indexOf(place);
        if (index != -1)
        {
            var frame = frameOfPlaceAtIndex(index);
            var placeHTML = "<div class='package-delivery-place' id='place"+index+"' style='left:"+frame[0]+"px; top:"+frame[1]+"px; width:"+frame[2]+"px; height:"+frame[3]+"px;'>"
            placeHTML += "<img width='"+frame[2]+"px' height='"+frame[3]+"px' src='"+place.img+"' alt='"+place.name+"' /></div>";
            return placeHTML;
        }
        else return "";
    };

    function placesHTML() {
        var pHTML = "<img alt='<?php echo _('Ciudad'); ?>' src='"+allplaces.background+"' width='100%' height='100%' />";
        $.each(places, function(index, element){
            pHTML = pHTML+placeHTML(element);
        });

        return pHTML;
    };

    function nextStepIntro() {
        $("#exercise-description").fadeOut('slow', function(){
            switch(currentIntroIndex)
            {
                case 0:
                    $("#exercise-description").html("<?php echo _('Cuando finalice el recorrido aparecerá una pantalla con distintas cosas. Únicamente deberás pulsar en los botones 1, 2, 3 o 4 para indicar la cantidad exacta de aquellas cosas que quedan en la bolsa. Vamos a prácticar un poco antes de comenzar. Pulsa <strong>Continuar</strong> cuando estés preparado.'); ?>");
                    $("#mediaPanel").html(placesHTML());
                    break;
                case 1:
                    $("#exercise-description").html("<?php echo _('Tendrás que observar con atención todas las acciones que se realizan en la ciudad que se muestra abajo. Trata de llevar la cuenta de los objetos que quedan en la bolsa tras finalizar todas las acciones'); ?>");
                    $("#mediaContainer").fadeIn('fast');
                    break;
                case 2:
                    replayExercise();
                    break;
                case 3:
                    $("#exercise-description").html("<?php echo _('<strong>¡Enhorabuena!</strong> Has completado la prueba. Recuerda prestar atención y memorizar aquellas cosas que van quedando en la bolsa. Cuando estés preparado para comenzar el ejercicio pulsa <strong>Comenzar ejercicio</strong>.'); ?>");
                    $("#introNextStep").fadeOut('fast');
                    $("#beginExercise").fadeIn('slow');

                    break;
            }

            $("#exercise-description").fadeIn('slow', function(){
                currentIntroIndex++;
            });
        });
    };

    function checkExercise() {
        var omissions = 0;
        var corrects = 0;
        var fails = 0;
        var semicorrects = 0;

        $.each(bag, function(index, bagItem){
            var entered = false;
            for (var i=0; i< selectedItems.length; i++)
            {
                if (selectedItems[i].id == bagItem.id)
                {
                    entered = true;
                    break;
                }
            }
            if (!entered && bagItem.quantity > 0) omissions++;
        });

        $.each(selectedItems, function(index, item){
            var entered = false;
            for (var i=0; i< bag.length; i++)
            {
                if ((item.id == bag[i].id) && (item.quantity > 0 || item.quantity == bag[i].quantity))
                {
                    entered = true;
                    if (item.quantity == bag[i].quantity) corrects++;
                    else semicorrects++;
                    break;
                }
            }
            if (!entered) fails++;
        });

        var score = (corrects+(0.5*semicorrects))-fails-omissions;
        //normalize score
        if (score <= 0 ) score = 0;
        else score = (score/(corrects+semicorrects+fails))*100;

        //recalculate the level
        repetitions++;
        repetitionsInLevel++;
        var totalScore = score+exercise.finalScore();

        $("#bagitems-result-table").remove();
        $("#bagitems-bag").fadeOut('slow', function(){
            if ((fails==0 && omissions == 0) || (repetitions >= 3))
            {
                nextStepIntro();
            }
            else {
                $("#exercise-description").html("<?php echo _('Has tenido algún fallo. Vamos a repetir la prueba de nuevo, por si no has comprendido bien el funcionamiento del ejercicio.'); ?>");
                $("#replayExercise").fadeIn('slow');
            }
        });
    };

    function sprite(step, fx) {
        var toShowImg;
        var toHideImg;
        counterAnimation++;
        if (counterAnimation < 5){
            toShowImg = '#bagitem-character-img1';
            toHideImg='#bagitem-character-img2';
        }
        else {
            toShowImg = '#bagitem-character-img2';
            toHideImg='#bagitem-character-img1';
            if (counterAnimation == 10) counterAnimation = 0;
        }
        $(toShowImg).show();
        $(toHideImg).hide();
    };

    function selectedItemIndex(itemID) {
        var result = -1;
        $.each(selectedItems, function(index, item){
            if (item.id == itemID)
            {
                result = index;
                return false;
            }
        });
        return result;
    }

    function selectItem(element, itemIndex, num) {
        var item = allitems.Item[itemIndex];
        var index = selectedItemIndex(item.id);

        if (element.hasClass('active'))
        {
            if (index != -1) selectedItems.splice(index, 1);
            element.removeClass('active');
        }
        else
        {
            if (num == 0 && index != -1) selectedItems.splice(index, 1);
            else if (index != -1)
            {
                var selectedItem = selectedItems[index];
                selectedItem.quantity = num;
            }
            else selectedItems.push({id:item.id, quantity:num});

            $(".element"+itemIndex).removeClass('active');
            element.addClass('active');
        }
    };

    function showResultSelector(){
        selectedItems = [];

        var pHTML = "<table id='bagitems-result-table' class='hide'>";
        var col = 0;
        var row = 0;
        $.each(allitems.Item, function(index, item){
            if (col == 6)
            {
                pHTML += "<tr>";
                col = 0;
                row++;
            }
            else if (col == 0) pHTML += "</tr>";
            //capitalize string
            var name = item.pluralname.toLowerCase().replace(/\b[a-z]/, function(letter) {
                return letter.toUpperCase();
            });
            pHTML += "<td>";
            if (row == 0) pHTML += "<div class='bagitems-result-table-wrapper first-row'>";
            else pHTML += "<div class='bagitems-result-table-wrapper'>";
            pHTML += "<p>"+name+"</p>";
            pHTML += "<div class='thumbnail'><img src='"+item.img+"' alt='"+item.simplename+"' width='50px' height='50px' /></div>";
            pHTML += "<div class='bagitems-result-selector'>";
            pHTML += "<a class='element"+index+" btn' href='javascript:void(0);' onclick='javascript:selectItem($(this),"+index+", 1)'>1</a>";
            pHTML += "<a class='element"+index+" btn' href='javascript:void(0);' onclick='javascript:selectItem($(this),"+index+", 2)'>2</a>";
            pHTML += "<a class='element"+index+" btn' href='javascript:void(0);' onclick='javascript:selectItem($(this),"+index+", 3)'>3</a>";
            pHTML += "<a class='element"+index+" btn' href='javascript:void(0);' onclick='javascript:selectItem($(this),"+index+", 4)'>4</a>";
            pHTML += "</div></div></td>";
            col++;
        });
        pHTML +="</table>";
        $("#bagitems-bag").html(pHTML);
        $("#bagitems-bag").fadeIn('slow', function(){
            $("#bagitems-result-table").show();
        });
    };

    function finishAnimation() {
        currentPlaceIndex++;
        if (currentPlaceIndex >= positions.length)
        {
            //show selector
            currentPlaceIndex = -1;
            currentPlace = null;
            $("#exercise-description").html("<?php echo _('Selecciona los elementos que crees que hay en la bolsa y pulsa en'); ?>&nbsp;&nbsp;<a class='btn btn-primary' href='javascript:void(0);' onclick='javascript:checkExercise();'><?php echo _('Finalizar'); ?></a>");
            showResultSelector();
        }
        else
        {
            currentPlace = places[currentPlaceIndex];
            if (currentPlaceIndex < 0) currentPlaceIndex = 0;
            if (typeof places[currentPlaceIndex].items != 'undefined')
            {
                if (places[currentPlaceIndex].items.length > 0)
                {
                    //show actions and then execute "gotoPlace()"
                    showBag(function(){
                        $("#bagitems-bag").fadeOut('slow', function(){
                            gotoPlace();
                        });
                    });
                }
                else gotoPlace();
            }
            else gotoPlace();
        }
    };

    function timeForSpace(space){
        //velocity is 100px/seg
        return (space/100.0)*1000;
    };

    function gotoPlace(){
        var orig = null;
        if (currentPlaceIndex == -1) orig = initialPosition;
        else orig = positions[currentPlaceIndex];

        var dest = null;
        if (currentPlaceIndex+1 >= positions.length) dest = initialPosition;
        else dest = positions[currentPlaceIndex+1];

        var xSpace = Math.abs(dest[0]-orig[0]+50);
        var ySpace = Math.abs(dest[1]-orig[1]+90);
        var xTime = timeForSpace(xSpace);
        var yTime = timeForSpace(ySpace);

        counterAnimation = 0;

        //decide animation
        if (dest[1]-orig[1] == 0)
        {
            //only horizontal movement
            $("#bagitem-character").animate(
                {left: dest[0]+50},
                { duration:xTime,
                    step: sprite,
                    easing: 'linear',
                    complete: finishAnimation
                });
        }
        else if (currentPlaceIndex+1 == 4)
        {
            //steps to the right
            $("#bagitem-character").animate(
                {left: "+=130"},
                { duration: timeForSpace(130),
                    step: sprite,
                    easing: 'linear',
                    complete: function(){
                        //mirror images
                        $("#bagitem-character-img1").addClass('bagitem-img-mirror');
                        $("#bagitem-character-img2").addClass('bagitem-img-mirror');
                        //begin now all the animation
                        $("#bagitem-character").animate({top: dest[1]+90},
                            {step: sprite,
                                duration:yTime,
                                easing:'linear',
                                complete:function(){
                                    $("#bagitem-character").animate(
                                        {left: dest[0]+50},
                                        { duration:xTime,
                                            step: sprite,
                                            easing: 'linear',
                                            complete: finishAnimation
                                        });
                                }});
                    }
                }
            );
        }
        else if (currentPlaceIndex+1 >= 8)
        {
            //steps to the left
            $("#bagitem-character").animate(
                {left: "-=100"},
                { duration: timeForSpace(100),
                    step: sprite,
                    easing: 'linear',
                    complete: function(){
                        //mirror images
                        $("#bagitem-character-img1").removeClass('bagitem-img-mirror');
                        $("#bagitem-character-img2").removeClass('bagitem-img-mirror');
                        //begin now all the animation
                        $("#bagitem-character").animate({top: dest[1]},
                            {step: sprite,
                                duration:1000,
                                easing:'linear',
                                complete: finishAnimation
                            });
                    }
                }
            );
        }
        else
        {
            $("#bagitem-character").animate({top: dest[1]+90},
                {step: sprite,
                    duration:yTime,
                    easing:'linear',
                    complete:function(){
                        $("#bagitem-character").animate(
                            {left: dest[0]+50},
                            { duration:xTime,
                                step: sprite,
                                easing: 'linear',
                                complete: finishAnimation
                            });
                    }});
        }
    };

    function beginAnimations() {
        var charHTML = "<div id='bagitem-character' class='hide' style='left:"+initialPosition[0]+"px; top:"+initialPosition[1]+"px;'>";
        //preload imgs
        charHTML += "<img id='bagitem-character-img1' src='exercises/img/BagItemsExercise/AbueloPaso1.png' alt='<?php echo _('personaje'); ?>' /><img id='bagitem-character-img2' src='exercises/img/BagItemsExercise/AbueloPaso2.png' alt='<?php echo _('personaje'); ?>' class='hide' />";
        charHTML += "</div>";

        showHomePlace(function(){
            $("#bagitems-bag").fadeOut('slow', function(){
                currentPlace = null;
                $("#mediaContainer").append(charHTML);
                $("#bagitem-character").fadeIn('slow', function(){
                    gotoPlace();
                });
            });
        });
    };

    function replayExercise() {
        initializeHome();
        takePlaces();
        takeItems();
        cart = [];
        hasBegun = true;
        currentPlace = null;
        currentPlaceIndex = -1;

        $("#exercise-description").html("<?php echo _('Observa las acciones que se van realizando en la ciudad.'); ?>");
        $("#replayExercise").fadeOut('slow', function(f){
            $("#mediaContainer").fadeOut('slow', function(evt){
                $("#introExercise").fadeOut('slow');
                $("#shoppingPanel").removeClass('hide').show();
                $("#mediaPanel").html(placesHTML());
                $("#mediaContainer").fadeIn('slow', function(e){
                    beginAnimations();
                });
            });
        });
    };

    function containsItem(arr, item) {
        for (var i=0; i<arr.length; i++)
        {
            if (arr[i].id == item.id) return true;
        }
        return false;
    };

    function range(n) {
        var result = [];
        for (var i=0; i<n; i++) result.push(i);
        return result;
    };

    function actionForID(actionID){
        var result = null;
        $.each(allplaces.Actions.Action, function(index, action){
            if (action.id == actionID)
            {
                result = action;
                return false;
            }
        });
        return result;
    };

    function characterNameForCharacterID(charID) {
        var name = "";
        $.each(allplaces.Characters.Character, function(index, character){
            if (character.id == charID)
            {
                name = character.name;
                return false;
            }
        });

        return name;
    };

    function takeItems() {
        //randomize items at shops
        workingPlaces = range(8);
        workingPlaces.sort(function(a,b){
            return 0.5-getRandom();
        });

        workingPlaces.splice(placesByLevel[level], workingPlaces.length-placesByLevel[level]);

        //number of actions to put
        var numberToPut = itemsByLevel[level]-2;
        for (var i=0; i<workingPlaces.length; i++)
        {
            var place = places[workingPlaces[i]];
            place.items = [];
        }

        var i=0;
        var potentialBag = [];
        while(numberToPut>0)
        {
            var place = places[workingPlaces[i]];
            if (typeof place.Item.length == 'undefined')
            {
                var action = place.Item;
                if (containsItem(homePlace.items, action) || containsItem(potentialBag, action))
                {
                    potentialBag.push(action)
                    place.items.push(action);
                    numberToPut--;
                }
            }
            else
            {
                //take random item
                var indices = range(place.Item.length);

                indices.sort(function(a,b){
                    return 0.5-getRandom();
                });

                for (var k=0; k<indices.length; k++)
                {
                    var action = place.Item[indices[k]];
                    var actionOBJ = actionForID(action.action);
                    if (actionOBJ.add == "0")
                    {
                        //check that if we have to give, then the element should be in the home
                        //or we are going to take during the "trip"
                        if (containsItem(homePlace.items, action) || containsItem(potentialBag, action))
                        {
                            if (!containsItem(place.items, action))
                            {
                                place.items.push(action);
                                numberToPut--;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (!containsItem(place.items, action))
                        {
                            potentialBag.push(action);
                            place.items.push(action);
                            numberToPut--;
                            break;
                        }
                    }
                }
            }
            var oldI = i;
            i = (i+1)%workingPlaces.length;
            //reset the potential bag if we are iterating again
            if (oldI > i) potentialBag = [];
        }
    };

    function takePlaces(){
        places = [];
        var maxPlaces = 8;

        allplaces.Place.sort(function(a,b){
            return 0.5-getRandom();
        });

        var count = 0;
        $.each(allplaces.Place, function(index, element){
            if (count < maxPlaces)
            {
                if (element.id != "casa") {
                    var location = positions[count];
                    element.x = location[0];
                    element.y = location[1];
                    element.items = [];
                    element.allitems = element.Item;
                    places.push(element);
                    count++;
                }
            }
            else return false;
        });
    };

    function indexOfBagID(itemID) {
        for (var i=0; i< bag.length; i++)
        {
            if (bag[i].id == itemID) return i;
        }
        return -1;
    }

    function initializeHome() {
        bag = [];
        homePlace = null;
        $.each(allplaces.Place, function(index, place){
            if (place.id == "casa")
            {
                homePlace = place;
                homePlace.items = [];
                return false;
            }
        });

        homePlace.Item.sort(function(a,b){
            return 0.5-getRandom();
        });

        var count = 0;
        var max = 2;
        $.each(homePlace.Item, function(index, action){
            if (count < max)
            {
                homePlace.items.push(action);
                count++;
            }
            else return false;
        });
    };

    function itemObjWithId(itemID){
        var result = null;
        $.each(allitems.Item, function(index, item){
            if (itemID == item.id)
            {
                result = item;
                return false;
            }
        });
        return result;
    };

    function animateOutBag(times, callback){
        if (times <= 0) callback();
        $("#shownActionImg").animate({/*left:'43%', top:'40%'*/}, 1000, function(){
            $("#shownActionImg").fadeOut(1000, function(){
                if (times-1 <= 0) callback();
                else
                {
                    // $("#shownActionImg").css({left:'25%'});
                    $("#shownActionImg").fadeIn(1000, function(){
                        animateOutBag(times-1, callback);
                    });
                }
            });
        });
    };

    function animateInBag(times, callback){
        if (times <= 0) callback();
        $("#shownActionImg").animate({/*left:'25%', top:'40%'*/}, 1000, function(){
            $("#shownActionImg").fadeOut(1000, function(){
                if (times-1 <= 0) callback();
                else
                {
                    // $("#shownActionImg").css({left:'43%'});
                    $("#shownActionImg").fadeIn(1000, function(){
                        animateInBag(times-1, callback);
                    });
                }
            });
        });
    };

    function showActions(actionIndex, allActions, callback){
        if (actionIndex < allActions.length)
        {
            var action = allActions[actionIndex];
            var actionOBJ = actionForID(action.action);
            var itemToShow = null;
            var add = false;
            if (actionOBJ.add == "0") {
                //take some from bag
                var bagIndex = indexOfBagID(action.id);
                if (bagIndex == -1)
                {
                    showActions(actionIndex+1, allActions, callback);
                    return;
                }
                var bagItem = bag[bagIndex];
                if (bagItem.quantity > 0)
                {
                    var quantity = Math.floor(getRandom()*bagItem.quantity)+1;
                    bagItem.quantity -= quantity;
                    itemToShow = {id:bagItem.id, quantity:quantity};
                }
                else
                {
                    showActions(actionIndex+1, allActions, callback);
                    return;
                }
            }
            else {
                //add to bag
                add = true;
                var quantity = Math.floor(getRandom()*3)+1;
                var index = indexOfBagID(action.id);
                var bagItem = null;
                if (index != -1)
                {
                    bagItem = bag[index];
                    bagItem.quantity += quantity;
                    if (bagItem.quantity > 3)
                    {
                        quantity = bagItem.quantity-3;
                        bagItem.quantity = 3;
                    }
                }
                else{
                    bagItem = {id: action.id, quantity:quantity};
                    bag.push(bagItem);
                }
                itemToShow = {id: bagItem.id, quantity:quantity};
            }

            if (itemToShow.quantity > 0)
            {
                var itemOBJ = itemObjWithId(itemToShow.id);
                var bagHTML = "<div class='package-delivery-place hide' id='bagItem' style='left:15%; top:30%; width:250px; height:250px;'>";
                bagHTML += "<img src='exercises/img/BagItemsExercise/bag.png' alt='bolsa' width='250' height='250' /></div>";
                $("#bagitems-bag").append(bagHTML);

                var pHTML = "<div class='package-delivery-place hide' id='shownAction' style='left:38%; top:30%; width:250px; height:200px;'>";
                pHTML += "<p class='lead' style='text-align:center;'> "+actionOBJ.name+" "+itemToShow.quantity+" "+((itemToShow.quantity==1)?itemOBJ.singularname:itemOBJ.pluralname)+" "+characterNameForCharacterID(action.character)+"</p></div>";
                if (add) pHTML += "<div class='package-delivery-place hide' id='shownActionImg' style='left:43%; top:40%; width:150px; height:150px;'>";
                else pHTML += "<div class='package-delivery-place hide' id='shownActionImg' style='left:25%; top:40%; width:150px; height:150px;'>";
                pHTML += "<p style='text-align:center;'><img width='150px' height='150px' src='"+itemOBJ.img+"' alt='"+itemOBJ.simplename+"' /></p></div>";
                $("#bagitems-bag").append(pHTML);
                $("#bagItem").fadeIn('slow');
                $("#shownActionImg").fadeIn('slow');
                $("#shownAction").fadeIn('slow', function(){
                    var funcToExecute;
                    if(add) funcToExecute = animateInBag;
                    else funcToExecute = animateOutBag;

                    funcToExecute(itemToShow.quantity, function(){
                        $("#bagItem").fadeOut('slow');
                        if (add) $("#shownActionImg").fadeOut('slow');
                        $("#shownAction").fadeOut('slow', function(){
                            $("#shownAction").remove();
                            $("#shownActionImg").remove();
                            $("#bagItem").remove();
                            showActions(actionIndex+1, allActions, callback);
                        });
                    });

                });
            }
            else
            {
                showActions(actionIndex+1, allActions, callback);
                return;
            }
        }
        else callback();
    };

    function showHomePlace(callback){
        $("#bagitems-bag").fadeIn('slow', function(){
            currentPlace = homePlace;

            var pHTML = "<div class='package-delivery-place hide' id='shownPlace' style='left:42%; top:30%; width:150px; height:150px;'>";
            pHTML += "<p class='lead' style='text-align:center;'><?php echo _('En'); ?> "+currentPlace.name+":</p>";
            pHTML += "<img width='150px' height='150px' src='"+currentPlace.img+"' alt='"+currentPlace.name+"' /></div>";
            $("#bagitems-bag").append(pHTML);
            $("#shownPlace").fadeIn('slow', function(){
                setTimeout(function(){
                    $("#shownPlace").fadeOut('slow', function(){
                        $("#shownPlace").remove();
                        showActions(0, currentPlace.items, callback);
                    });
                }, 1000);
            });
        });
    }

    function showCurrentPlace(callback){
        //remove all the items in the currentPlace whose bagItem quantity is 0
        var itemsToRemove = [];
        $.each(currentPlace.items, function(index, action){
            var actionOBJ = actionForID(action.action);
            var bagIndex = indexOfBagID(action.id);
            if (bagIndex >= 0 && actionOBJ.add == "0")
            {
                var bagItem = bag[bagIndex];
                if (bagItem.quantity == 0) itemsToRemove.push(index);
            }
        });

        while(itemsToRemove.length > 0)
        {
            currentPlace.items.splice(itemsToRemove[0], 1);
            itemsToRemove.splice(0, 1);
        }

        if (currentPlace.items.length > 0)
        {
            var frame = frameOfPlaceAtIndex(currentPlaceIndex);
            var pHTML = "<div class='package-delivery-place hide' id='shownPlace' style='left:42%; top:30%; width:"+frame[2]+"px; height:"+frame[3]+"px;'>";
            pHTML += "<p class='lead' style='text-align:center;'><?php echo _('En'); ?> "+currentPlace.name+":</p>";
            pHTML += "<img width='"+frame[2]+"px' height='"+frame[3]+"px' src='"+ currentPlace.img+"' alt='"+currentPlace.name+"' /></div>";
            $("#bagitems-bag").append(pHTML);
            $("#shownPlace").fadeIn('slow', function(){
                setTimeout(function(){
                    $("#shownPlace").fadeOut('slow', function(){
                        $("#shownPlace").remove();
                        showActions(0, currentPlace.items, callback);
                    });
                }, 1000);
            });
        }
    }

    function showBag(callback) {
        $("#bagitems-bag").fadeIn('slow', function(){
            showCurrentPlace(function(){
                callback();
            });
        });
    };

    $(function() {
        repetitions = 0;
        sessionID = parseInt(lastSession['sessionID']);
        exerciseID = parseInt(lastSession['exerciseID']);

        $.ajax({
            type: "GET",
            url: placesXML,
            dataType: "xml",
            success: function(placesXMLResult) {
                allplaces = $.xml2json(placesXMLResult);
                $.ajax({
                    type: "GET",
                    url: itemsXML,
                    dataType: "xml",
                    success: function(itemsXMLResult) {
                        allitems = $.xml2json(itemsXMLResult);

                        initializeHome();
                        takePlaces();
                        takeItems();

                        numberOfReplays = Math.ceil(exercise.duration()/2);

                        //show the panels
                        $("#mediaPanel").css('width', allplaces.width);
                        $("#mediaPanel").css('height', allplaces.height);
                        $("#mediaContainer").css('width', allplaces.width);
                    }
                });
            }
        });
    });

</script>