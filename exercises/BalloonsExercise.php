<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!BalloonsExercise) {

var BalloonsExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry, longTerm) {
		this._super('<?php echo _('&iexcl;Globos!'); ?>', '<?php echo _('<strong>&iexcl;Vamos a entrenar tu atención!</strong> Para ello, en este ejercicio deberás fijarte en los globos con letras que aparecerán en el cuadro de abajo. Tu tarea consistirá en pulsar sobre el fondo azul cuando aparezcan dos o más globos seguidos con la misma letra. Pulsa <strong>Continuar</strong>.'); ?>', 'exercises/BalloonsExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._seconds = 0;
		this._omissions = 0;
		this._level = 0;
		this._subLevel = 0;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	increaseCorrects : function(c) {
		this._corrects++;
	},
	omissions : function() {
		return this._omissions;
	},
	setOmissions : function(c) {
		this._omissions = c;
	},
	demoUIurl : function() {
		//override to return the demo UI
		/*if (parseInt(this.sessionID())==3) return "exercises/BalloonsExerciseDemoUI.php";
		else return null;*/
		return "exercises/BalloonsExerciseDemoUI.php";
	},
	increaseOmissions : function(c) {
		this._omissions++;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	increaseFails : function(f) {
		this._fails++;
	},
	updateResults : function() {
		this.finishExercise();
	},
	level : function() {
		return this._level;
	},
	setLevel : function(l) {
		this._level = l;
	},
	subLevel : function() {
		return this._subLevel;
	},
	setSubLevel : function(l) {
		this._subLevel = l;
	},
	canAvoidHelp : function(){
		if (this._sessionID == 6 && !this._repeating)
		{
			return true;
		}
		else return false;
	},
	updateUserLevel : function() {
		var qry = 'exerciseType=4&userLevel='+this._level+'&userSubLevel='+this._subLevel;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_level.php',
			success: function(data){	
			}
		});
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		this.updateUserLevel();
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_omissions_exercise_medal.php',
						success: function(data2){
							obj._medal = parseInt(data2);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
