<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!FirstHelpExercise) {

var FirstHelpExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Gracias por completar el cuestionario!'); ?>', '<?php echo _('Ahora vamos a hacer varios ejercicios que medirán cómo está actualmente tu memoria, atención, planificación y razonamiento. Al final de cada ejercicio conseguir&aacute;s una medalla de oro, plata o bronce, seg&uacute;n cómo lo hayas hecho, as&iacute; que &iexcl;intenta hacerlo lo mejor posible!'); ?>', 'exercises/FirstHelpExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
	}
});

}
</script>
