<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!WordListExercise) {

var WordListExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry, longTerm) {
		if (longTerm)
		{
			this._super('<?php echo _('&iexcl;Lista de Palabras (Largo plazo)!'); ?>', '<?php echo _('Ahora vamos a ver si recuerdas la <span style=\"color:blue\">LISTA AZUL</span> que memorizaste al principio de esta sesión. Se te mostró tres veces. Para ello, en primer lugar, tendrás que escribir las palabras que recuerdes en un recuadro que se te mostrará, sin importar el orden ni las faltas de ortografía. Lo importante es que sepamos cuáles recuerdas. Intenta ser rápido para que no se te olvide ninguna palabra. Cada vez que escribas una palabra debes pulsar el botón <strong>Escribir siguiente palabra</strong>. Cuando no recuerdes ninguna más, pulsa <strong>No recuerdo más palabras</strong>. Pulsa <strong>Continuar</strong> cuando estés preparado.'); ?>', 'exercises/WordListExerciseAlternativeUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		}
		else
		{
			this._super('<?php echo _('&iexcl;Lista de Palabras!'); ?>', '<?php echo _('En este ejercicio te mostraré una lista de palabras que tendrás que memorizar durante un minuto. Una vez transcurrido ese tiempo, tendrás que escribir las palabras que recuerdes sin importar el orden y las faltas de ortografía. Lo importante es que sepamos cuáles recuerdas, aunque no escribas las palabras completas. Debes intentar ser rápido al escribirlas para que no se te olvide ninguna. Lo harás tres veces de manera que puedas aprenderlas. Cuando estés listo pulsa <strong>Continuar</strong>.'); ?>', 'exercises/WordListExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		}
		this._corrects = 0;
		this._fails = 0;
		this._seconds = 0;
		this._omissions = 0;
		this._wordlist = "";
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	omissions : function() {
		return this._omissions;
	},
	setOmissions : function(c) {
		this._omissions = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	setWordlist : function(f) {
		this._wordlist = f;
	},
	wordlist : function() {
		return this._wordlist;
	},
	updateResults : function(shownWords, rec) {
		this.finishExercise();
		this.updateWordlist(shownWords, rec);
	},
	updateWordlist : function(shownWords, rec) {
		var list = this._wordlist.split("\n").join();
		
		this.updatePartialResult(JSON.stringify({"writtenWords":list, "time":this._seconds, "corrects":this._corrects, "fails": this._fails, "omissions": this._omissions, "shownWords":shownWords.join(), "recognizementList": rec}), this._repetition);
		this._repetition += 1;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_omissions_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) >= parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
