<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise">
	<p class="lead" id="introText"><?php echo _('Deberás escribir las palabras que recuerdes de la <span style="color:blue;">LISTA AZUL</span>. Para ello, deberás ir escribiendo cada palabra en un recuadro que se te mostrará y pulsar el botón <strong>Escribir Siguiente Palabra</strong>. Cuando termines de escribir las palabras que recuerdes deberás pulsar el botón <strong>No recuerdo más palabras</strong>.'); ?></p>
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="wordlistExerciseAnswers" class="hide">
	<div style="margin-left:5px; text-align: center;"><input type="text" name="answersList" id="answersList" cols="50" class="input-large" style="font-size:24px; height:35px;" />&nbsp;<a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:nextWord();" style="margin-top:-10px;"><?php echo _('Escribir Siguiente Palabra'); ?></a></div>
	<div style="margin-top:80px;margin-left:5px;text-align: center;"><a class="btn btn-primary btn-large" href="javascript:void(0);" onclick="javascript:checkExercise();" id="continue-buttonAlt"><?php echo _('No recuerdo más palabras'); ?></a></div>
</div>

<!--'.'-->

<div id="wordlistExercise" class="hide" style="margin-left: 25%; margin-right: 25%;">
	<!--<div class="pagination pagination-centered pull-left">
		<ul>
			<li id='leftPage'><a href="javascript:void(0);" onclick="javascript:previousPage();">&larr; <?php echo _('Anterior'); ?></a></li>
			<li id='rightPage'><a href="javascript:void(0);" onclick="javascript:nextPage();"><?php echo _('Siguiente'); ?>&nbsp;&rarr;</a></li>
		</ul>
		<div class="pull-right" style="margin-left:20px;margin-top:4px;"><p><a class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:checkExercise();" id="continue-button"><?php echo _('Listo'); ?></a></p></div>
	</div>-->
	<div style="font-size:48px;" id="WordList"></div>
	<div class="text-center" style="margin-top:70px;"><a class="btn btn-success btn-large" href="javascript:void(0);" onclick="javascript:accept();"><i class="icon-thumbs-up icon-white"></i> <?php echo _('Sí'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-danger btn-large" href="javascript:void(0);" onclick="javascript:decline();"><i class="icon-thumbs-down icon-white"></i> <?php echo _('No'); ?></a></div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var wordlistExercise = null;
var correctWords = null;
var checkedWords = null;
var timing = new Timer();
var blueColor = "#0000ff";
var redColor = "#ff0000";
var executionNumber = 0;
var currentPage = 0;
var totalAnswerList = "";
var shouldShow = true;
var shownWords = null;

function nextWord(){
	totalAnswerList += $("#answersList").val()+"\n";
	$("#answersList").val("");
	$("#answersList").focus();
};

function previousPage() {
	$("#continue-button").fadeOut('fast');
	
	if (currentPage == 1) loadList(0);
	else if (currentPage == 2) loadList(1);
	else if (currentPage == 3) loadList(2);
};

function nextPage() {
	if (currentPage == 0) loadList(1);
	else if (currentPage == 1) loadList(2);
	else if (currentPage == 2)
	{
		$("#continue-button").fadeIn('slow');
		loadList(3);
	}
};

function accept(){
	checkedWords[currentPage] = true;
	currentPage++;
	loadList(currentPage);
};

function decline(){
	checkedWords[currentPage] = false;
	currentPage++;
	loadList(currentPage);
};

function toggleClass(element){
	var index = parseInt(element.attr('id').split("word")[1]);
	checkedWords[index] = !checkedWords[index];
	
	if (element.hasClass("wordlist-toggle"))
	{
		element.removeClass("wordlist-toggle");
	}
	else
	{
		element.addClass("wordlist-toggle");
	}
};

function loadList(page) {
	/*	
	currentPage = page;
	var initialIndex = page*9;

	$("#WordList").css("color", blueColor);
	var finalValue = "<table style='margin-left:auto; margin-right:auto;'>";
	$.each(wordlistExercise, function(index, element){
		if (index >= initialIndex && index < initialIndex+9)
			finalValue += "<tr><td><a id='word"+index+"' href='javascript:void(0);' class='thumbnail wordlist-exercise text-center "+(checkedWords[index]?"wordlist-toggle":"")+"' onclick='javascript:toggleClass($(this));' style='text-align:center;width:320px;padding:10px;'>"+element+"</a></td></tr>";
	});
	finalValue += "</table>";
	
	$("#page0").removeClass('active');
	$("#page1").removeClass('active');
	$("#page2").removeClass('active');
	$("#page3").removeClass('active');
	
	$("#page"+page).addClass('active');
	if (page == 0) $("#leftPage").addClass('active');
	else $("#leftPage").removeClass('active');
	
	if (page == 3) $("#rightPage").addClass('active');
	else $("#rightPage").removeClass('active');
	
	$("#WordList").html(finalValue);*/
	
	if (page >= wordlistExercise.length)
	{
		checkExercise();
	}
	else
	{
		$("#WordList").css("color", blueColor);
		var finalValue = "<p class='wordlist-exercise text-center' id='word"+page+"'>"+wordlistExercise[page]+"</p>";
		
		$("#WordList").html(finalValue);
	}
}

function beginExercise()
{
	if (!exercise.afterDemo() && shouldShow)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				showMiniMenu(true);
				$("#exerciseCounter").show();
				shouldShow = false;
				setTimeout(beginExercise, 4000);
			});
		});
	}
	else
	{
		currentPage = 0;
		$("#finalExercise").fadeOut('slow');
		$("#introExercise").fadeOut('slow', function(){
			
			if (executionNumber == 0)
			{
				totalAnswerList = "";
				
				showMiniMenu(true);
				$("#exercise-description").html('<?php echo _('Escribe las palabras que recuerdes de la <span style="color:blue">LISTA AZUL</span>. Para ello, deberás ir escribiendo cada palabra en el recuadro que se te muestra y pulsar el botón <strong>Escribir Siguiente Palabra</strong>. Cuando termines de escribir las palabras que recuerdes, pulsa <strong>No recuerdo más palabras</strong>.'); ?>');
			}
			else
			{
				showMiniMenu(true);
				$("#exercise-description").html('<?php echo _('Indica si las palabras que se muestran estaban o no en la <span style="color:blue">LISTA AZUL</span> que memorizaste. Para ello, deberás pulsar en los botones <strong>Sí</strong> o <strong>No</strong>.'); ?>');
			}
			
			$("#answersList").val("");
			repetitions = 0;
			sessionID = parseInt(lastSession['sessionID']);
			exerciseID = parseInt(lastSession['exerciseID']);
			
			//retrieve current exercise plan
			$.ajax({
				type: "GET",
				url: "exercises/xml/wordlist-plan.xml",
				dataType: "xml",
				success: function(xml) {
					var plan = $.xml2json(xml);
					var numberOfExercise = -1;
					
					$.ajax({
						type: "GET",
						url: "exercises/xml/wordlist-series.php",
						dataType: "xml",
						success: function(result){
							var wordlistExercises = $.xml2json(result);
							$.each(plan.WordListPlanEntry, function(index, value){
								if (value.exerciseID == exerciseID && value.sessionID == sessionID)
								{
									//the first are the corrects
									var correctWordsAux = wordlistExercises.WordListExercise[value.list];
									
									correctWords = [];
									$.each(correctWordsAux.Elements.Element, function(index, element){
										correctWords.push(element.value);
									});
									
									var maxIndex = 36-correctWords.length;
									var otherWords = [];
									$.each(wordlistExercises.WordListExercise, function(index, element){
										if (element.id == "randomWords")
										{
											element.Elements.Element.sort(function(){
												return 0.5 - getRandom();
											});
											$.each(element.Elements.Element, function(index, element){
												if (index>=maxIndex) return false;
												otherWords.push(element.value);
											});
											return false;
										}
									});
									
									//create a copy of the array
									wordlistExercise = correctWords.slice(0);
									shownWords = correctWords;
									wordlistExercise = correctWords.concat(otherWords);
									
									wordlistExercise.sort(function(){
										return 0.5 - getRandom();
									});
									
									checkedWords = [];
									$.each(wordlistExercise, function(index, element){
										checkedWords.push(false);
									});
														
									return false; //break the loop
								}
							});
							
							if (wordlistExercise != null){
								if (executionNumber == 0)
								{
									$("#wordlistExerciseAnswers").fadeIn('slow', function(){
                                        timing.reset();
                                        timing.start();
									});
								}
								else
								{
									loadList(0);
									$("#wordlistExerciseAnswers").fadeOut('slow', function(){
										showMiniMenu(true);
										$("#wordlistExercise").fadeIn('slow', function(evt){
                                            timing.reset();
                                            timing.resume();
										});
									});
								}
							}
							
							else console.debug("error: wordlist exercise not defined");
						}
					});
				}
			});
		});
	}
}

function checkExercise(){
	currentPage = 0;
    timing.stop();
	
	//compare the written list with the original list
	var corrects = 0;
	var fails = 0;
	var omissions = 0;
	
	var rec = null;
	if (executionNumber == 0)
	{
		if ($("#answersList").val() != "")  //the user entered a word, but didn't click on next word button
		{
			nextWord();
		}
		
		var lines = totalAnswerList.split("\n");
		
		var ansList = [];
		for (var i=0; i<lines.length; i++)
		{
			lines[i] = $.trim(lines[i]);
			
			if (lines[i].length > 0) ansList.push(lines[i]);
		}
		
		exercise.setWordlist(ansList.join("\n"));
		
		//check the correct ones
		$.each(shownWords, function(index, element){
			var entered = false;
			var trimmedValue = $.trim(element).toLowerCase();
			for (var i=0; i<lines.length; i++)
			{
				if (lines[i].toLowerCase()==trimmedValue)
				{
					corrects++;
					entered = true;
					break;
				}
			}
			if (!entered) omissions++;
		});
		
		//check the fails: one written element is not in the word list
		for (var i=0; i<lines.length; i++)
		{
			var comp = lines[i].toLowerCase();
			if (comp.length > 0)
			{
				var entered = false;
				$.each(shownWords, function(index, element){
					if (comp==$.trim(element).toLowerCase())
					{
						entered = true;
						return false;
					}
				});
				
				if (!entered) fails++;
			}
		}
	}
	else
	{
		shownWords = correctWords;
		rec = {};
		$.each(wordlistExercise, function(index, element){
			rec[element] = (checkedWords[index])?'1':'0';
			if (checkedWords[index]) //if checked
			{
				exercise.setWordlist(exercise.wordlist()+element+"\n");
				
				//check if the element is in correct words
				if (correctWords.indexOf(element) >= 0) corrects++;
				else fails++;
			}
			else
			{
				//if not checked, but in correct words, then it's an omission
				//if (correctWords.indexOf(element) >= 0) omissions++;
				if (correctWords.indexOf(element) >= 0) fails++;
				else corrects++;
			}
		});
	}
	
	exercise.setSeconds(timing.getSeconds());
	exercise.setCorrects(corrects);
	exercise.setFails(fails);
	exercise.setOmissions(omissions);
	
	exercise.updateResults(shownWords, rec);
	
	exercise.setWordlist("");
	
	executionNumber++;
	if (executionNumber>=2)
	{
		exercise.setSeconds(0);
		exercise.setFails(0);
		exercise.setCorrects(0);
		exercise.setOmissions(0);
	
		endExercise();
	}
	else
	{
		var element = (executionNumber==1)?'#wordlistExerciseAnswers':'#wordlistExercise';
		
		$(element).fadeOut('slow', function(){
			showInstructions(true);
			$("#introText").hide();
			$("#exercise-description").html('<?php echo _('Ahora se te van a mostrar distintas palabras de una en una, algunas de ellas son las que aparecieron en la <span style=\"color:blue\">LISTA AZUL</span> que se te mostró tres veces y que tuviste que memorizar, otras no las has memorizado en ningún momento. Tienes que intentar recordar y pulsar <strong>SI</strong>, en aquellas palabras que estaban en esa lista y, <strong>NO</strong> en aquellas que no estaban en la lista.'); ?>');
			$("#introExercise").fadeIn('slow');
		});
	}
};
</script>
