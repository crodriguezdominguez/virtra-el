<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise">
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="wordlistExercise" class="hide" style="width:50%">
	<center><p class="lead" id="timingP" style="text-align:center;"><span id="timer">0</span>/<span id="max-timer"></span> <?php echo _('segundos'); ?></p></center>
	<center><div class="lead" id="WordList"></div></center>
	<center><p class="lead" id="repetitionID"></p></center>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var correctWords = null;
var checkedWords = null;
var timing = 0;
var resultTiming = 0;
var endTiming = false;
var executionNumber = 0;
var maxMemoryTime = 30;
var maxExecutionNumber = 0;
var memoryObjects = [];
var selectedObjects = [];
var shownObjects = [];
var checkedObjects = [];
var exerciseLevel = 0;
var numRowsMemoryPerLevel = [3, 3, 4, 4, 5];
var numColsMemoryPerLevel = [3, 4, 4, 5, 6];
var numRowsSelectionPerLevel = [3, 3, 4, 4, 5];
var numColsSelectionPerLevel = [6, 8, 8, 10, 12];
var numObjectsPerCategoriesPerLevel = [3, 4, 4, 5, 6];
var numCategoriesPerLevel = [3, 3, 4, 4, 5];
var shouldShow = true;

function showMemoryObjects(){
	$("#timingP").show();
	var finalValue = "<table style='margin-left:auto; margin-right:auto;'>";
	var col = 0;
	var row = 0;
	$.each(memoryObjects, function(index, item){
		if (col == 0) finalValue+="<tr>";
		else if (col == numColsMemoryPerLevel[exerciseLevel])
		{
			finalValue += "</tr>";
			col = 0;
			row++;
		}
		finalValue += "<td><img class='thumbnail' src='"+item.src+"' alt='' width='80' height='80' /></td>";
		col++;
	});
	finalValue += "</table>";
	$("#WordList").html(finalValue);
};

function showObjects(){
	$("#timingP").hide();
	showMiniMenu(true);
	$("#exercise-mini-description").html('<?php echo _('Selecciona las imágenes que has memorizado y pulsa <strong>Listo</strong>'); ?>');
	$("#exercise-description").html('<?php echo _('Selecciona las imágenes que has memorizado y pulsa <strong>Listo</strong>. Si te equivocas al seleccionar una imagen, podrás pulsar de nuevo sobre ella para deseleccionarla.'); ?>');
	var finalValue = "<a class='btn btn-primary btn-large pull-left' style='margin-left:18px;margin-bottom:30px;' href='javascript:void(0);' onclick='javascript:checkExercise();' id='check-exercise-button'><?php echo _('Listo'); ?></a><table style='margin-left:auto; margin-right:auto;'>";
	var col = 0;
	var row = 0;
	$.each(shownObjects, function(index, item){
		if (col == 0) finalValue+="<tr>";
		else if (col == numColsSelectionPerLevel[exerciseLevel])
		{
			finalValue += "</tr>";
			col = 0;
			row++;
		}
		finalValue += "<td><a id='word"+index+"' href='javascript:void(0);' class='thumbnail wordlist-exercise text-center' onclick='javascript:toggleClass($(this));' style='text-align:center;width:80px;height:80px;'><img src='"+item.src+"' alt='' width='80' height='80' /></a></td>";
		col++;
	});
	finalValue += "</table>";
	$("#WordList").html(finalValue);
};

function timerFunction(){
	timing++;
	
	if (timing >= maxMemoryTime){
		$("#timer").html("0");
		showObjects();
	}
	else
	{
		$("#timer").html(timing);
		setTimeout(timerFunction, 1000);
	} 
};

function toggleClass(element){
	var index = parseInt(element.attr('id').split("word")[1]);
	checkedObjects[index] = !checkedObjects[index];
	
	if (element.hasClass("classify-objects-toggle"))
	{
		element.removeClass("classify-objects-toggle");
	}
	else
	{
		element.addClass("classify-objects-toggle");
	}
};

function checkExercise(){
	endTiming = true;
	
	$("#check-exercise-button").fadeOut('fast').remove();
	$("#WordList").html('<a class="btn btn-primary btn-large pull-left" style="margin-left:18px;margin-bottom:30px;" href="javascript:void(0);" id="continue-button"><?php echo _('Continuar'); ?></a>'+$("#WordList").html());
	
	//compare the written list with the original list
	var corrects = 0;
	var fails = 0;
	var omissions = 0;
	$.each(shownObjects, function(index, element){
		$("#word"+index).removeClass('classify-objects-toggle');
		$("#word"+index).attr('onclick', 'javascript:void(0)');
		$("#word"+index).unbind('click');
		if (checkedObjects[index]) //if checked
		{
			//check if the element is in correct words
			if (containsItem(memoryObjects, element)){
				corrects++;
				$("#word"+index).addClass('classify-objects-ok');
			} 
			else{
				fails++;
				$("#word"+index).addClass('classify-objects-bad');
			} 
		}
		else
		{
			//if not checked, but in correct words, then it's an omission
			if (containsItem(memoryObjects, element)){
				omissions++;
				$("#word"+index).addClass('classify-objects-omission');
			}
		}
	});
	
	if (fails == 0 && omissions <= 1)
	{
		exerciseLevel++;
		if (exerciseLevel>4) exerciseLevel = 4;
		exercise.setLevel(exerciseLevel);
		exercise.updateUserLevel();
	}
	
	exercise.setSeconds(timing);
	exercise.setCorrects(corrects);
	exercise.setFails(fails);
	exercise.setOmissions(omissions);
	
	exercise.updateResults();
	
	executionNumber++;
	
	$("#repetitionID").html("<strong><?php echo _('Repetición:'); ?></strong> "+(executionNumber+1)+"/"+maxExecutionNumber);
	
	if (executionNumber>=maxExecutionNumber)
	{
		exercise.setSeconds(0);
		exercise.setFails(0);
		exercise.setCorrects(0);
		exercise.setOmissions(0);
	
		$("#exercise-mini-description").html('');
		endExercise();
	}
	else
	{
		showInstructions(true);
		$("#exercise-description").html('<span style="color:red;font-weight:bold;"><?php echo _('Fallos'); ?>:</span> <strong>'+fails+'</strong>, <span style="color:green;font-weight:bold;"><?php echo _('aciertos'); ?>:</span> <strong>'+corrects+'</strong>, <span style="color:rgb(171, 171, 0);font-weight:bold;"><?php echo _('has olvidado'); ?>:</span> <strong>'+omissions+'</strong>. <?php echo _('Cuando estés listo pulsa en <strong>Continuar</strong>'); ?>.');
		$("#continue-button").bind('click', function(f){
			$("#wordlistExercise").fadeOut('slow', function(evt){
				$("#exercise-description").html("<?php echo _('Se te va a mostrar otro conjunto de imágenes para que las memorices.'); ?>");
				$("#introExercise").fadeIn('slow');
			});
		});
	}
};

function containsItem(arrayItems, item) {
	var result = false;
	$.each(arrayItems, function(index, compare){
		if (item.img == compare.img)
		{
			result = true;
			return false;
		}
	});
	return result;
};

function beginExercise()
{
	$("#exercise-mini-description").html('');
	
	if (shouldShow)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				$("#exerciseCounter").show();
				shouldShow = false;
				setTimeout(beginExercise, 4000);
			});
		});
	}
	else
	{
		shouldShow = true;
		
		var amount = numColsMemoryPerLevel[exerciseLevel]*numRowsMemoryPerLevel[exerciseLevel];
		if (amount < 12)
		{
			maxMemoryTime = 30;
		}
		else if (amount >= 12 && amount < 16)
		{
			maxMemoryTime = 45;
		}
		else if (amount >= 16 && amount < 20)
		{
			maxMemoryTime = 60;
		}
		else if (amount >= 20 && amount < 24)
		{
			maxMemoryTime = 75;
		}
		else if (amount >= 24 && amount < 28)
		{
			maxMemoryTime = 90;
		}
		else
		{
			maxMemoryTime = 90;
		}
		
		$("#max-timer").html(maxMemoryTime);
		
		$("#introExercise").fadeOut('slow', function(){
			showMiniMenu(true);
			$("#exercise-description").html("<?php echo _('Memoriza las imágenes que te mostramos.'); ?>");
					
			$.ajax({
				type: "GET",
				url: "exercises/xml/classify-objects-categories.php",
				dataType: "xml",
				success: function(result){
					var objects = $.xml2json(result);
					//shuffle categories
					/*objects.Category.sort(function(a,b){
						return 0.5-getRandom();
					});*/
					
					//shuffle items in categories
					var allitems = [];
					$.each(objects.Category, function(index, cat){
						for (var i=0; i<cat.Item.length; i++)
						{
							var item = cat.Item[i];
							item.src = cat.folder+item.img;
						}
						
						/*cat.Item.sort(function(a,b){
							a.src = cat.folder+a.img;
							b.src = cat.folder+b.img;
							return 0.5-getRandom();
						});*/
						allitems = allitems.concat(cat.Item);
					});
					
					/*
					allitems.sort(function(a,b){
						return 0.5-getRandom();
					});*/
					
					var cats = [];
					$.each(objects.Category, function(index, cat){
						if (index < numCategoriesPerLevel[exerciseLevel])
						{
							cats.push(cat);
						}
						else return false;
					});
					
					shownObjects = [];
					memoryObjects = [];
					checkedObjects = [];
					$.each(cats, function(index, cat){
						$.each(cat.Item, function(j, item){
							if(j < numObjectsPerCategoriesPerLevel[exerciseLevel])
							{
								item.src = cat.folder+item.img;
								shownObjects.push(item);
								memoryObjects.push(item);
								checkedObjects.push(false);
							}
							else return false;
						});
					});
					
					var totalObjectsToAdd = (numRowsSelectionPerLevel[exerciseLevel]*numColsSelectionPerLevel[exerciseLevel])- shownObjects.length;
					var count = 0;
					$.each(allitems, function(index, item){
						if (count < totalObjectsToAdd)
						{
							if (!containsItem(shownObjects, item))
							{
								count++;
								shownObjects.push(item);
								checkedObjects.push(false);
							}
						}
						else return false;
					});
					
					shownObjects.sort(function(a,b){
						return 0.5-getRandom();
					});
					
					/*
					memoryObjects.sort(function(a,b){
						return 0.5-getRandom();
					});*/
					
					showMemoryObjects();
					$("#wordlistExercise").fadeIn('slow', function(evt){
						timing = 0;
						endTiming = false;
						timerFunction();
					});
				}
			});
		});
	}
}

$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	//var minutesPerExecution = maxMemoryTime/60.0;
	maxExecutionNumber = exercise.duration(); //Math.ceil(exercise.duration()/minutesPerExecution);
	$("#max-timer").html(maxMemoryTime);
	$("#repetitionID").html("<strong><?php echo _('Repetición:'); ?></strong> "+(executionNumber+1)+"/"+maxExecutionNumber);
	
	if (exercise.afterDemo())
	{
		$("#exercise-description").fadeOut('slow', function(){
			$("#exercise-description").html("<?php echo _('Ahora tendrás que memorizar las imágenes que te mostraremos. Tras un determinado tiempo deberás seleccionar las que recuerdes de entre un conjunto de imágenes.'); ?>");
			$("#exercise-description").fadeIn('slow');
			shouldShow = false;
		});
		exerciseLevel = exercise.level();
	}
});

</script>
