<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!VRExercise) {

var VRExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry, pre) {
		this._super('<?php echo _('&iexcl;Realidad Virtual!'); ?>', '<?php echo _('En este ejercicio llevarás a cabo tareas de realidad virtual.');?>', pre?'exercises/VRExerciseUI.php?pre=1':'exercises/VRExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._omissions = 0;
		this._seconds = 0;
		this._finalScore = 0;
		this._level = 0;
		this._pre = pre;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	finalScore : function() {
		return this._finalScore;
	},
	setFinalScore : function(f) {
		this._finalScore = f;
	},
	setOmissions: function(f){
		this._omissions = f;
	},
	omissions: function(f){
		return this._omissions;
	},
	updateResults : function() {
		this.finishExercise();
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	increaseOmissions : function() {
		this._omissions++;
	},
	setLevel : function(f) {
		this._level = f;
	},
	updateUserLevel : function() {
		var qry = 'exerciseType='+(this._pre?'20':'21')+'&userLevel='+this._level+'&userSubLevel=0';
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_level.php',
			success: function(data){	
			}
		});
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		this.updateUserLevel();
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var finalScore = this._finalScore;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&finalScore='+finalScore+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_score_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
