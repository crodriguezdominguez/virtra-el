<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!ClassifyExercise) {

var ClassifyExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('¿Cuál de los objetos es diferente?'); ?>', '<?php echo _('<strong>&iexcl;Perfecto, trabajemos razonamiento!</strong> Cuando comiences el ejercicio, pulsa el dibujo que no encaje con el resto. A continuación, pulsa el botón <strong>Listo</strong>. Dispones de un botón <strong>Ayuda</strong>, por si necesitas alguna pista para completar el ejercicio.'); ?>', 'exercises/ClassifyExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._correct = false;
	},
	correct : function() {
		return this._correct;
	},
	setCorrect : function(cor) {
		this._correct = cor;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = 0;
		var countFails = 0;
		
		if (this._correct)
		{
			countCorrects = 1;
		}
		else countFails = 1;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_reasoning_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		else return {medal: false, type:0};
	}
});

}
</script>
