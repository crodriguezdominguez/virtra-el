<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!TaskListExercise) {

var TaskListExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry, longTerm) {
		if (longTerm)
		{
			this._super('<?php echo _('&iexcl;Lista de Recados (Largo plazo)!'); ?>', '<?php echo _('<strong>&iexcl;Genial, trabajemos memoria!</strong> Ahora vamos a intentar recordar todos los recados que tuviste que memorizar al principio de la sesión. Para ello, irán apareciendo recados de uno en uno. Deberás marcar SI en los recados que aparecieron en la lista y, NO a los que no tuviste que aprender. Cuando estés preparado pulsa <strong>Continuar</strong>.'); ?>', 'exercises/TaskListExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		}
		else
		{
			this._super('<?php echo _('&iexcl;Lista de Recados!'); ?>', '<?php echo _('¡En este ejercicio trabajaremos memoria! Voy a mostrarte durante un tiempo una lista con varios recados que tendrás que tratar de memorizar. Cuando estés preparado pulsa <strong>Continuar</strong>.'); ?>', 'exercises/TaskListExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		}
		this._longTerm = longTerm;
		this._corrects = 0;
		this._fails = 0;
		this._seconds = 0;
		this._omissions = 0;
		this._counterRep = 0;
	},
	isLongTerm : function() {
		return this._longTerm;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	omissions : function() {
		return this._omissions;
	},
	setOmissions : function(c) {
		this._omissions = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	updateResults : function(shownWords, rec) {
		this.finishExercise();
		
		this.updatePartialResult(JSON.stringify({"corrects":this._corrects, "fails": this._fails, "omissions": this._omissions, "shownWords":shownWords.join(), "time":this._seconds, "recognizementList": rec}), this._counterRep);
		this._counterRep += 1;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_omissions_exercise_medal.php',
						success: function(data2){
							obj._medal = parseInt(data2);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
