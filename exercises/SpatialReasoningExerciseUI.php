<?php
	require_once('locale/localization.php');
?>

<div id="introExercise" class="hide">
	<p><a href="javascript:void(0);" onclick="javascript:showSemanticExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>

<div id="semanticExercise" class="hide" style="padding-top: 20px;">
	<p class="lead" style="margin-top:20px;"><img class="thumbnail" id="imageFront" src='' alt='' width="300" /></p>
	<ul class="thumbnails">
		<li class="span1"><a id="answer0" href="javascript:void(0);" onclick="javascript:checkAnswer(0);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer1" href="javascript:void(0);" onclick="javascript:checkAnswer(1);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer2" href="javascript:void(0);" onclick="javascript:checkAnswer(2);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer3" href="javascript:void(0);" onclick="javascript:checkAnswer(3);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer4" href="javascript:void(0);" onclick="javascript:checkAnswer(4);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer5" href="javascript:void(0);" onclick="javascript:checkAnswer(5);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer6" href="javascript:void(0);" onclick="javascript:checkAnswer(6);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
		<li class="span1"><a id="answer7" href="javascript:void(0);" onclick="javascript:checkAnswer(7);" class="thumbnail spatial-reasoning-exercise hide"></a></li>
	</ul>
	<p id="result-explanation" class="lead"></p>
	<a id="continue-button" class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:endExercise();"><?php echo _('Continuar'); ?></a>
	<a id="check-button" class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:checkExercise();"><?php echo _('Listo'); ?></a>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var spatialExercise = null;
var answers = [];
var difficulty = 0;
var distractorPositions = [];

function divThumbnailFromImage(imageSrc, position){
	var result = "<div id='thumbImg"+position+"' class='spatial-imgThumbnail'>";
	var left = -(getRandom()*220);
	var top = -(getRandom()*115);
	result += "<img src='"+imageSrc+"' alt='' width='300' style='position:absolute;min-width:300px;max-width:300px;left:"+left+"px;top:"+top+"px;' />"
	result+="</div>";
	
	return result;
};

function exercisesWithCurrentDifficulty(spatialExercises) {
	var result = [];
	$.each(spatialExercises.SpatialReasoningExercise, function(index, item){
		if (parseInt(item.difficulty)==difficulty) result.push(item);
	});
	return result;
};

function showSemanticExercise(){
	$('#introExercise').fadeOut('slow', function(evt){
		$('#semanticExercise').fadeIn('slow');
	});
	
	showMiniMenu(true);
	$('#exercise-description').html("<?php echo _('Selecciona los <strong>dos</strong> trozos de imagen que <strong>no</strong> pertenecen a la imagen principal y pulsa en el botón <strong>Listo</strong>'); ?>.");
};

function checkAnswer(num) {	
	if ($('#answer'+num).hasClass('spatial-exercise-toggle'))
	{
		$("#check-button").fadeOut('fast');
		answers.splice(answers.indexOf(num), 1);
		
		$('#answer'+num).removeClass('spatial-exercise-toggle');
		$('#thumbImg'+num).css('opacity', '1');
	}
	else if (answers.length<2)
	{
		answers.push(num);
	
		$('#answer'+num).addClass('spatial-exercise-toggle');
		$('#thumbImg'+num).css('opacity', '0.5');
		if (answers.length>=2) $("#check-button").fadeIn('fast');
	}
};

function checkExercise(){
	$("#check-button").fadeOut('fast');
	
	var initialText = null;
	
	var corrects = 0;
	var fails = 0;
	$.each(answers, function(index, answer){
		if (distractorPositions.indexOf(answer) == -1){
			fails++;
			$("#answer"+answer).removeClass('spatial-exercise-toggle').addClass("spatial-exercise-bad");
		}
		else
		{
			corrects++;
			$("#answer"+answer).removeClass('spatial-exercise-toggle').addClass("spatial-exercise-ok");
		}
	});
	
	for (var k=0; k<distractorPositions.length; k++)
	{
		var answer = distractorPositions[k];
		if (!$("#answer"+answer).hasClass('spatial-exercise-bad') && !$("#answer"+answer).hasClass('spatial-exercise-ok'))
		{
			$("#answer"+answer).addClass('spatial-exercise-toggle');
		}
	}
	
	exercise.setCorrects(corrects);
	exercise.setFails(fails);
	
	for (i=0; i<8; i++)
	{
		$('#answer'+i).addClass('disabled').attr('onclick', 'javascript:void(0);');
	}
	
	//canShowMiniMenu(false);
	//showInstructions(true);
	if (fails == 0)
	{
		$('#result-explanation').html("<?php echo _('<strong>¡Muy bien!</strong> Seleccionaste todos los trozos que no pertenecen a la imagen principal'); ?>.");
	}
	else if (corrects == 0)
	{
		$('#result-explanation').html("<?php echo _('<strong>¡Lo siento!</strong> Los trozos que seleccionaste pertenecen a la imagen principal'); ?>.");
	}
	else
	{
		$('#result-explanation').html("<?php echo _('<strong>¡No esta mal!</strong> Un trozo pertenece a la imagen principal, pero el otro no'); ?>.");
	}
	
	//$('#result-explanation').append("<?php echo _(' Pulsa en el botón <strong>Continuar</strong>.'); ?>");
	
	$('#continue-button').fadeIn('fast');
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (repetitions==0) //show intro
	{
		$('#introExercise').show();
	}
	else //show exercise
	{
		showSemanticExercise();
	}
	
	//retrieve current exercise plan
	$.ajax({
		type: "GET",
		url: "exercises/xml/spatial-reasoning-plan.xml",
		dataType: "xml",
		success: function(xml) {
			var plan = $.xml2json(xml);
			var numberOfExercise = -1;
			
			$.ajax({
				type: "GET",
				url: "exercises/xml/spatial-reasoning-series.php",
				dataType: "xml",
				success: function(result){
					var spatialExercises = $.xml2json(result);
					spatialExercises = exercisesWithCurrentDifficulty(spatialExercises);
					var randomExercises = false;
					
					$.each(plan.SpatialReasoningPlanEntry, function(index, value){
						if (value.exerciseID == exerciseID && value.sessionID == sessionID)
						{
							if (value.exercises == "random")
							{
								randomExercises = true;
								//take one random exercise
								numberOfExercise = Math.floor(getRandom()*spatialExercises.length);
							}
							else
							{
								var array = value.exercises.split(",");
								if (repetitions < array.length)
									numberOfExercise = parseInt(array[repetitions]);
								else numberOfExercise = Math.floor(getRandom()*spatialExercises.length);
							}
							
							spatialExercise = spatialExercises[numberOfExercise];
												
							return false; //break the loop
						}
					});
					
					if (!randomExercises)
					{
						spatialExercises = $.xml2json(result);
						$.each(plan.SpatialReasoningPlanEntry, function(index, value){
							if (value.exerciseID == exerciseID && value.sessionID == sessionID)
							{
								var array = value.exercises.split(",");
								if (repetitions < array.length)
									numberOfExercise = parseInt(array[repetitions]);
								else numberOfExercise = Math.floor(getRandom()*spatialExercises.length);
								
								spatialExercise = spatialExercises.SpatialReasoningExercise[numberOfExercise];
													
								return false; //break the loop
							}
						});
					}
					
					if (spatialExercise != null)
					{
						var answers = [0,1,2,3,4,5,6,7];
						answers.sort(function(a,b){
							return 0.5-getRandom();
						});
						
						//take 6 thumbnails from the element
						$.each(answers, function(index, answer){
							if (index < 6){								
								var idAnswer = '#answer'+answer;
								var str = divThumbnailFromImage(spatialExercise.Element.img, answer);
								$(idAnswer).html(str).removeClass('hide').show();
							}
							else return false;
						});
						
						//take 2 thumbnails from the distractor
						distractorPositions.push(answers[6]);
						distractorPositions.push(answers[7]);
						
						var idAnswer = '#answer'+answers[6];
						var str = divThumbnailFromImage(spatialExercise.Distractor.img, answers[6]);
						$(idAnswer).html(str).removeClass('hide').show();
						
						idAnswer = '#answer'+answers[7];
						str = divThumbnailFromImage(spatialExercise.Distractor.img, answers[7]);
						$(idAnswer).html(str).removeClass('hide').show();
						
						$("#imageFront").attr('src', spatialExercise.Element.img);
					}
					else
					{
						//error
						console.debug("error: spatial reasoning exercise not defined");
					}
				}
			});
		}
	});
});
</script>