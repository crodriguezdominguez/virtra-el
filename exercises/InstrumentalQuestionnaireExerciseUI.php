<?php
	require_once('locale/localization.php');
?>

<!-- Questionnaire -->
<div id="questionnaire">
	<h3><?php echo _('A la hora de ...'); ?></h3>
	<form method="post" action="javascript:updateUser();" class="form-horizontal" id="contact-form" style="font-size:18px;">
	<fieldset>
	<div id="nextQuestionnaire0">
		<div class="control-group">
			<label class="control-label" for="phoneHelp"><?php echo _('... usar el tel&eacute;fono'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="phoneHelp" name="phoneHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire0').fadeOut('fast'); else $('#button-questionnaire0').fadeIn('fast');}">
					<option value="0">-</option>
					<option value="1"><?php echo _('Soy capaz de usar el tel&eacute;fono plenamente'); ?></option>
					<option value="2"><?php echo _('Soy capaz de marcar bien algunos n&uacute;meros familiares'); ?></option>
					<option value="3"><?php echo _('Soy capaz de contestar al tel&eacute;fono, pero no marcar'); ?></option>
					<option value="4"><?php echo _('No soy capaz de usar el tel&eacute;fono'); ?></option>
				</select>
			</div>
		</div>
		<div class="control-group">
		    <div class="controls">
		    	<div><a id="button-questionnaire0" class="btn btn-primary hide" href="javascript:void(0);" onclick="javascript:$('#nextQuestionnaire0').fadeOut('fast', function(){$('#nextQuestionnaire0b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a></div>
		    </div>
	    </div>
	</div>
    <div id="nextQuestionnaire0b" class="hide">
        <div class="control-group">
            <label class="control-label" for="shoppingHelp"><?php echo _('... hacer compras'); ?></label>
            <div class="controls">
                <select class="input-xxlarge" id="shoppingHelp" name="shoppingHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire0b').fadeOut('fast'); else $('#button-questionnaire0b').fadeIn('fast');}">
                    <option value="0">-</option>
                    <option value="1"><?php echo _('Realizo todas las compras necesarias de forma independiente'); ?></option>
                    <option value="2"><?php echo _('Solo realizo peque&ntilde;as compras de forma independiente'); ?></option>
                    <option value="3"><?php echo _('Necesito ir acompa&ntilde;ado/a para hacer cualquier compra'); ?></option>
                    <option value="4"><?php echo _('Soy totalmente incapaz de comprar'); ?></option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire0b').fadeOut('fast', function(){$('#nextQuestionnaire0').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                        id="button-questionnaire0b" class="btn btn-primary hide" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire0b').fadeOut('fast', function(){$('#nextQuestionnaire1').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                </div>            </div>
        </div>
    </div>
	<div id="nextQuestionnaire1" class="hide">
	    <div class="control-group">
			<label class="control-label" for="cookingHelp"><?php echo _('... hacer la comida'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="cookingHelp" name="cookingHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire1').fadeOut('fast'); else $('#button-questionnaire1').fadeIn('fast');}">
					<option value="0">-</option>
					<option value="1"><?php echo _('Soy capaz de organizar, preparar y servir las comidas'); ?></option>
					<option value="2"><?php echo _('Soy capaz de preparar las comidas si me dan los ingredientes'); ?></option>
					<option value="3"><?php echo _('Soy capaz de preparar, calentar y servir las comidas, pero no sigo una dieta adecuada'); ?></option>
					<option value="4"><?php echo _('Necesito que me preparen y sirvan las comidas'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
		    <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire1').fadeOut('fast', function(){$('#nextQuestionnaire0b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                        id="button-questionnaire1" class="btn btn-primary hide" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire1').fadeOut('fast', function(){$('#nextQuestionnaire2').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                </div>
            </div>
	    </div>
	</div>
	<div id="nextQuestionnaire2" class="hide">
	    <div class="control-group">
		<label class="control-label" for="homecareHelp"><?php echo _('... el cuidado de la casa'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="homecareHelp" name="homecareHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire2').fadeOut('fast'); else $('#button-questionnaire2').fadeIn('fast');}">
					<option value="0">-</option>
					<option value="1"><?php echo _('Soy capaz de mantener la casa solo/a o con ayuda ocasional'); ?></option>
					<option value="2"><?php echo _('Solo soy capaz de hacer tareas ligeras, como limpiar los platos o hacer la cama'); ?></option>
					<option value="3"><?php echo _('Soy capaz de hacer tareas ligeras, pero no puedo mantener un adecuado nivel de limpieza'); ?></option>
					<option value="4"><?php echo _('Necesito ayuda en todas las labores de la casa'); ?></option>
					<option value="5"><?php echo _('No participo en ninguna labor de la casa'); ?></option>
				</select>
			</div>
    	</div>
	    <div class="control-group">
		    <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire2').fadeOut('fast', function(){$('#nextQuestionnaire1').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                        id="button-questionnaire2" class="btn btn-primary hide" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire2').fadeOut('fast', function(){$('#nextQuestionnaire3').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                </div>
            </div>
	    </div>
	</div>
	<div id="nextQuestionnaire3" class="hide">
	    <div class="control-group">
			<label class="control-label" for="clothCleaningHelp"><?php echo _('... lavar la ropa'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="clothCleaningHelp" name="clothCleaningHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire3').fadeOut('fast'); else $('#button-questionnaire3').fadeIn('fast');}">
					<option value="0">-</option>
					<option value="1"><?php echo _('Soy capaz de lavar toda mi ropa'); ?></option>
					<option value="2"><?php echo _('Solo soy capaz de lavar prendas peque&ntilde;as'); ?></option>
					<option value="3"><?php echo _('Toda la ropa me la debe lavar otra persona'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
		    <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire3').fadeOut('fast', function(){$('#nextQuestionnaire2').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                        id="button-questionnaire3" class="btn btn-primary hide" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire3').fadeOut('fast', function(){$('#nextQuestionnaire3b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                </div>
            </div>
	    </div>
	</div>
    <div id="nextQuestionnaire3b" class="hide">
        <div class="control-group">
            <label class="control-label" for="transportHelp"><?php echo _('... utilizar medios de transporte'); ?></label>
            <div class="controls">
                <select class="input-xxlarge" id="transportHelp" name="transportHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire3b').fadeOut('fast'); else $('#button-questionnaire3b').fadeIn('fast');}">
                    <option value="0">-</option>
                    <option value="1"><?php echo _('Soy capaz de viajar solo en transporte p&uacute;blico o soy capaz de conducir mi propio coche'); ?></option>
                    <option value="2"><?php echo _('Solo viajo en transporte p&uacute;blico si voy acompa&ntilde;ado'); ?></option>
                    <option value="3"><?php echo _('Solo uso el taxi o el autom&oacute;vil con ayuda de otros'); ?></option>
                    <option value="4"><?php echo _('No soy capaz de viajar'); ?></option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire3b').fadeOut('fast', function(){$('#nextQuestionnaire3').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                        id="button-questionnaire3b" class="btn btn-primary hide" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire3b').fadeOut('fast', function(){$('#nextQuestionnaire4').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div id="nextQuestionnaire4" class="hide">
	    <div class="control-group">
			<label class="control-label" for="medicationHelp"><?php echo _('... tomar mis medicamentos'); ?></label>
			<div class="controls">
				<select class="input-xxlarge" id="medicationHelp" name="medicationHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire4').fadeOut('fast'); else $('#button-questionnaire4').fadeIn('fast');}">
					<option value="0">-</option>
					<option value="1"><?php echo _('Soy capaz de tomar mi medicaci&oacute;n a la hora y con la dosis correcta'); ?></option>
					<option value="2"><?php echo _('Solo soy capaz de tomar mi medicaci&oacute;n cuando es preparada previamente por otra persona'); ?></option>
					<option value="3"><?php echo _('No soy capaz de tomar mi medicaci&oacute;n yo solo'); ?></option>
				</select>
			</div>
	    </div>
	    <div class="control-group">
		    <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire4').fadeOut('fast', function(){$('#nextQuestionnaire3b').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;<a
                        id="button-questionnaire4" class="btn btn-primary hide" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire4').fadeOut('fast', function(){$('#nextQuestionnaire4b').fadeIn('fast');});"><?php echo _('Continuar'); ?></a>
                </div>
            </div>
	    </div>
	</div>
    <div id="nextQuestionnaire4b" class="hide">
        <div class="control-group">
            <label class="control-label" for="economyHelp"><?php echo _('... manejar los asuntos econ&oacute;micos'); ?></label>
            <div class="controls">
                <select class="input-xxlarge" id="economyHelp" name="economyHelp"
                        onchange="javascript:{if (value == 0) $('#button-questionnaire4b').fadeOut('fast'); else $('#button-questionnaire4b').fadeIn('fast');}">
                    <option value="0">-</option>
                    <option value="1"><?php echo _('Soy capaz de encargarme de mis asuntos econ&oacute;micos'); ?></option>
                    <option value="2"><?php echo _('Solo realizo las compras de cada d&iacute;a, pero necesito ayuda en las grandes compras y en los bancos'); ?></option>
                    <option value="3"><?php echo _('Soy incapaz de manejar dinero'); ?></option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div><a class="btn" href="javascript:void(0);"
                        onclick="javascript:$('#nextQuestionnaire4b').fadeOut('fast', function(){$('#nextQuestionnaire4').fadeIn('fast');});"><?php echo _('Atrás'); ?></a>&nbsp;&nbsp;
                    <button id="button-questionnaire4b" type="submit" class="btn btn-primary"><?php echo _('Finalizar'); ?></button>
                </div>
            </div>
        </div>
    </div>
	</fieldset>
	</form>
</div>

<link href="css/listbox.css" rel="stylesheet">
<script type="text/javascript" src="js/listbox.js"></script>
<script type="text/javascript">
$('#exercise-container select').listbox({'searchbar':false});

function updateUser()
{
	var update = true;
	$('#contact-form').find('select').each(function(){
		if ($(this).val() == '0')
		{
			$(this).focus();
			update = false;
			return false;
		}
	});

	if (update)
	{
		var qry = 'phoneHelp='+$('#phoneHelp').val()+'&'+
				  'shoppingHelp='+$('#shoppingHelp').val()+'&'+
				  'cookingHelp='+$('#cookingHelp').val()+'&'+
				  'homecareHelp='+$('#homecareHelp').val()+'&'+
				  'clothCleaningHelp='+$('#clothCleaningHelp').val()+'&'+
				  'transportHelp='+$('#transportHelp').val()+'&'+
				  'medicationHelp='+$('#medicationHelp').val()+'&'+
				  'economyHelp='+$('#economyHelp').val();
		
		$.ajax({
			type: "POST",
			url: 'backend/update_userprofile2.php',
			data: qry,
			success: function(html){
				endExercise();
			}
		});
	}
	else
	{
		sweetAlert({title:"<?php echo _('Error'); ?>", text:"<?php echo _('Debes responder todas las preguntas.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
	}
}
</script>