/**
* Exercise definition. Depends on class.js
*
* @author Carlos Rodriguez Dominguez
*/

if (!Exercise) {

var Exercise = Class.extend({
	init: function(title, description, UIurl, sessionID, exerciseID, repetition, exerciseEntry) {
		this._title = title;
		this._description = description;
		this._UIurl = UIurl;
		this._demoUIurl = null;
		this._nextExercise = null;
		this._sessionID = sessionID;
		this._exerciseID = exerciseID;
		this._longTerm = false;
		this._exerciseEntry = exerciseEntry;
		this._repetition = repetition;
		this._afterDemo = false;
		this._repeating = false;
	},
	title : function(){
		return this._title;
	},
	description : function() {
		return this._description;
	},
	UIurl : function() {
		return this._UIurl;
	},
	setAfterDemo : function(f) {
		this._afterDemo = f;
	},
	afterDemo : function(f) {
		return this._afterDemo;
	},
	demoUIurl : function() {
		return this._demoUIurl;
	},
	repeating : function() {
		return this._repeating;
	},
	setRepeating : function(rep) {
		this._repeating = rep;
	},
	canRepeat : function(){
		if (this._sessionID == 1 || this._sessionID == 2 || this._sessionID == 12 || this._sessionID == 13)
		{
			return false;
		}
		else return true;
	},
	canAvoidHelp : function(){
		if (this._sessionID == 1 || this._sessionID == 2 || this._sessionID == 12 || this._sessionID == 13 || this._repeating)
		{
			return false;
		}
		else return true;
	},
	demoUI : function() {
		var data = null;
		$.ajax({
        	url: this.demoUIurl(),
         	success: function(result) {
         		data = result;    
            },
        	async: false
    	});
    	return data;
	},
	ui : function(){
		var data = null;
		$.ajax({
        	url: this._UIurl,
         	success: function(result) {
         		data = result;    
            },
        	async: false
    	});
    	return data;
	},
	nextExercise : function() {
		return this._nextExercise;
	},
	sessionID : function() {
		return this._sessionID;
	},
	exerciseID : function() {
		return this._exerciseID;
	},
	longTerm : function() {
		return this._longTerm;
	},
	duration : function() {
		return this._exerciseEntry.duration;
	},
	repetitions : function() {
		return this._exerciseEntry.repetitions;
	},
	maxTime : function() {
		return this._exerciseEntry.maxTime;
	},
	exerciseEntry : function() {
		return this._exerciseEntry;
	},
	finishExercise : function() {
	},
	updatePartialResult : function(entry, repetition) {
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&repetition='+repetition+'&exerciseType='+this._exerciseEntry.type+'&entry='+encodeURIComponent(entry);
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_partial_result.php',
			success: function(data){
				//console.log(data);
			}
		});
	},
	hasMedal : function() {
		return {medal:false, type:0};
	},
});

}