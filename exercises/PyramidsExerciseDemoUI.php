<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<a style="margin-bottom: 20px;" href="javascript:void(0);" onclick="javascript:errorHelp();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<p><img src="exercises/img/PyramidsExercise/c1.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 1" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/c2.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 2" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/c3.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 3" width="105" height="105" /></p>
</div>

<div class="lead" id="result-explanation"></div>

<div id="introDemo" class="hide">
	<table cellspacing="5" cellpadding="5">
		<tr>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 0);"><img src="exercises/img/PyramidsExercise/c1.jpg" alt="pirámide 1" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 1);"><img src="exercises/img/PyramidsExercise/d4.jpg" alt="pirámide 2" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 2);"><img src="exercises/img/PyramidsExercise/c2.jpg" alt="pirámide 3" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 3);"><img src="exercises/img/PyramidsExercise/c3.jpg" alt="pirámide 4" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 4);"><img src="exercises/img/PyramidsExercise/d5.jpg" alt="pirámide 5" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 5);"><img src="exercises/img/PyramidsExercise/d9.jpg" alt="pirámide 6" width="105" height="105" /></a></td>
		</tr>
		<tr>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 6);"><img src="exercises/img/PyramidsExercise/d17.jpg" alt="pirámide 7" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 7);"><img src="exercises/img/PyramidsExercise/c4.jpg" alt="pirámide 8" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 8);"><img src="exercises/img/PyramidsExercise/d19.jpg" alt="pirámide 9" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 9);"><img src="exercises/img/PyramidsExercise/d22.jpg" alt="pirámide 10" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 10);"><img src="exercises/img/PyramidsExercise/c5.jpg" alt="pirámide 11" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 11);"><img src="exercises/img/PyramidsExercise/d23.jpg" alt="pirámide 12" width="105" height="105" /></a></td>
		</tr>
		<tr>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 12);"><img src="exercises/img/PyramidsExercise/c6.jpg" alt="pirámide 13" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 13);"><img src="exercises/img/PyramidsExercise/d24.jpg" alt="pirámide 14" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 14);"><img src="exercises/img/PyramidsExercise/d25.jpg" alt="pirámide 15" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 15);"><img src="exercises/img/PyramidsExercise/c7.jpg" alt="pirámide 16" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 16);"><img src="exercises/img/PyramidsExercise/c8.jpg" alt="pirámide 17" width="105" height="105" /></a></td>
		    <td><a href="javascript:void(0);" class="thumbnail" onclick="javascript:checkIndex($(this), 17);"><img src="exercises/img/PyramidsExercise/d26.jpg" alt="pirámide 18" width="105" height="105" /></a></td>
		</tr>
	</table>
</div>

<a href="javascript:void(0);" class="btn btn-primary btn-large hide" id="introDemoButton"><?php echo _('Continuar'); ?></a>

<div id="errorHelp" class="hide" style="margin-top:40px;">
	<p><img src="exercises/img/PyramidsExercise/d24.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 1" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/d4.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 2" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/d19.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 3" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/d22.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 4" width="105" height="105" />&nbsp;&nbsp;<img src="exercises/img/PyramidsExercise/d25.jpg" class="img-polaroid" alt="<?php echo _('pirámide'); ?> 5" width="105" height="105" /></p>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var demoNumberCorrects = 8;
var demoExplanations = <?php echo _('["", "No es correcto. No hay pirámide grande y las pequeñas no tienen puerta.", "", "", "En esa imagen no hay pirámides pequeñas.", "No es correcto. Si te fijas bien verás que las pirámides pequeñas no tienen puerta.", "Sólo una de las pirámides pequeñas tienen puerta en esa postal. En las imágenes correctas tiene que haber dos pirámides pequeñas con puerta.", "", "Fíjate bien, solo una de las pirámides pequeñas tiene puerta.", "Esa imagen es casi correcta, pero las puertas están en el lado con sombra de la pirámides. Las puertas deben estar en el lado del sol.", "", "En esa imagen sólo hay una pirámide pequeña con puerta y además está a la sombra.", "", "En esa imagen no hay pirámide grande y las pirámides pequeñas tienen la puerta a la sombra.", "En esa imagen una de las pirámides pequeñas tiene la puerta en el lado de la sombra.", "", "", "En esa imagen una de las pirámides pequeñas no tiene puerta."]'); ?>;

var correctImgs = ["c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8"];
var badImgs = ["d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "d11", "d12", "d13", "d14", "d15", "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23", "d24", "d25", "d26", "d27", "d28", "d29", "d30", "d31", "d6"];
var commonArray = null;

function beginExercise() {
	$('#errorHelp').fadeOut('slow', function(evt){
		$('#introDemoButton').fadeOut('slow');
		$('#exercise-description').fadeOut('slow', function(evt){
			showInstructions(false);
			$('#introDemo').fadeIn('slow');
			$('#exercise-description').html("<?php echo _('Ahora vamos a hacer una prueba. Debes seleccionar aquellas postales que tengan <strong>una</strong> pirámide grande  y además <strong>dos pequeñas con puerta en el lado soleado</strong>. En el ejercicio habrá más pirámides aparte de éstas.'); ?>");
			$('#exercise-description').fadeIn('slow');
		});
		
	});
};

function beginDemo() {
	$('#introDemo').fadeOut('slow', function(evt){
		replayExercise();
	});
};

function checkIndex(but, index) {
	var isCorrect = (demoExplanations[index].length == 0);
	if (isCorrect)
	{
		demoNumberCorrects--;
		if (demoNumberCorrects > 0)
		{
			if (demoNumberCorrects == 1)
			{
				$("#result-explanation").html("<span style='color:green;'><?php echo _('<strong>¡Correcto!</strong> Queda 1 imagen correcta'); ?></span>");
			}
			else
			{
				$("#result-explanation").html("<span style='color:green;'><?php echo _('<strong>¡Correcto!</strong> Quedan'); ?>"+" "+demoNumberCorrects.toString()+" <?php echo _('imágenes correctas');?></span>");
			}
		}
		else
		{
			replayExercise();
		}
		but.attr('onclick', 'javascript:void(0);');
		but.addClass("pyramid-ok");
	}
	else
	{
		$("#result-explanation").html("<span style='color:red;'>"+demoExplanations[index]+"</span>");
		but.addClass("pyramid-bad");
	}
	
	but.children().css('opacity', '0.6');
};

function errorHelp(){
	$('#introExercise').fadeOut('slow', function(evt){
		$('#exercise-description').fadeOut('slow', function(evt){
			$("#result-explanation").html("");
			$("#introDemo").fadeOut('slow', function(evt){
				showInstructions(false);
				$('#exercise-description').html("<?php echo _('<strong>&iexcl;Ten cuidado!</strong> Porque hay postales como las de abajo donde no hay una pirámide grande, donde las pirámides peque&ntilde;as no tienen puerta o donde las tienen pero mirando a la sombra. Esas no me interesan. Cuando estés listo pulsa el botón <strong>Continuar</strong>.') ?>");
				$('#exercise-description').show();
				$('#errorHelp').fadeIn('fast');
				$("#introDemoButton").unbind('click');
				$("#introDemoButton").bind('click', beginExercise);
				$('#introDemoButton').show();
			});
		});
	});
}

function replayExercise() {
	$('#exercise-description').fadeOut('slow', function(evt){
		$("#result-explanation").html("");
		$("#introDemo").fadeOut('slow', function(evt){
			showInstructions(false);
			$('#exercise-description').html("<?php echo _('Has finalizado la prueba correctamente, ¡Enhorabuena! Ahora comenzarás el ejercicio. Cuando estés listo pulsa el botón <strong>Comenzar ejercicio</strong>.') ?>");
			$('#exercise-description').show();
			$("#introDemoButton").html("<?php echo _('Comenzar ejercicio'); ?>");
			$("#introDemoButton").unbind('click');
			$("#introDemoButton").bind('click', endDemo);
			$('#introDemoButton').show();
		});
	});
};

$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	/*
	$("#introDemoButton").unbind('click');
	$("#introDemoButton").bind('click', errorHelp);*/
});

</script>
