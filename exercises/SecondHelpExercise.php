<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!SecondHelpExercise) {

var SecondHelpExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Enhorabuena!'); ?>', '<?php echo _('Has completado los ejercicios de la primera sesi&oacute;n. Ahora te har&eacute; unas cuantas preguntas m&aacute;s antes de terminar por hoy.'); ?>', 'exercises/SecondHelpExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
	}
});

}
</script>
