<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<p class="lead" id="introMessage"></p>
	<a href="javascript:void(0);" onclick="javascript:nextIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="begin-exercise-id" class="hide">
	<a class="btn btn-primary" href="javascript:void(0);" onclick="javascript:endDemo();" id="continue-button"><?php echo _('Comenzar ejercicio'); ?></a>
</div>

<div id="classifyExercise" class="hide">
	<div class="lead" id="WordList"></div>
</div>
<div class="hide" id="categories" style="width:100%">
	<div class="classify-objects-category" id="cat0"></div>
	<div class="classify-objects-category" id="cat1"></div>
	<div class="classify-objects-category" id="cat2"></div>
	<div class="classify-objects-category" id="cat3"></div>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var shownObjects = null;
var cats = null;
var checkedObject = null;
var checkedObjectCat = null;
var objs0 = [];
var objs1 = [];
var objs2 = [];
var objs3 = [];
var currentIntroIndex = 0;

function showObjects(){
	var finalValue = "<table onclick='javascript:removeCheckedObject();'>";
	var col = 0;
	var row = 0;
	finalValue +="<th colspan='8' style='text-align:left;'><?php echo _('Objetos');  ?></th>";
	for (var i=0; i<16; i++) {
		if (col == 0) finalValue+="<tr>";
		else if (col == 8)
		{
			finalValue += "</tr>";
			col = 0;
			row++;
		}
		if (i<shownObjects.length)
		{
			var item = shownObjects[i];
			finalValue += "<td><a id='word"+i+"' href='javascript:void(0);' class='thumbnail wordlist-exercise text-center' onclick='javascript:toggleClass($(this));' style='text-align:center;width:80px;height:80px;'><img src='"+item.src+"' alt='' width='80' height='80' /></a></td>";
		}
		else finalValue += "<td><span class='thumbnail' style='text-align:center;width:80px;height:80px;'></span></td>";
		col++;
	}
	finalValue += "</table>";
	$("#WordList").html(finalValue);
};

function showCat(index){
	var objs = null;
	var color = null;
	switch(index){
		case 0: objs=objs0;color='lightpink';break;
		case 1: objs=objs1;color='lightgreen';break;
		case 2: objs=objs2;color='lightyellow';break;
		case 3: objs=objs3;color='lightblue';break;
	}
	if (objs != null)
	{
		var element = $("#cat"+index);
		var catElement = cats[index];
		var finalValue = "<div class='thumbnail' style='width:165px;height:22px;text-align:left;background-color:"+color+";'>"+catElement.name+"</div>";
		finalValue += "<table style='display:inline;' onclick='javascript:addObjectToCat("+index+");'>";
		var col = 0;
		var row = 0;
		for (var i=0; i<4; i++)
		{
			if (col == 0) finalValue+="<tr>";
			else if (col == 2)
			{
				finalValue += "</tr>";
				col = 0;
				row++;
			}
			if (i<objs.length)
			{
				var item = objs[i];
				finalValue += "<td width='50%'><a id='word"+i+"-cat"+index+"' href='javascript:void(0);' class='thumbnail wordlist-exercise text-center' onclick='javascript:toggleClassCat($(this));' style='text-align:center;width:75px;height:75px;'><img src='"+item.src+"' alt='' width='75' height='75' /></a></td>";
			}
			else finalValue += "<td><span class='thumbnail' style='text-align:center;width:75px;height:75px;'></span></td>";
			col++;
		}
		finalValue += "</table>";
		element.html(finalValue);
	}
};

function removeCheckedObject() {
	if (checkedObjectCat != null && checkedObject == null)
	{
		var element = $(".classify-objects-toggle");
		var elements = element.attr('id').split("-");
		var item = parseInt(elements[0].split("word")[1]);
		var cat = parseInt(elements[1].split("cat")[1]);
		var objs = null;
		switch(cat){
			case 0: objs=objs0;break;
			case 1: objs=objs1;break;
			case 2: objs=objs2;break;
			case 3: objs=objs3;break;
		}
		if (objs != null)
		{
			var obj = objs[item];
			objs.splice(item, 1);
			shownObjects.push(obj);
			showCat(cat);
			showObjects();
			return;
		}
	}
};

function toggleClass(element){
	if (element.hasClass("classify-objects-toggle")){
		element.removeClass("classify-objects-toggle");
		checkedObject = null;
	}
	else {
		$(".classify-objects-toggle").removeClass('classify-objects-toggle');
		element.addClass("classify-objects-toggle");
		checkedObject = element;
	}
};

function toggleClassCat(element){
	if (element.hasClass("classify-objects-toggle")){
		element.removeClass("classify-objects-toggle");
		checkedObjectCat = null;
	}
	else {
		$(".classify-objects-toggle").removeClass('classify-objects-toggle');
		element.addClass("classify-objects-toggle");
		checkedObjectCat = element;
	}
};

function addObjectToCat(index){
	var objs = null;
	switch(index){
		case 0: objs=objs0;break;
		case 1: objs=objs1;break;
		case 2: objs=objs2;break;
		case 3: objs=objs3;break;
	}
	if (objs != null && checkedObject != null)
	{
		if (objs.length<4)
		{
			var indexObjs = parseInt(checkedObject.attr('id').split("word")[1]);
			var obj = shownObjects[indexObjs];
			if (indexOfItem(objs, obj) == -1)
			{
				shownObjects.splice(indexObjs, 1);
				objs.push(obj);
				showObjects();
				showCat(index);
			}
			
			checkExercise();
		}
	}
	
	checkedObject = null;
};

function containsAllObjs(catIndex, objs) {
	if (objs.length != 4) return false;
	var cat = cats[catIndex];
	var result = true;
	$.each(objs, function(index, obj){
		if (indexOfItem(cat.Item, obj) == -1)
		{
			result = false;
			return false;
		}
	});
	
	return result;
}

function checkExercise(){
	var finish = false;
	
	//compare...
	if (containsAllObjs(0, objs0) && containsAllObjs(1, objs1) && containsAllObjs(2, objs2) && containsAllObjs(3, objs3))
	{
		finish = true;
	}
	
	if (finish)
	{
		$("#categories").fadeOut('slow');
		$("#classifyExercise").fadeOut('slow', function(){
			$("#exercise-description").html('<?php echo _('<strong>¡Enhorabuena!</strong> Ya has terminado la prueba. Cuando estés listo para comenzar el ejercicio pulsa <strong>Comenzar ejercicio</strong>.'); ?>');
			$("#begin-exercise-id").fadeIn('fast');
		});
	}
};

function indexOfItem(arrayItems, item) {
	var result = -1;
	$.each(arrayItems, function(index, compare){
		if (item.img == compare.img)
		{
			result = index;
			return false;
		}
	});
	return result;
};

function nextIntro(){
	$("#exercise-description").fadeOut('slow', function(){
		switch(currentIntroIndex){
			case 0:
			$("#exercise-description").html('<?php echo _('Antes de comenzar el ejercicio vamos a hacer una prueba de clasificación de imágenes. Te presentaré distintos dibujos y tendrás que colocarlos en las categorías a las que pertenecen que en este caso serán: herramientas, comidas, transportes y deportes. Pulsa <strong>Continuar</strong> cuando estés preparado.'); ?>');
			$("#exercise-description").fadeIn('slow');
			break;
			case 1:
			$("#exercise-description").html('<?php echo _('Para colocar cada dibujo en su categoría correspondiente, pulsa sobre el dibujo y luego sobre algún hueco en blanco de esa categoría. Si te equivocas, podrás pulsar de nuevo sobre el dibujo, y luego sobre los huecos blancos de la lista donde se encontraba al principio. Pulsa <strong>Continuar</strong>.'); ?>');
			$("#exercise-description").fadeIn('slow');
			break;
			default:
			beginExercise();
			break;
		}
		
		currentIntroIndex++;
	});
};

function beginExercise()
{
	$("#introExercise").fadeOut('slow', function(){
		$("#exercise-description").html('<?php echo _('Selecciona un objeto y luego pulsa sobre un hueco en la categoría correspondiente.'); ?>&nbsp;<?php echo _('Si deseas terminar la prueba pulsa en el siguiente botón: <a class="btn btn-primary" href="javascript:void(0);" onclick="javascript:endDemo();" id="continue-button">Terminar prueba</a>'); ?>');
		$("#exercise-description").fadeIn('slow');
				
		$.ajax({
			type: "GET",
			url: "exercises/xml/classify-objects-categories.php",
			dataType: "xml",
			success: function(result){
				var objects = $.xml2json(result);
				//shuffle categories
				objects.Category.sort(function(a,b){
					return 0.5-getRandom();
				});
				
				//shuffle items in categories
				$.each(objects.Category, function(index, cat){
					cat.Item.sort(function(a,b){
						a.src = cat.folder+a.img;
						b.src = cat.folder+b.img;
						return 0.5-getRandom();
					});
				});
				
				//TAKE 4 CATS and 16 OBJS
				cats = [];
				$.each(objects.Category, function(index, cat){
					if (index < 4)
					{
						cats.push(cat);
					}
					else return false;
				});
				
				shownObjects = [];
				$.each(cats, function(index, cat){
					$.each(cat.Item, function(j, item){
						if(j < 4)
						{
							item.src = cat.folder+item.img;
							shownObjects.push(item);
						}
						else return false;
					});
				});
				
				
				shownObjects.sort(function(a,b){
					return 0.5-getRandom();
				});
				
				showObjects();
				showCat(0);
				showCat(1);
				showCat(2);
				showCat(3);
				$("#classifyExercise").fadeIn('slow');
				$("#categories").fadeIn('slow');
			}
		});
	});
}

$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
});

</script>
