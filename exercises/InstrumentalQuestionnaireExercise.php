<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!InstrumentalQuestionnaireExercise) {

var InstrumentalQuestionnaireExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Enhorabuena!'); ?>', '<?php echo _('Has completado los ejercicios. Ahora deber&aacute;s responder unas cuantas preguntas m&aacute;s antes de terminar la primera sesi&oacute;n. Para ello, elige la respuesta que mejor te describa actualmente.'); ?>', 'exercises/InstrumentalQuestionnaireExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
	}
});

}
</script>