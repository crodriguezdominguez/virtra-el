<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise">
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<table cellspacing="5" cellpadding="5" style="background-color:lightyellow;margin-bottom:20px; margin-top:20px;">
		<tr>
		    <td><img src="exercises/img/PositionsExercise/ventana-close.png" alt="<?php echo _('Ventana cerrada'); ?>" width="105" height="105" /></td>
		    <td><img src="exercises/img/PositionsExercise/ventana-open.png" alt="<?php echo _('Ventana abierta'); ?>" width="105" height="105" /></td>
		    <td><img src="exercises/img/PositionsExercise/ventana-close.png" alt="<?php echo _('Ventana cerrada'); ?>" width="105" height="105" /></td>
		</tr>
	</table>
</div>

<div id="helpEntry" class="hide" style="margin-bottom: 20px;">
	<p class="lead" id="helpEntry-p"></p>
	<a href="javascript:void(0);" onclick="javascript:{replay=true;replayExercise();}" class="btn btn-primary btn-large" id="helpExerciseButton"><?php echo _('Nuevo Intento'); ?></a>
</div>

<div id="positionsPanel" class="hide">
	<table cellspacing="5" cellpadding="5" style="background-color:lightyellow;">
		<tr>
		    <td><a id="position0" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 0);"></a></td>
		    <td><a id="position1" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 1);"></a></td>
		    <td><a id="position2" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 2);"></a></td>
		    <td><a id="position3" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 3);"></a></td>
		    <td><a id="position4" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 4);"></a></td>
            <td id="file0" class="hide" style="background-color:white;"><b><span id="index0" style="margin-left:10px;font-size:20px">0</span></b></td>
		</tr>
		<tr>
		    <td><a id="position5" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 5);"></a></td>
		    <td><a id="position6" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 6);"></a></td>
		    <td><a id="position7" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 7);"></a></td>
		    <td><a id="position8" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 8);"></a></td>
		    <td><a id="position9" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 9);"></a></td>
            <td id="file1" class="hide" style="background-color:white;"><b><span id="index1" style="margin-left:10px;font-size:20px">0</span></b></td>
		</tr>
		<tr>
			<td><a id="position10" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 10);"></a></td>
		    <td><a id="position11" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 11);"></a></td>
		    <td><a id="position12" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 12);"></a></td>
		    <td><a id="position13" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 13);"></a></td>
		    <td><a id="position14" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 14);"></a></td>
            <td id="file2" class="hide" style="background-color:white;"><b><span id="index2" style="margin-left:10px;font-size:20px">0</span></b></td>
		</tr>
		<tr>
			<td><a id="position15" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 15);"></a></td>
		    <td><a id="position16" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 16);"></a></td>
		    <td><a id="position17" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 17);"></a></td>
		    <td><a id="position18" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 18);"></a></td>
		    <td><a id="position19" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 19);"></a></td>
            <td id="file3" class="hide" style="background-color:white;"><b><span id="index3" style="margin-left:10px;font-size:20px">0</span></b></td>
		</tr>
		<tr>
			<td><a id="position20" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 20);"></a></td>
		    <td><a id="position21" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 21);"></a></td>
		    <td><a id="position22" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 22);"></a></td>
		    <td><a id="position23" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 23);"></a></td>
		    <td><a id="position24" href="javascript:void(0);" class="position-exercise-window-disabled" onclick="javascript:checkIndex($(this), 24);"></a></td>
            <td id="file4" class="hide" style="background-color:white;"><b><span id="index4" style="margin-left:10px;font-size:20px">0</span></b></td>
		</tr>
	</table>
</div>

<div class="pull-left lead" id="result-explanation"></div>

<div id="checkDiv" class="hide" style="margin-bottom: 20px;margin-top: 20px;">
	<a href="javascript:void(0);" onclick="javascript:updateCorrects();" class="btn btn-primary btn-large"><?php echo _('Finalizar'); ?></a>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;

var positionsXML = "exercises/xml/positions-grids.xml";

var grids = null;
var currentGridN = 0; //start in second grid

var disabled = true;

var commonArray = null;
var selectedElements = null;

var currentHelp = 0;

var endBlinking = false;

var replay = false;

var sequentialCorrects = 0;

var responseTime = new Timer();

function beginExercise() {
	if (!exercise.afterDemo() && exercise.repetition() == 0)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				$("#exerciseCounter").show();
				setTimeout(replayExercise, 4000);
			});
		});
	}
	else
	{
		$('#introExercise').fadeOut('slow', function(evt){
			replayExercise();
		});
	}
};

function checkIndex(but, index) {

	if (!disabled)
	{
		if (!but.hasClass("position-exercise-window-selected")) but.addClass("position-exercise-window-selected");
		else but.removeClass("position-exercise-window-selected");
		
		if ($(".position-exercise-window-selected").length >= 5)
		{
			$("#checkDiv").fadeIn('slow');
		}
		else{
			$("#checkDiv").fadeOut('slow');
		}
	}
};

function blink(n)
{
	if (endBlinking) return;
	
	var i=0;
	for (i=n; i<commonArray.length; i++)
	{
		if (commonArray[i]=='1')
		{
			$("#position"+i).effect('highlight', {color:'green'}, 3000, function(){
				blink(i+1);
			});
			break;
		}
	}
	
	if (i >= commonArray.length)
	{
		showInstructions(false);
		$("#exercise-description").html('<?php echo _('Cuando estés preparado para repetir el ejercicio de nuevo pulsa en el botón <strong>Nuevo Intento</strong>.'); ?>');
		$("#positionsPanel").fadeOut('fast');
		$("#helpExerciseButton").fadeIn('fast');
	}
}

function nextHelp() {
	currentHelp++;
	var help = null;
	
	if (currentHelp > 8)
	{	
		$("#exercise-description").fadeOut('slow', function(){
			showInstructions(false);
			$("#exercise-title").html('<?php echo _('&iexcl;Enhorabuena! &iexcl;No est&aacute; nada mal!'); ?>');
			$("#exercise-description").html('<?php echo _('Has ganado una medalla de <strong>Bronce</strong>. Has tenido algunos fallos. Vamos a pasar al siguiente ejercicio. Cuando estés preparado, pulsa en el botón <strong>Continuar</strong>.'); ?>');
			$("#helpEntry-p").html("<img src='img/bronze_medal.png' alt='<?php echo _('Bronce'); ?>' />");
			$("#helpExerciseButton").html("<?php echo _('Continuar'); ?>");
			$("#helpExerciseButton").unbind('click');
			$("#helpExerciseButton").attr('onclick', '');
			$("#helpExerciseButton").bind('click', endExercise);
			
			//force ending
			exercise.commitHelpLevel();
			exercise.setRepetition(exercise.repetitions());
			setForceNext(true);
			
			$("#exercise-description").fadeIn('slow');
			$("#helpEntry").fadeIn('slow');
		});
		
		return;
	}
	
	$("#helpExerciseButton").show();
	
	switch(currentHelp)
	{
		case 1:
			help = "<?php echo _('Fíjate bien en las ventanas que hay abiertas. Están parpadeando ahora mismo para las veas mejor.'); ?>";
			$(".position-exercise-window").addClass('position-exercise-window-disabled').removeClass("position-exercise-window").removeClass('position-exercise-window-selected');
			$("#helpExerciseButton").hide();
			$("#positionsPanel").fadeOut('slow', function(evt){
				fillTable();
				$("#positionsPanel").fadeIn('slow', function(evt){
					endBlinking = false;
					
					setTimeout(function(){
						blink(0);
					}, 5000);
				});
			});
		break;
		case 2:
			help = "<?php echo _('Vamos a intentar hacerlo un poco mejor. Fíjate bien y señala directamente con tu dedo siguiendo el orden donde se encuentra cada una de las ventanas abiertas. Cuando estés preparado para repetir el ejercicio de nuevo pulsa en el botón <strong>Nuevo Intento</strong>.'); ?>";
		break;
		case 3:
		case 5:
		case 7:{
            var rows = [];
            for (var i = 0; i < commonArray.length; i++) {
                if (commonArray[i] == '1') {
                    rows.push(i+1-(5*rows.length));
                }
            }

            for (var i = 0; i < 5; i++) {
                $("#index"+i).html(rows[i]);
                $("#file"+i).removeClass("hide");
            }
			
			help = "<?php echo _('Vamos a intentar recordar un poco mejor donde están situadas las ventanas abiertas en cada fila. Puede ayudarte tratar de contarlas antes de poner el dedo directamente. Por ejemplo, en el edificio de abajo, la primera ventana abierta está en el cuadrado '); ?>"+rows[0]+"<?php echo _(' de la primera fila y la segunda está en el cuadrado '); ?>"+rows[1]+"<?php echo _(' de la segunda fila. Cuando estés preparado para repetir el ejercicio de nuevo pulsa en el botón <strong>Nuevo Intento</strong>.'); ?>";
			$("#positionsPanel").fadeOut('slow', function(evt){
				$(".position-exercise-window").addClass('position-exercise-window-disabled').removeClass("position-exercise-window").removeClass('position-exercise-window-selected');
				/*for (var i=0; i<commonArray.length; i++)
				{
					$("#position"+i).html("<img src='exercises/img/PositionsExercise/ventana-close.png' alt='<?php echo _('Ventana cerrada'); ?>' width='80' height='80' />");
				}
				$("#position0").html("<img src='exercises/img/PositionsExercise/ventana-open.png' alt='<?php echo _('Ventana abierta'); ?>' width='80' height='80' />");
				$("#position21").html("<img src='exercises/img/PositionsExercise/ventana-open.png' alt='<?php echo _('Ventana abierta'); ?>' width='80' height='80' />");
				$("#positionsPanel").fadeIn('slow');*/
				
				fillTable();
				$("#positionsPanel").fadeIn('slow');
			});
		break;
		}
		case 4:
		case 6:
		case 8:{
			var rows = [];
			for (var i=0; i<commonArray.length; i++)
			{
				if (commonArray[i]=='1')
				{
					rows.push(i+1-(5*rows.length));
				}
			}

            for (var i = 0; i < 5; i++) {
                $("#index"+i).html(rows[i]);
                $("#file"+i).removeClass("hide");
            }

			help = "<?php echo _('Vamos a intentarlo otra vez. Ahora te voy a dar otra ayuda que seguramente te va a servir también para hacer mejor la tarea. Fíjate bien, en la primera fila, la ventana abierta está, como antes decíamos, en el cuadrado '); ?>"+rows[0]+"<?php echo _(', que es arriba a la izquierda, el '); ?>"+rows[0]+"<?php echo _('º. En la segunda fila, la ventana abierta está en el cuadrado '); ?>"+rows[1]+"<?php echo _(', ¿ves? En la tercera, la ventana abierta está en el cuadrado '); ?>"+rows[2]+"<?php echo _('. En la cuarta fila, la ventana está en el cuadrado '); ?>"+rows[3]+"<?php echo _('. Y, en la última fila, el cuadrado está en el cuadrado '); ?>"+rows[4]+"<?php echo _('. Cuando estés preparado para repetir el ejercicio de nuevo pulsa en el botón <strong>Nuevo Intento</strong>.'); ?>";
			$("#positionsPanel").fadeOut('slow', function(evt){
				$(".position-exercise-window").addClass('position-exercise-window-disabled').removeClass("position-exercise-window").removeClass('position-exercise-window-selected');
				
				fillTable();
				$("#positionsPanel").fadeIn('slow');
			});
		break;
		}
		default:
		break;
	}
	
	if (help!=null)
	{
		$("#exercise-description").fadeOut('slow', function(){
			showInstructions(false);
			$("#exercise-description").html('<?php echo _('Has tenido algún fallo.'); ?> '+help);
			$("#exercise-description").fadeIn('slow');
			$("#helpEntry").fadeIn('slow');
		});
	}
};

function updateCorrects()
{
	$("#checkDiv").fadeOut('slow');
	disabled = true;
	
	//replay exercise and update corrects
    responseTime.stop();

	var corrects = 0;
	var omissions = 0;
	var failures = 0;
	var selectedWindows = [];
	for (var i=0; i<commonArray.length; i++)
	{
		var position = $("#position"+i);
		var open = commonArray[i];
		var phrase = "<?php echo _('abi'); ?>:"+open+",<?php echo _('sel'); ?>:";
		if (position.hasClass("position-exercise-window-selected"))
		{
			phrase += "1";
			if (open!='1')
			{
				//it is not a correct image
				failures++;
			}
			else corrects++;
		}
		else //we didn't choose it
		{
			phrase += "0";
			if (open == '1')
			{
				//it is correct, but we didn't choose it
				omissions++;
			}
		}
		
		selectedWindows.push(phrase);
	}

	exercise.setCorrects(exercise.corrects() + corrects);
	exercise.setFails(exercise.fails() + failures);
	exercise.setOmissions(exercise.omissions() + omissions);
    exercise.setSeconds(responseTime.getSeconds());

	$("#positionsPanel").fadeOut('slow', function(evt){
		if (failures > 0)
		{
			sequentialCorrects = 0;
			
			nextHelp();
			exercise.updateHelpLevel(currentHelp, 0, selectedWindows);
		}
		else
		{
			exercise.updateHelpLevel(currentHelp, sequentialCorrects+1, selectedWindows);
			sequentialCorrects++;
			
			showInstructions(false);
			if (sequentialCorrects >= 2)
			{
				sequentialCorrects = 0;
				exercise.commitHelpLevel();
				$("#exercise-description").fadeOut('slow', function(){
					if (currentGridN < grids.grid.length)
					{
						$("#exercise-description").html('<?php echo _('<strong>&iexcl;Perfecto!</strong> Has vuelto a colocar las cinco ventanas abiertas en sus lugares correctos. Ya te sabes perfectamente este edificio, veamos si eres ahora capaz de aprender un edificio nuevo. Cuando estés preparado, pulsa en el botón <strong>Siguiente Edificio</strong>.'); ?>');
						$("#helpExerciseButton").html('<?php echo _('Siguiente Edificio'); ?>');
					}
					else
					{
						$("#exercise-description").html('<?php echo _('<strong>&iexcl;Estupendo! &iexcl;Lo has hecho muy bien!</strong> Has vuelto a colocar las cinco ventanas abiertas en sus lugares correctos. Ya has terminado el ejercicio. Cuando estés preparado, pulsa en el botón <strong>Terminar Ejercicio</strong>.'); ?>');
						$("#helpExerciseButton").html('<?php echo _('Terminar Ejercicio'); ?>');
					}
					
					$("#helpExerciseButton").unbind('click');
					$("#helpExerciseButton").attr('onclick', '');
					$("#helpExerciseButton").bind('click', endExercise);
					
					$("#exercise-description").fadeIn('slow');
					$("#helpEntry").fadeIn('slow');
				});
			}
			else
			{
				$("#exercise-description").fadeOut('slow', function(){
					$("#exercise-description").html('<?php echo _('<strong>&iexcl;Perfecto, lo has hecho muy bien!</strong> Has sido capaz de recordar la posición de cada ventana abierta. Veamos si eres capaz de recordar de nuevo la posición de las ventanas abiertas. Cuando estés preparado, pulsa en el botón <strong>Nuevo Intento</strong>.'); ?>');
					$("#exercise-description").fadeIn('slow');
					$("#helpEntry").fadeIn('slow');
				});
			}
		}
		/*
		if (currentGridN >= grids.grid.length)
		{
			endExercise();
		}
		else
		{
			replayExercise();
		}*/
	});
};

function timingFunction(t) {
	if (t<10)
	{
		var str = "<?php echo _('Trata de memorizar las posiciones de las ventanas abiertas.'); ?>";
		if ((10-t) == 1) $('#timer').html(str+" <strong><?php echo _('Queda 1 segundo'); ?></strong>");
		else $('#timer').html(str+" <strong><?php echo _('Quedan'); ?> "+(10-t).toString()+" <?php echo _('segundos'); ?></strong>");
		setTimeout(function(){
			timingFunction(t+1);
		}, 1000);
	}
	else
	{
		disabled = false;
		
		$(".position-exercise-window-disabled").html("<img src='exercises/img/PositionsExercise/ventana-close.png' alt='<?php echo _('Ventana abierta'); ?>' width='80' height='80' />");
	
		$(".position-exercise-window-disabled").addClass('position-exercise-window').removeClass("position-exercise-window-disabled");
	
		//setTimeout(function(){
			showMiniMenu(true);
		//}, 3000);
		
		$('#exercise-description').html("<?php echo _('Marca ahora las posiciones de las ventanas que estaban abiertas.'); ?>");
		$('#timer').html("");

        responseTime.resume();
	}
};

function fillTable() {
	for (var i=0; i<commonArray.length; i++)
	{
		var open = commonArray[i]=='1';
		$("#position"+i).html("<img src='exercises/img/PositionsExercise/ventana-"+(open?'open':'close')+".png' alt='<?php echo _('Ventana'); ?> "+(open?'<?php echo _('abierta'); ?>':'<?php echo _('cerrada'); ?>')+"' width='80' height='80' />");
	}
};

function replayExercise() {
	disabled = true;
	endBlinking = true;
	showInstructions(true);

    for (var i = 0; i < 5; i++) {
        $("#file"+i).addClass("hide");
    }

	$("#helpEntry").fadeOut('slow', function(){
		if (exercise.repetition() > 0 && !replay)
		{
			showInstructions(false);
			$("#exercise-description").html("<?php echo _('Continuamos con el ejercicio. Recuerda que debes memorizar la posición de las ventanas abiertas, para luego marcarlas'); ?>");
		}
		
		replay = false;
		
		$(".position-exercise-window").addClass('position-exercise-window-disabled').removeClass("position-exercise-window").removeClass('position-exercise-window-selected');

		showInstructions(false);
		$('#exercise-description').fadeOut('slow', function(evt){
			currentGridN = exercise.repetition();
			commonArray = grids.grid[currentGridN].positions.split(',');
			currentGridN++;
			
			$('#exercise-description').html("<?php echo _('Repetición:') ?> "+currentGridN+"/"+(grids.grid.length)+". <span id='timer'></span>");
			$('#exercise-description').show();
			$('#timer').html("");
			$("#positionsPanel").fadeOut('slow', function(evt){
				fillTable();
				
				$("#positionsPanel").fadeIn('slow', function(evt){
					timingFunction(0);
                    responseTime.start();
				});
			});
		});
	});
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (exercise.afterDemo() || exercise.repetition() > 0)
	{
		$("#exercise-description").html("<?php echo _('Continuamos con el ejercicio. Recuerda que debes memorizar la posición de las ventanas abiertas, para luego marcarlas'); ?>");
		if (exercise.repetition() > 0)
		{
			$("#introExercise").children("table").hide();
		}
	}
	
	$.ajax({
		type: "GET",
		url: positionsXML,
		dataType: "xml",
		success: function(gridsXMLResult) {
			grids = $.xml2json(gridsXMLResult);
		}
	});
	
	/*
	if (repetitions >= 1)
	{
		$('#exercise-description').html("<?php echo _('Continuamos con el ejercicio. Recuerda que debes seleccionar aquellas imágenes que tengan <strong>una</strong> pirámide grande y <strong>dos</strong> pequeñas con puerta en el <strong>lado soleado</strong>. Debajo puedes ver varios ejemplos del tipo de imágenes que debes seleccionar.'); ?>");
	}*/
});

</script>
