<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!DirectNumbersExercise) {

var DirectNumbersExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Dictado de Números!'); ?>', '<?php echo _('Este ejercicio sirve para medir como está tu memoria. Para ello te mostraré varios números en una pizarra como la de abajo. Irán apareciendo de uno en uno, de manera que deberás ir memorizándolos para después marcarlos en el mismo orden en que te los mostré. Si en algún momento te equivocas podrás pulsar en el botón <strong>Borrar</strong>. Cuando estés listo, pulsa en <strong>Continuar</strong>.'); ?>', 'exercises/DirectNumbersExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._seconds = 0;
		this._level = 2;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	updateResults : function() {
		this.finishExercise();
	},
	demoUIurl : function() {
		//override to return the demo UI
		/*if (parseInt(this.sessionID())==2) return "exercises/DirectNumbersExerciseDemoUI.php";
		else return null;*/
		return "exercises/DirectNumbersExerciseDemoUI.php";
	},
	level : function() {
		return this._level;
	},
	setLevel : function(l) {
		this._level = l;
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	updateUserLevel : function() {
		var qry = 'exerciseType=7&userLevel='+this._level+'&userSubLevel=0';
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_level.php',
			success: function(data){	
			}
		});
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		this.updateUserLevel();
		
		var countCorrects = this._corrects;
		var countFails = this._fails;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_memory_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
