<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<!--<p><img src="exercises/img/DirectNumbersExercise/pizarraIntro.png" width="400" alt="<?php echo _('Pizarra'); ?>" /></p>-->
</div>

<div id="keypad" style="overflow: auto; width:100%;" class="hide">
	<div id="keypadHeader" class="pull-left" style="position:relative; margin-top:-20px;">
      <img src="exercises/img/DirectNumbersExercise/pizarra.png" width="400" alt="<?php echo _('Pizarra'); ?>" />
      <span id="blackboardKeypad" style="position:absolute; top:120px; text-align:center; left:40px; width:320px; height:200px; font-size:90px; color:white;"></span>
	</div>
	<table cellspacing="5" cellpadding="5" class="pull-left" style="margin-left:35px;margin-top:20px;">
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="1" onclick="javascript:checkValue('1');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="2" onclick="javascript:checkValue('2');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="3" onclick="javascript:checkValue('3');" /></td>
		</tr>
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="4" onclick="javascript:checkValue('4');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="5" onclick="javascript:checkValue('5');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="6" onclick="javascript:checkValue('6');" /></td>
		</tr>
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="7" onclick="javascript:checkValue('7');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="8" onclick="javascript:checkValue('8');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="9" onclick="javascript:checkValue('9');" /></td>
		</tr>
		<tr>
			<!--<td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="0" onclick="javascript:checkValue('0');" /></td>-->
		    <td colspan="3" style="height: 50px; width: 300px"><input id="clearButton" type="button" class="btn" style="height: 50px; width: 100%; font-family: Arial; font-size:32px; color:#555;" value="<?php echo _('BORRAR'); ?>" onclick="javascript:eraseLastCharacter();" disabled /></td>
		</tr>
	</table>
</div>

<div id="blackboard" class="hide">
	<div style="position:relative; width:100%;">
      <img src="exercises/img/DirectNumbersExercise/pizarra.png" width="400" alt="<?php echo _('Pizarra'); ?>" />
      <span id="blackboardNumber" style="position:absolute; top:130px; left:180px; width:80%; font-size:80px; color:white;"></span>
	</div>
</div>

<div style="margin-top:20px; margin-left:20px;"><a id="continue-evaluation-btn" class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:continueEvaluation();"><?php echo _('Listo'); ?></a></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var timing = 0;
var resultTiming = 0;
var executionNumber = 0;
var exerciseLevel = 2;
var currentNumberStr = null;
var currentKeypadIndex = 0;
var typedNumber = null;
var numberOfSequentialErrors = 0;
var numberOfLevelCorrects = 0;
var amountLevelShown = 0;
var shownSequences = [];

function beginExercise() {
	$('#introExercise').fadeOut('slow', function(evt){
		showMiniMenu(false);
		replayExercise();
	});
};

function eraseLastCharacter() {
	$("#continue-evaluation-btn").hide();
	if (currentKeypadIndex>0)
	{
		currentKeypadIndex--;
		
		$('input[type="button"][value="'+(typedNumber.charAt(currentKeypadIndex))+'"]').css('color', '#555');
		
		if (currentKeypadIndex == 0)
		{
			typedNumber = "";
		}
		else
		{
			typedNumber = typedNumber.slice(0, currentKeypadIndex);
		}
		
		if (currentKeypadIndex==0)
		{
			$("#clearButton").attr('disabled', 'disabled');
		}
		
		//put an square in the deleted character
		var stubStr = $('#blackboardKeypad').html();
		//stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9744;" + stubStr.substr(currentKeypadIndex+1);
		stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9633;" + stubStr.substr(currentKeypadIndex+1);
		$('#blackboardKeypad').html(stubStr);
	}
	else
	{
		$('input[type="button"][value="'+typedNumber+'"]').css('color', '#555');
		typedNumber = "";
	}
};

function continueEvaluation()
{
	$("#blackboardNumber").html("");
	$('#keypad').hide();
	
	amountLevelShown++;
	if (typedNumber == currentNumberStr)
	{
		exercise.increaseCorrects();
		numberOfSequentialErrors = 0;
		
		numberOfLevelCorrects++;
	}
	else
	{
		exercise.increaseFailures();
		numberOfSequentialErrors++;
	}
	
	if (numberOfLevelCorrects > 0 && amountLevelShown >= 3)
	{
		exerciseLevel++;
		exercise.setLevel(exerciseLevel);
		numberOfLevelCorrects = 0;
		amountLevelShown = 0;
		numberOfSequentialErrors = 0;
	}
	
	if (numberOfSequentialErrors < 3 && exerciseLevel < 9)
	{
		replayExercise();
	}
	else
	{
		$('#exercise-description').html("");
		$('#exercise-description').removeClass('hide').show();
		endExercise();
	}
}

function checkValue(value) {
	if (currentKeypadIndex >= currentNumberStr.length)
	{
		sweetAlert({title:"<?php echo _('Error'); ?>", text:"<?php echo _('No puedes escribir más hasta que no le des al botón Listo, o bien le des al botón Borrar para escribir de nuevo si crees que te has equivocado.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
		return;
	}
	
	//$('input[type="button"][value="'+value+'"]').css('color', 'blue');
	
	var isCorrect = (currentNumberStr.charAt(currentKeypadIndex) == value);
	typedNumber += value;
	
	//show a tick for the typed number
	var stubStr = $('#blackboardKeypad').html();
	stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9632;" + stubStr.substr(currentKeypadIndex+1);
	//stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9745;" + stubStr.substr(currentKeypadIndex+1);
	//stubStr = stubStr.substr(0, currentKeypadIndex) + value + stubStr.substr(currentKeypadIndex+1);
	$('#blackboardKeypad').html(stubStr);
	
	currentKeypadIndex++;
	
	if (currentKeypadIndex>0)
	{
		$("#clearButton").removeAttr('disabled');
	}
	
	if (currentKeypadIndex >= currentNumberStr.length)
	{
		$("#continue-evaluation-btn").fadeIn('fast');
	}
}

function replayNumber() {
	$("#continue-evaluation-btn").hide();
	$('#keypad').fadeOut('slow', function(evt){
		$('#blackboardNumber').html("");
		$('#blackboard').fadeIn('slow', function(evt){
			showNumber(currentNumberStr, 0);
		});
	});
};

function replayExercise() {
	showMiniMenu(true);
	
	for (var i=0; i<=9; i++)
	{
		$('input[type="button"][value="'+i+'"]').css('color', '#555');
	}
	
	$("#continue-evaluation-btn").hide();
	$('#exercise-description').html("<?php echo _('Introduce los números en el mismo orden en el que se te muestran en la pizarra.'); ?>");
	executionNumber++;
	$("#continue-evaluation-btn").hide();
		
	configureKeypad();
	
	typedNumber = "";
	currentKeypadIndex = 0;
	var n = 0;
	do{
		n = randomNumber(exerciseLevel);
	}while(shownSequences.indexOf(n) !== -1);
	
	currentNumberStr = n;
	
	shownSequences.push(currentNumberStr);
	
	replayNumber();
};

function configureKeypad() {
	$("#clearButton").removeClass("hide").show();
	$("#keypadHeader").removeClass("hide").show();
	
	$("#clearButton").attr('disabled', 'disabled');
};

function showNumber(nStr, currentIndex){
	if (currentIndex < nStr.length)
	{
		$("#blackboardNumber").fadeOut('fast', function(evt){
			$("#blackboardNumber").html(nStr.charAt(currentIndex));
			$("#blackboardNumber").fadeIn('fast', function(evt){
				setTimeout(function(){
					showNumber(nStr, currentIndex+1);
				}, 1500);
			});
		});
	}
	else
	{
		$('#blackboard').fadeOut('fast', function(evt){
			var stubStr = "";
			//font size decreases by 10px each 2 numbers (log2(nStr.len))
			var divisions = Math.ceil(Math.log(nStr.length)/Math.log(2));
			var total = 70-(divisions*10);
			if (divisions == 1) total = 70;
			if (total <= 0) total = 1;
			total = total.toString()+"px";
			$('#blackboardKeypad').css('font-size', total);
			
			for (var i=0; i<nStr.length; i++)
			{
				//stubStr += "&#9744;";
				stubStr += "&#9633;";
			}
			$('#blackboardKeypad').html(stubStr);
			$('#keypad').fadeIn('fast');
		});
	}
};

function randomNumber(n) {
    var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    numbers.sort(function(){
        return 0.5 - getRandom();
    });
    if (n>0)
    {
        var result = "";
        for (var i=0; i<n; i++)
        {
            var r = Math.floor(getRandom() * 9) % 9;
            var rStr = numbers[r];
            var k=0;
            while (result.indexOf(rStr) != -1)
            {
                r = (r+1)%9;
                rStr = numbers[r];
                if (k==10) //avoid infinite loop
                {
                    return numbers;
                }
                k++;
            }
            result += rStr;
        }

        return result;
    }
    else return numbers[0];
};

function numberToString(n) {
	return n.toString();
};

$(function() {
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	executionNumber = 0;
	if (exercise.afterDemo())
	{
		$("#introExercise").hide();
		exerciseLevel = exercise.level();
		beginExercise();
	}
});

</script>
