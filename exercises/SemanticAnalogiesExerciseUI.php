<?php
	require_once('locale/localization.php');
?>

<div id="exerciseCounter" class="hide" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;">
</div>

<div id="introExercise" class="hide">
	<p class="lead"><?php echo _('Por ejemplo:'); ?> <?php echo _('La cáscara es para el huevo como la piel es para...'); ?></p>
	<ul class="thumbnails">
		<li class="span2"><span class="fake-thumbnail" style="color:green"><?php echo _('La Manzana'); ?></span></li>
		<li class="span2"><span class="fake-thumbnail"><?php echo _('El Árbol'); ?></span></li>
		<li class="span2"><span class="fake-thumbnail"><?php echo _('El Papel'); ?></span></li>
		<li class="span2"><span class="fake-thumbnail"><?php echo _('La Tierra'); ?></span></li>
	</ul>
	<p class="lead"><?php echo _('En este caso, la respuesta correcta sería <strong><span style="color:green;">Manzana</span></strong>, ya que la piel es el recubrimiento de la manzana, tal y como la cáscara es el recubrimiento del huevo.'); ?></p>
	<a href="javascript:void(0);" onclick="javascript:showSemanticExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="semanticExercise" class="hide">
	<p class="lead" style="font-weight: bold;margin-top:20px; font-size: 32px;"><span id="semanticPrase"></span> <span id="semanticResult"></span></p>
	<ul class="thumbnails">
		<li class="span2"><a id="answer0" href="javascript:void(0);" onclick="javascript:checkAnswer(0);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer1" href="javascript:void(0);" onclick="javascript:checkAnswer(1);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer2" href="javascript:void(0);" onclick="javascript:checkAnswer(2);" class="thumbnail hide"></a></li>
		<li class="span2"><a id="answer3" href="javascript:void(0);" onclick="javascript:checkAnswer(3);" class="thumbnail hide"></a></li>
	</ul>
	<a class="btn btn-large btn-info" href="javascript:void(0);" onclick="javascript:showHelp();" id="help-button"><i class="icon-question-sign"></i> <?php echo _('Ayuda'); ?></a>&nbsp;&nbsp;<span class="lead" id="exercise-explanation"></span>
	<p style="margin-bottom:30px;margin-top: 30px;"><a class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:endExercise();" id="end-button"><?php echo _('Continuar'); ?></a><a class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:checkExercise();" id="continue-button"><?php echo _('Listo'); ?></a></p>
</div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var semanticExercise = null;
var answer = -1;
var difficulty = 0;

function exercisesWithCurrentDifficulty(semanticExercises) {
	var result = [];
	$.each(semanticExercises.SemanticAnalogiesExercise, function(index, item){
		if (parseInt(item.difficulty)==difficulty) result.push(item);
	});
	return result;
};

function allExercises(semanticExercises) {
	var result = [];
	$.each(semanticExercises.SemanticAnalogiesExercise, function(index, item){
		result.push(item);
	});
	return result;
};

function showHelp() {
	$("#exercise-explanation").html("<strong><?php echo _('Pista') ?>:</strong> "+semanticExercise.help);
};

function showSemanticExercise(){
	if (repetitions == 0)
	{
		$('#introExercise').fadeOut('fast', function(evt){
			$("#exerciseCounter").load('beginExercise.php?only_fade=1&out=exerciseCounter', function(){
				$("#exerciseCounter").show();
				setTimeout(function(){
					showMiniMenu(true);
					$('#semanticExercise').fadeIn('slow', function(){
						$('#exercise-title').html(exercise.title());
						$('#exercise-description').html("<?php echo _('Selecciona la palabra que completa la frase y pulsa <strong>Listo</strong>'); ?> ");
					});
				}, 4000);
			});
		});
	}
	else
	{
		$('#introExercise').fadeOut('slow', function(evt){
			showMiniMenu(true);
			$('#semanticExercise').fadeIn('slow', function(){
				$('#exercise-title').html(exercise.title());
				$('#exercise-description').html("<?php echo _('Selecciona la palabra que completa la frase y pulsa <strong>Listo</strong>'); ?> ");
			});
		});
	}
};

function checkAnswer(num) {
	answer = num;
	
	$("#continue-button").fadeIn('fast');
	
	$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle');
	$('#answer'+num).addClass('semantic-exercise-toggle');
};

function checkExercise(){
	var initialText = null;
	if (answer == -1)
	{
		$('#exercise-explanation').html("<?php echo _('Debe seleccionar alguna respuesta'); ?>");
	}
	else
	{
		if (parseInt(semanticExercise.Answer.position) == answer) //win
		{
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-ok");
			initialText = "<span style='color:green;'><b><?php echo _('&iexcl;Correcto!'); ?></b></span><br />";
			exercise.setCorrect(true);
		}
		else //lose
		{
			var goodAnswer = semanticExercise.Answer.position;
			$(".semantic-exercise-toggle").removeClass('semantic-exercise-toggle').addClass("semantic-exercise-bad");
			initialText = "<b><?php echo _('&iexcl;Lo siento!'); ?></b> <?php echo _('La palabra correcta es'); ?> <b>"+semanticExercise.Elements.Element[goodAnswer].value+"</b><br />";
			$('#answer'+goodAnswer).addClass("semantic-exercise-ok");
			exercise.setCorrect(false);
		}
		
		for (i=0; i<6; i++)
		{
			$('#answer'+i).addClass('disabled').attr('onclick', 'javascript:void(0);');
		}
		
		if (exercise.offerHelp())
		{
			$('#exercise-explanation').html(initialText+"<b><?php echo _('Explicaci&oacute;n: '); ?></b>"+semanticExercise.explanation);
		}
		
		$('#help-button').fadeOut('fast');
		$('#continue-button').fadeOut('fast', function(evt){
			$('#exercise-description').html('<?php echo _('Pulsa el botón <strong>Continuar</strong> cuando estés listo.'); ?>');
			$('#end-button').fadeIn('fast');
		});
	}
};

$(function() {
	repetitions = parseInt(lastSession['repetitions']);
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	if (repetitions==0) //show intro
	{
		$('#introExercise').show();
	}
	else //show exercise
	{
		showSemanticExercise();
	}
	
	//retrieve current exercise plan
	$.ajax({
		type: "GET",
		url: "exercises/xml/semantic-analogies-plan.xml",
		dataType: "xml",
		success: function(xml) {
			var plan = $.xml2json(xml);
			var numberOfExercise = -1;
			
			$.ajax({
				type: "GET",
				url: "exercises/xml/semantic-analogies-series.php",
				dataType: "xml",
				success: function(result){
					var semanticExercises = $.xml2json(result);
					
					$.each(plan.SemanticAnalogiesPlanEntry, function(index, value){
						if (value.exerciseID == exerciseID && value.sessionID == sessionID)
						{
							if (value.exercises == "random")
							{
								semanticExercises = exercisesWithCurrentDifficulty(semanticExercises);
								
								//take one random exercise
								numberOfExercise = Math.floor(getRandom()*semanticExercises.length);
							}
							else
							{
								semanticExercises = allExercises(semanticExercises);
								
								var array = value.exercises.split(",");
								if (repetitions < array.length)
								{
									numberOfExercise = parseInt(array[repetitions]);
								
									for (var k=0; k<semanticExercises.length; k++)
									{
										var ex = semanticExercises[k];
										if (ex.id == numberOfExercise)
										{
											numberOfExercise = k;
											break;
										}
									}
								}
								else numberOfExercise = Math.floor(getRandom()*semanticExercises.length);
							}
							
							semanticExercise = semanticExercises[numberOfExercise];
												
							return false; //break the loop
						}
					});
					
					if (semanticExercise != null)
					{
						$.each(semanticExercise.Elements.Element, function(index, element){
							var idAnswer = '#answer'+element.position;
							$(idAnswer).html(element.value).removeClass('hide').addClass('semantic-exercise').show();
						});
						
						$("#semanticPrase").html(semanticExercise.Answer.sample);
					}
					else
					{
						//error
						console.debug("error: semantic exercise not defined");
					}
				}
			});
		}
	});
});
</script>