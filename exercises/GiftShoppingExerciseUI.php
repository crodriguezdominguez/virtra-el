<?php
	require_once('locale/localization.php');
?>

<div id="introExercise" class="hide">
	<p><a id="introNextStep" href="javascript:void(0);" onclick="javascript:nextStepIntro();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<div id="replayExercise" class="hide">
	<p><a href="javascript:void(0);" onclick="javascript:replayExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a></p>
</div>
<div id="mediaContainer" style="position:relative;">
	<div class="alert alert-block alert-error package-delivery-movement-alert hide" id="mediaAlert">
	</div>
	<div id="shoppingPanel" class="hide" style="margin-bottom:30px;">
		<a href="#criteriasModal" role="button" class="btn btn-large" data-toggle="modal" style="position:absolute;left:0px;z-index:1000;"><i class="icon-align-center"></i> <?php echo _('Gustos de personas'); ?></a>&nbsp;&nbsp;<a class="btn btn-large" href="javascript:void(0);" onclick="javascript:showCart();" style="position:absolute;left:230px;z-index:1000;"><i class="icon-shopping-cart"></i> <?php echo _('Mis compras'); ?></a>&nbsp;&nbsp;<span class="hide" id="giftshopping-timer"></span><span id="giftshopping-budget"></span>
	</div>
	<div id="mediaPanel" style="position:relative;margin-bottom:10px;">
	</div>
</div>

<!-- Criterias Modal -->
<div id="criteriasModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="criterialModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="criterialModalLabel"><?php echo _('Gustos de personas'); ?></h3>
	</div>
	<div class="modal-body">
		<p id="giftsCriteriasModalContent"></p>
	</div>
	<div class="modal-footer">
		<button class="btn btn-large btn-danger" data-dismiss="modal" aria-hidden="true"><?php echo _('Ir a las tiendas'); ?></button>
	</div>
</div>

<!-- Cart Modal -->
<div id="cartModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="cartModalLabel"><?php echo _('Mis compras'); ?></h3>
	</div>
	<div class="modal-body">
		<p>
			<table id="cartModalContent" style="margin-left:auto; margin-right:auto;">
			</table>
		</p>
	</div>
	<div class="modal-footer">
		<button class="btn btn-large btn-danger" data-dismiss="modal" aria-hidden="true"><?php echo _('Ir a las tiendas'); ?></button>
	</div>
</div>

<!-- Items Modal -->
<div id="itemsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="itemsModalLabel" aria-hidden="true" style="width:50%;margin-left:-25%;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="itemsModalLabel"></h3>
	</div>
	<div class="modal-body" style="overflow: hidden;">
		<p id="itemsModalContent">
			<table style="margin-left:auto; margin-right:auto;">
				<tr>
					<td class="giftsItemDescription"><a id="shopitem0" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(0);"></a></td>
					<td class="giftsItemDescription"><a id="shopitem1" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(1);"></a></td>
					<td class="giftsItemDescription"><a id="shopitem2" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(2);"></a></td>
				</tr>
				<tr>
					<td class="giftsItemDescription"><a id="shopitem3" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(3);"></a></td>
					<td class="giftsItemDescription"><a id="shopitem4" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(4);"></a></td>
					<td class="giftsItemDescription"><a id="shopitem5" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(5);"></a></td>
				</tr>
				<tr>
					<td class="giftsItemDescription"><a id="shopitem6" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(6);"></a></td>
					<td class="giftsItemDescription"><a id="shopitem7" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(7);"></a></td>
					<td class="giftsItemDescription"><a id="shopitem8" href="javascript:void(0);" class="thumbnail" onclick="javascript:addToCart(8);"></a></td>
				</tr>
			</table>
		</p>
	</div>
	<div class="modal-footer">
		<button class="btn btn-large btn-danger" data-dismiss="modal" aria-hidden="true"><?php echo _('Salir de la tienda'); ?></button>
	</div>
</div>


<script type="text/javascript">
var repetitions = 0;
var sessionID = null;
var exerciseID = null;
var placesXML = "exercises/xml/giftshopping-places.php";
var itemsXML = "exercises/xml/giftshopping-items.php";
var criteriasXML = "exercises/xml/giftshopping-criterias.php";
var places = [];
var cart = null;
var criterias = [];
var currentIntroStep = 0;
var currentPlace = null;
var endTimer = false;
var timing = 0;
var level = 0;
var criteriasByLevel = [4, 6, 9];
var shopsByLevel = [6, 12, 15];
var currentBudget = 0;
var totalBudget = 0;
var allplaces = null;
var allitems = null;
var allcriterias = null;
var hasBegun = false;
var numberOfReplays = 0;
var MAX_TIME = 600; //600 seconds: 10 minutes
var expectedItems = null;

function similarity(item1, item2) {
	var tags1 = item1.tags.split(",");
	var tags2 = item2.tags.split(",");
	var maxTagsPossible = Math.min(tags1.length, tags2.length);
	var auxSimilarity = 0;
			
	for (var i=0; i<tags1.length; i++) {
		for (var j=0; j<tags2.length; j++) {
			if (tags1[i] == tags2[j]) auxSimilarity += (1.0 / maxTagsPossible);
		}				
	}
			
	return auxSimilarity;
};

function updateItemsModal() {
	$("#itemsModalLabel").html(currentPlace.name);
	$('[id^="shopitem"]').html("");
	$('[id^="shopitem"]').hide();
	$('[id^="shopitem"]').css('position', 'relative');
	
	if (currentPlace.items.length < 9)
	{
		//clone random items with other prices
		var correctsElementsHere = [];
		var restOfElements = [];
		for (var k=0; k<expectedItems.length; k++)
		{
			if (currentPlace.id==expectedItems[k].shop)
			{
				correctsElementsHere.push(expectedItems[k]);
			}
		}
		
		for (var k=0; k<currentPlace.items.length; k++)
		{
			var item = currentPlace.items[k];
			var enter = true;
			for (var w=0; w<correctsElementsHere.length; w++)
			{
				if (correctsElementsHere[w].id == item.id)
				{
					enter = false;
					break;
				}
			}
			
			if (enter) restOfElements.push(item);
		}
		
		var endIndex = currentPlace.items.length+correctsElementsHere.length;
		for (var i=currentPlace.items.length; i<9 && i<endIndex; i++)
		{
			var rIndex = Math.floor(getRandom()*correctsElementsHere.length);
			var oldObject = correctsElementsHere[rIndex];
			var newObject = jQuery.extend(true, {}, oldObject);
			newObject.finalprice = calculateFinalPrice(newObject);
			
			currentPlace.items.push(newObject);
		}
		
		for (var i=currentPlace.items.length; i<9; i++)
		{
			//take one random object
			var rIndex = Math.floor(getRandom()*restOfElements.length);
			var oldObject = restOfElements[rIndex];
			var newObject = jQuery.extend(true, {}, oldObject);
			newObject.finalprice = calculateFinalPrice(newObject);
			
			currentPlace.items.push(newObject);
		}
	}
	
	for (var i=0; i<currentPlace.items.length; i++)
	{
		var item = currentPlace.items[i];
		var contains = containsCart(item);
		$("#shopitem"+i).html("<img src='"+item.img+"' alt='"+item.name+"' width='80px' height='80px' style='height:80px;width:80px;' class='"+(contains?"giftsItemBought":'')+"' /><p>"+item.name+"</p>");
		if(contains)
		{
			$("#shopitem"+i).append("<div class='giftsItemBought'><?php echo _('Comprado'); ?></div>");
		}
		else
		{
			$("#shopitem"+i).append("<div class='giftsItemNotBought'>"+item.finalprice+" <?php echo _('€'); ?></div>");
		}
		$("#shopitem"+i).show();
	}
};

function showCart() {
	updateCartModal();
	$("#cartModal").modal('toggle');
};

function updateCartModal() {
	var result = "";
	if (cart.length == 0)
	{
		result += "<tr><td class='lead' style='width:100%;text-align:center;font-weight:bold;'><?php echo _('Ahora está vacío porque todavía no has comprado nada'); ?></td></tr>";
	}
	else
	{
		for (var i=0; i<cart.length; i++)
		{
			var item = cart[i];
			result += "<tr class='giftsCartItem'>";
			result += "<td style='width:15%'><img src='"+item.img+"' alt='"+item.name+"' width='50px' height='50px' /></td>";
			result += "<td style='width:70%;text-align:left;font-size:24px;'><p style='margin-top:5px;'>"+item.name+"</p></td>";
			result += "<td style='text-align:left;'><a href='javascript:void(0);' class='btn btn-danger' onclick='javascript:removeFromCart("+i+")'>"+"<?php echo _('Devolver'); ?> ("+(item.finalprice)+" <?php echo _('€'); ?>)</a></td>";
			result += "</tr>";
		}
	}
	
	$("#cartModalContent").html(result);
};

function updateBudget() {
	//$("#giftshopping-budget").html(""+(totalBudget-currentBudget)+" <?php echo _('€'); ?> / "+totalBudget+" <?php echo _('€'); ?>");
	
	$("#giftshopping-budget").html("<?php echo _('Tengo'); ?> "+currentBudget+" <?php echo _('€'); ?>, <?php echo _('he gastado'); ?> "+(totalBudget-currentBudget)+" <?php echo _('€'); ?>");
};

function timingFunction() {
	if (!endTimer){
		timing++;
		var res = MAX_TIME-timing;
		if (res>0)
		{
			var minutes = Math.floor(res/60);
			var seconds = res%60;
			if (seconds<10) seconds = "0"+seconds;
			$("#giftshopping-timer").html(""+minutes+":"+seconds);
			setTimeout(timingFunction, 1000);
		}
		
		else
		{
			$("#giftshopping-timer").html("0:00");
			checkExercise();
		}
	}
};

function frameOfPlaceAtIndex(index) {
	return frameOfPlace(places.Place[index]);
};

function frameOfPlace(place) {
	var x = parseInt(place.x);
	var y = parseInt(place.y);
	var w = parseInt(place.w);
	var h = parseInt(place.h);
	return [x,y,w,h];
};

function showShopAtIndex(index){
	if (hasBegun)
	{
		currentPlace = places[index];
		updateItemsModal();
		$("#itemsModal").modal('toggle');
	}
};

function placeHTML(place) {
	var index = places.indexOf(place);
	if (index != -1)
	{
		var frame = frameOfPlace(place);
		var placeHTML = "<div onclick='javascript:showShopAtIndex("+index+");' class='package-delivery-place' id='place"+index+"' style='left:"+frame[0]+"px; top:"+frame[1]+"px; width:"+frame[2]+"px; height:"+frame[3]+"px;'>"
		placeHTML += "<img width='"+frame[2]+"px' height='"+frame[3]+"px' src='"+place.img+"' alt='"+place.name+"' /></div>";
		return placeHTML;
	}
	else return "";
};

function placesHTML() {
	var pHTML = "<img alt='<?php echo _('Ciudad'); ?>' src='"+allplaces.background+"' width='100%' height='100%' />";
	$.each(places, function(index, element){
		pHTML = pHTML+placeHTML(element);
	});
	
	return pHTML;
};

function nextStepIntro() {
	$("#introExercise").fadeOut('slow', function(evt){
		showMiniMenu(true);
		$("#exercise-description").html("<?php echo _('Compra regalos a tus amigos y familiares.'); ?>");
		replayExercise();
	});
};

function makeFinalCheck(){
	endTimer = true;
	var sim = 0;
	$.each(cart, function(index, item){
		$.each(expectedItems, function(j, itemToCompare){
			sim += similarity(item, itemToCompare);
		});
	});
	
	sim = sim * (100/criteriasByLevel[level]);
	if (sim>80)
	{
		level++;
		MAX_TIME += 60;
		if (level>2) level = 2;
		exercise.setLevel(level);
		exercise.updateUserLevel();
	}
	
	exercise.setSeconds(exercise.seconds()+timing);
	exercise.setFinalScore(sim+exercise.finalScore());
	exercise.updateResults(sim, timing);
	
	$("#mediaContainer").fadeOut('slow', function(evt){
		showInstructions(true);
		$("#exercise-mini-description").html("");
		$("#exercise-description").html("<?php echo _('Ya has terminado las compras. Ahora realizarás algunas más. Cuando estés preparado pulsa en el botón <strong>Continuar</strong>.'); ?>");
		repetitions++;
		if (repetitions >= numberOfReplays)
		{
			exercise.setFinalScore(Math.ceil(exercise.finalScore()/numberOfReplays));
			endExercise();
		}
		else $("#replayExercise").fadeIn('slow');
	});
}

function checkExercise() {
	//calculate similary for items in the cart and items to be bought
	//similarity = similarity * (100/criteriasByLevel[level])
	//if similarity > 80 level++   if (level>2) level = 2
	
	if (MAX_TIME-timing > 0)
	{
		swal({
				title: "<?php echo _('¿Estás seguro?'); ?>",
				text: "<?php echo _('¿Seguro que deseas finalizar el ejercicio?'); ?>",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "<?php echo _('Si, terminar'); ?>",
				cancelButtonText: "<?php echo _('No terminar'); ?>",
				closeOnConfirm: true
			},
			function(isConfirm){
				if (isConfirm)
				{
					makeFinalCheck();
				}
			});
	}
	else
	{
		makeFinalCheck();
	}
};

function replayExercise() {	
	takePlaces();
	takeCriterias();
	takeItems();
	cart = [];
	hasBegun = true;
	
	$("#replayExercise").fadeOut('slow', function(f){
		$("#mediaContainer").fadeOut('slow', function(evt){
			updateBudget();
			$("#shoppingPanel").removeClass('hide').show();
			$("#mediaPanel").html(placesHTML());
			$("#exercise-description").html("<?php echo _('Compra regalos a tus amigos y familiares.'); ?>");
			$("#exercise-mini-description").html("<a class='btn btn-primary' href='javascript:void(0);' onclick='javascript:checkExercise();'><?php echo _('Finalizar compra'); ?></a>");
			
			showMiniMenu(true);
			$("#mediaContainer").fadeIn('slow', function(e){
				//$('#criteriasModal').modal('show');
				timing = 0;
				endTimer = false;
				timingFunction();
			});
		});
	});
};

function containsCart(item){
	for (var i=0; i<cart.length; i++)
	{
		if (cart[i].id == item.id && cart[i].finalprice == item.finalprice) return true;
	}
	return false;
};

function addToCart(itemIndex) {
	if (itemIndex < currentPlace.items.length)
	{
		var item = currentPlace.items[itemIndex];
		if (!containsCart(item))
		{
			if ((currentBudget-item.finalprice) >= 0)
			{
				currentBudget -= item.finalprice;
				updateBudget();
				cart.push(currentPlace.items[itemIndex]);
				updateItemsModal();
			}
			else
			{
				sweetAlert({title:"<?php echo _('Error'); ?>", text:"<?php echo _('No tienes dinero suficiente para comprar este artículo. Devuelve algunos artículos ya comprados, si lo crees necesario.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
			}
		}
	}
};

function removeFromCart(itemIndex) {
	currentBudget += cart[itemIndex].finalprice;
	updateBudget();
	cart.splice(itemIndex, 1);
	updateCartModal()
};

function itemForID(idItem){
	var result = null;
	$.each(allitems.Item, function(index, element){
		if (element.id == idItem)
		{
			result = element;
			return false;
		}
	});
	return result;
};

function placeContainsItem(place, itemID) {
	var result = false;
	$.each(place.items, function(index, element){
		if (element.id == itemID)
		{
			result = true;
			return false;
		}
	});
	return result;
}

function placeForItem(item){
	var result = null;
	$.each(places, function(index, element){
		if (element.id == item.shop)
		{
			result = element;
			return false;
		}
	});
	return result;
};

function takeItems() {
	allitems.Item.sort(function(a,b){
		return 0.5-getRandom();
	});
	
	$.each(allitems.Item, function(index, element){
		var p = placeForItem(element);
		if (p!=null)
		{
			if (p.items.length < 9 && !placeContainsItem(p, element.id))
			{
				element.finalprice = calculateFinalPrice(element);
				p.items.push(element);
			}
		}
	});
	
	//randomize items at shops
	$.each(places, function(index, p){
		p.items.sort(function(a,b){
			return 0.5-getRandom();
		});
	});
};

function takePlaces(){
	allplaces.Place.sort(function(a,b){
		return 0.5-getRandom();
	});
	places = [];
	var maxPlaces = shopsByLevel[level];
	$.each(allplaces.Place, function(index, element){
		if (index < maxPlaces)
		{
			var location = allplaces.Locations.Location[index];
			element.x = location.x;
			element.y = location.y;
			element.items = [];
			places.push(element);
		}
	});
};

function calculateFinalPrice(item){
	var min = 0;
	var max = 0;
	if (getRandom()<0.5) //cheap price
	{
		min = parseFloat(item.cheapmin);
		max = parseFloat(item.cheapmax);
	}
	else //expensive price
	{
		min = parseFloat(item.expensivemin);
		max = parseFloat(item.expensivemax);
	}
	return Math.floor(getRandom()*(max-min))+min;
};

function takeCriterias(){
	//look for criterias that correspond to items in the available places
	var takeAgain = true;
	
	while (takeAgain)
	{
		allcriterias.Criteria.sort(function(a,b){
			return 0.5-getRandom();
		});
		
		//if three consecutive items are for the same place, take others
		var previousPlace = null;
		var countConsPlaces = 0;
		takeAgain = false;
		$.each(allcriterias.Criteria, function(index, element){
			var item = itemForID(element.item);
			if (item != null)
			{
				var p = placeForItem(item);
				if (p != null)
				{
					if (previousPlace != null)
					{
						if (p.id == previousPlace.id)
						{
							countConsPlaces++;
						}
						else countConsPlaces = 0;
					}
					
					if (countConsPlaces >= 3)
					{
						takeAgain = true;
						return false;
					}
					
					previousPlace = p;
				}
			}
		});
	}
	
	criterias = [];
	var maxCriterias = criteriasByLevel[level];
	var count = 0;
	expectedItems = [];
	$.each(allcriterias.Criteria, function(index, element){
		if (count < maxCriterias)
		{
			var item = itemForID(element.item);
			if (item != null)
			{
				var p = placeForItem(item);
				if (p != null)
				{
					expectedItems.push(item);
					p.items.push(item);
					criterias.push(element);
					count++;
				}
			}
		}
		else return false;
	});
	
	var finalPrice = 0.0;
	
	$.each(places, function(i, place){
		$.each(place.items, function(j, item){
			item.finalprice = calculateFinalPrice(item);
			finalPrice += item.finalprice;
		});
	});
	
	//give more money than required
	currentBudget = Math.ceil(((finalPrice/50.0)+1.0)*50.0);
	totalBudget = currentBudget;
	
	var modalHTML = "<ul>";
	$.each(criterias, function(index, element){
		modalHTML += "<li>"+element.phrase+"</li>";
	});
	modalHTML+="</ul>";
	
	$("#giftsCriteriasModalContent").html(modalHTML);
};

$(function() {
	repetitions = 0;
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	
	$.ajax({
		type: "GET",
		url: placesXML,
		dataType: "xml",
		success: function(placesXMLResult) {
			allplaces = $.xml2json(placesXMLResult);
			$.ajax({
				type: "GET",
				url: itemsXML,
				dataType: "xml",
				success: function(itemsXMLResult) {
					allitems = $.xml2json(itemsXMLResult);
					$.ajax({
						type: "GET",
						url: criteriasXML,
						dataType: "xml",
						success: function(criteriasXMLResult) {
							allcriterias = $.xml2json(criteriasXMLResult);
							
							takePlaces();
							takeCriterias();
							takeItems();
							
							numberOfReplays = Math.ceil(exercise.duration()/4);
							
							//show the panels
							$("#mediaPanel").css('width', allplaces.width);
							$("#mediaPanel").css('height', allplaces.height);
							$("#mediaContainer").css('width', allplaces.width);
							$("#mediaPanel").html(placesHTML());
							if (!exercise.afterDemo()) $("#introExercise").fadeIn('slow');
							else nextStepIntro();
						}
					});
				}
			});
			
		}
	});
});

</script>
