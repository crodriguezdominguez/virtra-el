<?php
	require_once('locale/localization.php');
?>

<div id="introExercise">
	<a href="javascript:void(0);" onclick="javascript:beginExercise();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
	<p><img src="exercises/img/NumbersAndVowelsExercise/pizarraIntro.png" width="400" alt="<?php echo _('Pizarra'); ?>" /></p>
</div>
<div id="exerciseResult" class="hide">
	<!-- <p class="lead"><?php // echo _('Has acertado') ?>&nbsp;<span id="totalCorrects"></span>&nbsp;<?php // echo _('de un total de');?>&nbsp;<span id="totalCharacters"></span>&nbsp;<?php // echo _('caracteres. Pulsa el botón <strong>Continuar</strong> cuando estés preparado'); ?></p> -->
    <p class="lead"><br/><?php echo _('Pulsa el botón <strong>Continuar</strong> cuando estés preparado'); ?></p>
	<a href="javascript:void(0);" onclick="javascript:hideResult();" class="btn btn-primary btn-large"><?php echo _('Continuar'); ?></a>
</div>

<div id="keypad" class="hide" style="width:100%;overflow: auto;">
	<div id="keypadHeader" style="position:relative; width:auto; margin-top:0px;" class="pull-left">
      <img src="exercises/img/DirectNumbersExercise/pizarra.png" width="400" alt="<?php echo _('Pizarra'); ?>" />
      <span id="blackboardKeypad" style="position:absolute; top:120px; text-align:center; left:40px; width:320px; height:200px; font-size:90px; color:white;"></span>
	</div>
	<table cellspacing="5" cellpadding="5" style="margin-top:70px;">
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="1" onclick="javascript:checkValue('1');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="2" onclick="javascript:checkValue('2');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="3" onclick="javascript:checkValue('3');" /></td>
		    
		    <td><input type="button" class="btn" style="margin-left:20px; height: 50px; width: 100px; font-family: serif; font-style: italic; font-size:32px; color:#555;" value="a" onclick="javascript:checkValue('a');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: serif; font-style: italic; font-size:32px; color:#555;" value="e" onclick="javascript:checkValue('e');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: serif; font-style: italic; font-size:32px; color:#555;" value="i" onclick="javascript:checkValue('i');" /></td>
		</tr>
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="4" onclick="javascript:checkValue('4');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="5" onclick="javascript:checkValue('5');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="6" onclick="javascript:checkValue('6');" /></td>
		    
		    <td><input type="button" class="btn" style="margin-left:20px; height: 50px; width: 100px; font-family: serif; font-style: italic; font-size:32px; color:#555;" value="o" onclick="javascript:checkValue('o');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: serif; font-style: italic; font-size:32px; color:#555;" value="u" onclick="javascript:checkValue('u');" /></td>
		</tr>
		<tr>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="7" onclick="javascript:checkValue('7');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="8" onclick="javascript:checkValue('8');" /></td>
		    <td><input type="button" class="btn" style="height: 50px; width: 100px; font-family: Arial; font-size:32px; color:#555;" value="9" onclick="javascript:checkValue('9');" /></td>
		    
		    <td colspan="3" style="padding-left:25px; height: 50px; width: 300px;"><input id="clearButton" type="button" class="btn" style="height: 50px; width: 100%; font-family: Arial; font-size:32px; color:#555;" value="<?php echo _('BORRAR'); ?>" onclick="javascript:eraseLastCharacter();" disabled /></td>
		</tr>
	</table>
</div>

<div id="blackboard" class="hide">
	<div style="position:relative; width:100%;">
      <img src="exercises/img/DirectNumbersExercise/pizarra.png" width="400" alt="<?php echo _('Pizarra'); ?>" />
      <span id="blackboardNumber" style="position:absolute; top:130px; left:180px; width:80%; font-size:80px; color:white;"></span>
	</div>
</div>

<div style="margin-top:20px; margin-left:20px;"><a id="continue-evaluation-btn" class="btn btn-primary btn-large hide" href="javascript:void(0);" onclick="javascript:continueEvaluation();"><?php echo _('Listo'); ?></a></div>

<script type="text/javascript">
var repetitions = null;
var sessionID = null;
var exerciseID = null;
var timing = 0;
var resultTiming = 0;
var executionNumber = 0;
var exerciseLevel = 2;
var currentSequenceStr = null;
var currentRandomSequence = null;
var currentKeypadIndex = 0;
var typedSequence = null;
var numberOfSequentialErrors = 0;
var numberOfLevelCorrects = 0;
var numberOfCorrects = 0;
var amountLevelShown = 0;
var shownSequences = [];

function beginExercise() {
	$('#introExercise').fadeOut('slow', function(evt){
		replayExercise();
	});
};

function eraseLastCharacter() {
	$("#continue-evaluation-btn").hide();
	if (currentKeypadIndex>0)
	{
		$('input[type="button"][value="'+typedSequence[currentKeypadIndex]+'"]').css('color', '#555');
		
		currentKeypadIndex--;
		
		if (currentKeypadIndex >= 0 && currentKeypadIndex < currentSequenceStr.length && currentKeypadIndex < typedSequence.length)
		{
			var wasCorrect = (currentSequenceStr.charAt(currentKeypadIndex) == typedSequence.charAt(currentKeypadIndex));
			if (wasCorrect) numberOfCorrects--;
		}
		
		typedSequence = typedSequence.substring(0, typedSequence.length - 1);
		
		if (currentKeypadIndex==0)
		{
			$("#clearButton").attr('disabled', 'disabled');
		}
		
		//put an square in the deleted character
		var stubStr = $('#blackboardKeypad').html();
		//stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9744;" + stubStr.substr(currentKeypadIndex+1);
		stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9633;" + stubStr.substr(currentKeypadIndex+1);
		$('#blackboardKeypad').html(stubStr);
	}
};

function continueEvaluation()
{
	$("#continue-evaluation-btn").hide();
	$("#blackboardNumber").html("");
	$('#keypad').hide();

	amountLevelShown++;
	if (typedSequence == currentSequenceStr)
	{
		exercise.increaseCorrects();
		numberOfSequentialErrors = 0;
		
		numberOfLevelCorrects++;
	}
	else
	{
		exercise.increaseFailures();
		numberOfSequentialErrors++;
	}
	
	if (numberOfLevelCorrects > 0 && amountLevelShown>=3 && exerciseLevel < 14) // 5 vowels + 9 numbers = 14
	{
		exerciseLevel++;
		exercise.setLevel(exerciseLevel);
		numberOfLevelCorrects = 0;
		amountLevelShown = 0;
		numberOfSequentialErrors = 0;
	}
	
	if (numberOfSequentialErrors < 3 && exerciseLevel < 9)
	{
		// $("#totalCorrects").html(numberOfCorrects);
		// $("#totalCharacters").html(currentRandomSequence.length);
		$("#exerciseResult").fadeIn('slow');
	}
	else
	{
		$('#exercise-description').html("");
		$('#exercise-description').removeClass('hide').show();
		endExercise();
	}
};

function checkValue(value) {
	if (currentKeypadIndex >= currentSequenceStr.length)
	{
		sweetAlert({title:"<?php echo _('Error'); ?>", text:"<?php echo _('No puedes escribir más hasta que no le des al botón Listo, o bien le des al botón Borrar para escribir de nuevo si crees que te has equivocado.'); ?>", type:"error", confirmButtonText:"<?php echo _('Continuar'); ?>"});
		return;
	}
	
	//$('input[type="button"][value="'+value+'"]').css('color', 'blue');
	
	var isCorrect = (currentSequenceStr.charAt(currentKeypadIndex) == value);
	if (isCorrect) numberOfCorrects++;
	
	typedSequence += value;
	
	//show a tick for the typed number
	var stubStr = $('#blackboardKeypad').html();
	stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9632;" + stubStr.substr(currentKeypadIndex+1);
	//stubStr = stubStr.substr(0, currentKeypadIndex) + "&#9745;" + stubStr.substr(currentKeypadIndex+1);
	//stubStr = stubStr.substr(0, currentKeypadIndex) + value + stubStr.substr(currentKeypadIndex+1);
	$('#blackboardKeypad').html(stubStr);
	
	currentKeypadIndex++;
	
	if (currentKeypadIndex>0)
	{
		$("#clearButton").removeAttr('disabled');
	}
	
	if (currentKeypadIndex >= currentSequenceStr.length)
	{
		$("#continue-evaluation-btn").fadeIn('fast');
	}
};

function hideResult() {
	$("#continue-evaluation-btn").hide();
	$("#exerciseResult").fadeOut('slow', function(evt){
		replayExercise();
	});
};

function replaySequence() {
	$("#continue-evaluation-btn").hide();
	for (var i=0; i<=9; i++)
	{
		$('input[type="button"][value="'+i+'"]').css('color', '#555');
	}
	
	$('input[type="button"][value="a"]').css('color', '#555');
	$('input[type="button"][value="e"]').css('color', '#555');
	$('input[type="button"][value="i"]').css('color', '#555');
	$('input[type="button"][value="o"]').css('color', '#555');
	$('input[type="button"][value="u"]').css('color', '#555');
	
	$('#keypad').fadeOut('slow', function(evt){
		$('#blackboardNumber').html("");
		$('#blackboard').fadeIn('slow', function(evt){
			showSequence(currentRandomSequence, 0);
		});
	});
};

function replayExercise() {
	showMiniMenu(true);
	$("#continue-evaluation-btn").hide();
	$('#exercise-description').fadeOut('slow', function(evt){
		$('#button-explanation').hide();
		executionNumber++;
		
		configureKeypad();
		
		typedSequence = "";
		currentKeypadIndex = 0;
		numberOfCorrects = 0;
		currentRandomSequence = randomSequence(exerciseLevel);
		currentSequenceStr = currentRandomSequence.split("");
		currentSequenceStr.sort(function(a, b){
			if ($.isNumeric(a) && $.isNumeric(b))
			{
				if (a<b) return -1;
				else if (b<a) return 1;
				else return 0;
			}
			else if ($.isNumeric(a) && !$.isNumeric(b))
			{
				return -1;
			}
			else if (!$.isNumeric(a) && $.isNumeric(b))
			{
				return 1;
			}
			else
			{
				var aL = a.toLowerCase();
				var bL = b.toLowerCase();
				if (aL<bL) return -1;
				else if (bL<aL) return 1;
				else return 0;
			}
		});
		currentSequenceStr = currentSequenceStr.join("");
		shownSequences.push(currentSequenceStr);
		replaySequence();
	});
};

function configureKeypad() {
	$("#clearButton").removeClass("hide").show();
	$("#keypadHeader").removeClass("hide").show();
	
	$("#clearButton").attr('disabled', 'disabled');
};

function showSequence(nStr, currentIndex){
	if (currentIndex < nStr.length)
	{
		$("#blackboardNumber").fadeOut('fast', function(evt){
			var ch = nStr.charAt(currentIndex);
			if (!$.isNumeric(ch))
			{
				ch = '<span style="font-family:serif;font-style:italic;">'+ch+"</span>";
			}
			$("#blackboardNumber").html(ch);
			$("#blackboardNumber").fadeIn('fast', function(evt){
				setTimeout(function(){
					showSequence(nStr, currentIndex+1);
				}, 1500);
			});
		});
	}
	else
	{
		$('#blackboard').fadeOut('fast', function(evt){
			var stubStr = "";
			//font size decreases by 10px each 2 numbers (log2(nStr.len))
			var divisions = Math.ceil(Math.log(nStr.length)/Math.log(2));
			var total = 70-(divisions*10);
			if (divisions == 1) total = 70;
			if (total <= 0) total = 1;
			total = total.toString()+"px";
			$('#blackboardKeypad').css('font-size', total);
			
			for (var i=0; i<nStr.length; i++)
			{
				//stubStr += "&#9744;";
				stubStr += "&#9633;";
			}
			$('#blackboardKeypad').html(stubStr);
			$('#keypad').fadeIn('fast', function(evt){
					$('#exercise-description').html("<?php echo _('Introduce <strong>primero</strong> los números que se mostraron en la pizarra <strong>del más pequeño al más grande</strong> y <strong>luego</strong> las vocales en orden <strong>aeiou</strong>.'); ?>");
					$('#exercise-description').show();
			});
		});
	}
};

function randomSequence(n){
	//divide the sequence between numbers and words
    var N1 = (n <= 10) ? Math.floor(n/2) : 5;
	var N2 = n-N1;
	
	if (N1 != N2 && n <= 10)
	{
		//random exchange between N1 and N2 if they are different
		if (getRandom()<0.5)
		{
			var temp = N1;
			N1 = N2;
			N2 = temp;
		}
	}
	
	var word = randomWord(N1);
	var number = randomNumber(N2);
	var result = word+number;
	result = result.split("");
	result.sort(function(){
		return 0.5 - getRandom();
	});
	result = result.join("");
	
	if (shownSequences.indexOf(result) === -1) return result;
	else return randomSequence(n); //generate another one
};

function randomWord(n) {
	var letters = ["a", "e", "i", "o", "u"];
	letters.sort(function(){
			return 0.5 - getRandom();
	});
	if (n>0 && n<=5)
	{
		var result = "";
		for (var i=0; i<n; i++)
		{
			var r = Math.floor(getRandom() * 5) % 5;
			var rStr = letters[r];
			var k=0;
			while (result.indexOf(rStr) != -1)
			{
				r = (r+1)%5;
				rStr = letters[r];
				if (k==10) //avoid infinite loop
				{
					return letters;
				}
				k++;
			}
			result += rStr;
		}
		
		return result;
	}
	else if (n>=5) return letters;
	else return letters[0];
};

function randomNumber(n) {
	var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
	numbers.sort(function(){
			return 0.5 - getRandom();
	});
	if (n>0)
	{
		var result = "";
		for (var i=0; i<n; i++)
		{
			var r = Math.floor(getRandom() * 9) % 9;
			var rStr = numbers[r];
			var k=0;
			while (result.indexOf(rStr) != -1)
			{
				r = (r+1)%9;
				rStr = numbers[r];
				if (k==10) //avoid infinite loop
				{
					return numbers;
				}
				k++;
			}
			result += rStr;
		}
		
		return result;
	}
	else return numbers[0];
};

function numberToString(n) {
	return n.toString();
};

$(function() {
	sessionID = parseInt(lastSession['sessionID']);
	exerciseID = parseInt(lastSession['exerciseID']);
	executionNumber = 0;
	
	if (exercise.afterDemo())
	{
		$("#introExercise").hide();
		beginExercise();
	}
});

</script>
