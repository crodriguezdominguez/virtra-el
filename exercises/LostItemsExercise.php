<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!LostItemsExercise) {

var LostItemsExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		this._super('<?php echo _('&iexcl;Objetos desordenados!'); ?>', '<?php echo _('En este ejercicio verás diferentes objetos repartidos por las habitaciones de una casa. Tendrás que buscar los objetos que no pertenezcan a las habitaciones y llevarlos a las habitaciones adecuadas. También deberás recoger las monedas que encuentres por el suelo o en cualquier otro lugar. Pulsa <strong>Continuar</strong>.');?>', 'exercises/LostItemsExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		this._corrects = 0;
		this._fails = 0;
		this._omissions = 0;
		this._seconds = 0;
		this._finalScore = 0;
		this._level = 0;
		this._sublevel = 0;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	demoUIurl : function() {
		//override to return the demo UI
		/*if (parseInt(this.sessionID())==4) return "exercises/LostItemsExerciseDemoUI.php";
		else return null;*/
		
		return "exercises/LostItemsExerciseDemoUI.php";
	},
	finalScore : function() {
		return this._finalScore;
	},
	setFinalScore : function(f) {
		this._finalScore = f;
	},
	setOmissions: function(f){
		this._omissions = f;
	},
	omissions: function(f){
		return this._omissions;
	},
	updateResults : function() {
		this.finishExercise();
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	level : function() {
		return this._level;
	},
	setLevel : function(l) {
		this._level = l;
	},
	subLevel : function() {
		return this._sublevel;
	},
	setSubLevel : function(l) {
		this._sublevel = l;
	},
	updateUserLevel : function() {
		var qry = 'exerciseType=9&userLevel='+this._level+'&userSubLevel='+this._sublevel;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_level.php',
			success: function(data){	
			}
		});
	},
	updateResults : function(coinsFound, totalCoins, foundObjects, unsortedObjects) {
		this.updatePartialResult(JSON.stringify({"totalScore":this._finalScore, "coinsFound":coinsFound, "totalCoins":totalCoins,"sortedObjects":foundObjects, "totalUnsortedObjects":unsortedObjects, "time":this._seconds, "level":this._level, "sublevel": this._sublevel}), this._repetition);
		this._repetition += 1;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		this.updateUserLevel();
		var countCorrects = this._corrects;
		var countFails = this._fails;
		var countOmissions = this._omissions;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var finalScore = this._finalScore;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&countOmissions='+countOmissions+'&finalScore='+finalScore+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				if (parseInt(repetition) >= parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_score_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}
			}
		});
	},
	canAvoidHelp : function(){
		if ( (this._sessionID == 6 || this._sessionID == 8 || this._sessionID == 10) && !this._repeating)
		{
			return true;
		}
		else return false;
	},
	hasMedal : function(){
		//if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		//else return {medal: false, type:0};
	}
});

}
</script>
