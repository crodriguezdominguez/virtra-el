<?php
	require_once('locale/localization.php');
?>

<script type="text/javascript">
if (!MouseDialogExercise) {

var MouseDialogExercise = Exercise.extend({
	init: function(sessionID, exerciseID, repetition, exerciseEntry) {
		var message = null;
		var title = null;
		
		if (sessionID == 1) 
		{
			title = '<?php echo _('&iexcl;Prueba de Punter&iacute;a!'); ?>';
			message = '<?php echo _('Antes de comenzar con la explicación y la realización de ejercicios vamos a comprobar tu punter&iacute;a. Para ello, tendrás que pulsar con el ratón en los cuadrados azules que aparezcan por la pantalla. Pulsa en el bot&oacute;n <strong>Continuar</strong> cuando est&eacute;s listo para comenzar.'); ?>';
		}
		else
		{
			title = '<?php echo _('&iexcl;Prueba de Punter&iacute;a!'); ?>';
			message = '<?php echo _('Vamos a comprobar nuevamente tu punter&iacute;a. Para ello, tendr&aacute;s que pulsar en los botones que aparezcan por la pantalla. Pulsa en el bot&oacute;n <strong>Continuar</strong> cuando est&eacute;s listo para comenzar'); ?>';
		}
		
		this._super(title, message, 'exercises/MouseDialogExerciseUI.php', sessionID, exerciseID, repetition, exerciseEntry);
		
		this._corrects = 0;
		this._fails = 0;
		this._seconds = 0;
	},
	repetition : function(){
		return this._repetition;
	},
	seconds : function() {
		return this._seconds;
	},
	setSeconds : function(secs) {
		this._seconds = secs;
	},
	corrects : function() {
		return this._corrects;
	},
	setCorrects : function(c) {
		this._corrects = c;
	},
	fails : function() {
		return this._fails;
	},
	setFails : function(f) {
		this._fails = f;
	},
	increaseCorrects : function() {
		this._corrects++;
	},
	increaseFailures : function() {
		this._fails++;
	},
	finishExercise : function() {
		//override finishExercise() to store the results
		var countCorrects = this._corrects;
		var countFails = this._fails;
		
		var exerciseEntry = this._exerciseEntry;
		var repetition = this._repetition;
		var sessionID = this._sessionID;
		var exerciseID = this._exerciseID;
		var seconds = this._seconds;
		var qry = 'sessionID='+this._sessionID+'&exerciseID='+this._exerciseID+'&countCorrects='+countCorrects+'&countFails='+countFails+'&seconds='+seconds;
		var obj = this;
		
		$.ajax({
			type: 'POST',
			data: qry,
			async: false,
			url: 'backend/update_exercise_result.php',
			success: function(data){
				/*if (parseInt(repetition) == parseInt(exerciseEntry.repetitions)-1)
				{
					//create medal
					var qry2 = 'sessionID='+sessionID+'&exerciseID='+exerciseID; 
					$.ajax({
						type: 'POST',
						data: qry2,
						async: false,
						url: 'backend/calculate_reasoning_exercise_medal.php',
						success: function(data){
							obj._medal = parseInt(data);
						}
					});
				}*/
			}
		});
	}/*,
	hasMedal : function(){
		if (parseInt(this._repetition) == parseInt(this._exerciseEntry.repetitions)-1)
			return {medal:true, type:this._medal};
		else return {medal: false, type:0};
	}*/
});

}
</script>
