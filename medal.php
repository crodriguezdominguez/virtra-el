<?php
	require_once('locale/localization.php');
	
	session_start();
?>

<div id="medal-container" class="hide">
	<img id="medal-img" src="" alt="" />
	<a class="btn btn-large btn-primary" href="javascript:void(0);" onclick="javascript:checkWhatToDo();"><?php echo _('Siguiente Ejercicio'); ?></a>&nbsp;&nbsp;&nbsp;<a id="repeatExerciseID" class="btn btn-large hide" href="javascript:void(0);" onclick="javascript:forceRepeatExercise();"><?php echo _('Repetir Ejercicio'); ?></a>
</div>
<script type="text/javascript">
	
canShowMiniMenu(false);

function checkWhatToDo()
{
	if (lastExercise) //show session ending
	{
		$.get('endSession.php', function(data){
 			$('#exercise-container').html(data);
		});
	}
	else{
		nextExercise();
	}
}

var medalStr = "<?php echo _('Bronce'); ?>";
var titleText = "<?php echo _('&iexcl;Enhorabuena! &iexcl;No est&aacute; nada mal!'); ?>";
var extraText = "<?php echo _('Sigue entren&aacute;ndote y podr&aacute;s conseguir medallas de plata y oro. &iexcl;&Aacute;nimo!'); ?>";

if (exercise.canRepeat())
{
	extraText += " <?php echo _('Si deseas repetir el ejercicio pulsa sobre el botón <strong>Repetir Ejercicio</strong>.'); ?>";
}

switch(medalType) {
	case 1:
		medalStr = "<?php echo _('Plata'); ?>";
		$('#medal-img').attr('src', 'img/silver_medal.png');
		titleText = "<?php echo _('&iexcl;Enhorabuena! &iexcl;Lo has hecho muy bien!'); ?>";
		extraText = "<?php echo _('Sigue entren&aacute;ndote y podr&aacute;s conseguir una medalla de oro. &iexcl;&Aacute;nimo!'); ?>";
		break;
	case 2:
		medalStr = "<?php echo _('Oro'); ?>";
		$('#medal-img').attr('src', 'img/gold_medal.png');
		titleText = "<?php echo _('&iexcl;Enhorabuena! &iexcl;Excelente!'); ?>";
		extraText = "<?php echo _('Lo has hecho todo perfectamente. &iexcl;Sigue as&iacute;!'); ?>";
		break;
	default:
		$('#medal-img').attr('src', 'img/bronze_medal.png');
		if (exercise.canRepeat()) $('#repeatExerciseID').removeClass('hide');
		break;
}

$('#medal-img').attr('alt', medalStr);
$('#medal-container').fadeIn('slow');

$('#exercise-title').html(titleText);
$('#exercise-description').html("<?php echo _('Has ganado una medalla de'); ?>"+" <b>"+ medalStr+"</b>. "+extraText);
</script>
