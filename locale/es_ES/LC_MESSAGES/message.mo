��          ,      <       P      Q   7  e   >	  �                     PROJECT_DESCRIPTION Project-Id-Version: VIRTRA-EL
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-05-31 21:04+0100
PO-Revision-Date: 2015-02-07 16:00+0100
Last-Translator: Carlos Rodríguez <carlosrodriguez@ugr.es>
Language-Team: MYDASS <carlosrodriguez@ugr.es>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.7.4
X-Poedit-SearchPath-0: /Users/carlosrodriguezdominguez/Documents/Dropbox/Software/VIRTRA-EL/web/
 <p class="lead">El objetivo principal de este proyecto es desarrollar e implementar una plataforma virtual para la evaluación y estimulación cognitiva de personas mayores a través de Internet. Se trata de una estrategia destinada a la prevención e intervención sobre el deterioro cognitivo disponible para muchas personas y que será accesible a aquellas personas mayores que viven alejadas de los recursos asistenciales y con dificultadas de desplazamiento para acceder a ellos. Las pruebas de evaluación y las estrategias de estimulación cognitiva tendrán un enfoque ecológico con el fin de obtener una mejor estimación del grado de discapacidad y dependencia de la persona evaluada y como resultado final una disminución de éste.</p> <p class="lead">Nuestra finalidad última es prevenir y retrasar la dependencia, así como intervenir en sus estadíos iniciales. Para alcanzar nuestro objetivo se ofrecen nuevos instrumentos de evaluación neuropsicológica y funcional que permitirán realizar una evaluación de la persona mayor a distancia, y nuevas tareas que permitirán realizar la estimulación o rehabilitación a distancia de esas personas. Para ello, VIRTRA-EL permite la configuración y administración de las tareas, supervisión y monitorización en línea de las actividades que realiza la persona mayor, así como el seguimiento de su evolución.</p><p class="lead">La plataforma software se ha desarrollado prestando especial atención al cumplimiento de ciertos atributos de calidad (escalabilidad, portabilidad, usabilidad, extensibilidad, etc.) que garanticen fundamentalmente:<ol>	<li>Adaptación a perfiles de usuario e incluso ciudadanos concretos</li>	<li>Fácil implantación, configuración, mantenimiento y ampliación para asegurar la operatividad incluso en ausencia de personal experto, existiendo como única necesidad la disponibilidad de un ordenador con acceso a internet (domicilios particulares y centros de acceso público/privado a internet), con el propósito de abarcar todo el territorio andaluz</li></ol></p><p class="lead">Además, en este proyecto se han incorporado tecnologías de realidad virtual y aumentada que dotarán a las pruebas de evaluación y estimulación cognitiva de una mayor validez ecológica y de una mejor estimación del grado de discapacidad y dependencia de la persona evaluada.</p> 