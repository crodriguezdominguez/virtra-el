<?php
	require_once('../locale/localization.php');
	
	
	session_start();
	
	
	if(isset($_SESSION['userID']))
	{
		require_once('../php/User.php');
		$user = new User($_SESSION['userID']);
		$easy_access = (!$user->get_therapist() && !$user->get_superuser() && !$user->get_carer());
?>

 
<!DOCTYPE HTML >
<html lang="es">
	<head>
		<title>VIRTRA-EL - <?php echo _('Plataforma Virtual de Evaluaci&oacute;n e Intervenci&oacute;n Cognitiva en Mayores'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <meta charset="UTF-8">

		<!-- css styles -->
		<link rel="stylesheet" type="text/css" href="../css/sweet-alert.css">
	    <link rel="stylesheet" href="../css/bootstrap-combined.no-icons.min.css">
	    <link rel="stylesheet" href="../css/virtrael.css">
	    <link rel="stylesheet" href="../css/listbox.css" >
	    <link rel="stylesheet" href="css/personal.css" >
	    
	    
 		<script src='https://www.google.com/jsapi?autoload={"modules":[{"name":"visualization","version":"1","packages":["corechart"]}]}'></script>
	    <!-- scripts -->
		<script src="../js/jquery-1.9.1.min.js"></script>
		<script src="../js/jquery-migrate-1.2.1.min.js"></script>
		<script src="../js/jquery-ui-1.10.1.custom.min.js"></script>
		<script src="../js/jquery.ui.touch-punch.min.js"></script>
	    <script src="../js/bootstrap.min.js"></script>
	    <script src="../js/listbox.js"></script>
	    <script src="js/personal.js"></script>
	</head>
	<body>
			<div id="bodyContainer" class="container" style="padding: 0; width:100%;">



<!-- CONTENIDO -->

	<form id="questionary-form" class="form-horizontal" method="post">
	 <fieldset>
	 	<div class="row-fluid">
			<div class="span6">			
				<label class="control-label" for="qt1"><?php echo _('1- Por favor, indica tu nivel de renta mensual.'); ?></label>
				<div class="controls" >
					<select id="qt1" name="qt1" class="required">
						<option value="0">-</option>
						<option value="1"><?php echo _('menos de 750 €'); ?></option>
						<option value="2"><?php echo _('entre 751 € y 1.300 €'); ?></option>
						<option value="3"><?php echo _('entre 1.301 y 2.300 €'); ?></option>
						<option value="4"><?php echo _('más de 2.301€'); ?></option>
					</select>
				</div>
			</div>
			<div class="span6">		
				<label class="control-label" for="qt2"><?php echo _('2- ¿Qué medio de transporte utilizas para desplazarse al taller de memoria?'); ?></label>
				<div class="controls" style="width:350px">
					<select id="qt2" name="qt2" class="required">
						<option value="0">-</option>
						<option value="1"><?php echo _('transporte público'); ?></option>
						<option value="2"><?php echo _('transporte privado'); ?></option>
						<option value="2"><?php echo _('vengo andando'); ?></option>
						<option value="3"><?php echo _('no me tengo que desplazar, hago el taller en mi domicilio'); ?></option>
					</select>
				</div>
			</div>
		</div>
		<div class="row-fluid" style="padding-top:30px;">
			<div class="span6">	
				<label class="control-label" for="qt3" class="required"><?php echo _('3- ¿Sueles ir solo/a o acompañado de otra persona?'); ?></label>
				<div class="controls" style="width:350px">
					<select id="qt3" name="qt3">
						<option value="0">-</option>
						<option value="1"><?php echo _('solo'); ?></option>
						<option value="2"><?php echo _('acompañado'); ?></option>
					</select>
				</div>
		    </div>

		    <div class="span6">
				<label class="control-label" for="qt4"><?php echo _('4- Solo si estás realizando estos ejercicios en tu casa: ¿Has necesitado ayuda para realizar los ejercicios?'); ?></label>
				<div class="controls" style="width:350px">
					<select id="qt4" name="qt4">
						<option value="0">-</option>
						<option value="1"><?php echo _('sólo al inicio'); ?></option>
						<option value="2"><?php echo _('todo el tiempo: poco'); ?></option>
						<option value="3"><?php echo _('todo el tiempo: regular'); ?></option>
						<option value="4"><?php echo _('todo el tiempo: mucho'); ?></option>
					</select>
				</div>
		    </div>
		</div>
		
	
	</fieldset>
			<p id="psubmit" style="width:100%; margin-top:20px" class="text-center">
				<button id="submit" type="submit" class="btn btn-primary btn-large">Siguiente Cuestionario</button>
			</p>
		</div>
	</form>
	<div id="mensaje"></div>
		</div>
<!-- fin CONTENIDO -->		
		
	    <script>
			$(document).ready(function() {
					$('#questionary-form select').listbox({'searchbar':false});

			$("#questionary-form").submit(function(){
				
    			if ($("#qt1").val()==="0"||$("#qt2").val()==="0"||$("#qt2").val()==="0"){
    				$("#mensaje").html('<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
		  							'<div class="modal-dialog">'+
		    						'<div class="modal-content">'+
		     							' <div class="modal-header">'+
									        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									        '<h3 class="modal-title" id="myModalLabel"></h3>'+
									      '</div>'+
									      '<div class="modal-body">'+
									        '<h3>Debes responder al menos las preguntas 1, 2 y 3</h3>'+
									      '</div>'+
									      '<div class="modal-footer">'+
									        '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>'+	       
									      '</div>'+
									    '</div>'+
									  '</div>'+
									'</div>');
			        $("#myModal").modal();
			       
    				return false;
    			}
    			else{
			    data = $(this).serializeArray();
			   
			    $.ajax({
			      type: "POST",
			      dataType: "json",
			      url: "../backend/save_questionnaire.php", //Relative or absolute path to response.php file
			      data: data,
			      success: function(data) {		
			      		       /* $("#push").html('<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
		  							'<div class="modal-dialog">'+
		    						'<div class="modal-content">'+
		     							' <div class="modal-header">'+
									        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									        '<h3 class="modal-title" id="myModalLabel"></h3>'+
									      '</div>'+
									      '<div class="modal-body">'+
									        '<h3>'+data["result"]+'</h3>'+
									      '</div>'+
									      '<div class="modal-footer">'+
									        '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>'+	       
									      '</div>'+
									    '</div>'+
									  '</div>'+
									'</div>');
			        $("#myModal").modal();
			        $("#questionary-form").hide();
			        $('#myModal').on('hidden', function () {
						     window.location.href = "../index.php";
						});     */
						window.location.href = "../nutrition/cuestionario-frecuencia-alimenticia.php";
			       },

			       error: function(xhr, status, errorThrown){
			       		alert("Error: "+errorThrown);
			       }
			    });
			    return false;
				}
			  });	


});
	    </script>
	  
	</body>
</html>
<?php
}
?>