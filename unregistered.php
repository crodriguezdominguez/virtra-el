<?php
	require_once('locale/localization.php');
	$init = false;
	if (isset($_GET['init']))
	{
		$init = ($_GET['init']=='1');
	}
?>

<div class="page-header">
  <?php if ($init){ ?>
  <h1><?php echo _('Iniciar Sesi&oacute;n'); ?></h1>
  <p class="lead"><?php echo _('Introduce tus datos de acceso a VIRTRA-EL para acceder.'); ?></p>
  <?php } else { ?>
  <h1><?php echo _('Usuario no registrado'); ?></h1>
  <p class="lead"><?php echo _('Para acceder a esta p&aacute;gina deber&aacute;s darte de alta como usuario de VIRTRA-EL.'); ?></p>
  <?php } ?>
</div>

<?php if (!$init){ ?>
<p class="lead">
<?php echo _('Por favor introduce tus datos de usuario en el siguiente formulario o'); ?> <a href="javascript:void(0);" id="register-link"><?php echo _('reg&iacute;strate aqu&iacute;'); ?></a>
</p>
<?php } ?>

<div class="row-fluid">
	<div class="span6">
		<span class="login-error" id="login-error2"></span>
		<form method="post" action="javascript:login2();" accept-charset="UTF-8" class="well form-inline">
		  <input id="email-login" type="text" class="input-large" placeholder="<?php echo _('E-Mail'); ?>">
		  <input id="password-login" type="password" class="input-small" placeholder="<?php echo _('Contrase&ntilde;a'); ?>">
		  <button id="login-button" type="submit" class="btn btn-primary"><?php echo _('Acceder'); ?></button>
		  <div class="control-group">
		  	<div class="controls">
		  		<label class="checkbox inline" for="remember_me">
		  			<input type="checkbox" id="remember_me">
		  			<?php echo _('Mantener conectado'); ?>
		  		</label>
		  		<!--<a href="" class="forgot-password2 inline"><?php echo _('Olvid&eacute; mi contrase&ntilde;a'); ?></a>-->
		  	</div>
		  </div>
		</form>
	</div>
</div>

<script type="text/javascript">
$('#register-link').click(function(evt){
	$('#registerMenuItem').click();
});

function login2()
{
	var user = $("#email-login").val();
	var password = md5($("#password-login").val());
	var remember = $("#remember_me").prop('checked')?'1':'0';
	
	var qry = 'user='+ user + '&pass=' + password + '&rem=' + remember;
				
	$.ajax({
		type: "POST",
		url: "backend/login.php",
		data: qry,
		success: function(html) {
			if(html=='true')
			{
				$('#login-error2').html("");
				window.location.hash = '<?php if (isset($_GET['app']) && $_GET['app'] == '1') echo '#exercises'; ?>';
				window.location.reload();
			}
			else if (html == 'non-active')
			{
				$('#login-error2').html("<?php echo _('Tu acceso está desactivado. Contacta con el administrador'); ?>");
			}
			else
			{
				$('#login-error2').html("<?php echo _('Usuario o contrase&ntilde;a no v&aacute;lidos'); ?>");
			}
		}
	});
};
</script>